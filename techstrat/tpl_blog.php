<?php
/*
 * Template Name: Blog
 */
?>

<?php get_header('static'); ?>

<main id="main" class="page-main" role="main">
    <div class="blog-page">
        <div class="blog__banner"

            <?php
            $banner = get_field('banner');

            if ($banner) : ?>
                style="background-image: url('<?php echo $banner; ?>')"
            <?php endif;

            ?>>
            <div class="page-container flex flex-row">
                <div class="wrap flex flex-row">
                    <script>
                        let options = {
                            strings: ["<?php the_field('description'); ?>"],
                            typeSpeed: 20,
                        };

                    </script>
                    <h2 class="title-brand-color-bold reset"><?php the_field('title'); ?></h2>
                    <span class="element typed-description-brand-color" id="typedJs"></span>
                </div>
            </div>
        </div>

        <div class="event-section">
            <div class="page-container">
                <h2 class="title-brand-color"><?php the_field('events_title'); ?></h2>
                <div class="events">

                    <?php
                    $args = array(
                        'post_type' => 'events',
                        'posts_per_page' => 3,
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post(); ?>

                        <div class="event-item">

                            <div class="image">

                                <img src="<?php the_field('events_image') ?>" alt="">

                            </div>

                            <div class="text-info">

                                <h3 class="event-title"><?php the_field('events_title'); ?></h3>

                               <div class="event-info">
                                   <img src="<?php the_field('logo_date') ?>" alt=""><p class="event__date"><?php the_field('events_date') ?></p>
                               </div>

                                <div class="event-info">
                                   <img src="<?php the_field('logo_address') ?>" alt=""><p class="event__address"><?php the_field('events_address') ?></p>
                               </div>

                                <p class="event__description"><?php the_field("events_description"); ?></p>

                                <a href="#" class="btn_events">RSVP</a>
                            </div>

                        </div>

                    <?php endwhile;
                        wp_reset_postdata(); ?>

                    <?php else : ?>

                        <?php get_template_part('template-parts/content', 'none'); ?>

                    <?php endif; ?>

                </div>
            </div>

        </div>

        <div class="article-section">
        <div class="page-container">

            <div class="article-container"><h3 class="title"><?php the_field('subtitle'); ?></h3>
            </div>
            <div class="container flex flex-row flex-wrap">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 9,
                );

                $post_query = new WP_Query($args);

                if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'article'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php else : ?>

                    <?php get_template_part('template-parts/content', 'none'); ?>

                <?php endif; ?>


            </div>
        </div>
        </div>
    </div>
</main>

<?php get_footer('static'); ?>
