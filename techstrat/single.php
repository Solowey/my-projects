<?php get_header('single'); ?>
<main id="main" class="site-main" role="main">

    <div class="single_post">

        <a class="btn_back" href="<?php echo get_page_link( 264 ); ?>">

            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 22 27.5" version="1.1" x="0px" y="0px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill="#000000"><polygon points="20 2 11.354 10.647 2.707 2 2 2.707 10.646 11.354 2 20 2.707 20.707 11.354 12.061 20 20.707 20.707 20 12.06 11.354 20.707 2.707"/></g></g></svg>

        </a>

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('template-parts/content', 'single'); ?>

        <?php endwhile; ?>

    </div>

</main>

<?php get_footer('single'); ?>
