<div class="single-post_item">

    <h3 class="post-title"><?php the_title(); ?></h3>
    <p class="full-post-description"><?php the_field("full_description"); ?></p>

</div>
