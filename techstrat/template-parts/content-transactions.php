<div class="transactions__inner">
    <div class="transactions__item">

        <div class="transactions__info">

            <?php the_field("transaction_info"); ?>

        </div>

        <div class="transactions__item--image"><img src="<?php the_field("image"); ?>" alt=""></div>
        <h3 class="transactions__description"><?php the_field("title"); ?></h3>
        <div class="transactions__item--logo" ><img src="<?php the_field("card-logo"); ?>"
                                                    alt=""></div>
        <p class="transactions__date"><?php echo get_the_date(' Y'); ?></p>

    </div>
    <p class="transactions__recommendation"><?php the_field("recommendation"); ?></p>
</div>

