<?php

if (have_rows('transactions_card')):

    while (have_rows('transactions_card')) : the_row(); ?>
        <a href="<?php echo get_permalink(); ?>" class="inner">

            <div class="transactions-item">

                <div class="image"><img src="<?php echo the_sub_field("image"); ?>" alt=""></div>
                <h3 class="transactions-description"><?php echo the_sub_field("title"); ?></h3>
                <img class="transactions-card-logo" src="<?php echo the_sub_field("card-logo"); ?>"
                     alt="">
                <p class="transactions-date"><?php echo get_the_date('F j Y'); ?></p>

            </div>
            <p class="recommendation"><?php echo the_sub_field("recommendation"); ?></p>
        </a>

    <?php endwhile; else : endif; ?>

