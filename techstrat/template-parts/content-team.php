<div class="team__card">

    <div class="team__photo">
        <a href="<?php echo get_permalink(); ?>">
            <div class="team__details"><p class="team__full-profile">READ FULL PROFILE</p>
                <svg xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     version="1.1" x="0px" y="0px" viewBox="0 0 100 125"
                     enable-background="new 0 0 100 100"
                     xml:space="preserve"><g>
                        <polygon
                            points="97.172,47.172 50.503,0.503 47.674,3.331 92.344,48 0,48 0,52 92.344,52 47.674,96.669 50.503,99.497    97.172,52.828 100,50  "/></svg>
            </div>
        </a>
        <img src="<?php the_field("photo"); ?>" alt="">
    </div>

    <p class="team__name description-brand-color"><?php the_field("name"); ?></p>
    <p class="team__position second-description-brand-color"><?php the_field("position"); ?></p>
    <div class="email team__mail"><img src="<?php the_field("email_icon"); ?>" alt=""><a href="mailto:<?php the_field("email"); ?>"><?php the_field("email"); ?></a></div>
    <p class="team__description"><?php the_field("short_description"); ?></p>

</div>
