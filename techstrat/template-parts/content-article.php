<a class="post__item" href="<?php echo get_permalink(); ?>">

    <h3 class="post-title"><?php the_title(); ?></h3>
    <p class="post__description"><?php the_field("short_description"); ?></p>
    <p class="post__date"><?php echo get_the_date('F j Y'); ?></p>
    <span class="read-more--articles">CONTINUE READING</span>

</a>

