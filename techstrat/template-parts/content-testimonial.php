<div class="testimonial__item">

    <h3 class="testimonial__name"><?php echo the_field("name"); ?></h3>
    <p class="testimonial__comment"><?php echo the_field("comment"); ?></p>
    <div class="testimonial__brand"><img src="<?php echo the_field("logo"); ?>" alt=""></div>

</div>
