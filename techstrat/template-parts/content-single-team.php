<div class="single-team-card flex flex-row">

    <div class="team-info flex flex-col">

        <img src="<?php the_field("photo"); ?>" alt="">
        <p class="team__name description-brand-color"><?php the_field("name"); ?></p>
        <p class="team__position second-description-brand-color"><?php the_field("position"); ?></p>
        <p class="email team__mail"><img src="<?php the_field("email_icon"); ?>" alt=""><a href="mailto:<?php the_field("email"); ?>"><?php the_field("email"); ?></a></p>
    </div>

    <div class="container">
        <p class="full-team-description"><?php the_field("full_description"); ?></p>
    </div>
</div>





