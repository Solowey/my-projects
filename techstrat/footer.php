</div>
<!--close content class tag-->
<footer class="page-footer section" role="contentinfo">

    <div class="page-footer__inner section ">
        <div class="page-container flex flex-col">
            <?php

            $logo = get_field('logo', 'option');
            if ($logo) : ?>

                <a href="<?php echo get_home_url(null, '/', 'http'); ?>" class="logo"
                   style="background-image: url('<?php echo $logo ?>')"></a>

            <?php endif; ?>
            <div class="container">

                <?php

                if (have_rows('contacts', 'option')):

                    while (have_rows('contacts', 'option')) : the_row(); ?>

                        <div class="footer_contacts">
                            <?php

                            if (have_rows('address', 'option')):

                                while (have_rows('address', 'option')) : the_row(); ?>

                                    <div class="wrap wrap--address">

                                        <img src="<?php echo the_sub_field("icon"); ?>" alt="">
                                        <p class="address"><?php echo the_sub_field("current_address"); ?></p>

                                    </div>

                                <?php endwhile; else : endif; ?>

                            <?php

                            if (have_rows('phone_fax', 'option')):

                            while (have_rows('phone_fax', 'option')) :
                            the_row(); ?>

                            <div class="wrap wrap--phone">
                                <img src="<?php echo the_sub_field("icon"); ?>" alt="">
                                <div class="inner">
                                    <a href="tel:<?php echo the_sub_field("phone"); ?>">Phone&nbsp;:&nbsp;<?php echo the_sub_field("phone"); ?></a>

                                    <p class="fax">Fax&nbsp;:&nbsp;<?php echo the_sub_field("fax"); ?></p>
                                </div>

                                <?php endwhile; else :
                                endif; ?>

                            </div>

                            <?php

                            if (have_rows('mail', 'option')):

                                while (have_rows('mail', 'option')) : the_row(); ?>

                                    <div class="wrap wrap--mail">

                                        <img src="<?php echo the_sub_field("icon"); ?>" alt="">
                                        <a href="mailto:<?php echo the_sub_field("mail"); ?>"><p
                                                class="mail"><?php echo the_sub_field("mail"); ?></p></a>

                                    </div>

                                <?php endwhile; else : endif; ?>
                        </div>

                    <?php endwhile; else : endif; ?>

                <div class="links">
                    <nav class="footer-nav" role="navigation">
                        <?php wp_nav_menu(array('theme_location' => 'menu-2', 'menu_class' => 'footer-nav__list', 'container' => false)); ?>
                    </nav>
                </div>
                <div class="social">

                    <?php

                    if (have_rows('social_links', 'option')):

                        while (have_rows('social_links', 'option')) : the_row(); ?>
                            <a href="<?php echo the_sub_field("twitter"); ?>" class="twitter"><i
                                    class="<?php echo the_sub_field("twitter_icon"); ?>"></i></a>
                            <a href="<?php echo the_sub_field("quora"); ?>" class="quora"><i
                                    class="<?php echo the_sub_field("quora_icon"); ?>"></i></a>
                            <a href="<?php echo the_sub_field("linkedin"); ?>" class="linkedin"><i
                                    class="<?php echo the_sub_field("linkedin_icon"); ?>"></i></a>

                        <?php endwhile; else : endif; ?>
                </div>
            </div>

        </div>
    </div>
</footer>
</div>
<!--close wrapper class tag-->
<?php wp_footer(); ?>

</body>
</html>
