<?php
/*
 * Template Name: Approach
 */
?>
<?php get_header('static'); ?>

<main class="page-approach">
    <div class="approach">

        <div class="approach__banner"

            <?php
            $banner = get_field('banner');

            if ($banner) : ?>
                style="background-image: url('<?php echo $banner; ?>')"
            <?php endif;

            ?>>

            <div class="page-container flex flex-row">

                <div class="wrap flex flex-row">
                    <script>
                        let options = {
                            strings: ["<?php the_field('description'); ?> </br> </br><?php the_field('second_description'); ?>"],
                            typeSpeed: 20,
                        };
                    </script>
                    <h2 class="reset title-brand-color-bold"><?php the_field('title'); ?></h2>
                    <span class="element typed-description-brand-color" id="typedJs"></span>
                </div>

            </div>

        </div>


        <div class="service">
            <div class="page-container">

                <h2 class="title-brand-color-bold"><?php the_field('service_title'); ?></h2>
                <h3 class="subtitle-brand-color"><?php the_field('service_description'); ?></h3>

                <div class="service__wrap flex flex-row flex-wrap">

                    <?php

                    $data_duration = 300;


                    if (have_rows('service_item')):


                        while (have_rows('service_item')) : the_row(); ?>

                            <div class="service__item"

                                 data-aos="fade-up"
                                 data-aos-delay="<?php echo $data_duration ?>"
                                 data-aos-duration="500"
                                 data-aos-once="true">

                                <div class="service__icon">
                                    <img src="<?php the_sub_field("icon"); ?>" alt="">

                                </div>
                                <div class="service__text second-description-brand-color">

                                    <p><?php the_sub_field("description") ?></p>

                                </div>

                            </div>

                            <?php $data_duration = $data_duration + 100; endwhile; else : endif; ?>


                </div>
            </div>
        </div>

        <div class="approach__our-process">

            <div class="page-container">

                <div class="approach__our-process--text"
                     data-aos="fade-left"
                     data-aos-duration="1000"
                     data-aos-once="true">
                    <h3 class="title-brand-color-bold"><?php the_field('process_title'); ?></h3>

                    <div class="subtitle-brand-color">
                        <?php the_field('process_description'); ?>
                    </div>

                </div>
                <div class="approach__alignment flex flex-row">

                    <div class="approach__alignment--overlay flex flex-col">

                        <div class="approach__alignment--text" data-aos="fade-up" data-aos-duration="1000"
                             data-aos-once="true" data-aos-offset="100">

                            <h3 class="second-title-brand-color"><?php the_field('alignment_title'); ?></h3>

                            <div class="fourth-description-brand-color">
                                <?php the_field('alignment_description'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="approach__alignment--image">

                        <img src="<?php the_field("alignment_image"); ?>" alt="" data-aos="fade-up"
                             data-aos-duration="1000" data-aos-once="true" data-aos-offset="100">
                    </div>
                </div>

                <div class="finance">

                    <div class="finance__item">

                        <div class="finance__icon"

                             data-aos="fade-right"
                             data-aos-duration="1000"
                             data-aos-once="true">
                            <img src="<?php the_field("finance_icon"); ?>" alt="">
                            <h3 class="title-brand-color-bold-mobile"><?php the_field("finance_title"); ?></h3>
                        </div>

                        <div class="finance__text"

                             data-aos="fade-left"
                             data-aos-duration="1000"
                             data-aos-once="true"

                        >

                            <h3 class="title-brand-color-bold"><?php the_field("finance_title"); ?></h3>

                            <p class="fourth-description-brand-color">
                                <?php the_field("finance_description"); ?>

                            </p>
                        </div>

                    </div>

                </div>

                <div class="timing">

                    <div class="timing__image">

                        <img src="<?php the_field("timing_image"); ?>" alt=""
                             data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-offset="100"
                             data-aos-once="true">
                    </div>

                    <div class="timing__item flex flex-col">
                        <div class="timing__item--inner" data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-offset="100"
                             data-aos-once="true">
                            <h3 class="second-title"><?php the_field("timing_title"); ?></h3>

                            <div class="description">

                                <?php the_field("timing_description"); ?>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="fit">

                    <div class="fit__item">

                        <div class="fit__text"
                             data-aos="fade-right"
                             data-aos-duration="1000"
                             data-aos-once="true">
                            <h3 class="second-title-brand-color"><?php the_field("fit_title"); ?></h3>
                            <div class="fourth-description-brand-color">

                                <?php the_field("fit_description"); ?>

                            </div>
                        </div>

                        <div class="fit__icon" data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">
                            <img src="<?php the_field("fit_icon"); ?>" alt="">
                            <h3 class="title-brand-color-bold-mobile"><?php the_field("fit_title"); ?></h3>
                        </div>

                    </div>

                </div>

                <div class="info-container flex">

                    <div class="process">

                        <h3 class="second-title" data-aos="fade-up"
                            data-aos-duration="1000"
                            data-aos-once="true"><?php the_field("process_item_title"); ?></h3>

                        <div class="description" data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-once="true">

                            <?php the_field("process_item_description"); ?>

                        </div>
                    </div>

                    <div class="valuation">

                        <h3 class="second-title brand-color" data-aos="fade-up"
                            data-aos-duration="1000"
                            data-aos-once="true"><?php the_field("valuation_title"); ?></h3>

                        <div class="description brand-color" data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-once="true">

                            <?php the_field("valuation_description"); ?>

                        </div>

                    </div>

                </div>

                <div class="structure">

                    <div class="structure__item">

                        <div class="structure__icon" data-aos="fade-right" data-aos-duration="1000"
                             data-aos-once="true">
                            <img src="<?php the_field("structure_icon"); ?>" alt="">
                            <h3 class="second-title-brand-color-mobile"><?php the_field("structure_title"); ?></h3>
                        </div>

                        <div class="structure__text" data-aos="fade-left"
                             data-aos-duration="1000"
                             data-aos-once="true">

                            <h3 class="second-title-brand-color"><?php the_field("structure_title"); ?></h3>

                            <div class="fourth-description-brand-color">

                                <?php the_field("structure_description"); ?>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="diligence">

                    <div class="diligence__text" data-aos="fade-up"
                         data-aos-duration="1000"
                         data-aos-offset="100"
                         data-aos-once="true">

                        <h3 class="second-title-brand-color"><?php the_field("diligence_title"); ?></h3>

                        <div class="fourth-description-brand-color"><?php the_field("diligence_description"); ?></div>

                    </div>

                    <div class="diligence__image">

                        <img src="<?php the_field("diligence_image"); ?>" alt=""
                             data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-offset="100"
                             data-aos-once="true"
                        >

                    </div>


                </div>

                <div class="about-container flex">


                    <div class="contracts">
                        <div class="contracts__text" data-aos="fade-right" data-aos-duration="1000"
                             data-aos-once="true">
                            <h3 class="second-title-brand-color"><?php the_field("contracts_title"); ?></h3>

                            <div class="fourth-description-brand-color">

                                <?php the_field("contracts_description"); ?>

                            </div>
                        </div>
                    </div>

                    <div class="risk">

                        <div class="risk__text" data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">


                            <h3 class="second-title-brand-color"><?php the_field("risk_title"); ?></h3>

                            <div class="fourth-description-brand-color">

                                <?php the_field("risk_description"); ?>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="closing">

                    <div class="closing__image">

                        <img src="<?php the_field("closing_image"); ?>" alt=""

                             data-aos="fade-up"
                             data-aos-duration="1000"
                             data-aos-offset="100"
                             data-aos-once="true"

                        >
                    </div>

                    <div class="closing__item">

                        <div class="closing__text"

                             data-aos="fade-up"
                             data-aos-duration="1300"
                             data-aos-offset="300"
                             data-aos-once="true"

                        >
                            <h3 class="second-title"><?php the_field("closing_title"); ?></h3>
                            <div class="description"><?php the_field("closing_description"); ?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php get_footer('static'); ?>

</main>


