import jQuery from 'jquery';
import Typed from 'typed.js';
import 'slick-carousel';
import fullpage from "fullpage.js";
import "@fortawesome/fontawesome-free/js/all.min";
import AOS from "aos";

document.addEventListener('DOMContentLoaded', function () {

    let hamburgerButton = document.getElementById("toggle");
    let menu__hamburger = document.getElementById("toggle");
    let html__hide = document.getElementById("html--hidden");

    hamburgerButton.addEventListener("click", function () {
        menu__hamburger.classList.toggle("menu__show");
        html__hide.classList.toggle("html--hidden")
    });



    (function ($) {

        var attached = false;//track it's current state
        manageFullPage();

        $(window).resize( function() {
            manageFullPage();
        } );

        function manageFullPage(){

            if( $(window).width() > 679 && $(window).height() > 480 ) {

                if(! attached){
                    new fullpage('#fullpage', {
                        scrollHorizontally: false,
                    });
                    attached = true;//note it's been attached
                }
            }else{//we don't want fullpage. destroy it if it's attached
                if(attached){
                    attached = false;//mark destroyed
                    fullpage_api.destroy('all');
                }
            }
        }

        $(".transactions-slider").slick({
            prevArrow: "<svg class='slick-prev' xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 125\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\"><g><polygon points=\"97.172,47.172 50.503,0.503 47.674,3.331 92.344,48 0,48 0,52 92.344,52 47.674,96.669 50.503,99.497    97.172,52.828 100,50  \"/></svg>",
            nextArrow: "<svg class='slick-next' xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 125\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\"><g><polygon points=\"97.172,47.172 50.503,0.503 47.674,3.331 92.344,48 0,48 0,52 92.344,52 47.674,96.669 50.503,99.497    97.172,52.828 100,50  \"/></svg>",
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            appendArrows: $('.nav-container'),
            responsive: [ {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 601,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });



        $(".article-slider").slick({
            infinite: true,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
                {
                    breakpoint: 993,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 2,
                    }
                },

                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ],
        });

        $(".testimonials-slider").slick({
            prevArrow: "<svg class='slick-prev' xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 125\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\"><g><polygon points=\"97.172,47.172 50.503,0.503 47.674,3.331 92.344,48 0,48 0,52 92.344,52 47.674,96.669 50.503,99.497    97.172,52.828 100,50  \"/></svg>",
            nextArrow: "<svg class='slick-next' xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 125\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\"><g><polygon points=\"97.172,47.172 50.503,0.503 47.674,3.331 92.344,48 0,48 0,52 92.344,52 47.674,96.669 50.503,99.497    97.172,52.828 100,50  \"/></svg>",
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            appendArrows: $('.testimonial-container'),
            responsive: [{
                breakpoint: 1281,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
                {
                    breakpoint: 993,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },

                {
                    breakpoint: 600,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });

        $(".transactions__item").on('click', function() {

            $(this).parent().parent().parent().siblings().find('.transactions__item').removeClass('transactions_show_info');
            $(this).parent().siblings().find('.transactions__item').removeClass('transactions_show_info');
            $(this).toggleClass('transactions_show_info')

        });

        if($("#typedJs").hasClass("element")) {

            let typed = new Typed(".element", options);
        }

    })(jQuery);

    overing('#wrapperIdent', '#blockIdent', 'show');
    overing('#testimonial_wrap', '#testimonial_block', 'show');

    function overing(wrapperIdent, blockIdent, showClass, side = "right") {


        let wrapper = document.querySelector(wrapperIdent);
        let block = document.querySelector(blockIdent);

        wrapper.addEventListener('mousemove', (e) => {
            let condition = (side === 'right') ? e.clientX > wrapper.offsetWidth / 2 : e.clientX < wrapper.offsetWidth / 2;
            if (condition) block.classList.add(showClass);
            else block.classList.remove(showClass);
        });
        wrapper.addEventListener('mouseout', () => {
            block.classList.remove(showClass);
        });
    }
});

AOS.init();










