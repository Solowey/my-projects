<?php
/*
 * Template Name: Home
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">

    <section class="section banner"

        <?php

        if (have_rows('first_screen')):

        while (have_rows('first_screen')) :
        the_row(); ?> style="background-image: url('<?php echo the_sub_field("banner_image"); ?>')">

            <div class="page-container flex flex-col">
                <h1 class="main-title-brand-color"><?php echo the_sub_field("title"); ?></h1>
                <h2 class="main-subtitle-brand-color"><?php echo the_sub_field("subtitle"); ?></h2>



                <?php

                if (have_rows('button')):

                    while (have_rows('button')) :
                        the_row(); ?>
                        <a class="touch_button btn"
                           href="<?php echo the_sub_field("link"); ?>"><?php echo the_sub_field("text"); ?></a>
                    <?php endwhile;

                else :
                endif; ?>
            </div>


            <div class="overlay" id="videoJs">

            <video loop muted autoplay="autoplay" class="fullscreen-bg__video" >

                <source src="<?php echo the_sub_field("banner"); ?>" type="video/webm" codecs="avc1.4D401E, mp4a.40.2">

            </video>

            </div>

        <?php endwhile;

        else :
        endif; ?>

    </section>

    <div class="intro section">

        <section class="right-bar"

            <?php

            if (have_rows('intro_right_bar')):

            while (have_rows('intro_right_bar')) :
            the_row(); ?>

                 style="background-image: url('<?php echo the_sub_field("banner"); ?>')">
            <div class="inner">
                <div class="inner__text">
                <h2 class="title-brand-color"><?php echo the_sub_field("title"); ?></h2>
                <p class="description-brand-color"><?php echo the_sub_field("description"); ?></p>

                </div>
                <?php

                if (have_rows('button_lern')):

                    while (have_rows('button_lern')) :
                        the_row(); ?>
                        <a class="touch_button btn" href="<?php echo get_page_link( 481 ); ?>"><?php echo the_sub_field("text"); ?></a>
                    <?php endwhile;

                else :
                endif; ?>
            </div>
            <?php endwhile;

            else :
            endif; ?>



        </section>
        <section class="left-bar"
            <?php

            if (have_rows('intro_left_bar')):

            while (have_rows('intro_left_bar')) :
            the_row(); ?>

                 style="background-image: url('<?php echo the_sub_field("banner"); ?>')">

            <div class="inner">

                <div class="inner__text">
                <h2 class="title-brand-color"><?php echo the_sub_field("title"); ?></h2>
                <p class="description-brand-color"><?php echo the_sub_field("description"); ?></p>
                </div>
                <?php

                if (have_rows('button_lern')):

                    while (have_rows('button_lern')) :
                        the_row(); ?>
                        <a class="touch_button btn"
                           href="<?php echo get_page_link( 436 ); ?>"><?php echo the_sub_field("text"); ?></a>
                    <?php endwhile;

                else :
                endif; ?>
            </div>
            <?php endwhile;

            else :
            endif; ?>

        </section>
    </div>

    <section class="sets section">


        <div class="page-container">

            <h2 class="title"><?php the_field('what_sets_us_apart'); ?></h2>

            <div class="sets__container  flex flex-row">
                <?php

                if (have_rows('item')):

                    while (have_rows('item')) : the_row(); ?>
                        <div class="sets__item">


                            <div class="sets__image"><img src="<?php echo the_sub_field("image"); ?>" alt=""></div>

                            <div class="sets__text">
                            <h3 class="subtitle"><?php echo the_sub_field("title"); ?></h3>

                            <p class="second-description"><?php echo the_sub_field("description"); ?></p>
                            </div>

                        </div>
                    <?php endwhile; else : endif; ?>
            </div>
        </div>


    </section>

    <section class="recent_transactions section" id="wrapperIdent">

        <div class="page-container flex flex-col">
            <div class="wrap flex flex-row">
            <h2 class="title-brand-color"><?php the_field('recent_transactions'); ?></h2>
            <div class="nav-container" id="blockIdent"></div>
            </div>
            <div class="container flex flex-row transactions-slider" id="show_info">

                <?php
                $args = array(
                    'post_type' => 'transaction',
                    'posts_per_page' => -1,
                );

                $the_query = new WP_Query($args);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'transactions'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php endif; ?>

            </div>

            <a href="<?php echo get_page_link( 223 ); ?>" class="read-more">SEE ALL TRANSACTIONS</a>

        </div>

    </section>

    <section class="team section">

        <div class="page-container flex flex-col">

            <h3 class="title-brand-color"><?php the_field('team_header'); ?></h3>
            <div class="container flex flex-row team-slider">


                <?php
                $args = array(
                    'post_type' => 'team',
                    'posts_per_page' =>4,
                );

                $team_query = new WP_Query($args);

                if ($team_query->have_posts()) : while ($team_query->have_posts()) : $team_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'team'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php else : ?>

                    <?php get_template_part( 'template-parts/content', 'none' ); ?>

                <?php endif; ?>
            </div>


        </div>
        <div class="read-more__container">
        <a href="<?php echo get_page_link( 334 ); ?>" class="read-more">MEET THE ENTIRE TEAM</a>
        </div>
    </section>

    <section class="testimonial flex flex-col section" id="testimonial_wrap">

        <div class="testimonial-inner">
        <div class="testimonial-container" id="testimonial_block"></div>

        <div class="figure">
            <img src="<?php the_field('testimonial_figure'); ?>" alt="">
        </div>

        <div class="container">

            <h3 class="title-brand-color"><?php the_field('testimonial_header'); ?></h3>
        </div>

        <div class="testimonial__wrap testimonials-slider">
            <?php
            $args = array(
                'post_type' => 'testimonial',
                'posts_per_page' => -1,
            );

            $testimonial_query = new WP_Query($args);

            if ($testimonial_query->have_posts()) : while ($testimonial_query->have_posts()) : $testimonial_query->the_post(); ?>

                <?php get_template_part('template-parts/content', 'testimonial'); ?>

            <?php endwhile;
                wp_reset_postdata(); ?>

            <?php else : ?>

                <?php get_template_part('template-parts/content', 'none'); ?>

            <?php endif; ?>

        </div>
        </div>
    </section>

    <section class="recent_post section">

        <div class="page-container">

            <div class="article-container">

                <h3 class="title"><?php the_field('recent_post_header'); ?></h3>
                <a href="<?php echo get_page_link( 264 ); ?>" class="read-more--articles-top">READ MORE ARTICLES</a>
            </div>


            <div class="container article-slider flex flex-row">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' =>4,
                );

                $post_query = new WP_Query($args);

                if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'article'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php else : ?>

                    <?php get_template_part( 'template-parts/content', 'none' ); ?>

                <?php endif; ?>
            </div>
        </div>

    </section>

    <section class="contact section">
        <div class="contact__container">
        <?php get_template_part('template-parts/map'); ?>

        <div class="contact__form">
            <div class="wrap">
                <?php

                if (have_rows('contact')):

                    while (have_rows('contact')) : the_row(); ?>

                        <h3 class="title title-brand-color"><?php echo the_sub_field("title"); ?></h3>
                        <p class="description-brand-color"><?php echo the_sub_field("description"); ?></p>
                    <?php endwhile; else : endif; ?>
                <?php echo do_shortcode('[contact-form-7 id="183" title="Contact form 1"]'); ?>
            </div>
        </div>
</div>
    </section>

    <div>
        <?php get_footer(); ?>
    </div>
</main>




