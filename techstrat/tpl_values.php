<?php
/*
 * Template Name: Value
 */
?>
<?php get_header('static'); ?>
<main id="main" class="page-main" role="main">

    <div class="value">
        <div class="value__banner"

            <?php
            $banner = get_field('banner');

            if ($banner) : ?>
                style="background-image: url('<?php echo $banner; ?>')"
            <?php endif;

            ?>>
            <div class="page-container flex flex-row">
                <div class="wrap flex flex-row">

                    <script>

                        var options = {
                            strings: ["<?php the_field('description'); ?> </br> </br><?php the_field('second_description'); ?>"],
                            typeSpeed: 20,
                        };

                    </script>

                    <h2 class="title-brand-color-bold reset"><?php the_field('title'); ?></h2>
                    <span class="element typed-description-brand-color" id="typedJs"></span>
                </div>
            </div>
        </div>

        <div class="page-container flex flex-row">
            <h2 class="title"><?php the_field('subtitle') ?></h2>
        </div>

        <div class="wrap">

            <?php
            $i = 0;

            if (have_rows('item')):

                while (have_rows('item')) : the_row();

                    if ($i % 2) {
                        $data_first = 'fade-left';
                        $data_second = 'fade-right';
                    } else {
                        $data_first = 'fade-right';
                        $data_second = 'fade-left';
                    }
                    $i++;
                    ?>

                    <div class="value__item">
                        <div class="inner">

                            <div class="value__item--img" data-aos="<?php echo $data_first ?>"
                                 data-aos-offset="300"
                                 data-aos-duration="1000"
                                 data-aos-once="true">
                                <img src="<?php the_sub_field('image') ?>" alt="">

                                <h3 class="second-title-brand-color-mobile"><?php the_sub_field('title') ?></h3>
                            </div>

                            <div class="value__item--text"
                                 data-aos="<?php echo $data_second ?>"
                                 data-aos-offset="300"
                                 data-aos-duration="1000"
                                 data-aos-once="true">

                                <h3 class="second-title-brand-color"><?php the_sub_field('title') ?></h3>
                                <div class="description">
                                    <?php the_sub_field('description') ?>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php endwhile; else : endif; ?>

        </div>
    </div>
</main>


<?php get_footer('static'); ?>
