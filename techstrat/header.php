<!DOCTYPE html>
<html id="html--hidden" <?php language_attributes(); ?>>
<head>
    <?php get_template_part('template-parts/head'); ?>
</head>

<body <?php body_class("page-body"); ?>>
<div class="wrapper" id="fullpage">
    <div class="content">
        <header class="page-header">
                <div class="page-container">
            <?php

            $header_logo = get_field('logo', 'option');
            if ($header_logo) : ?>

                <a href="<?php echo get_home_url(null, '/', 'http'); ?>" class="page-header__logo" style="background-image: url('<?php echo $header_logo ?>')"></a>

            <?php endif; ?>

            <button id="toggle" class="menu__hamburger ">

                <span class="span1 line"></span>
                <span class="span2 line"></span>
                <span class="span3 line"></span>

            </button>

            <nav class="main-nav" role="navigation">
                <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'main-nav__list', 'container' => false)); ?>
            </nav>

                </div>
        </header>
