<?php
/*
 * Template Name: Transaction
 */
?>

<?php get_header('static'); ?>


<div class="transactions__banner"

    <?php
    $banner = get_field('banner');

    if ($banner) : ?>
        style="background-image: url('<?php echo $banner; ?>')"
    <?php endif;

    ?>>

    <div class="page-container flex flex-row">
<div class="wrap flex flex-row">
    <script>
        let options = {
            strings: ["<?php the_field('description'); ?>"],
            typeSpeed: 20,};

    </script>
    <h2 class="title-brand-color-bold reset"><?php the_field('title'); ?></h2>
    <span class="element typed-description-brand-color" id="typedJs"></span>
</div>
    </div>
</div>

<section class="recent_transactions transactions_page">

    <div class="page-container flex flex-col">
        <h2 class="title-brand-color">Recent transactionss</h2>

        <div class="container flex flex-row flex-wrap">

            <?php
            $args = array(
                'post_type' => 'transaction',
                'posts_per_page' => 12,
            );

            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>

                <?php get_template_part('template-parts/content', 'transactions'); ?>

            <?php endwhile;
                wp_reset_postdata(); ?>

            <?php endif; ?>

        </div>

    </div>

</section>


<?php get_footer('static'); ?>

