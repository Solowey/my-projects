<?php
/*
 * Template Name: Team
 */
?>
<?php get_header('static'); ?>
<main id="main" class="page-main" role="main">
    <div class="team__banner"

        <?php
        $banner = get_field('banner');

        if ($banner) : ?>
            style="background-image: url('<?php echo $banner; ?>')"
        <?php endif;

        ?>>
        <div class="page-container flex flex-row">
            <div class="wrap flex flex-row">
                <script>
                    let options = {
                        strings: ["<?php the_field('description'); ?> </br> </br><?php the_field('second_description'); ?>"],
                        typeSpeed: 20,
                    };

                </script>
                <h2 class="title-brand-color-bold reset"><?php the_field('title'); ?></h2>
                <span class="element typed-description-brand-color" id="typedJs"></span>
            </div>
        </div>
    </div>

    <div class="team-page">

        <div class="page-container flex flex-row">

            <h3 class="title-brand-color"><?php the_field('team_header'); ?></h3>
            <div class="team-container">
                <?php
                $args = array(
                    'post_type' => 'team',
                    'posts_per_page' => 12,
                );

                $team_query = new WP_Query($args);

                if ($team_query->have_posts()) : while ($team_query->have_posts()) : $team_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'team'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php else : ?>

                    <?php get_template_part('template-parts/content', 'none'); ?>

                <?php endif; ?>
            </div>
        </div>
    </div>

</main>
<?php get_footer('static'); ?>
