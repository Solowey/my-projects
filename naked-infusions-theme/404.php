<?php get_header(); ?>


<section class="section-404">
    <div class="container">

        <div class="wrap-content">
            <div class="title-type-1 red">
                404 Error
            </div>

            <div class="title-type-2">Page not found</div>
        </div>

    </div>
</section>


<?php get_footer(); ?>
