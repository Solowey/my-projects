<?php get_header(); ?>

<div id="fullpage-single-product">

    <div id="staticImg">
        <div class="wrap-img">
            <img class="cap" src="<?php echo wp_get_attachment_image_url(get_field('cap_image'), 'large'); ?>"
                 alt="cap image">
            <img class="bottle"
                 src="<?php echo wp_get_attachment_image_url(get_field('bottle_image'), 'large'); ?>"
                 alt="bottle image">
        </div>
    </div>

    <div class="section1 section single-product-banner" id="sec1">

        <div class="bg" style="background-color: <?php the_field('color_theme', $prevPostID); ?>"></div>

        <div class="container">
            <div class="absolute-word">better naked</div>
            <div class="left-side">
                <div class="wrap-img-mobile">
                    <img class="cap" src="<?php echo wp_get_attachment_image_url(get_field('cap_image'), 'large'); ?>"
                         alt="cap image">
                    <img class="bottle"
                         src="<?php echo wp_get_attachment_image_url(get_field('bottle_image'), 'large'); ?>"
                         alt="bottle image">
                </div>
            </div>
            <div class="right-side">

                <h1 class="title-type-1 white"><?php the_title(); ?></h1>

                <div class="price">
                    <?php the_field('price'); ?>
                    <div class="plus-info">*plus shipping</div>
                </div>
                <div class="content little-font-size white-theme"><?php the_field('description'); ?></div>
                <ul class="wrap-characteristics">
                    <?php while (have_rows('characteristic_repeater')) : the_row(); ?>

                        <li class="item">
                            <img src="<?php echo wp_get_attachment_image_url(get_sub_field('icon'), 'medium'); ?>"
                                 alt="Icon">
                        </li>

                    <?php endwhile; ?>
                </ul>

                <div class="wrap-dropdown">

                    <?php the_field('short_code');?>
                </div>
            </div>
        </div>
    </div>

    <div class="section section2 section-ingredients" id="sec2">
        <div class="bg" style="background-color: <?php the_field('color_theme', $prevPostID); ?>"></div>

        <div class="container">
            <div class="left-side">
                <div class="wrap-img-mobile">
                    <img class="cap" src="<?php echo wp_get_attachment_image_url(get_field('cap_image'), 'large'); ?>"
                         alt="cap image">
                    <img class="bottle"
                         src="<?php echo wp_get_attachment_image_url(get_field('bottle_image'), 'large'); ?>"
                         alt="bottle image">
                </div>
            </div>
            <div class="right-side">

                <div class="top-info">
                    <h1 class="title-type-1 white">Made from the best organic ingredients</h1>
                </div>

                <ul class="wrap-ingredients first-row">
                    <?php while (have_rows('ingredients_repeater_first_row')) : the_row(); ?>

                        <li class="back-img" style="background-image: url(<?php echo wp_get_attachment_image_url(get_sub_field('image_hover'), 'medium'); ?>);">

                            <div class="hidden"
                                 style="background-color: <?php the_field('color_theme'); ?>">
                                <h4 class="title"><?php the_sub_field('title'); ?></h4>
                            </div>
                        </li>

                    <?php endwhile; ?>
                </ul>


                <ul class="wrap-ingredients second-row">
                    <?php while (have_rows('ingredients_repeater_second_row')) : the_row(); ?>

                        <li class="back-img" style="background-image: url(<?php echo wp_get_attachment_image_url(get_sub_field('image_hover'), 'medium'); ?>);">

                            <div class="hidden"
                                 style="background-color: <?php the_field('color_theme'); ?>">
                                <h4 class="title"><?php the_sub_field('title'); ?></h4>
                            </div>
                        </li>

                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>


    <?php
    $prevPost = get_previous_post();
    if ($prevPost) {
        $prevPostID = $prevPost->ID;
        ?>
        <div class="section fp-auto-height" id="footer-outer"
             style="background-color: <?php the_field('color_theme', $prevPostID); ?>">

            <div class="wrap-content">

                <div class="bg"></div>

                <div class="wrap-content">

                    <a href="<?php echo get_permalink($prevPostID); ?>">
                        <div class="wrap-img">
                            <img class="cap"
                                 src="<?php echo wp_get_attachment_image_url(get_field('cap_image', $prevPostID), 'large'); ?>"
                                 alt="cap image">
                            <img class="bottle"
                                 src="<?php echo wp_get_attachment_image_url(get_field('bottle_image', $prevPostID), 'large'); ?>"
                                 alt="bottle image">
                        </div>

                        <div
                                class="absolute-word"><?php the_field('next_post_title_background', $prevPostID); ?></div>
                    </a>
                </div>
            </div>
        </div>
    <?php } else {
        $latest_post = get_posts(array(
            'post_type' => 'products',
            'posts_per_page' => 1,
            'orderby' => 'menu_order'
        ));
        ?>


        <div class="section fp-auto-height" id="footer-outer"
             style="background-color: <?php the_field('color_theme', $latest_post[0]->ID); ?>">

            <div class="wrap-content">

                <div class="bg"></div>

                <div class="wrap-content">

                    <a href="<?php echo get_permalink($latest_post[0]->ID); ?>">
                        <div class="wrap-img">
                            <img class="cap"
                                 src="<?php echo wp_get_attachment_image_url(get_field('cap_image', $latest_post[0]->ID), 'large'); ?>"
                                 alt="cap image">
                            <img class="bottle"
                                 src="<?php echo wp_get_attachment_image_url(get_field('bottle_image', $latest_post[0]->ID), 'large'); ?>"
                                 alt="bottle image">
                        </div>

                        <div
                                class="absolute-word"><?php the_field('next_post_title_background', $latest_post[0]->ID); ?></div>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<?php
if (is_singular('products')) { ?>

    <script src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/src/js/vendor/fullpage.js"></script>

<?php } else { ?>

<?php }
?>
<?php get_footer(); ?>
