<?php
/**
 * Template Name: Products-page
 */
get_header(); ?>

    <section class="top-banner-products back-img-top-align"
             style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('top_banner_background_image'), 'fullhd'); ?>);">
        <div class="container">
            <h1 class="title-type-3 white">
                <span class="is_on_screen_item animation-first-row">
                    <?php the_field('top_banner_title_first_row') ?>
                </span>
                <span class="is_on_screen_item animation-second-row">
                    <?php the_field('top_banner_title_second_row') ?>
                </span>
            </h1>
        </div>
    </section>

    <section class="products">
        <div class="container">

            <div class="wrap-salsas">


                <?php

                $args = array(
                    'post_type' => 'products',
                    'posts_per_page' => 4,
                    'post_parent' => 0,
                    'orderby' => 'menu_order'
                );

                $the_query = new WP_Query($args);

                ?>

                <?php if ($the_query->have_posts()) : ?>

                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                        <div class="column">
                            <a href="<?php the_permalink() ?>" class="salsa-wrap">

                                <div class="wrap-imgs">

                                    <img class="cap"
                                         src="<?php echo wp_get_attachment_image_url(get_field('cap_image'), 'medium'); ?>"
                                         alt="bottle-cap image">
                                    <img class="bottle"
                                         src="<?php echo wp_get_attachment_image_url(get_field('bottle_image'), 'large'); ?>"
                                         alt="bottle image">
                                </div>

                                <span class="absolute-title"><?php the_field('background_bottle_text') ?></span>

                                <div class="item-circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <path fill-rule="evenodd" stroke-width="2px"
                                              stroke-linecap="round" stroke-linejoin="miter" fill="none"
                                              d="M260.500,2.000 C403.266,2.000 519.000,117.734 519.000,260.500 C519.000,403.266 403.266,519.000 260.500,519.000 C117.734,519.000 2.000,403.266 2.000,260.500 C2.000,117.734 117.734,2.000 260.500,2.000 Z"></path>
                                    </svg>
                                </div>
                            </a>
                        </div>

                        <?php endwhile; ?>

                <?php endif; ?>

                <?php wp_reset_postdata(); ?>

            </div>
        </div>

    </section>

    <section class="sign-up-today back-img-top-align"
             style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('sign_up_background_image'), 'fullhd'); ?>);">
        <div class="container">
            <h2 class="title-type-3 white is_on_screen_item animation-first-row">
                <?php the_field('sign_up_title') ?>
            </h2>

            <div class="content white-theme is_on_screen_item animation-second-row">
                <?php the_field('sign_up_content') ?>
            </div>

            <a href="<?php the_field('sign_up_button_link') ?>" class="btn-type-2 black">
                <?php the_field('sign_up_button_text') ?>
            </a>
        </div>
    </section>

<?php get_footer(); ?>