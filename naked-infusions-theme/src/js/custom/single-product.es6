(function ($) {

    if ($('body').hasClass('single-products')) {

        function createFullpageSingleProd() {
            new fullpage('#fullpage-single-product', {
                sectionSelector: '.section',
                navigation: true,
                navigationPosition: 'right',
                slidesNavigation: true,

                afterLoad: function (anchorLink, index) {

                },

                onLeave: function(origin, destination, direction){

                    $('#staticImg').toggleClass('moveDown', (origin.index == 2 && direction == 'up') || (origin.index == 1 && direction == 'down') || (origin.index == 0 && direction == 'down'));
                    $('#staticImg').toggleClass('moveUp', origin.index == 1 && direction == 'up');
                    $('.section2').toggleClass('visible', direction == 'down');

                },

                afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {

                },

                onSlideLeave: function (anchorLink, index, slideIndex, direction) {

                }
            });
        }

        var createFullPageSingle = false;


        if ($(window).width() <= 850) {
            createFullPageSingle = false;

        } else {
            createFullpageSingleProd();

        }

        $(window).resize(function () {

            if ($(window).width() <= 850) {

                console.log('small');
                if (createFullPageSingle == true) {
                    fullpage_api.destroy('all');
                    createFullPageSingle = false;
                }
            }
            if ($(window).width() > 769) {
                if (createFullPageSingle == false) {
                    createFullpageSingleProd();
                    createFullPageSingle = true;
                }

            }
        });

    }

})(jQuery);