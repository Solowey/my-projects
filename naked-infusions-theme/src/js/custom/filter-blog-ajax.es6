(function ($) {

    if ($('.blog').length) {
        var max_num_page2;
        //add "ALL" buttons for filters
        let categoriesIndexOfCategory = 0;
        $('.categories ul').each(function () {
            let item = `<li class="checkbox-all"><input type="checkbox" id="all` + categoriesIndexOfCategory + `" name="all"><label for="all` + categoriesIndexOfCategory + `">All</label></li>`;
            categoriesIndexOfCategory++;
            $(this).prepend(item);
        });


        $.fn.onEnter = function (func) {
            this.bind('keypress', function (e) {
                if (e.keyCode == 13) func.apply(this, [e]);
            });
            return this;
        };

        let projectItemNav = $('.filter-content .categories ul');
        let projectsGrid = $('.blog .container .blog-items-wrap');
        let projectItem = $('.blog .container .blog-items-wrap .item');

        class AjaxTaxonomyLoading {
            constructor(settings) {
                this.grid = settings.grid;
                this.taxonomy = settings.taxonomy || null;
                this.newListToggle = settings.newList || false; //set current page to 0 and clear output grid
                this.args = settings.args;
                this.state = {
                    currentPage: 1,
                    currentTaxId: 0,
                    havePosts: true,
                    dataLoading: false
                };
                this.action = settings.action;
                this.currentPage = 1;
                this.currentTaxId = [];
                this.callback = settings.callback || null;
                this.onNoPosts = settings.onNoPosts || null;
            }

            loadMoreAjax() {
                let that = this;
                //that.args.tax_id = this.state.currentTaxId;
                that.args.paged = this.state.currentPage;
                $.ajax({
                    url: ajaxMeta.ajaxurl,
                    data: {
                        action: that.action,
                        args: that.args,
                        pageID: ajaxMeta.pageID
                    },
                    type: 'POST',
                    success: function (data) {
                        that.onSuccess(JSON.parse(data));
                        max_num_page2 = max_num_page;

                        if (that.args.paged == max_num_page2) {

                            TweenMax.to($('.but-wrap .read-more'), 0.5, {css: {zIndex: -5, opacity: 0}});
                        } else {
                            TweenMax.to($('.but-wrap .read-more'), 0.5, {css: {zIndex: 1, opacity: 1}});
                        }

                    },
                    beforeSend: () => {
                        this.state.dataLoading = true;
                    },
                    error: () => {
                        console.log('error');
                        this.state.dataLoading = false;
                    }
                })
                ;
            }

            resetCurrentPage() {
                this.state.currentPage = 1;
            }

            loadNewList(taxonomyId) {
                let that = this;
                let grid = that.grid;
                //this.state.currentTaxId = taxonomyId || null;
                this.state.havePosts = true;
                grid.empty();
                this.resetCurrentPage();
                this.loadMoreAjax();
            }

            appendList() {
                if ((this.state.dataLoading === false) && (this.state.havePosts)) {
                    this.loadMoreAjax();
                }
            }

            onSuccess(data) {
                if (data.count > 0) {
                    let $items = $(data.data);

                    this.grid.append($items);
                    this.state.currentPage++;
                    typeof this.callback === 'function' ? this.callback() : null;
                } else {
                    this.state.havePosts = false;
                    typeof this.onNoPosts === 'function' ? this.onNoPosts() : null;
                }
                this.state.dataLoading = false;
            }

            setTaxonomyId(taxonomyId) {
                this.state.currentTaxId = taxonomyId || null;
            }

            clickHandler(taxonomyId) {
                this.setTaxonomyId(taxonomyId);
                if (this.newListToggle) {
                    this.loadNewList(taxonomyId);
                } else this.appendList();
            }

            scrollHandler(taxonomyId) {
                this.setTaxonomyId(taxonomyId);
                this.appendList();
            }
        }


        let dataGridList = {
            grid: projectsGrid,
            action: "resources_ajax",
            newList: false,
            args: {
                'paged': 1,
                'tax_id': '',
                'postPerPage': 18
            },
            callback: function () {
                $('.blog .blog-items-wrap .item').hide();
                $('.blog .blog-items-wrap .item').fadeIn('slow');
            }
        };
        let gridItemsAjax = new AjaxTaxonomyLoading(dataGridList);

        //gridItemsAjax.state.dataLoading === false ? gridItemsAjax.loadNewList($(".units-categories li").first().data('taxonomyid')) : null;

        var timeout;


        function navClickEvent() {
            if ((gridItemsAjax.state.dataLoading === false) && (gridItemsAjax.state.havePosts === true)) {
                max_num_page2 = max_num_page;
                projectsGrid.empty();

                //wait until user clicks on checkboxes
                timeout = setTimeout(function () {
                    let checkedElements = [];
                    $('.filter-content .categories ul li').each(function () {
                        if ($(this).hasClass('checked')) {
                            checkedElements.push($(this).data('taxonomyid'))
                        }
                    });


                    gridItemsAjax.state.currentPage = 1;
                    gridItemsAjax.args.tax_id = JSON.stringify(checkedElements);
                    projectsGrid.empty();
                    gridItemsAjax.loadNewList();
                    gridItemsAjax.state.havePosts = true;
                }, 1000);

                searchResults = false;
            } else {

            }
        }

        //fill grid for first time
        let adwiwuadh = [];
        gridItemsAjax.state.currentPage = 1;
        gridItemsAjax.args.tax_id = JSON.stringify(adwiwuadh);
        projectsGrid.empty();
        gridItemsAjax.loadNewList();
        gridItemsAjax.state.havePosts = true;


        //filter item press event
        projectItemNav.on('mouseup', 'li', function () {
            gridItemsAjax.state.havePosts = true;
            $('.filter-content .categories ul li.checked').removeClass('checked');
            navClickEvent();

            if ($(this).hasClass("checkbox-all")) {
                if ($(this).hasClass("checked")) {
                    $(this).siblings().removeClass('checked');
                    $(this).removeClass('checked');
                } else {
                    $(this).siblings().addClass('checked');
                    $(this).addClass('checked');
                }
            }
        });

        //clean timeout while checking many points
        projectItemNav.on('mousedown', 'li', function () {
            clearTimeout(timeout);
        });


        //search
        let searchGridList = {
            grid: projectsGrid,
            action: "resources_ajax",
            newList: false,
            args: {
                'paged': 1,
                'postPerPage': 18,
                'search': ''
            }
        };
        let searchItemsAjax = new AjaxTaxonomyLoading(searchGridList);

        var searchResults = false;
        var timeoutSearch;

        function searchEvent() {

            if ((searchItemsAjax.state.dataLoading === false) && (searchItemsAjax.state.havePosts === true)) {
                projectsGrid.empty();

                //wait until user clicks on checkboxes
                timeoutSearch = setTimeout(function () {
                    searchItemsAjax.state.currentPage = 1;
                    projectsGrid.empty();
                    searchItemsAjax.loadNewList();
                    searchItemsAjax.state.havePosts = true;
                }, 1000);

                searchResults = true;
            }


        }

        // scroll detected

        function scroll_detected_projects() {

            $('.but-wrap .read-more').on('click', function (e) {
                e.preventDefault();

                gridItemsAjax.scrollHandler(gridItemsAjax.args.tax_id);

                if (gridItemsAjax.state.currentPage == max_num_page2) {

                    TweenMax.to($(this), 0.5, {css: {zIndex: -5, opacity: 0}});
                } else {
                    TweenMax.to($(this), 0.5, {css: {zIndex: 1, opacity: 1}});
                }
            });
        }

        scroll_detected_projects();

    }


})(jQuery);
