(function ($) {

    //clickToggle
    $.fn.clickToggle = function (a, b) {
        return this.each(function () {
            var clicked = false;
            $(this).bind('click', function () {
                if (clicked) {
                    clicked = false;
                    return b.apply(this, arguments);
                }
                clicked = true;
                return a.apply(this, arguments);
            });
        });
    };

//menu
    $('#gumburger').clickToggle(function () {
        //add class to body
        $("html, body").addClass('disable-scroll');
        $(this).addClass('open');
        //animate menu
        $('#navbar').fadeIn(600);
        var count = 0;
        $('#menu-main-menu li').each(function (index) {
            count++;
            var curent = $(this);
            setTimeout(function () {
                curent.addClass('in');

            }, 600 + count * 100);
        });

        setTimeout(function () {
            $(".contact").fadeTo(1000, 1);
        }, 1100);
        $(this).toggleClass('collapsed');
    }, function () {
        //remove class from body
        $("html, body").removeClass('disable-scroll');

        //remove class
        $(this).toggleClass('collapsed');
        $(this).removeClass('open');
        $('#menu-main-menu li').removeClass('in');
        $(".contact").fadeTo(200, 0);
        $('#navbar').delay(300).fadeOut(600);
    });
    $("#menu-main-menu li a").on({
        mouseenter: function () {
            $("#menu-main-menu li a").not(this).addClass('link-no-hover');
        },
        mouseleave: function () {
            $("#menu-main-menu li a").not(this).removeClass('link-no-hover');
        }
    });


})(jQuery);