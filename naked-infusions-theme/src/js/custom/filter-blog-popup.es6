(function ($) {
    if ($('body').hasClass('page-template-blog')) {
        function filterBlog() {

            $('.blog .filter-button .filter-title').on("click", function (event) {
                event.preventDefault();

                //add class to body
                $("html, body").addClass('filter-active');

                //animate menu
                $('.filter-content').fadeIn(600);
                var count = 0;
                $('.filter-content ul li').each(function () {
                    count++;
                    var curent = $(this);
                    setTimeout(function () {
                        curent.addClass('in');

                    }, 600 + count * 100);
                });
            });

            $('.filter-content .close-button').on("click", function () {

                //remove class from body
                $("html, body").removeClass('filter-active');

                //remove class
                $(this).removeClass('open');
                $('.filter-content ul li').removeClass('in');
                $('.filter-content').delay(300).fadeOut(600);

            });

            $('.blog .categories ul').on("click", function () {
                setTimeout(function () {

                    //remove class from body
                    $("html, body").removeClass('filter-active');

                    //remove class

                    $('.filter-content').delay(300).fadeOut(600);
                }, 300);

            });

        }

        filterBlog();
    }

})(jQuery);
