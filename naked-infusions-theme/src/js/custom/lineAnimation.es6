 require('../vendor/scroller');

(function ($) {
    class LineAnimation {
        constructor(settings) {
            this.inputLineConfig = settings.points;
            this.$element = settings.$element;
            this.stroke = settings.stroke;

            this.state = {
                elementWidth: 0,
                elementHeight: 0,
                pointsConfig: [],
                totalLength: 0
            };

            this.$svgElement = $(document.createElementNS("http://www.w3.org/2000/svg", "svg"));
            this.$svgElement.addClass('line-animation-render');
            this.$polyline = $(document.createElementNS("http://www.w3.org/2000/svg", 'polyline'));
            this.$element.append(this.$svgElement);
            this.$element.css('position', 'relative');

            this.$svgElement.css({
                width: '100%',
                height: '100%',
                position: 'absolute',
                left: 0,
                top: 0,
                overflow: 'visible'
            });

            this.updateSize();
            this.parsedLineConfig = this.parseLineConfig(this.inputLineConfig);
            this.state.totalLength = this.getTotalDistance(this.parsedLineConfig);
            this.$svgElement.append(this.$polyline);
            this.drawPolyline();
            this.setDash(this.parsedLineConfig);

            //console.log(this.state);
        }

        resize(){
            this.updateSize();
            this.state.totalLength = this.getTotalDistance(this.parsedLineConfig);
            this.drawPolyline();
            this.setDash(this.parsedLineConfig);
        }

        getTotalDistance(lineConfig) {
            let totalDistance = 0;

            for (let i = 0; i < lineConfig.length - 1; i++) {
                let widthPxInPercent = this.state.elementWidth / 100;
                let heightPxInPercent = this.state.elementHeight / 100;

                let x1 = lineConfig[i][0] * widthPxInPercent,
                    y1 = lineConfig[i][1] * heightPxInPercent,
                    x2 = lineConfig[i + 1][0] * widthPxInPercent,
                    y2 = lineConfig[i + 1][1] * heightPxInPercent;

                totalDistance += this.getDistance(x1, y1, x2, y2);
            }
            return totalDistance;
        }

        drawPolyline() {
            let self = this;
            let pointsStr = '';

            this.parsedLineConfig.forEach(function (item) {
                pointsStr += self.state.elementWidth / 100 * item[0] + ", " + self.state.elementHeight / 100 * item[1] + " "
            });

            this.$polyline.attr("points", pointsStr);
            this.$polyline.attr("style", "stroke:" + this.stroke + "; stroke-width:" + "55" + "; fill:none"); //Set path's data
        }

        getDistance(x1, y1, x2, y2) {
            var a = x1 - x2;
            var b = y1 - y2;

            return (Math.sqrt(a * a + b * b));
        }

        parseLineConfig(config) {
            let bracket_points_arr = [];
            config.forEach(function (points_str) {
                let brackets_points = points_str.split(', ');

                brackets_points.forEach(function (item) {
                    bracket_points_arr.push(item.split(' '));
                });
            });

            return bracket_points_arr;
        }

        setDash() {
            this.$polyline.attr("stroke-dasharray", this.state.totalLength);
            this.$polyline.attr("stroke-dashoffset", this.state.totalLength);
        }


        setProgress(progress) {
            this.$polyline.attr("stroke-dashoffset", this.state.totalLength - (this.state.totalLength / 100 * progress));
        }

        updateSize() {
            this.state.elementWidth =  this.$element.outerWidth();
            this.state.elementHeight =  this.$element.outerHeight();
        }
    }


    class CrossSectionLineAnimation{
        constructor(settings){
            this.state = {
                sectionsConfig: []
            };

            this.initSectionsConfig(settings);
            this.setSectionsHeight(this.state.sectionsConfig);
            this.setTotalHeight(this.getTotalHeight());
            this.setTotalAnimationPercent();
        }

        resize(){
            this.setSectionsHeight(this.state.sectionsConfig);
            this.setTotalHeight(this.getTotalHeight());
            this.setTotalAnimationPercent();
        }

        getTotalHeight(){
            let totalHeight = 0;

            this.state.sectionsConfig.forEach((item) => {
                totalHeight += item.elementHeight;
            });

            return totalHeight;
        }

        setTotalHeight(totalHeight){
            this.state.totalHeight = totalHeight;
        }

        initSectionsConfig(config){
            config.forEach((item) => {
                this.state.sectionsConfig.push({
                    $element: item.$element,
                    lineAnimation: item.lineAnimation,
                    progress: 0
                })
            })
        }

        setSectionsHeight(config){
            config.forEach((item) => {
                item.elementHeight = item.$element.outerHeight()
            })
        }

        setTotalAnimationPercent() {
            this.state.sectionsConfig.forEach((section, index) => {
                section.totalAnimationPercent = section.elementHeight / (this.state.totalHeight / 100);
            })
        }

        setProgress(progress){
            let localProgress = progress;

            for (let section of this.state.sectionsConfig) {
                if (localProgress === 0) section.progress = 0;

                let progressTo = localProgress / section.totalAnimationPercent * 100;
                section.progress = progressTo > 100 ? 100 : progressTo;

                section.lineAnimation.setProgress(section.progress);

                localProgress = localProgress - section.totalAnimationPercent < 0 ? 0 : localProgress - section.totalAnimationPercent;
            }

        }
    }

    $(window).on('load', function() {
        setTimeout(() => {

            initLineAnimation6()//ABOUT US PAGE

        }, 1000)
    });


    //////////////////////////////////////////////////////////////////////////////
    //ABOUT US PAGE
    //////////////////////////////////////////////////////////////////////////////

    function initLineAnimation6() { 
        let $section1 = $('.about-item-2');

        let lineAnimations = [];

        lineAnimations.push(new LineAnimation({
            $element: $section1,
            stroke: '#ea5043',
            points: ['46.3 0, 100 0, 100 100']
        }));

        let tweenmaxProgressHelper = {
            progress: 0
        };

        let crossSectionLineAnimation6 = new CrossSectionLineAnimation(
            [
                {
                    $element: $section1,
                    lineAnimation: lineAnimations[0]
                },
            ]
        );

        $section1.on('hidden.scroller', function (item, progress) {
            if (progress < 20) {
                animateTo(0)
            }
            else {
                animateTo(100)
            }
        });

        $section1.on('visible.scroller progress.scroller', function (item, progress) {
            animateTo(progress);
        });

        function animateTo(progress){
            TweenMax.to(tweenmaxProgressHelper, .2, {progress: progress, onUpdate: () => {
                crossSectionLineAnimation6.setProgress(tweenmaxProgressHelper.progress);
            }})
        }

        $section1.scroller({
            triggerOffset: {
                top: '50vh',
                bottom: '-50vh'
            },
            $elementTo: $section1
        });

        $(window).on('resizeLineAnimationEnd', function(){
            lineAnimations.forEach((animation) => {
                animation.resize();
            })
        })
    }

    var resizeLineAnimationEnd;
    $(window).on('resize', function() {
        clearTimeout(resizeLineAnimationEnd);
        resizeLineAnimationEnd = setTimeout(function() {
            $(window).trigger('resizeLineAnimationEnd');
        }, 500);
    });

})(jQuery);