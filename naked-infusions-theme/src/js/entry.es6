require("../sass/style.scss");

require('../fonts/fontello/css/fontello.css');

require('./vendor/bootstrap-transition');
require('./vendor/bootstrap-collapse');
require('./vendor/slick.min.js');
require('./vendor/TweenMax.min.js');
require('./vendor/wow.min.js');
require('./vendor/tab.js');
require('./vendor/jquery.isonscreen.js');

require('./custom/single-product.es6');
require('./custom/menu.es6');
require('./custom/filter-blog-popup.es6');
require('./custom/filter-blog-ajax.es6');
require('./custom/lineAnimation.es6');

require('./vendor/spritespin');


/*Flip animation - Home page*/

require('./vendor/fullpage/velocity.min.js');
require('./vendor/fullpage/velocity.ui.min.js');
require('./vendor/fullpage/main.js');

(function ($) {

    $(document).ready(function () {
        //************************************
        // check top bar active plugin
        //************************************
        $(window).on('load resize', function () {
            if ($('#tpbr_topbar').length > 0) {
                $('body').addClass('topBar_active');
                $("#fullpage").css('padding-top',$('#tpbr_topbar').outerHeight()+'px');
                $("header").css('top',$('#tpbr_topbar').outerHeight()+'px');
            }
        });


        //************************************
        // slider home page
        //************************************

        $(".slider-better-naked").slick({
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            speed: 1000,
            autoplay: true,
            fade: true,
            adaptiveHeight: true,
            autoplaySpeed: 5000
        });

        // slider home page with custom dots

        if ($(".slider-why-naked").length > 0) {

            var activeClass = 'slick-active',
                ariaAttribute = 'aria-hidden';

            $('.slider-why-naked').on('init', function () {

                $('.slider-why-naked .slick-dots li:first-of-type').addClass(activeClass).attr(ariaAttribute, false);
            })
                .on('afterChange', function (event, slick, currentSlide) {
                    var $dots = $('.slider-why-naked .slick-dots');

                    $dots.each(function () {
                        $('li', $(this)).eq(currentSlide).addClass(activeClass).attr(ariaAttribute, false);
                    });
                });

            $(".slider-why-naked").slick({
                dots: true,
                appendDots: $(".slick-navigation"),
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                speed: 1000,
                fade: true,
                adaptiveHeight: false,
                pauseOnHover: false,
                autoplaySpeed: 4000,
                responsive: [
                    {
                        breakpoint: 650,
                        settings: {
                            adaptiveHeight: true
                        }
                    }
                ]
            });
        }

        //************************************
        // slider about page with custom dots
        //************************************

        if ($(".about-slider").length > 0) {

            var activeClass = 'slick-active',
                ariaAttribute = 'aria-hidden';

            $('.about-slider').on('init', function () {

                $('.about-slider .slick-dots li:first-of-type').addClass(activeClass).attr(ariaAttribute, false);
            })
                .on('afterChange', function (event, slick, currentSlide) {
                    var $dots = $('.about-slider .slick-dots');

                    $dots.each(function () {
                        $('li', $(this)).eq(currentSlide).addClass(activeClass).attr(ariaAttribute, false);
                    });
                });

            $(".about-slider").slick({
                dots: true,
                appendDots: $(".slick-navigation"),
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                speed: 1000,
                fade: true,
                adaptiveHeight: false,
                pauseOnHover: false,
                autoplaySpeed: 4000,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            adaptiveHeight: true
                        }
                    }
                ]
            });
        }

    });

    //************************************
    // bottle spin
    //************************************

    $(window).on('load', function () {
        $('#preloader').fadeOut(1000);
        if ($('body').hasClass('page-template-home-page')) {

            setTimeout(function () {

                $("#mySpriteSpin1").spritespin({

                    source: [
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C001-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C002-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C003-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C004-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C005-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C006-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C007-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C008-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C009-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C010-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C011-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C012-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C013-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C014-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C015-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C016-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C017-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C018-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C019-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C020-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C021-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C022-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C023-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Mild/00850091005121_E1_R01_C024-min.png"
                    ],
                    responsive: true,
                    animate: true,
                    frameTime: 160,
                    retainAnimate: true,
                    sense: -1,
                    width: 340,
                    height: 462

                });

                $("#mySpriteSpin2").spritespin({
                    source: [
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C01-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C02-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C03-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C04-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C05-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C06-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C07-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C08-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C09-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C10-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C11-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C12-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C13-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C14-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C15-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C16-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C17-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C18-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C19-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C20-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C21-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C22-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C23-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Medium/00850091005039_E1_R01_C24-min.png"
                    ],
                    responsive: true,
                    animate: true,
                    frameTime: 160,
                    retainAnimate: true,
                    width: 340,
                    height: 462,
                    //renderer: 'background',
                    sense: -1

                });

                $("#mySpriteSpin3").spritespin({
                    source: [
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C01-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C02-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C03-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C04-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C05-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C06-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C07-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C08-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C09-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C10-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C11-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C12-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C13-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C14-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C15-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C16-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C17-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C18-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C19-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C20-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C21-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C22-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C23-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Extra-Hot/00850091005145_E1_R01_C24-min.png"
                    ],
                    responsive: true,
                    animate: true,
                    frameTime: 160,
                    width: 340,
                    height: 462,
                    retainAnimate: true,
                    //renderer: 'background',
                    sense: -1

                });

                $("#mySpriteSpin4").spritespin({
                    source: [
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C01_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C02_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C03_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C04_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C05_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C06_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C07_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C08_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C09_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C10_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C11_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C12_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C13_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C14_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C15_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C16_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C17_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C18_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C19_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C20_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C21_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C22_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C23_optimized-min.png",
                        "/wp-content/themes/naked-infusions-theme/images/spin/Fire-Roasted-Garlic/00850091005053_E1_R01_C24_optimized-min.png"
                    ],
                    responsive: true,
                    animate: true,
                    frameTime: 160,
                    width: 340,
                    height: 462,
                    retainAnimate: true,
                    //renderer: 'background',
                    sense: -1

                });
            }, 2000);
        }


        //************************************
        // back-to-top
        //************************************

        if (!($("body").hasClass("home"))) {

            $("#back-top").click(function (event) {
                event.preventDefault();
                $('body,html').animate({scrollTop: 0}, 1000);
            });
        }
    });

    // ==============================================================
    // Is on screen
    // ==============================================================
    $(document).ready(function () {
        if (!($("body").hasClass("home"))) {
            function is_on_screen_item_init() {

                var is_on_screen_item = $('.is_on_screen_item');

                if (is_on_screen_item.length > 0) {
                    check_is_on_screen_item();
                }

                $(document).on('scroll load resize', function (event) {

                    if (is_on_screen_item.length > 0) {
                        check_is_on_screen_item();
                    }
                });


                function check_is_on_screen_item() {

                    is_on_screen_item.each(function (i) {

                        var _ = $(this);

                        if (_.isOnScreen(0.4, 0.4)) {

                            _.addClass('is_on_screen');

                            // is_on_screen_item.splice(i, 1);
                        }

                    });
                }
            }

            $(window).on('load resize', function () {
                is_on_screen_item_init();
            });
        }
    });

    //************************************
    // Blog filters section
    //************************************

    $('.blog .filter-content .categories li').on('click ', function () {

        if ($(this).hasClass("checked")) {
            $(this).removeClass('checked');
        } else {
            $(this).addClass('checked');
        }
    });

    //************************************
    // Resource filters section
    //************************************

    $('#fullpage-single-product .btn-open').on('click ', function () {
        $(this).toggleClass('show');
    });


    $(function () {
        var animateTime = 500,
            navLink = $('#fullpage-single-product .btn-open');

        function toggleCollapse($nav) {
            if ($nav.height() === 0) {
                autoHeightAnimate($nav, animateTime);
            } else {
                $nav.stop().animate({height: '0'}, animateTime);
            }
        }

        navLink.on('click', function () {
            var $nav = $(this).closest('.wrap-dropdown').find('.links');
            toggleCollapse($nav);
        })
    });

    /* Function to animate height: auto */
    function autoHeightAnimate(element, time) {
        var curHeight = element.height(), // Get Default Height
            autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({height: autoHeight}, time); // Animate to Auto Height
    }

    //************************************
    // Footer
    //************************************
    if (!$('body').hasClass('home', 'single-products')) {
        $(window).on('load resize scroll', function () {
            var f = $('footer');
            if (f.offset().top + f.height() < $(window).height()) {
                $('footer').addClass('sticky');
            }
        });

    }

//************************************
    // FB Share
    //************************************

    $('.facebookShareBtn').click(function () {
        let elem = $(this);
        postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'));

        return false;
    });

})(jQuery);