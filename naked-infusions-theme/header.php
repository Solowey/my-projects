<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/imgs/favicon.png"
          type="image/png"/>

    <!--[if lte IE 9 ]>
    <script>
        alert('Browser version is too old and site will not be displayed correctly. Please, upgrade your browser.');
    </script>

    <![endif]-->
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function () {
            FB.init({
                appId: '431442171128858',
                xfbml: true,
                version: 'v2.6'
            });
        };

        function postToFeed(title, desc, url, picture) {
            var obj = {method: 'feed', link: url, name: title, description: desc, picture: picture};

            function callback(response) {
            }

            FB.ui(obj, callback);
        }
    </script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

    <div id="preloader">
        <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/images/loader.gif" alt="loader gif">
    </div>

    <header class="cd-auto-hide-header">
        <?php
        $logo = get_field('logo', 'option');
        ?>

        <div class="container">
            <div class="navbar-head">
                <a class="logo" href="/">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Logotype image">
                </a>

                <div class="wrap-right-side">
                    <a class="btn-link" href="<?php the_field('shop_button_link', 'option') ?>">SHOP</a>

                    <button type="button" class="navbar-toggle collapsed" id="gumburger">
                        <span class="menu-open">MENU</span>
                        <span class="menu-close">CLOSE</span>
                    </button>
                </div>
            </div>

            <div class="navbar-collapse" id="navbar">
                <div class="container container2">
                    <div class="wrap-nav">
                        <?php
                        wp_nav_menu(array(
                                'menu' => 'Main menu',
                                'container' => '',
                                'menu_class' => 'nav navbar-nav'
                            )
                        );
                        ?>
                        <?php
                        $facebook = get_field('facebook', 'option');
                        $instagram = get_field('instagram', 'option');
                        $phone = get_field('phone', 'option');
                        $email = get_field('email', 'option');
                        $adress = get_field('address', 'option');

                        ?>
                        <div class="contact">
                            <div class="item info">
                                <h4>Get In touch</h4>
                                <?php
                                if ($adress) {
                                    ?>
                                    <p><?php echo $adress ?></p>

                                    <?php
                                }
                                ?>
                                <?php
                                if ($email) {
                                    ?>
                                    <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
                                    <br>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="item social">
                                <h4>Naked Infusion</h4>
                                <?php
                                if ($facebook) {
                                    ?>
                                    <a target="_blank" href="<?php echo $facebook ?>">facebook</a>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($instagram) {
                                    ?>
                                    <a target="_blank" href="<?php echo $instagram ?>">instagram</a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="content" class="site-content">