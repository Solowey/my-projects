<?php
/**
 * Template Name: Home-page
 */
get_header(); // подключаем header.php ?>

    <style>
        @media only screen and (min-width: 1050px) {
            html {
                overflow: hidden;
            }
        }
    </style>
    <div id="fullpage" class="fullpage-wrapper" data-hijacking="on" data-animation="rotate">
        <section class="section1 section  cd-section section-top-banner visible" id="sec1">
            <div class="section-inner">
                <div class="absolute-numbers"><span>1 <br>/ <br>5</span></div>

                <div class="parallax-container">
                    <div class="box back-img"
                         style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('top_banner_background_image'), 'fullhd'); ?>);">
                    </div>
                </div>

                <div class="right-bottom-info">

                    <div class="content">
                        <?php the_field('right_bottom_content') ?>
                    </div>

                    <img src="<?php echo wp_get_attachment_image_url(get_field('right_bottom_image'), 'large'); ?>"
                         alt="image">
                </div>
            </div>
        </section>

        <section class="section section2 cd-section section-better-naked" id="sec2">
            <div class="section-inner">
                <div class="absolute-numbers"><span>2 <br>/ <br>5</span></div>
                <div class="bg"></div>
                <div class="container">
                    <div class="wrapper-columns">
                        <div class="left-side">

                            <div id="bottleImg">
                                <div class="wrap-img">
                                    <img id="cap" class="cap"
                                         src="<?php bloginfo('stylesheet_directory'); ?>/images/LID.png"
                                         alt="bottle-cap image">
                                    <img id="bottle" class="bottle"
                                         src="<?php bloginfo('stylesheet_directory'); ?>/images/bottle.png"
                                         alt="bottle image">
                                </div>
                            </div>
                        </div>

                        <div class="right-side">

                            <div class="title-type-1 orange"><?php the_field('better_naked_title') ?></div>

                            <div class="slider-better-naked">
                                <?php while (have_rows('better_naked_slider')) : the_row(); ?>

                                    <div class="item">
                                        <div class="title-type-2"><?php the_sub_field('title') ?></div>
                                        <div class="content"><?php the_sub_field('content') ?></div>
                                        <a href="<?php the_sub_field('button_link') ?>" class="btn-type-1 black">
                                            <?php the_sub_field('button_text') ?>
                                        </a>
                                    </div>

                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section3 cd-section section-why-naked" id="sec3">
            <div class="section-inner">
                <div class="absolute-numbers"><span>3 <br>/ <br>5</span></div>
                <div class="bg"></div>
                <div class="slider-why-naked">
                    <?php while (have_rows('why_naked_slider')) : the_row(); ?>
                        <div class="item">
                            <div class="left-side">
                                <div class="wrap-content">
                                    <div class="subtitle"><?php the_sub_field('why_naked_subtitle') ?></div>
                                    <div class="title-type-1 black"><?php the_sub_field('why_naked_title') ?></div>
                                    <div class="slick-navigation"></div>
                                    <div class="title-type-2"><?php the_sub_field('title') ?></div>
                                    <div class="content"><?php the_sub_field('content') ?></div>
                                    <a href="<?php the_sub_field('button_link') ?>" class="btn-type-1 black">
                                        <?php the_sub_field('button_text') ?>
                                    </a>
                                </div>
                            </div>

                            <div class="right-side back-img"
                                 style="background-image: url(<?php echo wp_get_attachment_image_url(get_sub_field('image'), 'fullhd'); ?>);">
                            </div>

                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>

        <section class="section section4 cd-section section-our-products" id="sec4">
            <div class="section-inner">
                <div class="absolute-numbers"><span>4 <br>/ <br>5</span></div>
                <img class="absolute-figure" src="<?php bloginfo('stylesheet_directory'); ?>/images/figure.png"
                     alt="figure image">

                <div class="bg"></div>
                <div class="container">

                    <div class="wrapper">

                        <div class="title-type-1 black"><?php the_field('our_products_title') ?></div>
                        <div class="subtitle"><?php the_field('our_products_subtitle') ?></div>

                        <div class="wrap-products">

                            <?php if (have_rows('our_products_repeater')): ?>

                                <?php $tabs_counter = 1; ?>

                                <ul class="nav nav-tabs">

                                    <?php while (have_rows('our_products_repeater')) : the_row(); ?>

                                        <li class="<?php if ($tabs_counter == 1) {
                                            echo 'active';
                                        } ?>">
                                            <a data-toggle="tab"
                                               href="#<?php echo sanitize_title(get_sub_field('category')); ?>">
                                                <?php the_sub_field('category'); ?>
                                            </a>
                                        </li>

                                        <?php $tabs_counter++; ?>

                                    <?php endwhile; ?>

                                </ul>

                            <?php endif; ?>

                            <?php if (have_rows('our_products_repeater')): ?>

                                <?php $tabs_counter = 1; ?>
                                <div class="tab-content">

                                    <?php while (have_rows('our_products_repeater')) : the_row(); ?>

                                        <div id="<?php echo sanitize_title(get_sub_field('category')); ?>"
                                             class="tab-pane fade in <?php if ($tabs_counter == 1) {
                                                 echo 'active';
                                             } ?>">

                                            <div class="content-wrap"><?php the_sub_field('content'); ?></div>

                                            <a href="<?php the_sub_field('button_link') ?>" class="btn-type-2 red">
                                                <?php the_sub_field('button_text') ?>
                                            </a>

                                            <div class="absolute-bottle"
                                                 id='mySpriteSpin<?php echo $tabs_counter ?>'></div>
                                        </div>

                                        <?php $tabs_counter++; ?>

                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>

                        </div>

                        <div class="wrap-bottom-content">
                            <div class="title"><?php the_field('our_products_logos_title'); ?></div>

                            <ul class="wrap-logos">

                                <?php while (have_rows('our_products_logos_repeater')) : the_row(); ?>

                                    <li>
                                        <img
                                            src="<?php echo wp_get_attachment_image_url(get_sub_field('logotype_image'), 'medium'); ?>"
                                            alt="logotype">
                                    </li>

                                <?php endwhile; ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section5 cd-section section-naked-insights" id="sec5">
            <div class="section-inner">
                <div class="absolute-numbers"><span>5 <br>/ <br>5</span></div>
                <div class="bg"></div>
                <div class="wrap-content">
                    <div class="container">
                        <div class="wrap-columns">
                            <div class="wrap-titles-mobile">
                                <div class="title-type-1 black"><?php the_field('naked_insights_title') ?></div>
                                <div class="subtitle"><?php the_field('naked_insights_subtitle') ?></div>
                            </div>

                            <div class="wrap-boxes">

                                <div class="left-side">

                                    <?php

                                    $posts = get_field('naked_insights_posts_first_type');

                                    if ($posts): ?>

                                        <?php foreach ($posts as $post): // variable must be called $post (IMPORTANT) ?>
                                            <?php setup_postdata($post); ?>
                                            <a href="<?php the_permalink(); ?>" class="item">
                                                <div class="rect-outer">
                                                    <div class="rect-inner back-img"
                                                         style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image'), 'large'); ?>);">

                                                    </div>
                                                </div>

                                                <div class="title"><?php the_title(); ?></div>
                                            </a>
                                        <?php endforeach; ?>

                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                    <?php endif; ?>
                                </div>

                                <div class="right-side">

                                    <div class="wrap-titles">
                                        <div class="title-type-1 black"><?php the_field('naked_insights_title') ?></div>
                                        <div class="subtitle"><?php the_field('naked_insights_subtitle') ?></div>
                                    </div>

                                    <div class="wrap-items">

                                        <?php

                                        $posts = get_field('naked_insights_posts_second_type');

                                        if ($posts): ?>

                                            <?php foreach ($posts as $post): // variable must be called $post (IMPORTANT) ?>
                                                <?php setup_postdata($post); ?>
                                                <a href="<?php the_permalink(); ?>" class="item">
                                                    <div class="rect-outer">
                                                        <div class="rect-inner back-img"
                                                             style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image'), 'large'); ?>);">

                                                        </div>
                                                    </div>

                                                    <div class="title"
                                                         href="<?php the_permalink(); ?>"><?php the_title(); ?></div>
                                                </a>
                                            <?php endforeach; ?>

                                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="wrap-footer">
                        <div class="copyright"><?php the_field('footer_content', 'option') ?></div>
                        <?php
                        wp_nav_menu(array(
                                'menu' => 'Footer menu',
                                'container' => '',
                                'menu_class' => 'footer-nav'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </section>

    </div>


<?php get_footer(); ?>