<div class="sticky-footer-banner">
    <div class="sticky-footer-banner__description">Connect with us at ICAST! Schedule your meeting with VP Teeg Stouffer, July 8-12.
    </div>
    <a target="_blank" class="sticky-footer-banner__button primary-btn primary-btn_white" href="https://calendly.com/retailcomm">
       <span class="button__border button__border-1">
            <span class="button__border button__border-2">
                    <span class="button__text">
                      SCHEDULE YOUR MEET & GREET
                    </span>
            </span>
        </span>
    </a>
    
    <div class="sticky-footer-banner__close-btn">
        
    </div>
</div>