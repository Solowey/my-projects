<?php

function register_post_type_insights() {

    register_post_type( 'insights',
        array(
            'labels' => array(

                'name' => __('Insights', 'naked-infusions-theme'),
                'singular_name' => __('Insight', 'naked-infusions-theme'),
                'add_new_item'  => __('New Insight', 'naked-infusions-theme'),
                'view_item'     => __('View Insight', 'naked-infusions-theme')
            ),
            'public' 	   => true,
            'has_archive'  => true,
            'hierarchical' => true,
            'supports' => array('title', 'author', 'page-attributes'),
            'rewrite'  => array('slug' => 'insights'),
        )
    );
}

add_action( 'init', 'register_post_type_insights' );