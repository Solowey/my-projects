<?php

function register_post_type_products() {

    register_post_type( 'products',
        array(
            'labels' => array(

                'name' => __('Products', 'naked-infusions-theme'),
                'singular_name' => __('Product', 'naked-infusions-theme'),
                'add_new_item'  => __('New Product', 'naked-infusions-theme'),
                'view_item'     => __('View Product', 'naked-infusions-theme')
            ),
            'public' 	   => true,
            'has_archive'  => false,
            'hierarchical' => true,
            'supports' => array('title', 'author', 'page-attributes'),
            'rewrite'  => array('slug' => 'products'),
        )
    );
}

add_action( 'init', 'register_post_type_products' );