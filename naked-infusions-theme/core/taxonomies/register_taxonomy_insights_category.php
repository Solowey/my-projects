<?php

function register_taxonomy_insights_category()
{

    register_taxonomy('insights-category', array('insights'),
    
        array(
            'labels' => array(
                'name' => __('Insights Categories', 'naked-infusions-theme'),
                'singular_name' => __('Insight Category', 'naked-infusions-theme'),
                'search_items' => __('Search Insights Categories', 'naked-infusions-theme'),
                'all_items' => __('All Insights Categories', 'naked-infusions-theme'),
                'edit_item' => __('Edit Insight Category', 'naked-infusions-theme'),
                'update_item' => __('Update Insight Category', 'naked-infusions-theme'),
                'add_new_item' => __('Add New Insight Category', 'naked-infusions-theme'),
                'new_item_name' => __('New Insight Category', 'naked-infusions-theme'),
                'menu_name' => __('Insights Categories', 'naked-infusions-theme')
            ),
            'hierarchical' => true,
            'rewrite' => array('slug' => 'insights-category')
        ));
}


add_action('init', 'register_taxonomy_insights_category');