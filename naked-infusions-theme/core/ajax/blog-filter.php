<?php function true_load_resources() {

    $postFeature = get_field('feature_post', $_POST['pageID']);
$postFeatureID = array();


if ($postFeature):
        foreach ($postFeature as $p): // variable must NOT be called $post (IMPORTANT)

            $postFeatureID[] = $p->ID;
        endforeach;
    endif;

    $response = array();

    $args = $_POST['args'];
    $search = $args['search'];
    $tax_id = json_decode(stripslashes($args['tax_id']));
    $paged = $args['paged'];
    $post_per_page = $args['postPerPage'];

    $loop_params = array(
        'post_type' => 'post',
        'order' => 'DES',
        'post__not_in' => $postFeatureID,
        'orderby' => 'date',
        'paged' => $paged,
        's' => '',
        'posts_per_page' => $post_per_page,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $tax_id,
            )
        )
    );

    //check for filter
    if (gettype($tax_id) === 'array') {
        if (count($tax_id) === 0) {
            $loop_params['tax_query'] = array();
        }
    }

    //check for search request
    if ($search === "") {
        $loop_params['s'] = $search;
    } elseif ($search) {
        $loop_params['s'] = $search;
        $loop_params['tax_query'] = array();
        $loop_params['posts_per_page'] = -1;
    }

    $loop = new WP_Query($loop_params);

    $count = $loop->post_count;

    ?>
    <?php ob_start(); ?>

    <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
    <li class="item">
        <a href="<?php the_permalink() ?>">

            <div class="image-container">
                <div class="image-wrapper">
                    <div class="gatsby-image-wrapper">
                        <div class="rect-outer">
                            <div class="rect-inner back-img"
                                 style="background-image: url('<?php echo wp_get_attachment_image_src(get_field('image'), 'large')[0]; ?>')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrap-info">
                <div class="category">

                    <?php $categories = get_the_category();
                    if (!empty($categories)) {
                        echo esc_html($categories[0]->name);
                    } ?>

                </div>
                <div class="title"><?php the_title() ?></div>
                <div class="excerpt"><?php the_field('excerpt') ?></div>
            </div>
        </a>
    </li>
<?php endwhile; ?>

    <script>
        var max_num_page = <?php echo $loop->max_num_pages; ?>

    </script>
<?php endif;

    $data = ob_get_contents();
    ob_clean();

    wp_reset_query();

    $response['data'] = $data;
    $response['count'] = $count;
    $response['params'] = $search;


    echo json_encode($response);

    die();
}

add_action('wp_ajax_resources_ajax', 'true_load_resources');
add_action('wp_ajax_nopriv_resources_ajax', 'true_load_resources');
?>