<?php

// HELPERS
include('functions/helpers.php');

// FUNCTIONALITY
include("ajax/blog-filter.php");

// POST TYPES
include("post_types/register_post_type_products.php");


// TAXONOMIES
include("taxonomies/register_taxonomy_insights_category.php");