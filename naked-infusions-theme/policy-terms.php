<?php
/**
 * Template Name: Policy & Terms
 */
get_header();?>

<div class="main-page-wrap">
<?php while ( have_posts() ) : the_post(); ?>
    <div class="container container-small">

        <div class="policy">
            <h2 class="title-type-2"><?php echo get_the_title();?></h2>
            <?php echo get_the_content(); ?>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>