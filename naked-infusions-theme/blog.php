<?php
/**
 * Template Name: Blog-page
 */
get_header(); ?>

<?php $postFeature = get_field('feature_post');
$postFeatureID = array();
?>

<?php if ($postFeature): ?>
    <?php foreach ($postFeature as $p): // variable must NOT be called $post (IMPORTANT) ?>

        <?php $postFeatureID[] = $p->ID; ?>

        <section class="top-banner-feature-post back-img-top-align"
                 style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image', $p->ID), 'fullhd'); ?>);">
            <div class="container">
                <div class="item">
                    <h1 class="title-type-3 white is_on_screen_item animation-first-row"><?php echo get_the_title($p->ID); ?></h1>

                    <div class="wrap-btn is_on_screen_item animation-second-row">
                        <a href="<?php echo get_permalink($p->ID); ?>"
                           class="btn-type-1 white">
                            Read More
                        </a>
                    </div>
                </div>
            </div>
        </section>
    <?php endforeach; ?>
<?php endif; ?>


    <section class="blog">

        <div class="filter-button">
            <a class="filter-title">Filter</a>
        </div>

        <div class="filter-content">

            <div class="close-button">
                <i class="icon icon-cancel-circled-outline"></i>
            </div>

            <div class="categories">

                <ul>

                    <?php

                    $terms = get_terms(array(
                        'taxonomy' => 'category',
                        'hide_empty' => true
                    ));

                    $Counter = 1;
                    ?>

                    <?php foreach ($terms as $term): ?>

                        <li data-taxonomyid="<?php echo $term->term_taxonomy_id; ?>">
                            <input type="checkbox" id="point-<?php echo $Counter; ?>"
                                   name="point-<?php echo $term->name ?>"
                                   aria-labelledby="point-<?php echo $Counter; ?>-label">
                            <label id="point-<?php echo $Counter; ?>-label"
                                   for="point-<?php echo $term->name ?>"><?php echo $term->name ?></label>
                        </li>
                        <?php $Counter++ ?>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
        <div class="container">

            <div class="description"><?php the_field('posts_description') ?></div>
            <ul class="blog-items-wrap flex">

            </ul>

            <?php wp_reset_postdata(); ?>
        </div>
        <a href="" id="back-top">
            <span>BACK TO <br>TOP</span>
        </a>
    </section>

<?php get_footer(); ?>