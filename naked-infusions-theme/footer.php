</div><!-- #content -->

<?php if ((is_front_page()) || (is_home()) || (is_singular('products'))) : ?>


    <?php else : ?>

    <footer>


        <div class="container">

            <div class="left-side">
                <div class="copyright"><?php the_field('footer_content', 'option') ?></div>
            </div>

            <div class="right-side">
                <?php
                wp_nav_menu(array(
                        'menu' => 'Footer menu',
                        'container' => '',
                        'menu_class' => 'footer-menu'
                    )
                );
                ?>
            </div>
        </div>
    </footer>

<?php endif ?>

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>