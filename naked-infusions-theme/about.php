<?php
/**
 * Template Name: About-page
 */
get_header(); ?>

<section class="top-banner-about back-img-top-align"
         style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('top_banner_background_image'), 'fullhd'); ?>);">
    <div class="container flex flex-row">
        <h1 class="title-type-3 white is_on_screen_item animation-first-row" id="animateTitle">

                    <?php the_field('top_banner_title_first_row') ?>

        </h1>
    </div>
</section>

<section class="about">

    <div class="about-section-1">
        <?php if (have_rows('about_section_1')): ?>
            <?php while (have_rows('about_section_1')): the_row(); ?>
                <div class="about-item">

                    <div class="about-item-image"><img src="<?php the_sub_field('image') ?>" alt="image"></div>

                    <div class="about-item-text">
                        <h2 class="title-type-5 black"><?php the_sub_field('title') ?>
                        </h2>

                        <div class="content">
                            <?php the_sub_field('description') ?>
                        </div>
                    </div>

                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <div class="about-section-2">

        <?php if (have_rows('about_section_2')): ?>
            <?php while (have_rows('about_section_2')): the_row(); ?>
                <div class="about-item-2">
                    <div class="about-item-2-text">
                        <h2 class="title-type-5 black"><?php the_sub_field('title') ?></h2>
                        <div class="content">
                            <?php the_sub_field('description') ?>
                        </div>
                    </div>

                    <div class="about-item-2-image">
                        <img src="<?php the_sub_field('image') ?>" alt="image-2">
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

    </div>

    <div class="about-section-3">
        <div class="about-slider">
            <?php while (have_rows('slider')) : the_row(); ?>
                <div class="item">
                    <div class="left-side">
                        <div class="wrap-content">
                            <div class="title-type-5 black"><?php the_sub_field('title') ?></div>
                            <div class="slick-navigation"></div>
                            <div class="title-type-2"><?php the_sub_field('subtitle') ?></div>
                            <div class="content"><?php the_sub_field('content') ?></div>
                        </div>
                    </div>

                    <div class="right-side back-img"
                         style="background-image: url(<?php echo wp_get_attachment_image_url(get_sub_field('image'), 'fullhd'); ?>);">
                    </div>

                </div>
            <?php endwhile; ?>
        </div>
    </div>

    <div class="about-section-4">

        <div class="about-item">

            <div class="about-item-images"><img src="<?php the_field('image') ?>" alt="image"></div>

            <div class="about-item-text">
                <h2 class="title-type-5 black"><?php the_field('title') ?>
                </h2>

                <div class="content">
                    <?php the_field('description') ?>
                </div>
            </div>

        </div>

    </div>
</section>

<section class="sign-up-today back-img-top-align"
         style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('sign_up_background_image'), 'fullhd'); ?>);">
    <div class="container">
        <h2 class="title-type-3 white is_on_screen_item animation-first-row">
            <?php the_field('sign_up_title') ?>
        </h2>

        <div class="content white-theme">
            <?php the_field('sign_up_content') ?>
        </div>

        <a href="<?php the_field('sign_up_button_link') ?>" class="btn-type-2 black">
            <?php the_field('sign_up_button_text') ?>
        </a>
    </div>
</section>
<?php get_footer(); ?>
