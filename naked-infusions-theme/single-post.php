<?php get_header(); ?>

<div class="main-page-wrap">
    <div class="single-post-wrapper">

        <section class="top-banner-feature-post back-img-top-align"
                 style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image'), 'fullhd'); ?>);">

            <h2 class="title-type-3 white is_on_screen_item animation-first-row"><?php echo get_the_title(); ?></h2>

        </section>

        <div class="container container-small">

            <div class="content-head-line">
                <h2 class="title-type-4"><?php echo get_the_title(); ?></h2>

                <p class="post-info"><span class="post-info__infusion">Naked Infustion</span> | <span
                        class="post-info__date"><?php echo get_the_date('F j, Y'); ?></span></>

                <div class="sharing ">

                    <ul class="sharing-menu social">

                        <?php
                        $striped_content = strip_tags(get_field('content'));
                        $post_url = get_permalink();
                        $post_id = isset($_GET['post-id']) ? $_GET['post-id'] : false;

                        ?>

                        <li class="social-wrap">
                            <a class="social-item" href="//www.instagram.com/nakedinfusions/"
                               target="popup"
                               onclick="window.open('//www.instagram.com/nakedinfusions/')">
                                <i class="icon-instagram"></i>
                            </a>
                        </li>

                        <li class="social-wrap">
                            <a class="facebook-link facebookShareBtn social-item"
                               href="<?php the_permalink(); ?>"
                               data-title="<?php the_title(); ?>"
                               data-picture="<?php the_post_thumbnail_url(); ?>"
                               data-desc="<?php echo htmlspecialchars($striped_content); ?>">
                                <i class="icon icon-facebook"></i>
                            </a>
                        </li>

                        <li class="social-wrap">
                            <a class="social-item"
                               href="http://twitter.com/share?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                               target="popup"
                               onclick="window.open('http://twitter.com/share?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>','name','width=600,height=400')">
                                <i class="icon-twitter-bird"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="content content-single-post">
                <?php the_field('content'); ?>
            </div>

        </div>
        <div class="container container-small">
            <div class="comment">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     var disqus_config = function () {
                     this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                     };
                     */
                    (function () { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://naked-infusions-1.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                        powered by Disqus.</a></noscript>
            </div>
        </div>

        <div class="next-post">
            <?php
            $next_post = get_previous_post();
            ?>

            <?php
            if (get_adjacent_post(false, '', true)) { ?>

                <div class="bottom-banner-products back-img-top-align rect-outer"
                     style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image', $next_post), 'fullhd'); ?>);">

                    <?php previous_post_link('%link', '<h2 class="title-type-3 white">' . get_the_title($next_post) . '</h2>'); ?>

                </div>

            <?php } else { ?>
                <?php $first = new WP_Query('posts_per_page=1&order=DESC');
                $first->the_post(); ?>

                <div class="bottom-banner-products back-img-top-align rect-outer"
                     style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('image', $next_post), 'fullhd'); ?>);">

                    <?php echo '<a href="' . get_permalink() . '">' . '<h2 class="title-type-3 white">' . get_the_title($next_post) . '</h2>' . '</a>'; ?>

                </div>
                <?php wp_reset_postdata();
            }; ?>

        </div>

    </div>
</div>

<script id="dsq-count-scr" src="//naked-infusions-1.disqus.com/count.js" async></script>
</div>

<?php get_footer(); ?>
