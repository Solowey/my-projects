</div>
<!--close content class tag-->

<div class="footer-container__company">
    <footer class="page-footer" role="contentinfo">

        <div class="page-footer__inner page-container">

            <div class="footer-item-wrapper">
                <?php

                if (have_rows('footer_info', 'option')):

                    while (have_rows('footer_info', 'option')) : the_row(); ?>

                        <div class="page-footer__item">
                            <h3 class="info-title"><?php the_sub_field('title'); ?></h3>
                            <p class="phone"><?php the_sub_field('phone'); ?></p>
                            <p class="mobile"><?php the_sub_field('mobile'); ?></p>
                            <p class="fax"><?php the_sub_field('fax'); ?></p>
                            <a href="<?php the_sub_field('mail_to'); ?>" class="mailto">Send email</a>
                        </div>

                    <?php endwhile; else :endif; ?>

            </div>
            <div class="social">

                <?php

                if (have_rows('social_link', 'option')):

                    while (have_rows('social_link', 'option')) : the_row(); ?>

                        <a href="<?php the_sub_field('link_url'); ?>"><i
                                class="<?php the_sub_field('link_class') ?>"></i></a>

                    <?php endwhile; else :endif; ?>

            </div>

            <p class="copyright">&copy; <?php echo date("Y"); ?> All rights reserved.</p>
        </div>

    </footer>
</div>
</div>
<!--close wrapper class tag-->
<?php wp_footer(); ?>

</body>
</html>

