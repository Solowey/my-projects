<?php
/*
 * Template Name: Company
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">

    <div class="hero-company" style="background-image: url('<?php the_field('company_background') ?>')">

<div class="page-container">

    <div class="company-text">

        <h2 class="main-title"><?php the_title(); ?></h2>

        <div class="description"><?php the_field('company_description') ?></div>

    </div>

</div>

        <div class="after-section">
            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

        </div>

    </div>

</main>

<?php get_footer(); ?>
