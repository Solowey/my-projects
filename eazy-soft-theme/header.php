<!DOCTYPE html>
<html <?php language_attributes(); ?> id="html--hidden">
<head>
    <?php get_template_part('template-parts/head'); ?>
</head>

<body <?php body_class("page-body"); ?>>
<div class="wrapper" id="app">
    <div class="content">
        <header class="page-header">
            <div class="page-container">
                <div class="hot-tooltips">
                    <?php

                    $args = array(
                        'post_type' => 'careers',
                        'posts_per_page' => -1
                    );
                    $is_hot = false;
                    $query = new WP_Query($args);
                    $post_item = 100;
                    if ($query->have_posts()) : ?>


                        <?php while ($query->have_posts()):
                            $query->the_post(); ?>


                            <?php $hot = get_field('hot_position'); ?>

                            <?php if ($hot) : ?>


                        <?php endif; ?>


                            <?php $post_item = $post_item + 200;
                        endwhile; ?>


                        <?php wp_reset_query();
                    endif; ?>
                    <div class="hot-wrap">
                        <div class="hot">
                            <?php echo 'Hot' ?>
                        </div>
                    </div>
                </div>
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img class="logoImage"
                                                                                data-src-home="<?php the_field('logo', 'option'); ?>"
                                                                                data-src-sticky="<?php the_field('logo_ather', 'option'); ?>"
                                                                                src="

                                                                                         <?php

                                                                                if (is_page(6)) {

                                                                                    the_field('logo', 'option');


                                                                                } else the_field('logo_ather', 'option'); ?>">

                </a>

                <button id="toggle" class="menu__hamburger ">
                    <span class="span1 line"></span>
                    <span class="span2 line"></span>
                    <span class="span3 line"></span>
                </button>

                <nav class="main-nav" role="navigation">
                    <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'main-nav__list', 'walker' => new f5_walker_nav_menu(), 'container' => false)); ?>

                    <div class="social">
                        <?php

                        if (have_rows('social_button', 'option')):

                            while (have_rows('social_button', 'option')) : the_row(); ?>

                                <a href="<?php the_sub_field('social_url'); ?>" class="social__item"><i
                                        class="<?php the_sub_field('social_class') ?>"></i></a>


                            <?php endwhile; else :endif; ?>

                    </div>
                </nav>


            </div>
        </header>
