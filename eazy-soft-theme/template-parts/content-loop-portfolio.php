<a href="<?php echo get_permalink() ?>" class="hero-portfolio__link">
    <p class="link-overlay"></p>
    <?php the_post_thumbnail( 'content-portfolio' ); ?>
    <h4 class="hero-portfolio__title"><?php the_title(); ?></h4>
</a>
