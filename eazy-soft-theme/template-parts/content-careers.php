<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="hero-careers__inner">
        <div class="hero-careers__text">
            <h3 class="careers-title"><?php echo get_the_title(); ?></h3>

            <p class="hero-careers-info"><?php the_field('info'); ?></p>
        </div>

        <a href="<?php echo get_permalink() ?>" class="btn__apply">Learn more</a>

    </div>
</article>
