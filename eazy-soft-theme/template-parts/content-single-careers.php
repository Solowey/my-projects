<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="single-careers__inner">
        <div class="single-careers__text">
            <h3 class="careers-title"><?php echo get_the_title(); ?></h3>

            <p class="single-careers-info"><?php the_field('info'); ?></p>
        </div>

        <button class="btn__apply popupOpen">apply now</button>

    </div>
</article>
