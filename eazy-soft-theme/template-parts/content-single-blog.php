<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post__inner">
        <div class="post-image" >
            <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
        </div>
        <div class="post-description">
            <h3 class="single-post__title"><?php the_title(); ?></h3>
            <?php the_content(); ?>

            <div class="date">

                <?php echo get_the_date('M dS, Y'); ?>

            </div>
        </div>

    </div>

</article>
