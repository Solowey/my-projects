<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="portfolio-item">

        <div class="portfolio-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
        <div class="single-portfolio-text">
            <div class="portfolio__description"><?php the_field('description'); ?></div>
        </div>

    </div>

</article>
