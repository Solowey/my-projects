<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <a href="<?php echo get_permalink() ?>" class="post__inner">
        <div class="post-image">
            <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
        </div>
        <div class="post-description">

            <h3><?php the_title(); ?></h3>
            <p><?php echo the_excerpt()?></p>

            <div class="date">

                <p><?php echo get_the_date('M dS, Y'); ?></p>

            </div>
        </div>

    </a>

</article>
