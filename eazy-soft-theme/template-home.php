<?php
/*
 * Template Name: Home
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">

    <div class="hero" style="background-image: url('<?php the_field('hero_background') ?>')">
        <div class="overlay"></div>

        <div class="page-container home-slider" data-aos="fade-right">
            <?php

            if( have_rows('home_slider') ):

            while ( have_rows('home_slider') ) : the_row();?>
            <div class="hero-text">
                <h1 class="main-title"><?php the_sub_field('hero_title') ?></h1>
                <h4 class="main-subtitle"><?php the_sub_field('hero_subtitle') ?></h4>

            </div>
            <?php endwhile; else :endif; ?>


        </div>
        <div class="button" data-aos="fade-up">
            <a href="#quote" class="btn__get-quote" id="toQuote">GET A QUOTE</a>
        </div>

        <div class="hero-after-section">

            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 114.66"><defs></defs><title>after_hero</title><polygon class="cls-1" fill="#fff" points="0 0 0 113.66 1228.8 113.66 0 0"/><polygon class="cls-2" fill="#efefef" points="0 1 1228.8 114.66 1228.8 101.19 0 1"/></svg>

        </div>

    </div>

    <div class="services">

        <div class="page-container">

            <h2 class="section-title__gray"><?php the_field('services_title') ?></h2>

            <div class="services__inner centerX">
                <?php

                if( have_rows('services_item') ):

                    while ( have_rows('services_item') ) : the_row();?>

                <a href="<?php the_sub_field('services_item_link') ?>" class="services-item">

                    <div class="services-item__icon"><img src="<?php the_sub_field('services_item_image') ?>" alt=""></div>

                    <div class="services-item__text">

                        <h3 class="services-item__title"><?php the_sub_field('services_item_title') ?></h3>

                    </div>

                </a>

                    <?php endwhile; else :endif; ?>

            </div>
        </div>

    </div>

    <div class="process" style="background-image: url('<?php the_field('process_banner') ?>')">
        <div class="before-section"><svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 122.16"><defs></defs><title>Before_section</title><polygon fill="#fff" class="cls-1" points="0 0.67 0 0 1228.8 0 1228.8 122.16 0 0.67"/></svg>
            </svg>
        </div>
        <div class="overlay"></div>
        <div class="page-container">

            <h2 class="section-title__lite centerX"><?php the_field('process_title') ?></h2>

            <div class="process__inner centerX">

                <?php

                if( have_rows('process_item') ):

                while ( have_rows('process_item') ) : the_row();?>

                <div class="process-item">
                    <span class="counter"></span>
                    <h3 class="process-item__title"><?php the_sub_field('process_item_title') ?></h3>
                    <p class="process-item__description"><?php the_sub_field('process_item_description') ?></p>
                </div>
                <?php endwhile; else :endif; ?>
            </div>

        </div>
        <div class="after-section"><svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>
        </div>
    </div>

    <div class="reviews">

        <div class="page-container">

            <h2 class="section-title__gray"><?php the_field('reviews_title') ?></h2>
            <h4 class="section-subtitle"><?php the_field('reviews_subtitle') ?></h4>

            <div class="nav-container"></div>
            <div class="reviews__inner reviews-slider">

                <?php

                if( have_rows('reviews_item') ):

                while ( have_rows('reviews_item') ) : the_row();?>

                <div class="reviews-item">
                    <div class="reviews-item__image"><img src="<?php the_sub_field('item_photo') ?>" alt="" class="centerX"></div>
                    <div class="reviews-item__info">

                        <div class="reviews-item-name">
                            <p class="first-name"><?php the_sub_field('item_name') ?></p>
                            <p class="last-name"><?php the_sub_field('item_last_name') ?></p>
                        </div>
                        <p class="reviews-item__profession"><?php the_sub_field('item_profession') ?></p>
                        <img src="<?php the_sub_field('item_image') ?>" alt="" class="reviews-item__icon centerX">
                        <div class="reviews-item__description"><?php the_sub_field('item_description') ?></div>
                    </div>

                </div>

                <?php endwhile; else :endif; ?>

            </div>
        </div>
        <div class="after-section">

            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 122.16"><defs></defs><title>affter_section</title><polygon class="cls-1" fill="#fff" points="0 0 0 122.16 1228.8 122.16 0 0"/></svg>


        </div>
    </div>
    
    <div class="projects">
        
        <div class="page-container">
            
            <h2 class="section-title__gray"><?php the_field('project_title') ?></h2>
            
            <div class="projects__inner projects-slider">

                <?php

                if( have_rows('project_item') ):

                    while ( have_rows('project_item') ) : the_row();?>

                <div class="projects-item">

                    <img src="<?php the_sub_field('logo_image') ?>" alt="" class="projects-item__image centerX">
                    
                </div>

                <?php endwhile; else :endif; ?>

            </div>

        </div>
        
    </div>

    <div class="quote" id="quote" style="background-image: url('<?php the_field('quote_banner') ?>')">

        <div class="before-section"><svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 122.16"><defs></defs><title>Before_section</title><polygon fill="#fff" class="cls-1" points="0 0.67 0 0 1228.8 0 1228.8 122.16 0 0.67"/></svg>
        </div>

        <div class="page-container">

            <h2 class="section-title__lite centerX"><?php the_field('quote_title') ?></h2>
            <h4 class="section-subtitle centerX"><?php the_field('quote_description') ?></h4>

            <div class="quote__inner">

                <?php get_template_part('template-parts/content', 'contact-form'); ?>

            </div>

        </div>

<!--        <div class="after-section">-->
<!--            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>-->
<!---->
<!--        </div>-->

        <div class="prefooter-angle">

            <div class="after-section">
                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

            </div>

        </div>
    </div>


</main>

<?php get_footer(); ?>
