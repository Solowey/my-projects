<?php
/**
 * theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme
 */

if ( ! function_exists( 'theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function theme_setup() {
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails', array( 'post', 'blog', 'portfolio','page' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function theme_scripts() {
	// Styles
	//wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
	wp_enqueue_style( 'theme-main-style', get_template_directory_uri() . '/dist/css/bundle.css' );
  // Scripts
	wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/dist/js/bundle.js', array("jquery"), false, true );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/**
 * Clear WP HEAD
 */
require get_template_directory() . '/include/clear-wp-head.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/include/customizer.php';

/**
 * Create custom post types and taxonomy.
 */
require get_template_directory() . '/include/setup.php';
/**
 * Php helpers
 */
require get_template_directory() . "/helpers/index.php";

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',

    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));
}


// Register Custom Post Type --- Portfolio

function create_post_type()
{
    register_post_type('portfolio',
        array(
            'supports' => array('title', 'thumbnail', 'author'),
            'labels' => array(
                'name' => __('Portfolio'),
                'singular_name' => __('Portfolio')
            ),
            'public' => true,
            'menu_position' => 2,
            'rewrite' => array('slug' => 'portfolio'),
            'thumbnail' => true,
            'taxonomies'          => array( 'category' ),
        )
    );

    register_post_type('blog',
        array(
            'supports' => array('title', 'excerpt', 'thumbnail', 'author', 'editor'),
            'labels' => array(
                'name' => __('Blog'),
                'singular_name' => __('Blog')
            ),
            'public' => true,
            'menu_position' => 2,
            'rewrite' => array('slug' => 'blog'),
            'thumbnail' => true,
            'taxonomies'          => array( 'category' ),
        )
    );

    register_post_type('careers',
        array(
            'supports' => array('title'),
            'labels' => array(
                'name' => __('Careers'),
                'singular_name' => __('Careers')
            ),
            'public' => true,
            'menu_position' => 3,
            'rewrite' => array('slug' => 'careers'),
        )
    );
}

add_action('init', 'create_post_type');

add_action('wp_ajax_nopriv_load_more', 'load_more');
add_action('wp_ajax_load_more', 'load_more');

function load_more () {
    $e_pages = $_POST['pageNum'] ? $_POST['pageNum'] : 1;

    $args = array(
        'post_type'      => 'careers',
        'paged'          => $e_pages,
        'posts_per_page' => 3
    );

    $query = new WP_Query($args);
    if( $query->have_posts() ) : ?>
        <div></div>
        <?php wp_reset_query();
    endif;

    die();
}

add_action('wp_ajax_nopriv_load_mores', 'load_mores');
add_action('wp_ajax_load_mores', 'load_mores');

function load_mores () {
    $e_pages = $_POST['pageNum'] ? $_POST['pageNum'] : 1;

    $args = array(
        'post_type'      => 'blog',
        'paged'          => $e_pages,
        'posts_per_page' => 6
    );

    $the_query = new WP_Query($args);
    if( $the_query->have_posts() ) : ?>
        <?php  while( $the_query->have_posts() ): $the_query -> the_post();
            get_template_part( 'template-parts/content', 'blog');
        endwhile; ?>
        <?php if (  $the_query->max_num_pages > 1 && $e_pages !== $the_query->max_num_pages) { ;?>
            <div class="load-more-blog-wrp">
                <button class="btn btn--icon btn--dark btn-load-more" id="load-more" data-max-page="<?php echo $the_query->max_num_pages ?>" data-current-page="<?php echo $e_pages + 1; ?>">
                </button>
            </div>
        <?php } ?>
        <?php wp_reset_query();
    endif;

    die();
}

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

class f5_walker_nav_menu extends Walker_Nav_Menu
{

// add classes to ul sub-menus
    function start_lvl(&$output, $depth = 0)
    {
        // depth dependent classes
        $indent = ($depth > 0 ? str_repeat("\t", $depth) : ''); // code indent
        $display_depth = ($depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu' . $display_depth,
            ($display_depth % 2 ? 'menu-odd' : 'menu-even'),
            ($display_depth >= 2 ? 'sub-sub-menu' . $display_depth : ''),
            'menu-depth-' . $display_depth
        );

        $class_names = implode(' ', $classes);

        // build html
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
}
function my_walker_nav_menu_start_el($item_output, $item, $depth, $args) {
    $classes = 'link' .$depth;
    $item_output = preg_replace(

        '/<a /', '<a class="'.$classes.'"', $item_output, 1);

    return $item_output; }

add_filter('walker_nav_menu_start_el', 'my_walker_nav_menu_start_el', 10, 4);
