<?php get_header(); ?>

<main id="main" class="site-main" role="main">

    <?php while (have_posts()) : the_post(); ?>
        <div class="single-post-section" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">

            <div class="overlay"></div>

            <div class="hero-after-section">

                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 114.66"><defs></defs><title>after_hero</title><polygon class="cls-1" fill="#fff" points="0 0 0 113.66 1228.8 113.66 0 0"/><polygon class="cls-2" fill="#efefef" points="0 1 1228.8 114.66 1228.8 101.19 0 1"/></svg>

            </div>

        </div>
        <div class="single-blog-section">
            <div class="page-container single-blog__inner">
                <h2 class="main-title"><?php echo get_the_title(20); ?></h2>
                <?php get_template_part('template-parts/content', 'single-blog'); ?>
            </div>

            <div class="after-section">
                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

            </div>

        </div>
    <?php endwhile; ?>

</main>

<?php get_footer(''); ?>
