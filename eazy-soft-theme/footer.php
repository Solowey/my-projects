</div>
<!--close content class tag-->
<footer class="page-footer" role="contentinfo">

    <div class="page-footer__inner page-container">

        <div class="footer-item-wrapper">
            <?php

            if (have_rows('footer_info', 'option')):

                while (have_rows('footer_info', 'option')) : the_row(); ?>

                    <div class="page-footer__item">

                        <div class="page-footer__info">

                            <h3 class="info-title"><?php the_sub_field('country'); ?></h3>
                            <div class="city-row">
                                <?php
                                if (have_rows('city', 'option')):

                                    while (have_rows('city', 'option')) : the_row();
                                        $link = get_sub_field('mail_to');
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];

                                ?>
                                        <div class="city-info">
                                            <p class="city_name"><?php the_sub_field('city_name'); ?></p>
                                            <p class="address"><?php the_sub_field('address'); ?></p>
                                            <p class="phone"><?php the_sub_field('phone'); ?></p>
                                            <p class="mobile"><?php the_sub_field('mobile'); ?></p>
                                            <p class="fax"><?php the_sub_field('fax'); ?></p>
                                            <a href="mailto:<?php echo esc_url($link_url); ?>" class="mailto"><?php echo esc_html($link_title); ?></a>
                                        </div>
                                    <?php endwhile; else :endif; ?>
                            </div>

                        </div>


                    </div>

                <?php endwhile; else :endif; ?>
            <div class="social-container">
                <a href="https://clutch.co/profile/eazysoft">
                    <img src="https://clutch.co/badges/2018/2017_We_Deliver_White.png" style="height: 125px; width: 125px;">
                </a>
            </div>
        </div>

        <div class="social">

            <?php

            if (have_rows('social_link', 'option')):

                while (have_rows('social_link', 'option')) : the_row(); ?>

                    <a href="<?php the_sub_field('link_url'); ?>"><i
                            class="<?php the_sub_field('link_class') ?>"></i></a>

                <?php endwhile; else :endif; ?>

        </div>

        <div class="privacy-info">
            <p class="copyright"> © Copyright <?php echo date("Y"); ?> | All rights reserved.</p>
            <div class="privacy-info-page">
                <?php

                if (have_rows('privacy_info', 'option')):

                    while (have_rows('privacy_info', 'option')) : the_row();

                        $link = get_sub_field('page_link');
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        ?>

                        <a href="<?php echo esc_url($link_url); ?>"
                           class="page-link"><?php echo esc_html($link_title); ?></a>

                    <?php endwhile; ?>

                <?php endif; ?>
            </div>
        </div>

        <a href="#app" class="button-up" id="buttonUp"><img
                src="<?php echo get_template_directory_uri() . '/dist/icons/arrow_up.png' ?>" alt=""></a>

    </div>

</footer>
</div>
<!--close wrapper class tag-->
<?php wp_footer(); ?>

<!-- Start of HubSpot Embed Code -->

<script type="text/javascript" id="hs-script-loader" async defer
        src="//js.hs-scripts.com/5567724.js">

</script>

<!-- End of HubSpot Embed Code -->
</body>
</html>

