<?php
/*
 * Template Name: Portfolio
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">

    <div class="hero-portfolio" style="background-image: url('<?php the_field('hero_background') ?>')">
        <div class="overlay"></div>
        <div class="page-container">
            <h1 class="main-title"><?php the_title(); ?></h1>

            <div class="control-container"></div>
            <div class="hero-portfolio__inner portfolio-slider centerX">

                <?php
                $args = array(
                    'post_type' => 'portfolio',
                    'posts_per_page' => -1,
                );

                $the_query = new WP_Query($args);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>


                    <?php get_template_part('template-parts/content', 'loop-portfolio'); ?>

                <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php endif; ?>

            </div>

            <button id="portfolio-show" class="centerX"></button>
        </div>

        <div class="after-section">
            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

        </div>

    </div>

</main>

<?php get_footer(); ?>
