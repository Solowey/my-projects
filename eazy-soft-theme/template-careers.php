<?php
/*
 * Template Name: Careers
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">

    <div class="hero-careers" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')">
        <div class="overlay"></div>
        <div class="page-container">
            <h2 class="main-title"> <?php the_title() ?></h2>
        </div>

        <div class="page-container">

            <div class="item-wrap centerX">
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                    'post_type' => 'careers',
                    'paged' => $paged,
                    'posts_per_page' => 3
                );

                $query = new WP_Query($args);
                $post_item = 100;
                if ($query->have_posts()) : ?>


                    <?php while ($query->have_posts()): $query->the_post(); ?>


                        <?php get_template_part('template-parts/content', 'careers'); ?>


                        <?php $post_item = $post_item + 200; endwhile; ?>

                    <?php if ($query->max_num_pages > 1) { ?>
                        <div class="load-more-blog-wrp">
                            <button class="btn btn--icon btn--dark btn-load-more centerX" id="load-more"
                                    data-current-page="<?php echo $paged; ?>"
                                    data-max-page="<?php echo $query->max_num_pages ?>">
                            </button>
                        </div>
                    <?php } ?>


                    <?php wp_reset_query(); endif; ?>

            </div>
        </div>

        <div class="after-section">
            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

        </div>

    </div>

</main>

<?php get_footer(); ?>

