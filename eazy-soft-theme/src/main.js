// import styles
import '@scss/main.scss';
import '@lib/scss/base.lib.scss';

import 'slick-carousel/slick/slick.min';
import AOS from 'aos';
import '@fortawesome/fontawesome-free/js/all.min';
// require main js file
require("./framework/app");
require('@js/index');

AOS.init({
    offset: 200,
    duration: 1000,
    once: true
});
