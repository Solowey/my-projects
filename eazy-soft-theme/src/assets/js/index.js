jQuery(function($) {
    $('.menu-item-has-children .link0').on('click', function(e) {
        e.preventDefault();
    });
    // slick slider
    if(window.matchMedia('(min-width: 480px)').matches) {
        $('.projects-slider').on('init', function(event, slick) {
            let $items = slick.$dots.find('li');
            $items.addClass('circle');
            $items.find('button').remove();
        });
    }

    $('.home-slider').slick({
        swipeToSlide: true,
        speed: 1000,
        autoplay: 0,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: false,
        swipe: true,
        touchMove: true,
        infinite: true,
        fade: true,
        cssEase: 'linear'
    });

    $('.reviews-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: "<span class='slick-prev'><i class='fa fa-chevron-left'></i></span>",
        nextArrow: "<span class='slick-next'><i class='fa fa-chevron-right'></i></span>",
        appendArrows: $('.nav-container'),
        responsive:
            [ {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            } ]
    });

    $('.projects-slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: 1000,
        responsive:
            [ {
                breakpoint: 481,
                settings: {
                    slidesToShow: 2,
                    dots: false,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 376,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            } ]
    });
    $(window).on('load resize', function() {
        if(window.matchMedia('(max-width: 993px)').matches) {
            $('.portfolio-slider').slick({
                dots: false,
                slidesToShow: 2,
                rows: 2,
                infinite: true,
                prevArrow: "<span class='slick-prev'></span>",
                nextArrow: "<span class='slick-next'></span>",
                appendArrows: $('.control-container'),
                responsive: [
                    {
                        breakpoint: 701,
                        settings: { slidesToShow: 1 }
                    }
                ]
            });
        }
    });

    //Sticky-header
    $(window).load(function() {
        if($(window).width() > 0) {
            $(window).scroll(function() {
                let headerLogo = $('.home .logoImage').attr('data-src-home'),
                    stickyHeaderLogo = $('.home .logoImage').attr('data-src-sticky'),
                    stickyLogo = $('.home .logoImage'),
                    $sticky = $('.page-header'),
                    scroll = $(window).scrollTop();
                if(scroll < 65) {
                    $sticky.removeClass('sticky-header');
                    stickyLogo.attr('src', headerLogo);
                }
                if(scroll >= 65) {
                    $sticky.addClass('sticky-header');
                    stickyLogo.attr('src', stickyHeaderLogo);
                }
            });
        }
    });
    $(window).load(function() {
        let headerLogo = $('.home .logoImage').attr('data-src-home'),
            stickyHeaderLogo = $('.home .logoImage').attr('data-src-sticky'),
            stickyLogo = $('.home .logoImage'),
            $sticky = $('.page-header');
        if($('.page-header').offset().top < 65) {
            $sticky.removeClass('sticky-header');
            stickyLogo.attr('src', headerLogo);
        } else {
            $sticky.addClass('sticky-header');
            stickyLogo.attr('src', stickyHeaderLogo);
        }
    });
    //Careers popup
    $('.popupOpen').on('click', function H() {
        $('.careers-popup').fadeIn(500);
    });
    //Close
    $('#js-popup-close').on('click', function H() {
        $('.careers-popup').fadeOut(500);
    });

    //ajax load careers
    let myajax = `${window.location.protocol}//${window.location.host}/` + `wp-admin/admin-ajax.php`,
        dataPost = {
            pageNum: 1,
            action: 'load_more'
        };

    $('.item-wrap').on('click', '.btn-load-more', function H() {
        let _self = $(this),
            currentPage = parseInt(_self.data('current-page')),
            maxPage = parseInt(_self.data('max-page'));
        if(!dataPost.pageNum) {
            dataPost.pageNum = 1;
            dataPost.pageNum = dataPost.pageNum + 1;
        } else {
            dataPost.pageNum = dataPost.pageNum + 1;
        }

        $.ajax({
            url: myajax,
            data: dataPost,
            type: 'POST',
            success(data) {
                if(data) {
                    _self.parents('.load-more-blog-wrp').before(data);
                    _self.parents('.load-more-blog-wrp').remove();
                    currentPage = currentPage++;

                    if(currentPage === maxPage) _self.parents('.load-more-blog-wrp').remove();
                } else {
                    // console.log('dwqdqw');
                    _self.parents('.load-more-blog-wrp').remove();
                }
            }
        });
    });

    //ajax load post
    let myajaxPost = `${window.location.protocol}//${window.location.host}/` + `wp-admin/admin-ajax.php`,
        dataPosts = {
            pageNum: 1,
            action: 'load_mores'
        };

    $('.post-container').on('click', '.btn-load-more', function H() {
        let _self = $(this),
            currentPage = parseInt(_self.data('current-page')),
            maxPage = parseInt(_self.data('max-page'));
        if(!dataPosts.pageNum) {
            dataPosts.pageNum = 1;
            dataPosts.pageNum = dataPosts.pageNum + 1;
        } else {
            dataPosts.pageNum = dataPosts.pageNum + 1;
        }

        $.ajax({
            url: myajaxPost,
            data: dataPosts,
            type: 'POST',
            success(data) {
                if(data) {
                    _self.parents('.load-more-blog-wrp').before(data);
                    _self.parents('.load-more-blog-wrp').remove();
                    currentPage = currentPage++;

                    if(currentPage === maxPage) _self.parents('.load-more-blog-wrp').remove();
                } else {
                    // console.log('dwqdqw');
                    _self.parents('.load-more-blog-wrp').remove();
                }
            }

        });
    });

    $('#portfolio-show').on('click', function() {
        $('.hero-portfolio__link').addClass('show-item');
    });

    if($("input").is("#uploadBtn")) {
        document.getElementById('uploadBtn').onchange = function() {
            $('.attached-file-name').text(this.files.item(0).name);
        };
    }
    let $buttonUp = $("#buttonUp"),
        buttonQuote = $('#toQuote');

    $buttonUp.click(function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 800);
    });

    buttonQuote.click(function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: $(buttonQuote.attr('href')).offset().top }, 800);
    });
});


let hamburgerButton = document.getElementById("toggle"),
    menuHamburger = document.getElementById("toggle"),
    htmlHide = document.getElementById("html--hidden");

hamburgerButton.addEventListener("click", function() {
    menuHamburger.classList.toggle("menu__show");
    htmlHide.classList.toggle("html--hidden");
});
