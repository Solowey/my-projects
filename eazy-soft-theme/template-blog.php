<?php
/*
 * Template Name: Blog
 */
?>

<?php get_header(); ?>

<main id="main" class="page-main" role="main">
    <?php
    $args = array(
        'post_type' => 'blog',
        'posts_per_page' => 1,
    );

    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

       $post_id = get_the_ID();

    endwhile;
        wp_reset_query(); ?>

    <?php endif; ?>

    <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>


            <div class="hero-blog" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id) ?>')">
            <div class="overlay"></div>

            <div class="page-container flex flex-col aligncenter justify-center">
                <h2 class="main-title"><?php the_title(); ?></h2>

                <div class="hero-inner">
                <h3 class="banner-title centerX"><?php echo get_the_title($post_id); ?></h3>
                <a href="<?php echo get_permalink($post_id) ?>" class="btn__get-quote">read more</a>
                </div>
            </div>

                <div class="hero-after-section">

                    <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 114.66"><defs></defs><title>after_hero</title><polygon class="cls-1" fill="#fff" points="0 0 0 113.66 1228.8 113.66 0 0"/><polygon class="cls-2" fill="#efefef" points="0 1 1228.8 114.66 1228.8 101.19 0 1"/></svg>

                </div>

        </div>


<div class="post-section">
    <div class="page-container post-container">



        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'blog',
            'paged' => $paged,
            'posts_per_page' => 6
        );

        $the_query = new WP_Query($args);
        $post_item = 100;
        if ($the_query->have_posts()) : ?>


            <?php while ($the_query->have_posts()): $the_query->the_post(); ?>


                <?php get_template_part('template-parts/content', 'blog'); ?>


                <?php $post_item = $post_item + 200; endwhile; ?>

            <?php if ($the_query->max_num_pages > 1) { ?>
                <div class="load-more-blog-wrp">
                    <button class="btn btn--icon btn--dark btn-load-more centerX" id="load-more"
                            data-current-page="<?php echo $paged; ?>"
                            data-max-page="<?php echo $the_query->max_num_pages ?>">
                    </button>
                </div>
            <?php } ?>


            <?php wp_reset_query(); endif; ?>



    </div>

    <div class="after-section">
        <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

    </div>

</div>
    <?php endwhile; wp_reset_query();?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
