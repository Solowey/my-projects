<?php
/*
 * Template Name: Privacy & Terms
 */
?>
<?php get_header(); ?>

<main id="main" class="page-main" role="main">


    <div class="page-container">

        <div class="info">

            <?php the_field('privacy_content');?>

        </div>



    </div>


    <div class="prefooter-angle">

        <div class="after-section">
            <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

        </div>

    </div>
</main>

<?php get_footer(); ?>
