<?php
/*
 * Template Name: Industries
 */
?>

<?php get_header(); ?>

    <main id="main" class="page-main" role="main">
        <div class="industries-hero" style="background-image: url('<?php the_field('industries_background') ?>')">

            <div class="page-container">

                <div class="industries-hero-text centerX">
                    <h1 class="main-title"><?php the_field('hero_title') ?></h1>
                    <h4 class="main-subtitle"><?php the_field('hero_subtitle') ?></h4>
                    <div class="description"><?php the_field('hero_description') ?></div>
                </div>

            </div>

            <div class="hero-after-section">

                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 114.66"><defs></defs><title>after_hero</title><polygon class="cls-1" fill="#fff" points="0 0 0 113.66 1228.8 113.66 0 0"/><polygon class="cls-2" fill="#efefef" points="0 1 1228.8 114.66 1228.8 101.19 0 1"/></svg>

            </div>

        </div>

        <div class="to-do">

            <div class="page-container">

                <div class="to-do__inner"><?php the_field('to_do_item') ?></div>

            </div>

        </div>

        <div class="industries-quote" style="background-image: url('<?php the_field('industries_quote_background') ?>')">

            <div class="before-section">
                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1228.8 122.16"><defs></defs><title>Before_section</title><polygon fill="#fff" class="cls-1" points="0 0.67 0 0 1228.8 0 1228.8 122.16 0 0.67"/></svg>
            </div>

            <div class="page-container">

                <h2 class="section-title__lite centerX"><?php the_field('industries-quote_title') ?></h2>
                <h4 class="section-subtitle centerX"><?php the_field('industries_quote_description') ?></h4>

                <div class="quote__inner">

                    <?php get_template_part('template-parts/content', 'contact-form'); ?>

                </div>

            </div>

            <div class="after-section">
                <svg id="Слой_1" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1221.33 120.66"><defs></defs><title>Affter_section_gray</title><polygon fill="#efefef" class="cls-1" points="0 0 0 120.66 1221.33 120.66 0 0"/></svg>

            </div>

        </div>

    </main>

<?php get_footer(); ?>
