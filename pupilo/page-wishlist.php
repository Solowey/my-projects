<?php
/*
 * Template Name: Wishlist
 */

get_header();
?>

<main class="whishlist-wrapper">

    <?php

    if (is_user_logged_in()) {
        $user_id = get_current_user_id();

        $current_wishlist = get_user_meta($user_id, 'wishlist', true);
        $current_wishlist = explode(',', $current_wishlist);
    } else {
        $current_wishlist = $_COOKIE['wishlist'];
        $current_wishlist = explode(',', $current_wishlist);
    }

    $loop = new WP_Query(array(
        'post_type' => 'product',
        'orderby' => 'date',
        'post__in' => $current_wishlist
    ));

    ?>
    <div class="title-wrapper">
        <a class="my-account-link" href="/">
            <svg viewBox="0 0 12 12" width="12" height="12" class="chevron-icon">
                <use xlink:href="#chevron-left"></use>
            </svg>Wishlist (<span class="wish-product-count"><?php echo $loop->found_posts; ?></span>)
        </a>
    </div>
    <div class="page-container">
        <div class="wish-list-breadcrumbs">
            <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
        </div>
        <h2 class="wish-title">My wishlist (<span class="wish-counter"><?php echo $loop->found_posts; ?></span>)</h2>
        <div class="row product-row">
            <?php while ($loop->have_posts()): $loop->the_post(); ?>

                <div class="product-item-mobile" data-prod-id="<?php echo the_ID(); ?>">
                    <div class="product-image-mobile">
                        <a href="<?php echo the_permalink(); ?>">
                            <img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
                        </a>
                    </div>
                    <div class="product-title-mobile">
                        <a href="<?php echo the_permalink(); ?>">
                            <p class="product-title-text"><?php echo the_title(); ?></p>
                        </a>
                    </div>
                    <div class="product-dots-mobile">
                        <button type="button" class="three-dots-button" data-id="<?php echo the_ID(); ?>">
                            <svg viewBox="0 0 24 24" width="24" height="24" class="product-three-dots">
                                <use xlink:href="#three-dots"></use>
                            </svg>
                        </button>
                    </div>
                </div>

                <?php get_template_part('template-parts/content', 'category'); ?>

            <?php endwhile ?>

        </div>
    </div>
</main>
<?php get_template_part('template-parts/subscribe') ?>
<div class="wishlist-delete-mobile-wrapper">
    <button type="button" class="remove-button-close"></button>
    <button type="button" class="remove-from-wishlist">Remove</button>
</div>
<div class="wishlist-overlay"></div>
<?php get_footer(); ?>
