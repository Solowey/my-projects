<?php
/*
 * Template Name: FAQ Template
 */
?>

<?php get_header(); ?>
<main class="faq-main">
    <div class="page-container">
        <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
        <h1>Frequently asked questions</h1>

        <div class="filter_buttons" id="filter_buttons">
            <div class="filter_button filter_button-active" data-filter='all'>All</div>
            <?php while (have_rows('faq_filter')): the_row(); ?>
                <div class="filter_button" data-filter="<?php echo the_sub_field("faq_filter_item") ?>">
                    <?php echo the_sub_field('faq_filter_item') ?>
                </div>
            <?php endwhile; ?>
        </div>


        <div class="faq-container">

            <?php while (have_rows('faq_accordion')): the_row(); ?>
                <div class="acc-wrap <?php echo get_sub_field('faq_accordion_heading') ?>">
                    <h2><?php echo the_sub_field('faq_accordion_heading') ?></h2>
                    <?php while (have_rows('faq_accordion_info')): the_row(); ?>

                        <div class="accordion-item">
                            <div class="faq_accordion">
                                <p class="panel_heading">
                                    <?php echo the_sub_field('faq_accordion_info_title') ?>
                                </p>
                            </div>

                            <div class="faq_panel">
                                <p class="panel_info">
                                    <?php echo the_sub_field('faq_accordion_info_item') ?>
                                </p>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
            <?php endwhile; ?>
        </div>

</main>
<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>

<?php get_template_part('template-parts/subscribe') ?>

<?php get_footer(); ?>
