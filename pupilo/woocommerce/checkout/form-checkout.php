<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}

?>


<div class="checkout-step checkout-step1-wrapper <?php if (is_user_logged_in()) echo 'logged'; ?>">
    <form name="checkout" method="post" class="checkout woocommerce-checkout"
          action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

        <div class="step1-header checkout-step-header">
            <div class="number">
                <span>1</span>
                <svg viewBox="0 0 18 13" width="18" height="13" class="">
                    <use xlink:href="#step-check"></use>
                </svg>
            </div>
            <p class="text">Choose delivery method</p>
        </div>
        <div class="step1-wrapper step-wrapper">
            <div class="desktop-step1-main">
                <div class="select-country">
                    <label>Country</label>
                    <select class="choose-country" autocomplete="off">
                        <?php
                        $countries = new WC_Countries;
                        $countries_list = $countries->get_shipping_countries();

                        foreach ($countries_list as $key => $country) {
                            ?>
                            <option value="<?php echo $key; ?>"><?php echo $country; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="shipping-tabs-buttons">
                    <div class="shipping-tab shipping-tab1">
                        <p class="text">Delivery</p>
                    </div>
                    <div class="shipping-tab not-active shipping-tab2">
                        <p class="text">Pickup<span class="shipping-free-label">Free</span></p>
                    </div>
                </div>
                <div class="shipping-tabs">
                    <div class="tab tab1">
                        <div class="checkout-select-shipping">
                            <div class="omniva-shipping shipping-method-wrapper">
                                <div class="shipping-main">
                                    <div class="shipping-logo">
                                        <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                            <use xlink:href="#omniva"></use>
                                        </svg>
                                    </div>
                                    <div class="shipping-name"><p class="text">Omniva</p></div>
                                    <div class="shipping-cost"><p class="text">Free</p></div>
                                </div>
                                <div class="shipping-addresses display-none">
                                    <?php
                                    $user_id = get_current_user_id();
                                    $addresses = get_user_meta($user_id, 'wc_address_book', false);
                                    var_dump($addresses);
                                    if (is_user_logged_in()) {
                                        foreach ($addresses as $address) {
                                            ?>
                                            <div class="shipping-address-item">
                                                <label
                                                    for="omniva-<?php echo get_user_meta($user_id, $address . '_address_nickname', true) ?>"
                                                    class="shipping-label" data-address="<?php echo $address; ?>">
                                                    <input type="radio"
                                                           id="omniva-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                           name="address" class="omniva-radio">
                                                    <div class="shipping-info">
                                                        <div class="shipping-wrapper">
                                                            <div class="shipping-header">
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?></p>
                                                                <button type="button" data-bs-toggle="modal"
                                                                        data-bs-target="#editEstonianAddress"
                                                                        class="edit-address-button"
                                                                        data-address-id="<?php echo $address; ?>"
                                                                        data-address-name="<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                                        data-email="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                        data-first-name="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                        data-last-name="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                        data-phone="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                >Edit
                                                                </button>
                                                            </div>
                                                            <div class="shipping-body-preview">
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_email', true); ?></p>
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_first_name', true) . ' ' . get_user_meta($user_id, $address . '_last_name', true); ?></p>
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_phone', true); ?></p>
                                                            </div>
                                                            <div class="shipping-body display-none">
                                                                <div class="input-wrapper">
                                                                    <label>Email</label>
                                                                    <input type="email" autocomplete="off" placeholder="Enter Email"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                           disabled class="email">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label
                                                                        for="location-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>">Location</label>
                                                                    <select
                                                                        id="location-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>">
                                                                    </select>
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>First name</label>
                                                                    <input type="text" placeholder="Enter your First name"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                           disabled class="first-name">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>Last name</label>
                                                                    <input type="text" placeholder="Enter your Last name"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                           disabled class="last-name">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>Phone</label>
                                                                    <input type="tel" placeholder="Enter phone number"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                           disabled class="tel">
                                                                </div>
                                                                <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                                                <p class="show-notes">Add notes to your order?</p>
                                                                <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                                                <p class="hide-notes">Hide notes</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="add-new-wrapper">
                                            <button class="add-new-button omniva-add-new-button" data-bs-toggle="modal" type="button"
                                                    data-bs-target="#addEstonianAddress">Add new address
                                            </button>
                                        </div>
                                        <?php
                                    } else { ?>
                                        <div class="shipping-body unregistered-address">
                                            <div class="input-wrapper">
                                                <label>Email</label>
                                                <input type="email" class="email" autocomplete="off" placeholder="Enter Email">
                                            </div>
                                            <div class="input-wrapper">
                                                <label for="location-omniva">Location</label>
                                                <select id="location-omniva"></select>
                                            </div>
                                            <div class="input-wrapper">
                                                <label>First name</label>
                                                <input type="text" class="first-name" placeholder="Enter your First name">
                                            </div>
                                            <div class="input-wrapper">
                                                <label>Last name</label>
                                                <input type="text" class="last-name" placeholder="Enter your Last name">
                                            </div>
                                            <div class="input-wrapper">
                                                <label>Phone</label>
                                                <input type="text" class="tel" placeholder="Enter phone number">
                                            </div>
                                            <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                            <p class="show-notes">Add notes to your order?</p>
                                            <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                            <p class="hide-notes">Hide notes</p>
                                        </div>
                                        <?php
                                    }

                                    ?>
                                </div>
                            </div>
                            <div class="smartpost-shipping shipping-method-wrapper">
                                <div class="shipping-main">
                                    <div class="shipping-logo">
                                        <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                            <use xlink:href="#smartpost"></use>
                                        </svg>
                                    </div>
                                    <div class="shipping-name"><p class="text">Smartpost</p></div>
                                    <div class="shipping-cost"><p class="text">Free</p></div>
                                </div>
                                <div class="shipping-addresses display-none">
                                    <?php
                                    $user_id = get_current_user_id();
                                    $addresses = get_user_meta($user_id, 'wc_address_book', false);

                                    if (is_user_logged_in()) {
                                        foreach ($addresses as $address) {
                                            ?>
                                            <div class="shipping-address-item">
                                                <label
                                                    for="smartpost-<?php echo get_user_meta($user_id, $address . '_address_nickname', true) ?>"
                                                    class="shipping-label" data-address="<?php echo $address; ?>">
                                                    <input type="radio"
                                                           id="smartpost-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                           name="address" class="omniva-radio">
                                                    <div class="shipping-info">
                                                        <div class="shipping-wrapper">
                                                            <div class="shipping-header">
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?></p>
                                                                <button type="button" data-bs-toggle="modal"
                                                                        data-bs-target="#editEstonianAddress"
                                                                        class="edit-address-button"
                                                                        data-address-id="<?php echo $address; ?>"
                                                                        data-address-name="<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                                        data-email="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                        data-first-name="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                        data-last-name="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                        data-phone="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                >Edit
                                                                </button>
                                                            </div>
                                                            <div class="shipping-body-preview">
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_email', true); ?></p>
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_first_name', true) . ' ' . get_user_meta($user_id, $address . '_last_name', true); ?></p>
                                                                <p class="text"><?php echo get_user_meta($user_id, $address . '_phone', true); ?></p>
                                                            </div>
                                                            <div class="shipping-body display-none">
                                                                <div class="input-wrapper">
                                                                    <label>Email</label>
                                                                    <input type="email" autocomplete="off"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                           disabled class="email" placeholder="Enter Email">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label
                                                                        for="location-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>">Location</label>
                                                                    <select
                                                                        id="location-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>">

                                                                    </select>
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>First name</label>
                                                                    <input type="text" placeholder="Enter your First name"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                           disabled class="first-name">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>Last name</label>
                                                                    <input type="text" placeholder="Enter your Last name"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                           disabled class="last-name">
                                                                </div>
                                                                <div class="input-wrapper">
                                                                    <label>Phone</label>
                                                                    <input type="tel" placeholder="Enter phone number"
                                                                           value="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                           disabled class="tel">
                                                                </div>
                                                                <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                                                <p class="show-notes">Add notes to your order?</p>
                                                                <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                                                <p class="hide-notes">Hide notes</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="add-new-wrapper">
                                            <button class="add-new-button smartpost-add-new-button" data-bs-toggle="modal" type="button"
                                                    data-bs-target="#addEstonianAddress">Add new address
                                            </button>
                                        </div>
                                        <?php
                                    } else { ?>
                                        <div class="shipping-body unregistered-address">
                                            <div class="input-wrapper">
                                                <label>Email</label>
                                                <input type="email" class="email" autocomplete="off" placeholder="Enter Email">
                                            </div>
                                            <div class="input-wrapper">
                                                <label for="location-smartpost">Location</label>
                                                <select id="location-smartpost"></select>
                                            </div>
                                            <div class="input-wrapper">
                                                <label>First name</label>
                                                <input type="text" class="first-name" placeholder="Enter your First name">
                                            </div>
                                            <div class="input-wrapper">
                                                <label>Last name</label>
                                                <input type="text" class="last-name" placeholder="Enter your Last name">
                                            </div>
                                            <div class="input-wrapper">
                                                <label>Phone</label>
                                                <input type="text" class="tel" placeholder="Enter phone number">
                                            </div>
                                            <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                            <p class="show-notes">Add notes to your order?</p>
                                            <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                            <p class="hide-notes">Hide notes</p>
                                        </div>
                                        <?php
                                    }

                                    ?>
                                </div>
                            </div>
                            <div class="dhl-shipping shipping-method-wrapper display-none">
                                <div class="shipping-main">
                                    <div class="shipping-logo">
                                        <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                            <use xlink:href="#dhl"></use>
                                        </svg>
                                    </div>
                                    <div class="shipping-name"><p class="text">DHL</p></div>
                                    <div class="shipping-cost"><p class="text">Free</p></div>
                                </div>
                                <div class="shipping-addresses display-none">
                                    <div class="shipping-addresses display-none">
                                        <?php
                                        $user_id = get_current_user_id();
                                        $addresses = get_user_meta($user_id, 'wc_address_book', true);

                                        if (is_user_logged_in()) {
                                            foreach ($addresses as $address) {
                                                ?>
                                                <div class="shipping-address-item">
                                                    <label
                                                        for="dhl-<?php echo get_user_meta($user_id, $address . '_address_nickname', true) ?>"
                                                        class="shipping-label" data-address="<?php echo $address; ?>">
                                                        <input type="radio"
                                                               id="dhl-<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                               name="address" class="omniva-radio">
                                                        <div class="shipping-info">
                                                            <div class="shipping-wrapper">
                                                                <div class="shipping-header">
                                                                    <p class="text"><?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?></p>
                                                                    <button type="button" data-bs-toggle="modal"
                                                                            data-bs-target="#editEuropeanAddress"
                                                                            class="edit-address-button"
                                                                            data-address-id="<?php echo $address; ?>"
                                                                            data-address-name="<?php echo get_user_meta($user_id, $address . '_address_nickname', true); ?>"
                                                                            data-email="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                            data-first-name="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                            data-last-name="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                            data-phone="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                            data-city="<?php echo get_user_meta($user_id, $address . '_city', true); ?>"
                                                                            data-address="<?php echo get_user_meta($user_id, $address . '_address_1', true); ?>"
                                                                            data-postcode="<?php echo get_user_meta($user_id, $address . '_postcode', true); ?>"
                                                                    >Edit
                                                                    </button>
                                                                </div>
                                                                <div class="shipping-body-preview">
                                                                    <p class="text"><?php echo get_user_meta($user_id, $address . '_email', true); ?></p>
                                                                    <p class="text"><?php echo get_user_meta($user_id, $address . '_first_name', true) . ' ' . get_user_meta($user_id, $address . '_last_name', true); ?></p>
                                                                    <p class="text"><?php echo get_user_meta($user_id, $address . '_phone', true); ?></p>
                                                                </div>
                                                                <div class="shipping-body display-none">
                                                                    <div class="input-wrapper">
                                                                        <label>Email</label>
                                                                        <input type="email" autocomplete="off" placeholder="Enter Email"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_email', true); ?>"
                                                                               disabled class="email">
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>First name</label>
                                                                        <input type="text" placeholder="Enter your First name"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_first_name', true); ?>"
                                                                               disabled class="first-name">
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>Last name</label>
                                                                        <input type="text" placeholder="Enter your Last name"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_last_name', true); ?>"
                                                                               disabled class="last-name">
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>City</label>
                                                                        <input type="text" placeholder="Enter your city"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_city', true); ?>"
                                                                               disabled class="city">
                                                                    </div>
                                                                    <div class="input-wrapper dhl-input-spinner no-pointer">
                                                                        <div class="loadingio-spinner-dual-ring-mlu0dcv4kaf display-none">
                                                                            <div class="ldio-9xczbp2xegj">
                                                                                <div></div>
                                                                                <div>
                                                                                    <div></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <select class="dhl-rates-select"></select>
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>Address</label>
                                                                        <input type="text" placeholder="Enter your address"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_address_1', true); ?>"
                                                                               disabled class="address">
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>Postcode</label>
                                                                        <input type="text" placeholder="Enter your postcode"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_postcode', true); ?>"
                                                                               disabled class="postcode">
                                                                    </div>
                                                                    <div class="input-wrapper">
                                                                        <label>Phone</label>
                                                                        <input type="tel" placeholder="Enter phone number"
                                                                               value="<?php echo get_user_meta($user_id, $address . '_phone', true); ?>"
                                                                               disabled class="tel">
                                                                    </div>
                                                                    <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                                                    <p class="show-notes">Add notes to your order?</p>
                                                                    <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                                                    <p class="hide-notes">Hide notes</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="add-new-wrapper">
                                                <button class="add-new-button smartpost-add-new-button" data-bs-toggle="modal" type="button"
                                                        data-bs-target="#addEuropeanAddress">Add new address
                                                </button>
                                            </div>
                                            <?php
                                        } else { ?>
                                            <div class="shipping-body unregistered-address">
                                                <div class="input-wrapper">
                                                    <label>Email</label>
                                                    <input type="email" class="email" placeholder="Enter Email">
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>First name</label>
                                                    <input type="text" class="first-name" placeholder="Enter your First name">
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>Last name</label>
                                                    <input type="text" class="last-name" placeholder="Enter your Last name">
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>City</label>
                                                    <input type="text" class="city" placeholder="Enter your city">
                                                </div>
                                                <div class="input-wrapper dhl-input-spinner no-pointer">
                                                    <div class="loadingio-spinner-dual-ring-mlu0dcv4kaf display-none">
                                                        <div class="ldio-9xczbp2xegj">
                                                            <div></div>
                                                            <div>
                                                                <div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <select class="dhl-rates-select"></select>
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>Address</label>
                                                    <input type="text" class="address" placeholder="Enter your adress">
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>Postcode</label>
                                                    <input type="text" class="postcode" placeholder="Enter your postcode">
                                                </div>
                                                <div class="input-wrapper">
                                                    <label>Phone</label>
                                                    <input type="text" class="tel" placeholder="Enter phone number">
                                                </div>
                                                <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                                                <p class="show-notes">Add notes to your order?</p>
                                                <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                                                <p class="hide-notes">Hide notes</p>
                                            </div>
                                            <?php
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab tab2 display-none">
                        <div class="checkout-select-shipping">
                            <div class="local-pickup shipping-method-wrapper">
                                <div class="shipping-main">
                                    <div class="shipping-logo">
                                        <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                            <use xlink:href="#local-pickup"></use>
                                        </svg>
                                    </div>
                                    <div class="shipping-name"><p class="text">Vizit Pupilo office</p></div>
                                    <div class="shipping-cost"><p class="text">Free</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="desktop-step1-description">
                <div class="info-pay-delivery">
                    <div class="free-delivery-info">
                        <div class="info-image">
                            <svg width="58" height="43">
                                <use xlink:href="#delivery"></use>
                            </svg>
                        </div>
                        <div class="info-description">
                            <h6>Free Delivery</h6>
                            <p class="text">Congratulations, your order qualifies to free delivery and that is exactly what
                                you
                                get.</p>
                            <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                                <svg width="20" height="17">
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="free-returns-info">
                        <div class="info-image">
                            <svg width="50" height="50">
                                <use xlink:href="#free-return"></use>
                            </svg>
                        </div>
                        <div class="info-description">
                            <h6>Free Returns</h6>
                            <p class="text">If you order smth. you want to replace or change, no worries - all returns are
                                free
                                for 30 days.</p>
                            <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                                <svg width="20" height="17">
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="step1-desktop-continue-wrapper">
            <button class="next-step-button-desktop" disabled data-step="1">Continue</button>
        </div>
        <div class="shipping-summary display-none"></div>
        <div class="step2-header checkout-step-header not-active-step"
             data-payment="<?php echo get_user_meta($user_id, 'preferred_payment', true) ?>">
            <div class="number">
                <span>2</span>
                <svg viewBox="0 0 18 13" width="18" height="13" class="">
                    <use xlink:href="#step-check"></use>
                </svg>
            </div>
            <p class="text">Choose payment method</p>
        </div>
        <div class="step2-wrapper step-wrapper display-none">
            <div class="payment-method" data-payment-name="montonio">
                <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                    <use xlink:href="#credit-card"></use>
                </svg>
                <div class="payment-info">
                    <p class="heading">Montonio</p>
                    <p class="text">Visa, Mastercard</p>
                </div>
            </div>
            <div class="payment-method" data-payment-name="paypal">
                <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                    <use xlink:href="#paypal-method"></use>
                </svg>
                <div class="payment-info">
                    <p class="heading">Paypal</p>
                    <p class="text">Safe payment online</p>
                </div>
            </div>
            <div class="payment-method" data-payment-name="everypay">
                <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                    <use xlink:href="#credit-card"></use>
                </svg>
                <div class="payment-info">
                    <p class="heading">Everypay</p>
                    <p class="text">Visa, Mastercard</p>
                </div>
                <div class="add-card"></div>
            </div>
        </div>
        <div class="desktop-after-payment-wrapper display-none">
            <label class="custom-checkbox order-check-wrapper">
                <input type="checkbox" id="check-terms">
                <span class="text">I accept with&nbsp; <a href="/terms" class="terms-link">Terms & Conditions</a></span>
            </label>
            <label class="custom-checkbox order-check-wrapper">
                <input type="checkbox" id="check-email">
                <span class="text">Email me about hot items and savings</span>
            </label>
            <div class="step2-desktop-continue-wrapper">
                <button class="next-step-button-desktop" disabled data-step="1">Pay</button>
            </div>
        </div>
        <div class="payment-summary"></div>
        <div class="step3-header checkout-step-header not-active-step">
            <div class="number">
                <span>3</span>
                <svg viewBox="0 0 18 13" width="18" height="13" class="">
                    <use xlink:href="#step-check"></use>
                </svg>
            </div>
            <p class="text">Review & Pay</p>
        </div>
        <div class="step3-wrapper step-wrapper display-none">
            <?php

            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $product = $cart_item['data'];

                $product_id = $cart_item['product_id'];
                $quantity = $cart_item['quantity'];
                $price = WC()->cart->get_product_price($product);
                $subtotal = WC()->cart->get_product_subtotal($product, $cart_item['quantity']);
                $link = $product->get_permalink($cart_item);
//                // Anything related to $product, check $product tutorial
                $attributes = $product->get_attributes();
//                $whatever_attribute = $product->get_attribute( 'whatever' );
                $color_attribute = $product->get_attribute('pa_color');
                $size_attribute = $product->get_attribute('pa_size');
                $whatever_attribute_tax = $product->get_attribute('pa_whatever');
                $any_attribute = $cart_item['variation']['attribute_whatever'];
                $meta = wc_get_formatted_cart_item_data($cart_item);

                $lenses_configuration = $cart_item['lenses_configuration'];
                $prescriptions = $cart_item['prescriptions'];

                if ($lenses_configuration) {
                    $lens = str_replace("\\", "", $lenses_configuration);
                    $lens = substr($lens, 2);
                    $lens = substr($lens, 0, -2);
                    $lens = explode('},{', $lens);

                    $new_lens = [];

                    foreach ($lens as $lens_item) {
                        $lens_item = substr($lens_item, 1);
                        $item_array = explode('","', $lens_item);

                        $new_lens_item = [];

                        foreach ($item_array as $single_item) {
                            $single_item = str_replace("\"", "", $single_item);
                            $single_item = explode(':', $single_item);
                            $key = $single_item[0];
                            $value = $single_item[1];
                            $new_lens_item[$key] = $value;
                        }

                        array_push($new_lens, $new_lens_item);
                    }

                }

                $pre = str_replace("\\", "", $prescriptions);
                $pre = substr($pre, 2);
                $pre = substr($pre, 0, -6);
                $pre = explode(',', $pre);

                $new_pre = [];

                foreach ($pre as $pre_item) {
                    $pre_item = str_replace("\"", "", $pre_item);
                    $item = explode(":", $pre_item);
                    $key = $item[0];
                    $value = $item[1];
                    $new_pre[$key] = $value;
                }

                ?>
                <div class="checkout-item-wrapper">
                    <div class="item-img-wrapper">
                        <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" alt="">
                    </div>
                    <div class="item-main-info-wrapper">
                        <p class="text-heading"><?php echo get_the_title($product_id); ?></p>
                        <p class="text">Color: <span><?php echo $color_attribute; ?></span>&nbsp;&nbsp;&nbsp;Size:
                            <span><?php echo $size_attribute; ?></span></p>
                        <div class="info-quantity-price">
                            <p class="text1"><?php echo $quantity; ?>pcs</p>
                            <p class="text2"><?php echo $subtotal; ?></p>
                        </div>
                    </div>
                    <?php
                    if ($prescriptions) {
                        ?>
                        <div class="item-pre-info">
                            <div class="lens-config">
                                <p class="order-item-pre-heading">
                                    Lens: <span><?php echo $new_lens[0]['fullName']; ?></span>
                                    <svg viewBox="0 0 12 12" width="12" height="12" class="order-product-open-lenses">
                                        <use xlink:href="#chevron-left"></use>
                                    </svg>
                                </p>
                                <div class="lenses-configuration">
                                    <table class="prescription-table-item">
                                        <tr>
                                            <td></td>
                                            <td>SPH</td>
                                            <td>CYl</td>
                                            <td>Axis</td>
                                            <td>ADD</td>
                                            <td>PD</td>
                                        </tr>
                                        <tr>
                                            <td>OD<span>(R)</span></td>
                                            <td><?php echo $new_pre['odSph']; ?></td>
                                            <td><?php echo $new_pre['odCyl']; ?></td>
                                            <td><?php echo $new_pre['odAxis']; ?></td>
                                            <td><?php echo $new_pre['odAd']; ?></td>
                                            <td><?php echo $new_pre['pd1']; ?></td>
                                        </tr>
                                        <!--            --><?php //var_dump($new_pre);?>
                                        <tr>
                                            <td>OS<span>(L)</span></td>
                                            <td><?php echo $new_pre['osSph']; ?></td>
                                            <td><?php echo $new_pre['osCyl']; ?></td>
                                            <td><?php echo $new_pre['osAxis']; ?></td>
                                            <td><?php echo $new_pre['osAd']; ?></td>
                                            <td><?php echo $new_pre['pd2']; ?></td>
                                        </tr>
                                    </table>

                                    <?php
                                    foreach ($new_lens as $lens_step) {

                                        if ($lens_step['step'] !== 'step2') {
                                            ?>
                                            <p class="lenses-configuration-rows"><?php echo $lens_step['fullName']; ?></p>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }

            ?>
            <div class="subtotal-wrapper">
                <div class="subtotal">
                    <p class="text-heading">Subtotal (<?php echo WC()->cart->get_cart_contents_count(); ?> items)</p>
                    <p class="text-heading subtotal-field"><?php echo WC()->cart->subtotal; ?>€</p>
                </div>
                <div class="subtotal">
                    <p class="text">Taxes</p>
                    <p class="text taxes-field"><?php echo WC()->cart->get_taxes_total(); ?>€</p>
                </div>
                <div class="subtotal">
                    <p class="text">Delivery</p>
                    <p class="text delivery-field"><?php echo WC()->cart->get_cart_shipping_total(); ?></p>
                </div>
                <label class="custom-checkbox order-check-wrapper">
                    <input type="checkbox" id="check-terms-mob">
                    <span class="text">I accept with <a href="/terms">Terms & Conditions</a></span>
                </label>
                <label class="custom-checkbox order-check-wrapper">
                    <input type="checkbox" id="check-email">
                    <span class="text">Email me about hot items and savings</span>
                </label>
            </div>
        </div>

        <?php if ($checkout->get_checkout_fields()) : ?>

            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

            <div class="col2-set" id="customer_details">

                <div class="col-1">
                    <?php do_action('woocommerce_checkout_billing'); ?>
                </div>

                <div class="col-2">
                    <?php do_action('woocommerce_checkout_shipping'); ?>
                </div>
            </div>

            <?php do_action('woocommerce_checkout_after_customer_details'); ?>

        <?php endif; ?>

        <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

        <h3 id="order_review_heading"><?php esc_html_e('Your order', 'woocommerce'); ?></h3>

        <?php do_action('woocommerce_checkout_before_order_review'); ?>

        <div id="order_review" class="woocommerce-checkout-review-order">
            <?php do_action('woocommerce_checkout_order_review'); ?>
        </div>

        <?php do_action('woocommerce_checkout_after_order_review'); ?>

    </form>
</div>
<div class="checkout-right-desktop-wrapper">
<!--    <button id="dhl-button">download dhl</button>-->
    <div class="checkout-right-heading">
        <p class="text">Summary</p>
    </div>
    <?php
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        $product = $cart_item['data'];

        $product_id = $cart_item['product_id'];
        $quantity = $cart_item['quantity'];
        $price = WC()->cart->get_product_price($product);
        $subtotal = WC()->cart->get_product_subtotal($product, $cart_item['quantity']);
        $link = $product->get_permalink($cart_item);
//                // Anything related to $product, check $product tutorial
        $attributes = $product->get_attributes();
//                $whatever_attribute = $product->get_attribute( 'whatever' );
        $color_attribute = $product->get_attribute('pa_color');
        $size_attribute = $product->get_attribute('pa_size');
        $whatever_attribute_tax = $product->get_attribute('pa_whatever');
        $any_attribute = $cart_item['variation']['attribute_whatever'];
        $meta = wc_get_formatted_cart_item_data($cart_item);

        $lenses_configuration = $cart_item['lenses_configuration'];
        $prescriptions = $cart_item['prescriptions'];

        if ($lenses_configuration) {
            $lens = str_replace("\\", "", $lenses_configuration);
            $lens = substr($lens, 2);
            $lens = substr($lens, 0, -2);
            $lens = explode('},{', $lens);

            $new_lens = [];

            foreach ($lens as $lens_item) {
                $lens_item = substr($lens_item, 1);
                $item_array = explode('","', $lens_item);

                $new_lens_item = [];

                foreach ($item_array as $single_item) {
                    $single_item = str_replace("\"", "", $single_item);
                    $single_item = explode(':', $single_item);
                    $key = $single_item[0];
                    $value = $single_item[1];
                    $new_lens_item[$key] = $value;
                }

                array_push($new_lens, $new_lens_item);
            }

        }

        $pre = str_replace("\\", "", $prescriptions);
        $pre = substr($pre, 2);
        $pre = substr($pre, 0, -6);
        $pre = explode(',', $pre);

        $new_pre = [];

        foreach ($pre as $pre_item) {
            $pre_item = str_replace("\"", "", $pre_item);
            $item = explode(":", $pre_item);
            $key = $item[0];
            $value = $item[1];
            $new_pre[$key] = $value;
        }

        if (get_the_category_by_id(wc_get_product_term_ids($product->get_id(), 'product_cat')[0]) !== 'Contact Lenses') {
            ?>
            <div class="checkout-item-wrapper">
                <div class="item-img-wrapper">
                    <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" alt="">
                </div>
                <div class="item-main-info-wrapper">
                    <p class="text-heading"><?php echo get_the_title($product_id); ?></p>
                    <p class="text">Color: <span><?php echo $color_attribute; ?></span>&nbsp;&nbsp;&nbsp;Size:
                        <span><?php echo $size_attribute; ?></span></p>
                    <div class="info-quantity-price">
                        <p class="text1"><?php echo $quantity; ?>pcs</p>
                        <p class="text2"><?php echo $subtotal; ?></p>
                    </div>
                </div>

                <?php
                if ($prescriptions) {
                    ?>
                    <div class="item-pre-info">
                        <div class="lens-config">
                            <p class="order-item-pre-heading">
                                Lens: <span><?php echo $new_lens[0]['fullName']; ?></span>
                                <svg viewBox="0 0 12 12" width="12" height="12" class="order-product-open-lenses">
                                    <use xlink:href="#chevron-left"></use>
                                </svg>
                            </p>
                            <div class="lenses-configuration">
                                <table class="prescription-table-item">
                                    <tr>
                                        <td></td>
                                        <td>SPH</td>
                                        <td>CYl</td>
                                        <td>Axis</td>
                                        <td>ADD</td>
                                        <td>PD</td>
                                    </tr>
                                    <tr>
                                        <td>OD<span>(R)</span></td>
                                        <td><?php echo $new_pre['odSph']; ?></td>
                                        <td><?php echo $new_pre['odCyl']; ?></td>
                                        <td><?php echo $new_pre['odAxis']; ?></td>
                                        <td><?php echo $new_pre['odAd']; ?></td>
                                        <td><?php echo $new_pre['pd1']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>OS<span>(L)</span></td>
                                        <td><?php echo $new_pre['osSph']; ?></td>
                                        <td><?php echo $new_pre['osCyl']; ?></td>
                                        <td><?php echo $new_pre['osAxis']; ?></td>
                                        <td><?php echo $new_pre['osAd']; ?></td>
                                        <td><?php echo $new_pre['pd2']; ?></td>
                                    </tr>
                                </table>

                                <?php
                                foreach ($new_lens as $lens_step) {

                                    if ($lens_step['step'] !== 'step2') {
                                        ?>
                                        <p class="lenses-configuration-rows"><?php echo $lens_step['fullName']; ?></p>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
        } else {
            if ($cart_item['lens_side'] === 'right') {
                $current_lens_token_right = $cart_item['lenses_token'];
                $current_lens_side_right = $cart_item['lens_side'];

                $cl_bc = $product->get_attribute('pa_cl-bc');
                $cl_dia = $product->get_attribute('pa_cl-dia');
                $cl_right = $product->get_attribute('pa_cl-power');
                $lens_quantity = $cart_item['quantity'];

                $key_left = '';

                foreach (WC()->cart->get_cart() as $cart_item_key_left => $cart_item_left) {
                    if ($current_lens_token_right === $cart_item_left['lenses_token'] && $cart_item_left['lens_side'] === 'left') {
                        $product_left = $cart_item_left['data'];
                        $cl_left = $product_left->get_attribute('pa_cl-power');
                        $key_left = $cart_item_key_left;
                    }
                }
                ?>
                <div class="checkout-item-wrapper">
                    <div class="item-img-wrapper">
                        <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" alt="">
                    </div>
                    <div class="item-main-info-wrapper">
                        <p class="text-heading"><?php echo get_the_title($product_id); ?></p>
<!--                        <p class="text">Color: <span>--><?php //echo $color_attribute; ?><!--</span>&nbsp;&nbsp;&nbsp;Size:-->
<!--                            <span>--><?php //echo $size_attribute; ?><!--</span></p>-->
                        <div class="info-quantity-price">
                            <p class="text1"><?php echo $quantity; ?>pcs</p>
                            <p class="text2"><?php echo $subtotal; ?></p>
                        </div>
                    </div>
                    <div class="lens-config">
                        <p class="order-lens-item-pre-heading">
                            Prescription: <span><?php echo $new_lens[0]['fullName']; ?></span>
                            <svg viewBox="0 0 12 12" width="12" height="12"
                                 class="order-product-open-lenses rotate-270">
                                <use xlink:href="#chevron-left"></use>
                            </svg>
                        </p>

                        <div class="lenses-configuration">
                            <table class="prescription-table-item">
                                <tr>
                                    <td></td>
                                    <td>SPH</td>
                                    <td>BC</td>
                                    <td>DN</td>
                                </tr>
                                <tr>
                                    <td>OD<span>(R)</span></td>
                                    <td><?php echo $cl_right; ?></td>
                                    <td><?php echo $cl_bc; ?></td>
                                    <td><?php echo $cl_dia; ?></td>
                                </tr>
                                <tr>
                                    <td>OS<span>(L)</span></td>
                                    <td><?php echo $cl_left; ?></td>
                                    <td><?php echo $cl_bc; ?></td>
                                    <td><?php echo $cl_dia; ?></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>
            <?php
            }
        }
    }

    ?>
    <div class="subtotal-wrapper">
        <div class="subtotal">
            <p class="text-heading">Subtotal (<?php echo WC()->cart->get_cart_contents_count(); ?> items)</p>
            <p class="text-heading subtotal-field"><?php echo WC()->cart->subtotal; ?>€</p>
        </div>
        <div class="subtotal">
            <p class="text">Taxes</p>
            <p class="text taxes-field"><?php echo WC()->cart->get_taxes_total(); ?>€</p>
        </div>
        <div class="subtotal">
            <p class="text">Delivery</p>
            <p class="text delivery-field"><?php echo WC()->cart->get_cart_shipping_total(); ?></p>
        </div>
        <div class="subtotal total">
            <p class="text total-text">Total</p>
            <p class="text total-field"><?php echo WC()->cart->get_cart_total(); ?></p>
        </div>
    </div>
</div>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>

<?php
global $woocommerce;
?>

<div class="fixed-block">
    <p class="text">Total</p>
    <p class="big-text"><?php echo $woocommerce->cart->get_total(); ?></p>
    <button class="next-step-button" disabled data-step="1">Continue</button>
</div>

<div class="modal fade" id="editEstonianAddress" data-bs-toggle="modal" tabindex="-1" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Edit address</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-wrapper">
                    <label>Address name</label>
                    <input class="popup-edit-address-name" type="text" placeholder="Home, work, parents etc...">
                </div>
                <div class="input-wrapper">
                    <label>Email</label>
                    <input class="popup-edit-email" type="email" placeholder="Enter email" autocomplete="off">
                </div>
                <div class="input-wrapper">
                    <label>First name</label>
                    <input class="popup-edit-first-name" type="text" placeholder="Enter First name">
                </div>
                <div class="input-wrapper">
                    <label>Last name</label>
                    <input class="popup-edit-last-name" type="text" placeholder="Enter Last name">
                </div>
                <div class="input-wrapper">
                    <label>Phone</label>
                    <input class="popup-edit-phone" type="tel" placeholder="Enter contact phone">
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
                <button type="button" class="modal-button edit-estonian-button">Save address</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editEuropeanAddress" data-bs-toggle="modal" tabindex="-1" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Edit address</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-wrapper">
                    <label>Address name</label>
                    <input class="popup-edit-address-name" type="text" placeholder="Home, work, parents etc...">
                </div>
                <div class="input-wrapper">
                    <label>Email</label>
                    <input class="popup-edit-email" type="email" placeholder="Enter email">
                </div>
                <div class="input-wrapper">
                    <label>First name</label>
                    <input class="popup-edit-first-name" type="text" placeholder="Enter First name">
                </div>
                <div class="input-wrapper">
                    <label>Last name</label>
                    <input class="popup-edit-last-name" type="text" placeholder="Enter Last name">
                </div>
                <div class="input-wrapper">
                    <label>City</label>
                    <input class="popup-edit-city" type="text" placeholder="Enter city">
                </div>
                <div class="input-wrapper">
                    <label>Address</label>
                    <input class="popup-edit-address" type="text" placeholder="Enter address">
                </div>
                <div class="input-wrapper">
                    <label>Postcode</label>
                    <input class="popup-edit-postcode" type="text" placeholder="Enter postcode">
                </div>
                <div class="input-wrapper">
                    <label>Phone</label>
                    <input class="popup-edit-phone" type="tel" placeholder="Enter contact phone">
                </div>

            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
                <button type="button" class="modal-button edit-european-button">Save address</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addEstonianAddress" data-bs-toggle="modal" tabindex="-1" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Add new address</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-wrapper">
                    <label>Address name</label>
                    <input class="popup-add-address-name" type="text" placeholder="Home, work, parents etc..." required>
                </div>
                <div class="input-wrapper">
                    <label>Email</label>
                    <input class="popup-add-email" type="email" placeholder="Enter email" required autocomplete="off">
                </div>
                <div class="input-wrapper">
                    <label>First name</label>
                    <input class="popup-add-first-name" type="text" placeholder="Enter First name" required>
                </div>
                <div class="input-wrapper">
                    <label>Last name</label>
                    <input class="popup-add-last-name" type="text" placeholder="Enter Last name" required>
                </div>
                <div class="input-wrapper">
                    <label>Phone</label>
                    <input class="popup-add-phone" type="tel" placeholder="Enter contact phone" required>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
                <button type="submit" class="modal-button add-estonian-button">Save address</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="addEuropeanAddress" data-bs-toggle="modal" tabindex="-1" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Add new address</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-wrapper">
                    <label>Address name</label>
                    <input class="popup-add-address-name" type="text" placeholder="Home, work, parents etc..." required>
                </div>
                <div class="input-wrapper">
                    <label>Email</label>
                    <input class="popup-add-email" type="email" placeholder="Enter email" required>
                </div>
                <div class="input-wrapper">
                    <label>First name</label>
                    <input class="popup-add-first-name" type="text" placeholder="Enter First name" required>
                </div>
                <div class="input-wrapper">
                    <label>Last name</label>
                    <input class="popup-add-last-name" type="text" placeholder="Enter Last name" required>
                </div>
                <div class="input-wrapper">
                    <label>City</label>
                    <input class="popup-add-city" type="text" placeholder="Enter city" required>
                </div>
                <div class="input-wrapper">
                    <label>Address</label>
                    <input class="popup-add-address" type="text" placeholder="Enter address" required>
                </div>
                <div class="input-wrapper">
                    <label>Postcode</label>
                    <input class="popup-add-postcode" type="text" placeholder="Enter postcode" required>
                </div>
                <div class="input-wrapper">
                    <label>Phone</label>
                    <input class="popup-add-phone" type="tel" placeholder="Enter contact phone" required>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
                <button type="submit" class="modal-button add-european-button">Save address</button>
            </div>
        </form>
    </div>
</div>

<div class="empty-dhl-shipping-address-item display-none">
    <label
        for="dhl-"
        class="shipping-label" data-address="">
        <input type="radio"
               id="dhl-"
               name="address" class="omniva-radio">
        <div class="shipping-info">
            <div class="shipping-wrapper">
                <div class="shipping-header">
                    <p class="text"></p>
                    <button type="button" data-bs-toggle="modal"
                            data-bs-target="#editEuropeanAddress"
                            class="edit-address-button"
                            data-address-id=""
                            data-address-name=""
                            data-email=""
                            data-first-name=""
                            data-last-name=""
                            data-phone=""
                            data-city=""
                            data-address=""
                            data-postcode=""
                    >Edit
                    </button>
                </div>
                <div class="shipping-body-preview">
                    <p class="text"></p>
                    <p class="text"></p>
                    <p class="text"></p>
                </div>
                <div class="shipping-body display-none">
                    <div class="input-wrapper">
                        <label>Email</label>
                        <input type="email" autocomplete="off" placeholder="Enter Email"
                               value=""
                               disabled class="email">
                    </div>
                    <div class="input-wrapper">
                        <label>First name</label>
                        <input type="text" placeholder="Enter your First name"
                               value=""
                               disabled class="first-name">
                    </div>
                    <div class="input-wrapper">
                        <label>Last name</label>
                        <input type="text" placeholder="Enter your Last name"
                               value=""
                               disabled class="last-name">
                    </div>
                    <div class="input-wrapper">
                        <label>City</label>
                        <input type="text" placeholder="Enter your city"
                               value=""
                               disabled class="city">
                    </div>
                    <div class="input-wrapper dhl-input-spinner no-pointer">
                        <div class="loadingio-spinner-dual-ring-mlu0dcv4kaf display-none">
                            <div class="ldio-9xczbp2xegj">
                                <div></div>
                                <div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                        <select class="dhl-rates-select"></select>
                    </div>
                    <div class="input-wrapper">
                        <label>Address</label>
                        <input type="text" placeholder="Enter your address"
                               value=""
                               disabled class="address">
                    </div>
                    <div class="input-wrapper">
                        <label>Postcode</label>
                        <input type="text" placeholder="Enter your postcode"
                               value=""
                               disabled class="postcode">
                    </div>
                    <div class="input-wrapper">
                        <label>Phone</label>
                        <input type="tel" placeholder="Enter phone number"
                               value=""
                               disabled class="tel">
                    </div>
                    <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                    <p class="show-notes">Add notes to your order?</p>
                    <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                    <p class="hide-notes">Hide notes</p>
                </div>
            </div>
        </div>
    </label>
</div>

<div class="empty-shipping-address-item display-none">
    <label
        for=""
        class="shipping-label" data-address="">
        <input type="radio"
               id=""
               name="address" class="omniva-radio">
        <div class="shipping-info">
            <div class="shipping-wrapper">
                <div class="shipping-header">
                    <p class="text"></p>
                    <button type="button" data-bs-toggle="modal"
                            data-bs-target="#editEstonianAddress"
                            class="edit-address-button"
                            data-address-id=""
                            data-address-name=""
                            data-email=""
                            data-first-name=""
                            data-last-name=""
                            data-phone=""
                    >Edit
                    </button>
                </div>
                <div class="shipping-body-preview">
                    <p class="text"></p>
                    <p class="text"></p>
                    <p class="text"></p>
                </div>
                <div class="shipping-body display-none">
                    <div class="input-wrapper">
                        <label>Email</label>
                        <input type="email" placeholder="Enter Email"
                               value=""
                               autocomplete="off"
                               disabled class="email">
                    </div>
                    <div class="input-wrapper select-wrapper">
                        <label
                            for="">Location</label>
                        <select
                            id="">

                        </select>
                    </div>
                    <div class="input-wrapper">
                        <label>First name</label>
                        <input type="text" placeholder="Enter your First name"
                               value=""
                               disabled class="first-name">
                    </div>
                    <div class="input-wrapper">
                        <label>Last name</label>
                        <input type="text" placeholder="Enter your Last name"
                               value=""
                               disabled class="last-name">
                    </div>
                    <div class="input-wrapper">
                        <label>Phone</label>
                        <input type="tel" placeholder="Enter phone number"
                               value=""
                               disabled class="tel">
                    </div>
                    <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                    <p class="show-notes">Add notes to your order?</p>
                    <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                    <p class="hide-notes">Hide notes</p>
                </div>
            </div>
        </div>
    </label>
</div>

<div class="modal fade" id="paypalAddressModal" data-bs-toggle="modal" tabindex="-1" aria-labelledby="paypalAddressModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="paypalAddressModalLabel">Write shipping address</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="shipping-body">
                    <div class="input-wrapper">
                        <label>City</label>
                        <input type="text" name="pp-city" id="pp-city" class="city" placeholder="Enter your city">
                    </div>
                    <div class="input-wrapper">
                        <label>Address</label>
                        <input type="text" name="pp-address" id="pp-address" class="address" placeholder="Enter your address">
                    </div>
                    <div class="input-wrapper">
                        <label>Postcode</label>
                        <input type="text" name="pp-postcode" id="pp-postcode" class="postcode" placeholder="Enter your postcode">
                    </div>
                    <p class="add-comment-heading">We need it so the courier can contact you if needed</p>
                    <p class="show-notes">Add notes to your order?</p>
                    <textarea class="order-notes" placeholder="Enter order notes"></textarea>
                    <p class="hide-notes">Hide notes</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="save-paypal-address" data-bs-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php

get_footer();

?>
