<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.3.6
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="cart_totals <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>
    <?php

    $cost = get_field('delivery_cost_counter', 'option');
    if ( ! WC()->cart->prices_include_tax ) {
        $amount = WC()->cart->cart_contents_total;
    } else {
        $amount = WC()->cart->cart_contents_total + WC()->cart->tax_total;
    }
    $percentage = ($amount / $cost) * 100;
    $left = $cost - $amount;
        ?>
	<h2 class="summary-header"><?php esc_html_e( 'Summary', 'woocommerce' ); ?></h2>
    <div class="our-suggestions-info-wrapper">
		<svg viewBox="0 0 24 24" class="free-shipping--icon">
            <use xlink:href="#free-shipping"></use>
        </svg>
        <div class="free-shipping__suggestions">
        	<?php if($left <= 0): ?>
        	    <?php echo get_field('congratulations', 'option')?>
        	<?php else : ?>
        	<?php echo get_field('text_part1', 'option')?> <?php echo $left?>€
        	<?php echo get_field('text_part2', 'option')?>


        	<a href="#"
        	   class="add-suggestions-btn"
        	   data-bs-toggle="modal"
        	   data-bs-target="#suggestionsOpen"
        	>See our suggestions</a>
        	<?php endif;?>
		</div>
    <div class="our-suggestions-info" style="width: <?php echo $percentage; ?>%">


    </div>
    </div>
	<div class="shop_table shop_table_responsive">

		<div class="cart-subtotal">
			<p><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></p>
			<p data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></p>
        </div>

<!--		--><?php //foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
<!--			<div class="cart-discount coupon---><?php //echo esc_attr( sanitize_title( $code ) ); ?><!--">-->
<!--				<p>--><?php //wc_cart_totals_coupon_label( $coupon ); ?><!--</p>-->
<!--				<p data-title="--><?php //echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?><!--">--><?php //wc_cart_totals_coupon_html( $coupon ); ?><!--</p>-->
<!--			</div>-->
<!--		--><?php //endforeach; ?>

<!--		--><?php //if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
<!---->
<!--			--><?php //do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
<!---->
<!--			--><?php //wc_cart_totals_shipping_html(); ?>
<!---->
<!--			--><?php //do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
<!---->
<!--		--><?php //elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>
<!---->
<!--			<div class="shipping">-->
<!--				<p>--><?php //esc_html_e( 'Shipping', 'woocommerce' ); ?><!--</p>-->
<!--				<p data-title="--><?php //esc_attr_e( 'Shipping', 'woocommerce' ); ?><!--">--><?php //woocommerce_shipping_calculator(); ?><!--</p>-->
<!--			</div>-->
<!---->
<!--		--><?php //endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<div class="fee">
				<p><?php echo esc_html( $fee->name ); ?></p>
				<p data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></p>
			</div>
		<?php endforeach; ?>

		<?php
		if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) {
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = '';

			if ( WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ) {
				/* translators: %s location. */
				$estimated_text = sprintf( ' <small>' . esc_html__( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] );
			}

			if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) {
				foreach ( WC()->cart->get_tax_totals() as $code => $tax ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
					?>
					<div class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<p><?php echo esc_html( $tax->label ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
						<p data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></p>
					</div>
					<?php
				}
			} else {
				?>
				<div class="tax-total">
					<p><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<p data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></p>
				</div>
				<?php
			}
		}
		?>

		<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

		<div class="order-total">
			<p><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
			<p data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>"><?php wc_cart_totals_order_total_html(); ?></p>
        </div>

		<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

	</div>

    <?php if (!is_user_logged_in()) { ?>
    <div class="checkout-wrapper-info">
    <div class="radio-item">
        <input type="radio" id="checkoutGuestDesktop" name="checkoutDesktop" value="checked" checked>
        <label for="checkoutGuestDesktop">Checkout as a guest</label>
    </div>

    <div class="radio-item">
        <input type="radio" id="checkoutAccountDesktop" name="checkoutDesktop" value="checked2">
        <label for="checkoutAccountDesktop">Checkout with account</label>
    </div>
    <div class="choice">
        <div class="go-to-login">
            <button type="button" class="cart-sign-in">Sign In</button>
            <button type="button" class="cart-sign-up">Sign up</button>
        </div>
	<div class="wc-proceed-to-checkout go-to-checkout">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>
    </div>
    </div>
    <?php } else { ?>
        <div class="go-to-checkout">
            <div class="cart-subtotal">
                <p><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
                <p><?php wc_cart_totals_order_total_html(); ?></p>

            </div>
            <?php do_action('woocommerce_proceed_to_checkout'); ?>
        </div>
    <?php  } ?>
	<?php do_action( 'woocommerce_after_cart_totals' ); ?>

</div>
