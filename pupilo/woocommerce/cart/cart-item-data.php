<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-item-data.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     2.4.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>

<?php foreach ($item_data as $data) : ?>

    <?php

    if ($data['key'] === 'Lenses configuration') {
        $lenses_configuration = $data['display'];
    }
    if ($data['key'] === 'Prescriptions') {
        $prescriptions = $data['display'];
    }

    ?>

<?php endforeach; ?>

<?php

$pre = str_replace("\\", "", $prescriptions);
$pre = substr($pre, 2);
$pre = substr($pre, 0, -6);
$pre = explode(',', $pre);

$new_pre = [];

foreach ($pre as $pre_item) {
    $pre_item = str_replace("\"", "", $pre_item);
    $item = explode(":", $pre_item);
    $key = $item[0];
    $value = $item[1];
    $new_pre[$key] = $value;
}
// var_dump($new_pre);
$lens = str_replace("\\", "", $lenses_configuration);
$lens = substr($lens, 4);
$lens = substr($lens, 0, -3);
$lens = explode('},{', $lens);

$new_lens = [];

foreach ($lens as $lens_item) {
    $lens_item = substr($lens_item, 1);
    $item_array = explode('","', $lens_item);

    $new_lens_item = [];

    foreach ($item_array as $single_item) {
        $single_item = str_replace("\"", "", $single_item);
        $single_item = explode(':', $single_item);
        $key = $single_item[0];
        $value = $single_item[1];
        $new_lens_item[$key] = $value;
    }

    array_push($new_lens, $new_lens_item);
}

?>
<div class="product-item-column">
    <div class="variation">
        <?php foreach ($item_data as $data) : ?>
            <span
                class="<?php echo sanitize_html_class('variation-' . $data['key']); ?>"><?php echo wp_kses_post($data['key']); ?>:</span>
            <span
                class="<?php echo sanitize_html_class('variation-' . $data['key']); ?>"><?php echo wp_kses_post(wpautop($data['display'])); ?></span>
        <?php endforeach; ?>

    </div>
</div>

<?php if ($prescriptions) : ?>
    <div class="lens-config">

        <p class="order-item-pre-heading">
            Lens: <span><?php echo $new_lens[0]['fullName']; ?></span>
            <svg viewBox="0 0 12 12" width="12" height="12" class="order-product-open-lenses">
                <use xlink:href="#chevron-left"></use>
            </svg>
        </p>
        <div class="lenses-configuration">
            <table class="prescription-table-item">
                <tr>
                    <td></td>
                    <td>SPH</td>
                    <td>CYl</td>
                    <td>Axis</td>
                    <td>ADD</td>
                    <td>PD</td>
                </tr>
                <tr>
                    <td>OD<span>(R)</span></td>
                    <td><?php echo $new_pre['odSph']; ?></td>
                    <td><?php echo $new_pre['odCyl']; ?></td>
                    <td><?php echo $new_pre['odAxis']; ?></td>
                    <td><?php echo $new_pre['odAd']; ?></td>
                    <td><?php echo $new_pre['pd1']; ?></td>
                </tr>

                <tr>
                    <td>OS<span>(L)</span></td>
                    <td><?php echo $new_pre['osSph']; ?></td>
                    <td><?php echo $new_pre['osCyl']; ?></td>
                    <td><?php echo $new_pre['osAxis']; ?></td>
                    <td><?php echo $new_pre['osAd']; ?></td>
                    <td><?php echo $new_pre['pd2']; ?></td>
                </tr>
            </table>

            <?php

            foreach ($new_lens as $lens_step) {

                if ($lens_step['step'] !== 'step2') {
                    ?>
                    <p class="lenses-configuration-rows"><?php echo $lens_step['fullName']; ?>
                        <span><?php
                        if ($lens_step['ap'] > 0) :
                        echo $lens_step['ap'] . '€';
                        endif; ?> </span>
                    </p>
                    <?php
                }
            }

            ?>
        </div>

    </div>

<?php endif; ?>


