<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined('ABSPATH') || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */
do_action('woocommerce_cart_is_empty');

if (wc_get_page_id('shop') > 0) : ?>
    <div class="free-delivery_mobile">
        <svg class="shipping-image" height="24px" width="24px">
            <use xlink:href="#free-shipping-cart"></use>
        </svg>
        <p class="free-delivery-text"><?php echo get_field('delivery_text_part1', 'option')?> <span
                class="free-delivery-text-bold">
                <?php echo get_field('delivery_text_part2', 'option')?>
            </span></p>
    </div>
    <div class="page-container">
    
        <div class="empty-cart-row">
            <div class="empty-cart-container message-wrapper">

                <div class="message-icon">
                    <svg width="64" height="96">
                        <use xlink:href="#empty-bag"></use>
                    </svg>
                </div>
                <h5 class="empty-cart-header">Yep, add amazing products here</h5>
                <p class="empty-cart-description">This is a fantastic opportunity to fill the cart with something really
                    special.</p>
                <p class="return-to-shop">
                    <a class="button wc-backward"
                       href="<?php echo esc_url(home_url('/')); ?>">
                        <?php
                        /**
                         * Filter "Return To Shop" text.
                         *
                         * @param string $default_text Default text.
                         * @since 4.6.0
                         */
                        echo esc_html(apply_filters('woocommerce_return_to_shop_text', __('Continue Shopping', 'woocommerce')));
                        ?>
                    </a>
                    <a href="#" class="sign-up">Sign up</a>
                </p>
            </div>

            <div class="empty-cart-payment-info">
                <h5 class="cart-header">We accept different payment methods</h5>
                <div class="payment-image-wrapper">
                    <div class="payment-image">
                        <svg width="60" height="19">
                            <use xlink:href="#visa"></use>
                        </svg>
                    </div>
                    <div class="payment-image">
                        <svg width="52" height="33">
                            <use xlink:href="#master"></use>
                        </svg>
                    </div>
                    <div class="payment-image">
                        <svg width="66" height="19">
                            <use xlink:href="#paypal"></use>
                        </svg>
                    </div>
                </div>

                <div class="info-pay-delivery">
                     <div class="free-delivery-info">
                         <div class="info-image">
                             <svg width="58" height="43">
                                 <use xlink:href="#delivery"></use>
                             </svg>
                         </div>
                         <div class="info-description">
                             <h6>Free Delivery</h6>
                             <p class="text">Congratulations, your order qualifies to free delivery and that is exactly what you get.</p>
                             <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                             <svg width="20" height="15">
                                 <use xlink:href="#arrow"></use>
                             </svg>
                             </a>
                         </div>
                     </div>
                    <div class="free-returns-info">
                        <div class="info-image">
                            <svg  width="50" height="50">
                                <use xlink:href="#free-return"></use>
                            </svg>
                        </div>
                        <div class="info-description">
                            <h6>Free Returns</h6>
                            <p class="text">If you order smth. you want to replace or change, no worries - all returns are free for 30 days.</p>
                            <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                                <svg width="20" height="15">
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deliveryReturns" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                <div class="modal-body">
                    <?php echo get_field('delivery_returns_popup')?>
                </div>

            </div>
        </div>
    </div>

    <section class="categories-section">
        <div class="page-container">
            <h2 class="headline">Top Categories</h2>
            <div class="row multiple-items">
                <?php while (have_rows('product_categories')): the_row(); ?>
                    <div class="<?php echo get_sub_field('category_item_class') ?> category-item"
                         style="background-image: url('<?php echo get_sub_field('category_image')['url'] ?>')">

                        <img class="desktop-cat-image" src="<?php echo get_sub_field('category_image')['url'] ?>"
                             alt="<?php echo get_sub_field('category_image')['title'] ?>">

                        <a class="categories_link" href="<?php echo get_sub_field('category_link')['url'] ?>">
                            <?php echo get_sub_field('category_link')['title'] ?>
                            <span>
                                <svg class="categories_link--arrow">
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </span>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>

        </div>
    </section>


<?php endif; ?>
<?php
 if (is_page('cart')) : ?>
    <section class="recently-product-section">
        <?php get_template_part('template-parts/content', 'recently-product')?>
    </section>

<?php endif; ?>
