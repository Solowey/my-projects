<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_cart'); ?>


<?php
global $woocommerce;
$item_data = $woocommerce->cart->get_cart();

foreach ($item_data as $cart_item_key => $cart_item) {

    if ($cart_item['lenses_configuration']) {
        $lenses_configuration = $cart_item['lenses_configuration'];

    }
}
?>

<div class="cart-row">

    <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
        <div class="our-suggestions-info-wrapper our-suggestions-info-wrapper__mobile">
            <svg viewBox="0 0 24 24" class="free-shipping--icon">
                <use xlink:href="#free-shipping"></use>
            </svg>
            <div class="free-shipping__suggestions">
            <?php

            $cost = get_field('delivery_cost_counter', 'option');
            if (!WC()->cart->prices_include_tax) {
                $amount = WC()->cart->cart_contents_total;
            } else {
                $amount = WC()->cart->cart_contents_total + WC()->cart->tax_total;
            }
            $percentage = ($amount / $cost) * 100;
            $left = $cost - $amount;
            ?>
            <?php if ($left <= 0): ?>
                <?php echo get_field('congratulations', 'option') ?>
            <?php else : ?>
                <?php echo get_field('text_part1', 'option') ?> <span class="cost-for-free"><?php echo $left ?>€</span>
                <?php echo get_field('text_part2', 'option') ?>
                <a href="#"
                   class="add-suggestions-btn btn-open-suggestions-modal"
                   data-bs-toggle="modal"
                   data-bs-target="#suggestionsOpen"
                >See our suggestions</a>
            <?php endif; ?>
            <div data-cost="<?php echo $amount ?>" id="dataCost" class="our-suggestions-info"
                 style="width: <?php echo $percentage; ?>%">

            </div>
            </div>
        </div>
        <div class="count-cart-header">
            <h5 class="cart-header-count">My cart </h5>
        </div>
        <?php do_action('woocommerce_before_cart_table'); ?>

        <div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents">

            <?php do_action('woocommerce_before_cart_contents'); ?>

            <?php
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                if (!$cart_item['lenses_configuration']) {
                    $lenses_configuration = $cart_item['lenses_configuration'];
                }

                $addPrice = $cart_item['additional_price'];

                $lens = str_replace("\\", "", $lenses_configuration);
                $lens = substr($lens, 4);
                $lens = substr($lens, 0, -6);
                $lens = explode('},{', $lens);
                $new_lens = [];

                foreach ($lens as $lens_item) {
                    $lens_item = substr($lens_item, 1);
                    $item_array = explode('","', $lens_item);
                    $new_lens_item = [];

                    foreach ($item_array as $single_item) {
                        $single_item = str_replace("\"", "", $single_item);
                        $single_item = explode(':', $single_item);
                        $key = $single_item[0];
                        $value = $single_item[1];
                        $new_lens_item[$key] = $value;
                    }

                    array_push($new_lens, $new_lens_item);
                }

                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);


                    if (get_the_category_by_id(wc_get_product_term_ids($_product->get_id(), 'product_cat')[0]) !== 'Contact Lenses') {
                        ?>
                        <div
                            class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                            <div class="cart-product-wrapper">
                                <div class="product-info-wrapper">

                                    <div class="aside-product-remove">

                                        <?php
                                        echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                                            'woocommerce_cart_item_remove_link',
                                            sprintf(
                                                '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">
                                        <svg width="24" height="24">
                                        <use xlink:href="#cart-close">

                                        </use>
                                    </svg></a>',
                                                esc_url(wc_get_cart_remove_url($cart_item_key)),
                                                esc_html__('Remove this item', 'woocommerce'),
                                                esc_attr($product_id),
                                                esc_attr($_product->get_sku())
                                            ),
                                            $cart_item_key
                                        );
                                        ?>
                                    </div>

                                    <div class="product-thumbnail cart-product-thumbnail">
                                        <?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', get_the_post_thumbnail_url($product_id), $cart_item, $cart_item_key);
                                        $alt = $_product->get_name();
                                        if (!$product_permalink) {
                                            echo $thumbnail; // PHPCS: XSS ok.
                                        } else {
                                            printf('<a href="%s"><img src="%s" alt="%s"></a>', esc_url($product_permalink), $thumbnail, $alt); // PHPCS: XSS ok.
                                        }
                                        ?>

                                    </div>
                                    <div class="mobile-cart-product-info">
                                        <div class="mobile-control-panel">
                                            <?php
                                            echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<p class="cart-product-name">%s</p>', get_the_title($product_id)), $cart_item, $cart_item_key));

                                            ?>
                                            <span id="controlOpen">
                                    <span class="button-open-control-menu"></span>
                                </span>
                                        </div>
                                        <?php echo wc_get_formatted_cart_item_data($cart_item); ?>
                                    </div>
                                </div>
                                <div class="product-column">
                                    <div class="cart-product-info">
                                        <div class="product-name"
                                             data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                                            <?php
                                            if (!$product_permalink) {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', get_the_title($product_id), $cart_item, $cart_item_key) . '&nbsp;');
                                            } else {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<p class="cart-product-name">%s</p>', get_the_title($product_id)), $cart_item, $cart_item_key));
                                            }
                                            do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                            // Meta data.
                                            echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                            $crosssells = get_post_meta($product_id, '_crosssell_ids', true);

                                            if ($crosssells) : ?>

                                                <?php
                                                $args = array(
                                                    'status' => array('draft', 'pending', 'private', 'publish'),
                                                    'post_type' => array('product', 'product_variation'),
                                                    'post__in' => $crosssells,
                                                    'posts_per_page' => 10,
                                                );
                                                $products = new WP_Query($args);
                                                if ($products->have_posts()) :

                                                    echo '<div class="modal fade accessoriesModal"
                                                 id="mod' . $_product->get_id() . '"

                                                         tabindex="-1" aria-labelledby="accessoriesModal" aria-hidden="true">
                                                <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">

                                                    <div class="modal-content">

                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                                        <div class="modal-body">
                                                        <h2 class="accessories-modal-header">Add Accessories</h2>
                                                        <h3 class="accessories-modal-title">Select Your Accessories</h3>
                                                        <div class="add-accessories-nav-arrow-container"></div>
                                                        ';
                                                    woocommerce_product_loop_start();
                                                    while ($products->have_posts()) : $products->the_post();

                                                        get_template_part('template-parts/content', 'accesories');

                                                    endwhile; // end of the loop.
                                                    woocommerce_product_loop_end();
                                                    echo ' </div>

                                                 </div>

                                                </div>
                                            </div>'; endif;
                                                wp_reset_postdata();

                                            endif;

                                            // Backorder notification.
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                                            }
                                            ?>


                                        </div>
                                        <div class="product-price"
                                             data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                                            <?php
                                            echo $productPrice = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                            ?>

                                        </div>
                                    </div>

                                </div>
                                <div class="cart-quantity-subtotal-wrapper">
                                    <div class="product-quantity"
                                         data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                                        <?php
                                        if ($_product->is_sold_individually()) {
                                            $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                        } else {
                                            $product_quantity = woocommerce_quantity_input(
                                                array(
                                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                                    'input_value' => $cart_item['quantity'],
                                                    'max_value' => $_product->get_max_purchase_quantity(),
                                                    'min_value' => '0',
                                                    'product_name' => $_product->get_name(),
                                                ),
                                                $_product,
                                                false
                                            );
                                        }

                                        echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.
                                        ?>

                                    </div>

                                    <span class="product-result-price"><?php

                                        $var = $productPrice;
                                        $float_value_of_var = floatval(preg_replace('#[^\d.]#', '', $var));

                                        if ($addPrice <= 0) :
                                            $addPrice = 0;
                                            echo $float_value_of_var . '€';
                                        else:
                                            echo $float_value_of_var - $addPrice . '€';
                                        endif;
                                        ?>
                                </span>

                                </div>
                            </div>
                            <div class="product-subtotal base-price"
                                 data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>">
                                <?php
                                echo '<span class="sub-title">Subtotal: </span>';

                                echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                ?>

                            </div>
                            <div class="cart-product-button-wrapper">

                                <?php if ($crosssells) : ?>
                                    <button class="add-accessories-btn" type="button" data-bs-toggle="modal"
                                            data-bs-target="#mod<?php echo $_product->get_id() ?>">Add Accessories
                                    </button>
                                <?php endif; ?>
                                <div class="mobile-control_overlay">
                                    <div class="mobile-control">

                                    <span class="mobile-control-button-close">
                                        <span class="mobile-control-button-close__decor"></span>
                                    </span>
                                        <div class="wish-product-link cart-wish-product-link"
                                             data-id="<?php echo $cart_item['product_id']; ?>">
                                            <a class="save-for-later" href="#">
                                                <svg width="24" height="24">
                                                    <use xlink:href="#wish-list"></use>
                                                </svg>
                                                Move to wishlist
                                            </a>
                                        </div>
                                        <?php
                                        if (is_page('cart')) : ?>
                                            <button class="edit-lenses-btn" type="button" data-link="<?php echo esc_url($product_permalink); ?>">
                                                <svg width="18" height="18">
                                                    <use xlink:href="#edit"></use>
                                                </svg>
                                                Edit lenses
                                            </button>

                                        <?php endif; ?>

                                        <div class="product-remove">
                                            <svg width="20" height="20">
                                                <use xlink:href="#trash"></use>
                                            </svg>
                                            <?php
                                            echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                                                'woocommerce_cart_item_remove_link',
                                                sprintf(
                                                    '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">Remove</a>',
                                                    esc_url(wc_get_cart_remove_url($cart_item_key)),
                                                    esc_html__('Remove this item', 'woocommerce'),
                                                    esc_attr($product_id),
                                                    esc_attr($_product->get_sku())
                                                ),
                                                $cart_item_key
                                            );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                            if ($cart_item['lens_side'] === 'right') {
                            $current_lens_token_right = $cart_item['lenses_token'];
                            $current_lens_side_right = $cart_item['lens_side'];

                            $cl_bc = $_product->get_attribute('pa_cl-bc');
                            $cl_dia = $_product->get_attribute('pa_cl-dia');
                            $cl_right = $_product->get_attribute('pa_cl-power');
                            $lens_quantity = $cart_item['quantity'];

                            $key_left = '';

                            foreach (WC()->cart->get_cart() as $cart_item_key_left => $cart_item_left) {
                                if ($current_lens_token_right === $cart_item_left['lenses_token'] && $cart_item_left['lens_side'] === 'left') {
                                    $product_left = $cart_item_left['data'];
                                    $cl_left = $product_left->get_attribute('pa_cl-power');
                                    $key_left = $cart_item_key_left;
                                }
                            }
                            ?>
                        <div class="woocommerce-cart-form__cart-item lenses_item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                            <div class="cart-product-wrapper">
                                <div class="product-info-wrapper">

                                    <div class="aside-product-remove">
                                        <p class="lens-delete" data-token="<?php echo $cart_item['lenses_token']; ?>"
                                           data-key-right="<?php echo $cart_item_key; ?>"
                                           data-key-left="<?php echo $key_left; ?>"
                                           data-variation="<?php echo $_product->get_id(); ?>"
                                           data-id="<?php echo $product_id; ?>">
                                            <svg width="24" height="24">
                                                <use xlink:href="#cart-close">

                                                </use>
                                            </svg>
                                           </p>

                                    </div>

                                    <div class="product-thumbnail cart-product-thumbnail">
                                        <?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', get_the_post_thumbnail_url($product_id), $cart_item, $cart_item_key);
                                        $alt = $_product->get_name();
                                        if (!$product_permalink) {
                                            echo $thumbnail; // PHPCS: XSS ok.
                                        } else {
                                            printf('<a href="%s"><img src="%s" alt="%s"></a>', esc_url($product_permalink), $thumbnail, $alt); // PHPCS: XSS ok.
                                        }
                                        ?>

                                    </div>
                                    <div class="mobile-cart-product-info">
                                        <div class="mobile-control-panel">
                                            <?php
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<p class="cart-product-name">%s</p>', get_the_title($product_id)), $cart_item, $cart_item_key));
                                            ?>
                                            <span id="controlOpen">
                                    <span class="button-open-control-menu"></span>
                                </span>
                                        </div>

                                        <div class="lens-config">

                                            <p class="order-lens-item-pre-heading">
                                                Prescription: <span><?php echo $new_lens[0]['fullName']; ?></span>
                                                <svg viewBox="0 0 12 12" width="12" height="12"
                                                     class="order-product-open-lenses">
                                                    <use xlink:href="#chevron-left"></use>
                                                </svg>
                                            </p>

                                            <div class="lenses-configuration">
                                                    <table class="prescription-table-item">
                                                        <tr>
                                                            <td></td>
                                                            <td>SPH</td>
                                                            <td>BC</td>
                                                            <td>DN</td>
                                                        </tr>
                                                        <tr>
                                                            <td>OD<span>(R)</span></td>
                                                            <td><?php echo $cl_right; ?></td>
                                                            <td><?php echo $cl_bc; ?></td>
                                                            <td><?php echo $cl_dia; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>OS<span>(L)</span></td>
                                                            <td><?php echo $cl_left; ?></td>
                                                            <td><?php echo $cl_bc; ?></td>
                                                            <td><?php echo $cl_dia; ?></td>
                                                        </tr>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="product-column">
                                    <div class="cart-product-info">
                                        <div class="product-name"
                                             data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                                            <?php
                                            if (!$product_permalink) {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', get_the_title($product_id), $cart_item, $cart_item_key) . '&nbsp;');
                                            } else {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<p class="cart-product-name">%s</p>', get_the_title($product_id)), $cart_item, $cart_item_key));
                                            }
                                            do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                            // Meta data.
                                            echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                            $crosssells = get_post_meta($product_id, '_crosssell_ids', true);

                                            if ($crosssells) : ?>

                                                <?php
                                                $args = array(
                                                    'status' => array('draft', 'pending', 'private', 'publish'),
                                                    'post_type' => array('product', 'product_variation'),
                                                    'post__in' => $crosssells,
                                                    'posts_per_page' => 10,
                                                );
                                                $products = new WP_Query($args);
                                                if ($products->have_posts()) :

                                                    echo '<div class="modal fade accessoriesModal"
                                                 id="mod' . $_product->get_id() . '"

                                                         tabindex="-1" aria-labelledby="accessoriesModal" aria-hidden="true">
                                                <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">

                                                    <div class="modal-content">

                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                                        <div class="modal-body">
                                                        <h2 class="accessories-modal-header">Add Accessories</h2>
                                                        <h3 class="accessories-modal-title">Select Your Accessories</h3>
                                                        <div class="add-accessories-nav-arrow-container"></div>
                                                        ';
                                                    woocommerce_product_loop_start();
                                                    while ($products->have_posts()) : $products->the_post();

                                                        get_template_part('template-parts/content', 'accesories');

                                                    endwhile; // end of the loop.
                                                    woocommerce_product_loop_end();
                                                    echo ' </div>

                                                 </div>

                                                </div>
                                            </div>'; endif;
                                                wp_reset_postdata();

                                            endif;

                                            // Backorder notification.
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                                echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                                            }
                                            ?>


                                        </div>
                                        <div class="product-price"
                                             data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                                            <?php
                                            echo $productPrice = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                            ?>

                                        </div>
                                    </div>

                                </div>
                                <div class="cart-quantity-subtotal-wrapper">
                                    <div class="product-quantity">
                                    <div class="quantity">

                                        <?php $lens_quantity = $cart_item['quantity']; ?>
                                        <span style="padding: 0 10px;" class="lens-minus quantity-button"
                                           data-token="<?php echo $cart_item['lenses_token']; ?>"
                                           data-key-right="<?php echo $cart_item_key; ?>"
                                           data-key-left="<?php echo $key_left; ?>"
                                           data-qty="<?php echo $cart_item['qty']; ?>"
                                           data-current-qty="<?php echo $lens_quantity; ?>"
                                           data-variation="<?php echo $_product->get_id(); ?>"
                                           data-sign="minus"
                                           data-id="<?php echo $product_id; ?>">-</span>
                                        <span><?php echo $lens_quantity; ?></span>
                                        <span style="padding: 0 10px;" class="lens-plus quantity-button"
                                           data-token="<?php echo $cart_item['lenses_token']; ?>"
                                           data-key-right="<?php echo $cart_item_key; ?>"
                                           data-key-left="<?php echo $key_left; ?>"
                                           data-qty="<?php echo $cart_item['qty']; ?>"
                                           data-current-qty="<?php echo $lens_quantity; ?>"
                                           data-variation="<?php echo $_product->get_id(); ?>"
                                           data-sign="plus"
                                           data-id="<?php echo $product_id; ?>">+</span>

                                    </div>
                                    </div>
                                    <span class="product-result-price"><?php

                                        $var = $productPrice;
                                        $float_value_of_var = floatval(preg_replace('#[^\d.]#', '', $var));

                                        if ($addPrice <= 0) :
                                            $addPrice = 0;
                                            echo $float_value_of_var . '€';
                                        else:
                                            echo $float_value_of_var - $addPrice . '€';
                                        endif;
                                        ?>
                                </span>

                                </div>
                                <div class="lens-config">

                                    <p class="order-lens-item-pre-heading">
                                       Prescription: <span><?php echo $new_lens[0]['fullName']; ?></span>
                                        <svg viewBox="0 0 12 12" width="12" height="12"
                                             class="order-product-open-lenses">
                                            <use xlink:href="#chevron-left"></use>
                                        </svg>
                                    </p>
                                    <div class="lenses-configuration">
                                        <?php if ($cart_item['lens_side'] === 'right') {
                                            $current_lens_token_right = $cart_item['lenses_token'];
                                            $current_lens_side_right = $cart_item['lens_side'];

                                            $cl_bc = $_product->get_attribute('pa_cl-bc');
                                            $cl_dia = $_product->get_attribute('pa_cl-dia');
                                            $cl_right = $_product->get_attribute('pa_cl-power');
                                            $lens_quantity = $cart_item['quantity'];

                                            $key_left = '';



                                            foreach (WC()->cart->get_cart() as $cart_item_key_left => $cart_item_left) {
                                                if ($current_lens_token_right === $cart_item_left['lenses_token'] && $cart_item_left['lens_side'] === 'left') {
                                                    $product_left = $cart_item_left['data'];
                                                    $cl_left = $product_left->get_attribute('pa_cl-power');
                                                    $key_left = $cart_item_key_left;
                                                }
                                            }
                                            ?>

                                            <table class="prescription-table-item">
                                                <tr>
                                                    <td></td>
                                                    <td>SPH</td>
                                                    <td>BC</td>
                                                    <td>DN</td>
                                                </tr>
                                                <tr>
                                                    <td>OD<span>(R)</span></td>
                                                    <td><?php echo $cl_right; ?></td>
                                                    <td><?php echo $cl_bc; ?></td>
                                                    <td><?php echo $cl_dia; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>OS<span>(L)</span></td>
                                                    <td><?php echo $cl_left; ?></td>
                                                    <td><?php echo $cl_bc; ?></td>
                                                    <td><?php echo $cl_dia; ?></td>
                                                </tr>
                                            </table>

                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="product-subtotal base-price"
                                 data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>">
                                <?php
                                echo '<span class="sub-title">Subtotal: </span>';

                                echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                ?>

                            </div>

                            <div class="cart-product-button-wrapper">

                                <?php if ($crosssells) : ?>
                                    <button class="add-accessories-btn" type="button" data-bs-toggle="modal"
                                            data-bs-target="#mod<?php echo $_product->get_id() ?>">Add Accessories
                                    </button>
                                <?php endif; ?>
                                <div class="mobile-control_overlay">
                                    <div class="mobile-control">

                                    <span class="mobile-control-button-close">
                                        <span class="mobile-control-button-close__decor"></span>
                                    </span>
                                        <div class="wish-product-link cart-wish-product-link"
                                             data-id="<?php echo $cart_item['product_id']; ?>">
                                            <a class="save-for-later" href="#">
                                                <svg width="24" height="24">
                                                    <use xlink:href="#wish-list"></use>
                                                </svg>
                                                Move to wishlist
                                            </a>
                                        </div>
                                        <?php
                                        if (is_page('cart')) : ?>
                                            <button class="edit-lenses-btn" type="button" data-link="<?php echo esc_url($product_permalink); ?>">
                                                <svg width="18" height="18">
                                                    <use xlink:href="#edit"></use>
                                                </svg>
                                                Edit lenses
                                            </button>

                                        <?php endif; ?>

                                        <div class="product-remove">
                                            <svg width="20" height="20">
                                                <use xlink:href="#trash"></use>
                                            </svg>
                                            <p class="lens-delete" data-token="<?php echo $cart_item['lenses_token']; ?>"
                                               data-key-right="<?php echo $cart_item_key; ?>"
                                               data-key-left="<?php echo $key_left; ?>"
                                               data-variation="<?php echo $_product->get_id(); ?>"
                                               data-id="<?php echo $product_id; ?>">
                                               Remove
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php } ?>
                            <?php
                    }
                    ?>


                    <?php
                }
            }
            ?>

            <?php do_action('woocommerce_cart_contents'); ?>

            <tr>
                <td colspan="6" class="actions">

                    <?php if (wc_coupons_enabled()) { ?>
                        <div class="coupon">
                            <label for="coupon_code"><?php esc_html_e('Coupon:', 'woocommerce'); ?></label> <input
                                type="text" name="coupon_code" class="input-text" id="coupon_code" value=""
                                placeholder="<?php esc_attr_e('Coupon code', 'woocommerce'); ?>"/>
                            <button type="submit" class="button" name="apply_coupon"
                                    value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>"><?php esc_attr_e('Apply coupon', 'woocommerce'); ?></button>
                            <?php do_action('woocommerce_cart_coupon'); ?>
                        </div>
                    <?php } ?>

                    <button type="submit" class="button up" name="update_cart" style="display: none"
                            value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>

                    <?php do_action('woocommerce_cart_actions'); ?>

                    <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
                </td>
            </tr>

            <?php do_action('woocommerce_after_cart_contents'); ?>

        </div>
        <?php do_action('woocommerce_after_cart_table'); ?>
    </form>

    <?php do_action('woocommerce_before_cart_collaterals'); ?>
    <div class="totals-wrapper">
        <div class="cart-collaterals">

            <?php
            /**
             * Cart collaterals hook.
             *
             * @hooked woocommerce_cross_sell_display
             * @hooked woocommerce_cart_totals - 10
             */
            do_action('woocommerce_cart_collaterals');
            ?>

        </div>
        <div class="empty-cart-payment-info">
            <h5 class="cart-header">We accept different payment methods</h5>
            <div class="payment-image-wrapper">
                <div class="payment-image">
                    <svg width="60" height="19">
                        <use xlink:href="#visa"></use>
                    </svg>
                </div>
                <div class="payment-image">
                    <svg width="52" height="33">
                        <use xlink:href="#master"></use>
                    </svg>
                </div>
                <div class="payment-image">
                    <svg width="66" height="19">
                        <use xlink:href="#paypal"></use>
                    </svg>
                </div>
            </div>

            <div class="info-pay-delivery">
                <div class="free-delivery-info">
                    <div class="info-image">
                        <svg width="58" height="43">
                            <use xlink:href="#delivery"></use>
                        </svg>
                    </div>
                    <div class="info-description">
                        <h6>Free Delivery</h6>
                        <p class="text">Congratulations, your order qualifies to free delivery and that is exactly what
                            you
                            get.</p>
                        <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                            <svg width="20" height="15">
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="free-returns-info">
                    <div class="info-image">
                        <svg width="50" height="50">
                            <use xlink:href="#free-return"></use>
                        </svg>
                    </div>
                    <div class="info-description">
                        <h6>Free Returns</h6>
                        <p class="text">If you order smth. you want to replace or change, no worries - all returns are
                            free
                            for 30 days.</p>
                        <a href="#" class="link" data-bs-toggle="modal" data-bs-target="#deliveryReturns">Read more
                            <svg width="20" height="15">
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deliveryReturns" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">

            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

            <div class="modal-body">
                <?php echo get_field('delivery_returns_popup') ?>
            </div>

        </div>
    </div>
</div>

<?php do_action('woocommerce_after_cart'); ?>
