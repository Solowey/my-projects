<?php
    global $product;
?>

<div class="wizard-review display-none">
    <div class="wizard-total-wrapper">
        <div class="total-img">
            <img class="wizard-image" src="" alt="<?php the_title(); ?>">
        </div>
        <div class="total-brand">
            <?php echo $product->get_attribute('Brand'); ?>
        </div>
        <div class="total-name">
            <?php the_title(); ?>
        </div>
        <p class="frame">Frame</p>
        <div class="total-frame">
            <div class="variations"></div>
            <div class="price"></div>
        </div>
        <div class="total-prescriptions">
            <table>
                <tr>
                    <td></td>
                    <td>SPH</td>
                    <td>CYl</td>
                    <td>Axis</td>
                    <td>ADD</td>
                    <td>PD</td>
                </tr>
                <tr>
                    <td>OD<span>(R)</span></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>OS<span>(L)</span></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="frame-options"></div>
        <div class="wizard-subtotal">
            <p>Subtotal:</p>
            <p class="current-price">€<?php echo $product->get_price(); ?></p>
        </div>
    </div>
    <button class="btn add-to-cart display-none">Add to cart</button>
</div>
