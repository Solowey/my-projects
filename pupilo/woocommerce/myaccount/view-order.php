<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

defined('ABSPATH') || exit;

$notes = $order->get_customer_order_notes();
?>

<div class="my-account-single-order-wrapper">
<div class="order-header">
    <div class="order-header-item">
        <p>Date</p>
        <p><?php echo date_format($order->get_date_created(), 'd.m.Y'); ?></p>
    </div>
    <div class="order-header-item">
        <p>Status</p>
        <p><?php echo $order->get_status(); ?></p>
    </div>
    <div class="order-header-item">
        <p>Returns</p>
        <svg viewBox="0 0 12 12" width="12" height="12" class="">
            <use xlink:href="#chevron-left"></use>
        </svg>
    </div>
    <div class="order-header-item">
        <p>Track my order</p>
        <svg viewBox="0 0 12 12" width="12" height="12" class="">
            <use xlink:href="#chevron-left"></use>
        </svg>
    </div>
</div>
<!--<div class="order-quantity order-quantity-single">-->
<!--    <p class="quantity-text">--><?php //echo $order->get_item_count(); ?><!-- items</p>-->
<!--</div>-->

<?php do_action('woocommerce_view_order', $order_id); ?>
