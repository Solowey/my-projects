<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_account_orders', $has_orders); ?>
<?php //var_dump($title); ?>
<?php if ($has_orders) :
    foreach ($customer_orders->orders as $customer_order) {
        $order = wc_get_order($customer_order);
        ?>

        <div class="order-item">
            <div class="order-content">
                <div class="order-heading">
                    <div class="order-id"><?php echo $order->get_id(); ?></div>
                    <div class="order-date"><?php echo date_format($order->get_date_created(), 'd.m.Y'); ?></div>
                    <div class="order-view"><a href="<?php echo esc_url($order->get_view_order_url()); ?>">View</a></div>
                </div>

                <?php
                $products = 0;

                foreach ($order->get_items() as $item_id => $item) {
                    if ($products < 2) {
                        if ($item->get_variation_id()) {

                            $product_id = $item->get_product_id();
                            $variation_id = $item->get_variation_id();
                            $product = $item->get_product();
                            $name = $item->get_name();
                            $quantity = $item->get_quantity();
                            $subtotal = $item->get_subtotal();
                            $total = $item->get_total();
                            $tax = $item->get_subtotal_tax();
                            $taxclass = $item->get_tax_class();
                            $taxstat = $item->get_tax_status();
                            $allmeta = $item->get_meta_data();
                            $somemeta = $item->get_meta('_whatever', true);
                            $type = $item->get_type();
                            $variation = wc_get_product($variation_id);
                            ?>

                            <div class="order-product">
                                <div class="product-image"><?php echo $variation->get_image(); ?></div>
                                <div class="product-info">
                                    <div class="product-main">
                                        <div class="product-name">
                                            <p><?php echo get_the_title($product_id); ?></p>
                                        </div>
                                        <div class="product-parameters">
                                            <div><p>Color: <span><?php echo get_post_meta($variation_id, 'attribute_pa_color', true); ?></span></p></div>
                                            <div><p>Size: <span><?php echo get_post_meta($variation_id, 'attribute_pa_size', true); ?></span></p></div>
                                        </div>
                                    </div>
                                    <div class="product-price-wrapper">
                                        <div class="product-quantity"><p class="product-quantity-text"><?php echo $quantity; ?>pcs</p></div>
                                        <div class="product-price"><p class="product-price-text"><?php echo number_format($total, 2, '.', ' '); ?>€</p></div>
                                    </div>
                                </div>
                            </div>

                            <?php

                        } else {
                            $product_id = $item->get_product_id();
                            $quantity = $item->get_quantity();
                            $subtotal = $item->get_subtotal();
                            $total = $item->get_total();
                            ?>

                            <div class="order-product">
                                <div class="product-image">
                                    <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" class="img-responsive"
                                         alt=""/>
                                </div>
                                <div class="product-info">
                                    <div class="product-main">
                                        <div class="product-name">
                                            <p><?php echo $item->get_name(); ?></p>
                                        </div>
                                        <div class="product-parameters"></div>
                                    </div>
                                    <div class="product-price-wrapper">
                                        <div class="product-quantity"><p class="product-quantity-text"><?php echo $quantity; ?>pcs</p></div>
                                        <div class="product-price"><p class="product-price-text"><?php echo number_format($total, 2, '.', ' '); ?>€</p></div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }

                        $products += 1;
                    }


                }

                ?>

                <div class="order-bottom">
                    <div class="order-quantity">
                        <?php
                        if ($products == 2) {
                            $count = $order->get_item_count() - 2;
                            ?>
                            <p class="order-bottom-text"><?php echo '+' . $count . ' more'; ?></p>
                            <?php
                        }

                        ?>
                    </div>
                    <div class="order-status">
                        <?php
                            $status = $order->get_status();
                        ?>
                        <p class="order-bottom-text"><?php echo $status; ?></p>
                        <?php
                            if ($status === 'completed') {
                                ?>
                                <svg viewBox="0 0 16 16" width="16" height="16">
                                    <use xlink:href="#order-completed"></use>
                                </svg>
                                <?php
                            } else {
                                ?>
                                <svg viewBox="0 0 16 16" width="16" height="16">
                                    <use xlink:href="#order-pending"></use>
                                </svg>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
endif;

do_action('woocommerce_after_account_orders', $has_orders); ?>
