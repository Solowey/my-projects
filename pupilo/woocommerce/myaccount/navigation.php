<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );

    global $current_user;

?>

<nav class="woocommerce-MyAccount-navigation my-account-menu">
    <div class="account-header">
        <p>Welcome back,</p>
        <p><?php echo get_userdata(get_current_user_id())->display_name; ?></p>
    </div>
	<ul class="account-list">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?> account-list-item">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
    <div class="help-link-wrapper">
        <a href="/faq/">Help & FAQ’s</a>
    </div>
    <div class="logout-link-wrapper">
        <a href="<?php echo wp_logout_url(home_url()); ?>">Log out</a>
    </div>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
