<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_edit_account_form'); ?>

<?php

if ($_POST['account_first_name']) {
    update_user_meta($user->ID, 'first_name', $_POST['account_first_name']);
}

if ($_POST['account_last_name']) {
    update_user_meta($user->ID, 'last_name', $_POST['account_last_name']);
}

if ($_POST['account_email']) {
    update_user_meta($user->ID, 'user_email', $_POST['account_email']);
}

if ($_POST['gender']) {
    update_user_meta($user->ID, 'gender', $_POST['gender']);
}

if ($_POST['birthdate']) {
    update_user_meta($user->ID, 'birthdate', $_POST['birthdate']);
}

if ($_POST['tel']) {
    update_user_meta($user->ID, 'tel', $_POST['tel']);
}

?>

<form class="woocommerce-EditAccountForm edit-account my-details-main-wrapper" action=""
      method="post" <?php do_action('woocommerce_edit_account_form_tag'); ?> >

    <?php do_action('woocommerce_edit_account_form_start'); ?>

    <div class="my-details-input-wrapper">
        <label for="account_first_name"><?php esc_html_e('First name', 'woocommerce'); ?>&nbsp;</label>
        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text my-details-input"
               name="account_first_name" id="account_first_name" autocomplete="given-name"
               value="<?php echo esc_attr($user->first_name); ?>" placeholder="Enter your First name"/>
    </div>
    <div class="my-details-input-wrapper">
        <label for="account_last_name"><?php esc_html_e('Last name', 'woocommerce'); ?>&nbsp;</label>
        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text my-details-input"
               name="account_last_name" id="account_last_name" autocomplete="family-name"
               value="<?php echo esc_attr($user->last_name); ?>" placeholder="Enter your Last name"/>
    </div>
    <div class="my-details-input-wrapper">
        <label for="account_email"><?php esc_html_e('Email', 'woocommerce'); ?>&nbsp;</label>
        <input type="email" class="woocommerce-Input woocommerce-Input--email input-text my-details-input"
               name="account_email" id="account_email" autocomplete="email"
               value="<?php echo get_user_meta( $user->ID, 'user_email', true ); ?>" placeholder="Enter your email"/>
    </div>

    <?php do_action('woocommerce_edit_account_form'); ?>


    <p class="my-account-save-details-button">
        <?php wp_nonce_field('save_account_details', 'save-account-details-nonce'); ?>
        <button type="submit" class="woocommerce-Button my-details-save-details" name="save_account_details"
                value="<?php esc_attr_e('Save changes', 'woocommerce'); ?>">Save details
        </button>
        <input type="hidden" name="action" value="save_account_details"/>
    </p>

    <?php woo_delete_account_button(); ?>

    <?php do_action('woocommerce_edit_account_form_end'); ?>
</form>

<?php do_action('woocommerce_after_edit_account_form'); ?>

