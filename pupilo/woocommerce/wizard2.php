<?php

?>

<div class="wizard2 display-none step-wrapper step2">
    <div class="step-heading-wrapper step2-heading-wrapper">
        <p class="step2-heading-desktop">My Prescription</p>
        <?php
        if (!is_user_logged_in()) {
            ?>
            <p class="step-signin-text">
                <button class="prescription-login-button">Sign in</button>
                to use previous prescription
            </p>
            <?php
        } else {
            $user_id = get_current_user_id();

            $prescriptions = get_user_meta($user_id, 'prescriptions', true);
            ?>
            <p class="saved-text">Saved prescriptions</p>
            <div class="saved-select-wrapper">
                <select class="saved-select">
                    <option>My prescriptions</option>
                    <?php

                    foreach ($prescriptions as $pre) {
                        $current = $pre[key($pre)];
                        ?>
                        <option value="<?php echo key($pre); ?>"
                                data-od-sph="<?php echo $current['od-sph']; ?>"
                                data-od-cyl="<?php echo $current['od-cyl']; ?>"
                                data-od-axis="<?php echo $current['od-axis']; ?>"
                                data-od-ad="<?php echo $current['od-ad']; ?>"
                                data-pd-1="<?php echo $current['pd-1']; ?>"
                                data-os-sph="<?php echo $current['os-sph']; ?>"
                                data-os-cyl="<?php echo $current['os-cyl']; ?>"
                                data-os-axis="<?php echo $current['os-axis']; ?>"
                                data-os-ad="<?php echo $current['os-ad']; ?>"
                                data-pd-2="<?php echo $current['pd-2']; ?>"
                                data-comment="<?php echo $current['comment']; ?>"
                                data-od-vertical="<?php echo $current['od-vertical']; ?>"
                                data-od-v-basedirection="<?php echo $current['od-v-basedirection']; ?>"
                                data-os-vertical="<?php echo $current['os-vertical']; ?>"
                                data-os-v-basedirection="<?php echo $current['os-v-basedirection']; ?>"
                                data-od-horizontal="<?php echo $current['od-horizontal']; ?>"
                                data-od-h-basedirection="<?php echo $current['od-h-basedirection']; ?>"
                                data-os-horizontal="<?php echo $current['os-horizontal']; ?>"
                                data-os-h-basedirection="<?php echo $current['os-h-basedirection']; ?>"
                        ><?php echo key($pre); ?></option>
                        <?php
                    }
                    ?>
                </select>
                <button class="add-new-button">Add new</button>
            </div>
            <?php
        } ?>
    </div>
    <div class="prescription-wrapper">
        <div class="prescription-table">
            <div class="title-column">
                <div class="title title-row">
                    <p>OD</p>
                    <p>Right eye</p>
                </div>
                <div class="title title-row">
                    <p>OS</p>
                    <p>Left eye</p>
                </div>
            </div>
            <div class="content-columns">
                <div class="title">
                    <p>SPH</p>
                    <p><span>(</span>Sphere<span>)</span></p>
                </div>
                <div class="title">
                    <p>CYL</p>
                    <p><span>(</span>Cylinder<span>)</span></p>
                </div>
                <div class="select-wrapper">
                    <select name="od-sph" id="od-sph">
                        <option value="-16.00">-16.00</option>
                        <option value="-15.75">-15.75</option>
                        <option value="-15.50">-15.50</option>
                        <option value="-15.25">-15.25</option>
                        <option value="-15.00">-15.00</option>
                        <option value="-14.75">-14.75</option>
                        <option value="-14.50">-14.50</option>
                        <option value="-14.25">-14.25</option>
                        <option value="-14.00">-14.00</option>
                        <option value="-13.75">-13.75</option>
                        <option value="-13.50">-13.50</option>
                        <option value="-13.25">-13.25</option>
                        <option value="-13.00">-13.00</option>
                        <option value="-12.75">-12.75</option>
                        <option value="-12.50">-12.50</option>
                        <option value="-12.25">-12.25</option>
                        <option value="-12.00">-12.00</option>
                        <option value="-11.75">-11.75</option>
                        <option value="-11.50">-11.50</option>
                        <option value="-11.25">-11.25</option>
                        <option value="-11.00">-11.00</option>
                        <option value="-10.75">-10.75</option>
                        <option value="-10.50">-10.50</option>
                        <option value="-10.25">-10.25</option>
                        <option value="-10.00">-10.00</option>
                        <option value="-9.75">-9.75</option>
                        <option value="-9.50">-9.50</option>
                        <option value="-9.25">-9.25</option>
                        <option value="-9.00">-9.00</option>
                        <option value="-8.75">-8.75</option>
                        <option value="-8.50">-8.50</option>
                        <option value="-8.25">-8.25</option>
                        <option value="-8.00">-8.00</option>
                        <option value="-7.75">-7.75</option>
                        <option value="-7.50">-7.50</option>
                        <option value="-7.25">-7.25</option>
                        <option value="-7.00">-7.00</option>
                        <option value="-6.75">-6.75</option>
                        <option value="-6.50">-6.50</option>
                        <option value="-6.25">-6.25</option>
                        <option value="-6.00">-6.00</option>
                        <option value="-5.75">-5.75</option>
                        <option value="-5.50">-5.50</option>
                        <option value="-5.25">-5.25</option>
                        <option value="-5.00">-5.00</option>
                        <option value="-4.75">-4.75</option>
                        <option value="-4.50">-4.50</option>
                        <option value="-4.25">-4.25</option>
                        <option value="-4.00">-4.00</option>
                        <option value="-3.75">-3.75</option>
                        <option value="-3.50">-3.50</option>
                        <option value="-3.25">-3.25</option>
                        <option value="-3.00">-3.00</option>
                        <option value="-2.75">-2.75</option>
                        <option value="-2.50">-2.50</option>
                        <option value="-2.25">-2.25</option>
                        <option value="-2.00">-2.00</option>
                        <option value="-1.75">-1.75</option>
                        <option value="-1.50">-1.50</option>
                        <option value="-1.25">-1.25</option>
                        <option value="-1.00">-1.00</option>
                        <option value="-0.75">-0.75</option>
                        <option value="-0.50">-0.50</option>
                        <option value="-0.25">-0.25</option>
                        <option value="0.00" selected>0.00</option>
                        <option value="0.25">0.25</option>
                        <option value="0.50">0.50</option>
                        <option value="0.75">0.75</option>
                        <option value="1.00">1.00</option>
                        <option value="1.25">1.25</option>
                        <option value="1.50">1.50</option>
                        <option value="1.75">1.75</option>
                        <option value="2.00">2.00</option>
                        <option value="2.25">2.25</option>
                        <option value="2.50">2.50</option>
                        <option value="2.75">2.75</option>
                        <option value="3.00">3.00</option>
                        <option value="3.25">3.25</option>
                        <option value="3.50">3.50</option>
                        <option value="3.75">3.75</option>
                        <option value="4.00">4.00</option>
                        <option value="4.25">4.25</option>
                        <option value="4.50">4.50</option>
                        <option value="4.75">4.75</option>
                        <option value="5.00">5.00</option>
                        <option value="5.25">5.25</option>
                        <option value="5.50">5.50</option>
                        <option value="5.75">5.75</option>
                        <option value="6.00">6.00</option>
                        <option value="6.25">6.25</option>
                        <option value="6.50">6.50</option>
                        <option value="6.75">6.75</option>
                        <option value="7.00">7.00</option>
                        <option value="7.25">7.25</option>
                        <option value="7.50">7.50</option>
                        <option value="7.75">7.75</option>
                        <option value="8.00">8.00</option>
                        <option value="8.25">8.25</option>
                        <option value="8.50">8.50</option>
                        <option value="8.75">8.75</option>
                        <option value="9.00">9.00</option>
                        <option value="9.25">9.25</option>
                        <option value="9.50">9.50</option>
                        <option value="9.75">9.75</option>
                        <option value="10.00">10.00</option>
                        <option value="10.25">10.25</option>
                        <option value="10.50">10.50</option>
                        <option value="10.75">10.75</option>
                        <option value="11.00">11.00</option>
                        <option value="11.25">11.25</option>
                        <option value="11.50">11.50</option>
                        <option value="11.75">11.75</option>
                        <option value="12.00">12.00</option>
                        <option value="12.25">12.25</option>
                        <option value="12.50">12.50</option>
                        <option value="12.75">12.75</option>
                        <option value="13.00">13.00</option>
                        <option value="13.25">13.25</option>
                        <option value="13.50">13.50</option>
                        <option value="13.75">13.75</option>
                        <option value="14.00">14.00</option>
                        <option value="14.25">14.25</option>
                        <option value="14.50">14.50</option>
                        <option value="14.75">14.75</option>
                        <option value="15.00">15.00</option>
                        <option value="15.25">15.25</option>
                        <option value="15.50">15.50</option>
                        <option value="15.75">15.75</option>
                        <option value="16.00">16.00</option>
                        <option value="16.25">16.25</option>
                        <option value="16.50">16.50</option>
                        <option value="16.75">16.75</option>
                        <option value="17.00">17.00</option>
                    </select>
                </div>
                <div class="select-wrapper">
                    <select name="od-cyl" id="od-cyl">
                        <option value="-6.00">-6.00</option>
                        <option value="-5.75">-5.75</option>
                        <option value="-5.50">-5.50</option>
                        <option value="-5.25">-5.25</option>
                        <option value="-5.00">-5.00</option>
                        <option value="-4.75">-4.75</option>
                        <option value="-4.50">-4.50</option>
                        <option value="-4.25">-4.25</option>
                        <option value="-4.00">-4.00</option>
                        <option value="-3.75">-3.75</option>
                        <option value="-3.50">-3.50</option>
                        <option value="-3.25">-3.25</option>
                        <option value="-3.00">-3.00</option>
                        <option value="-2.75">-2.75</option>
                        <option value="-2.50">-2.50</option>
                        <option value="-2.25">-2.25</option>
                        <option value="-2.00">-2.00</option>
                        <option value="-1.75">-1.75</option>
                        <option value="-1.50">-1.50</option>
                        <option value="-1.25">-1.25</option>
                        <option value="-1.00">-1.00</option>
                        <option value="-0.75">-0.75</option>
                        <option value="-0.50">-0.50</option>
                        <option value="-0.25">-0.25</option>
                        <option value="0.00" selected>0.00</option>
                        <option value="0.25">0.25</option>
                        <option value="0.50">0.50</option>
                        <option value="0.75">0.75</option>
                        <option value="1.00">1.00</option>
                        <option value="1.25">1.25</option>
                        <option value="1.50">1.50</option>
                        <option value="1.75">1.75</option>
                        <option value="2.00">2.00</option>
                        <option value="2.25">2.25</option>
                        <option value="2.50">2.50</option>
                        <option value="2.75">2.75</option>
                        <option value="3.00">3.00</option>
                        <option value="3.25">3.25</option>
                        <option value="3.50">3.50</option>
                        <option value="3.75">3.75</option>
                        <option value="4.00">4.00</option>
                        <option value="4.25">4.25</option>
                        <option value="4.50">4.50</option>
                        <option value="4.75">4.75</option>
                        <option value="5.00">5.00</option>
                        <option value="5.25">5.25</option>
                        <option value="5.50">5.50</option>
                        <option value="5.75">5.75</option>
                        <option value="6.00">6.00</option>
                    </select>
                </div>
                <div class="select-wrapper">
                    <select name="os-sph" id="os-sph">
                        <option value="-16.00">-16.00</option>
                        <option value="-15.75">-15.75</option>
                        <option value="-15.50">-15.50</option>
                        <option value="-15.25">-15.25</option>
                        <option value="-15.00">-15.00</option>
                        <option value="-14.75">-14.75</option>
                        <option value="-14.50">-14.50</option>
                        <option value="-14.25">-14.25</option>
                        <option value="-14.00">-14.00</option>
                        <option value="-13.75">-13.75</option>
                        <option value="-13.50">-13.50</option>
                        <option value="-13.25">-13.25</option>
                        <option value="-13.00">-13.00</option>
                        <option value="-12.75">-12.75</option>
                        <option value="-12.50">-12.50</option>
                        <option value="-12.25">-12.25</option>
                        <option value="-12.00">-12.00</option>
                        <option value="-11.75">-11.75</option>
                        <option value="-11.50">-11.50</option>
                        <option value="-11.25">-11.25</option>
                        <option value="-11.00">-11.00</option>
                        <option value="-10.75">-10.75</option>
                        <option value="-10.50">-10.50</option>
                        <option value="-10.25">-10.25</option>
                        <option value="-10.00">-10.00</option>
                        <option value="-9.75">-9.75</option>
                        <option value="-9.50">-9.50</option>
                        <option value="-9.25">-9.25</option>
                        <option value="-9.00">-9.00</option>
                        <option value="-8.75">-8.75</option>
                        <option value="-8.50">-8.50</option>
                        <option value="-8.25">-8.25</option>
                        <option value="-8.00">-8.00</option>
                        <option value="-7.75">-7.75</option>
                        <option value="-7.50">-7.50</option>
                        <option value="-7.25">-7.25</option>
                        <option value="-7.00">-7.00</option>
                        <option value="-6.75">-6.75</option>
                        <option value="-6.50">-6.50</option>
                        <option value="-6.25">-6.25</option>
                        <option value="-6.00">-6.00</option>
                        <option value="-5.75">-5.75</option>
                        <option value="-5.50">-5.50</option>
                        <option value="-5.25">-5.25</option>
                        <option value="-5.00">-5.00</option>
                        <option value="-4.75">-4.75</option>
                        <option value="-4.50">-4.50</option>
                        <option value="-4.25">-4.25</option>
                        <option value="-4.00">-4.00</option>
                        <option value="-3.75">-3.75</option>
                        <option value="-3.50">-3.50</option>
                        <option value="-3.25">-3.25</option>
                        <option value="-3.00">-3.00</option>
                        <option value="-2.75">-2.75</option>
                        <option value="-2.50">-2.50</option>
                        <option value="-2.25">-2.25</option>
                        <option value="-2.00">-2.00</option>
                        <option value="-1.75">-1.75</option>
                        <option value="-1.50">-1.50</option>
                        <option value="-1.25">-1.25</option>
                        <option value="-1.00">-1.00</option>
                        <option value="-0.75">-0.75</option>
                        <option value="-0.50">-0.50</option>
                        <option value="-0.25">-0.25</option>
                        <option value="0.00" selected>0.00</option>
                        <option value="0.25">0.25</option>
                        <option value="0.50">0.50</option>
                        <option value="0.75">0.75</option>
                        <option value="1.00">1.00</option>
                        <option value="1.25">1.25</option>
                        <option value="1.50">1.50</option>
                        <option value="1.75">1.75</option>
                        <option value="2.00">2.00</option>
                        <option value="2.25">2.25</option>
                        <option value="2.50">2.50</option>
                        <option value="2.75">2.75</option>
                        <option value="3.00">3.00</option>
                        <option value="3.25">3.25</option>
                        <option value="3.50">3.50</option>
                        <option value="3.75">3.75</option>
                        <option value="4.00">4.00</option>
                        <option value="4.25">4.25</option>
                        <option value="4.50">4.50</option>
                        <option value="4.75">4.75</option>
                        <option value="5.00">5.00</option>
                        <option value="5.25">5.25</option>
                        <option value="5.50">5.50</option>
                        <option value="5.75">5.75</option>
                        <option value="6.00">6.00</option>
                        <option value="6.25">6.25</option>
                        <option value="6.50">6.50</option>
                        <option value="6.75">6.75</option>
                        <option value="7.00">7.00</option>
                        <option value="7.25">7.25</option>
                        <option value="7.50">7.50</option>
                        <option value="7.75">7.75</option>
                        <option value="8.00">8.00</option>
                        <option value="8.25">8.25</option>
                        <option value="8.50">8.50</option>
                        <option value="8.75">8.75</option>
                        <option value="9.00">9.00</option>
                        <option value="9.25">9.25</option>
                        <option value="9.50">9.50</option>
                        <option value="9.75">9.75</option>
                        <option value="10.00">10.00</option>
                        <option value="10.25">10.25</option>
                        <option value="10.50">10.50</option>
                        <option value="10.75">10.75</option>
                        <option value="11.00">11.00</option>
                        <option value="11.25">11.25</option>
                        <option value="11.50">11.50</option>
                        <option value="11.75">11.75</option>
                        <option value="12.00">12.00</option>
                        <option value="12.25">12.25</option>
                        <option value="12.50">12.50</option>
                        <option value="12.75">12.75</option>
                        <option value="13.00">13.00</option>
                        <option value="13.25">13.25</option>
                        <option value="13.50">13.50</option>
                        <option value="13.75">13.75</option>
                        <option value="14.00">14.00</option>
                        <option value="14.25">14.25</option>
                        <option value="14.50">14.50</option>
                        <option value="14.75">14.75</option>
                        <option value="15.00">15.00</option>
                        <option value="15.25">15.25</option>
                        <option value="15.50">15.50</option>
                        <option value="15.75">15.75</option>
                        <option value="16.00">16.00</option>
                        <option value="16.25">16.25</option>
                        <option value="16.50">16.50</option>
                        <option value="16.75">16.75</option>
                        <option value="17.00">17.00</option>
                    </select>
                </div>
                <div class="select-wrapper">
                    <select name="os-cyl" id="os-cyl">
                        <option value="-6.00">-6.00</option>
                        <option value="-5.75">-5.75</option>
                        <option value="-5.50">-5.50</option>
                        <option value="-5.25">-5.25</option>
                        <option value="-5.00">-5.00</option>
                        <option value="-4.75">-4.75</option>
                        <option value="-4.50">-4.50</option>
                        <option value="-4.25">-4.25</option>
                        <option value="-4.00">-4.00</option>
                        <option value="-3.75">-3.75</option>
                        <option value="-3.50">-3.50</option>
                        <option value="-3.25">-3.25</option>
                        <option value="-3.00">-3.00</option>
                        <option value="-2.75">-2.75</option>
                        <option value="-2.50">-2.50</option>
                        <option value="-2.25">-2.25</option>
                        <option value="-2.00">-2.00</option>
                        <option value="-1.75">-1.75</option>
                        <option value="-1.50">-1.50</option>
                        <option value="-1.25">-1.25</option>
                        <option value="-1.00">-1.00</option>
                        <option value="-0.75">-0.75</option>
                        <option value="-0.50">-0.50</option>
                        <option value="-0.25">-0.25</option>
                        <option value="0.00" selected>0.00</option>
                        <option value="0.25">0.25</option>
                        <option value="0.50">0.50</option>
                        <option value="0.75">0.75</option>
                        <option value="1.00">1.00</option>
                        <option value="1.25">1.25</option>
                        <option value="1.50">1.50</option>
                        <option value="1.75">1.75</option>
                        <option value="2.00">2.00</option>
                        <option value="2.25">2.25</option>
                        <option value="2.50">2.50</option>
                        <option value="2.75">2.75</option>
                        <option value="3.00">3.00</option>
                        <option value="3.25">3.25</option>
                        <option value="3.50">3.50</option>
                        <option value="3.75">3.75</option>
                        <option value="4.00">4.00</option>
                        <option value="4.25">4.25</option>
                        <option value="4.50">4.50</option>
                        <option value="4.75">4.75</option>
                        <option value="5.00">5.00</option>
                        <option value="5.25">5.25</option>
                        <option value="5.50">5.50</option>
                        <option value="5.75">5.75</option>
                        <option value="6.00">6.00</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="prescription-table">
            <div class="title-column">
                <div class="title title-row">
                    <p>OD</p>
                    <p>Right eye</p>
                </div>
                <div class="title title-row">
                    <p>OS</p>
                    <p>Left eye</p>
                </div>
            </div>
            <div class="content-columns">
                <div class="title">
                    <p>Axis</p>
                </div>
                <div class="title">
                    <p>ADD</p>
                </div>
                <div class="input-wrapper">
                    <input type="number" name="od-axis" id="od-axis" min="2" max="180">
                </div>
                <div class="select-wrapper">
                    <select name="od-ad" id="od-ad">
                        <option value="n/a">n/a</option>
                        <option value="+1.00">+1.00</option>
                        <option value="+1.25">+1.25</option>
                        <option value="+1.50">+1.50</option>
                        <option value="+1.75">+1.75</option>
                        <option value="+2.00">+2.00</option>
                        <option value="+2.25">+2.25</option>
                        <option value="+2.50">+2.50</option>
                        <option value="+2.75">+2.75</option>
                        <option value="+3.00">+3.00</option>
                        <option value="+3.25">+3.25</option>
                        <option value="+3.50">+3.50</option>
                    </select>
                </div>
                <div class="input-wrapper">
                    <input type="number" name="os-axis" id="os-axis" min="2" max="180">
                </div>
                <div class="select-wrapper">
                    <select name="os-ad" id="os-ad">
                        <option value="n/a">n/a</option>
                        <option value="+1.00">+1.00</option>
                        <option value="+1.25">+1.25</option>
                        <option value="+1.50">+1.50</option>
                        <option value="+1.75">+1.75</option>
                        <option value="+2.00">+2.00</option>
                        <option value="+2.25">+2.25</option>
                        <option value="+2.50">+2.50</option>
                        <option value="+2.75">+2.75</option>
                        <option value="+3.00">+3.00</option>
                        <option value="+3.25">+3.25</option>
                        <option value="+3.50">+3.50</option>
                    </select>
                </div>
            </div>
        </div>
        <button class="prescription-modal-button" data-bs-toggle="modal" data-bs-target="#descriptionPrescription">
            <svg viewBox="0 0 17 17" width="17" height="17" class="">
                <use xlink:href="#modal-i"></use>
            </svg>
        </button>
    </div>
    <div class="prescription-pupillary">
        <p class="prescription-pupillary-text">Pupillary Distance</p>
        <button class="prescription-modal-button" data-bs-toggle="modal" data-bs-target="#pupillaryDistance">
            <svg viewBox="0 0 17 17" width="17" height="17" class="">
                <use xlink:href="#modal-i"></use>
            </svg>
        </button>
    </div>
    <div class="pupillary-info">
        <div class="pupillary-title">
            <p class="pupillary-title-title">PD</p>
            <p class="pupilarity-subtitle">Pupillary Distance</p>
        </div>
        <div class="pupillary-selects">
            <div class="select-wrapper">
                <select name="pd-1" id="pd-1">
                    <option value="Right PD">Right PD</option>
                    <option value="23.0">23.0</option>
                    <option value="23.5">23.5</option>
                    <option value="24.0">24.0</option>
                    <option value="24.5">24.5</option>
                    <option value="25.0">25.0</option>
                    <option value="25.5">25.5</option>
                    <option value="26.0">26.0</option>
                    <option value="26.5">26.5</option>
                    <option value="27.0">27.0</option>
                    <option value="27.5">27.5</option>
                    <option value="28.0">28.0</option>
                    <option value="28.5">28.5</option>
                    <option value="29.0">29.0</option>
                    <option value="29.5">29.5</option>
                    <option value="30.0">30.0</option>
                    <option value="30.5">30.5</option>
                    <option value="31.0">31.0</option>
                    <option value="31.5">31.5</option>
                    <option value="32.0">32.0</option>
                    <option value="32.5">32.5</option>
                    <option value="33.0">33.0</option>
                    <option value="33.5">33.5</option>
                    <option value="34.0">34.0</option>
                    <option value="34.5">34.5</option>
                    <option value="35.0">35.0</option>
                    <option value="35.5">35.5</option>
                    <option value="36.0">36.0</option>
                    <option value="36.5">36.5</option>
                    <option value="37.0">37.0</option>
                    <option value="37.5">37.5</option>
                    <option value="38.0">38.0</option>
                    <option value="38.5">38.5</option>
                    <option value="39.0">39.0</option>
                    <option value="39.5">39.5</option>
                    <option value="40.0">40.0</option>
                </select>
            </div>
            <div class="select-wrapper select-wrapper-left display-none">
                <select name="pd-2" id="pd-2" class="display-none">
                    <option value="Left PD" selected>Left PD</option>
                    <option value="25.0">25.0</option>
                    <option value="25.5">25.5</option>
                    <option value="26.0">26.0</option>
                    <option value="26.5">26.5</option>
                    <option value="27.0">27.0</option>
                    <option value="27.5">27.5</option>
                    <option value="28.0">28.0</option>
                    <option value="28.5">28.5</option>
                    <option value="29.0">29.0</option>
                    <option value="29.5">29.5</option>
                    <option value="30.0">30.0</option>
                    <option value="30.5">30.5</option>
                    <option value="31.0">31.0</option>
                    <option value="31.5">31.5</option>
                    <option value="32.0">32.0</option>
                    <option value="32.5">32.5</option>
                    <option value="33.0">33.0</option>
                    <option value="33.5">33.5</option>
                    <option value="34.0">34.0</option>
                    <option value="34.5">34.5</option>
                    <option value="35.0">35.0</option>
                    <option value="35.5">35.5</option>
                    <option value="36.0">36.0</option>
                    <option value="36.5">36.5</option>
                    <option value="37.0">37.0</option>
                    <option value="37.5">37.5</option>
                    <option value="38.0">38.0</option>
                    <option value="38.5">38.5</option>
                    <option value="39.0">39.0</option>
                    <option value="39.5">39.5</option>
                    <option value="40.0">40.0</option>
                </select>
            </div>
        </div>
    </div>
    <div class="pupillary-checkbox">
<!--        <label>-->
<!--            <input type="checkbox" name="two-pd" id="two-pd">-->
<!--            Two PD Numbers-->
<!--        </label>-->
        <label class="custom-checkbox order-check-wrapper">
            <input type="checkbox" name="two-pd" id="two-pd">
            <span class="text">Two PD Numbers</span>
        </label>
    </div>
    <div class="open-additional-prescriptions">
        <p class="text">Show more options</p>
        <svg viewBox="0 0 8 14" width="8" height="14" class="">
            <use xlink:href="#chevron-left"></use>
        </svg>
    </div>
    <div class="prescription-additional-wrapper display-none">
        <div class="prescription-comment">
            <p class="comment-text">Prescription comments</p>
            <textarea name="comment" id="comment" placeholder="Enter Your comments"></textarea>
            <label class="save-new-pre-label display-none" for="new-name">
                Save this prescription as:
                <input type="text" id="new-name" placeholder="">
            </label>
        </div>
        <div class="add-prism-checkbox">
            <label class="custom-checkbox order-check-wrapper">
                <input type="checkbox" name="add-prism" id="add-prism">
                <span class="text">Add prism&nbsp;<b class="prism-weight">+19€</b></span>
            </label>
<!--            <label class="add-prism-text">-->
<!--                <input type="checkbox" name="add-prism" id="add-prism">-->
<!--                Add prism&nbsp;<span class="prism-weight">+19€</span>-->
<!--            </label>-->
        </div>
        <div class="prescription-wrapper prism-wrapper display-none">
            <div class="prescription-table">
                <div class="title-column">
                    <div class="title title-row">
                        <p>OD</p>
                        <p>Right eye</p>
                    </div>
                    <div class="title title-row">
                        <p>OS</p>
                        <p>Left eye</p>
                    </div>
                </div>
                <div class="content-columns">
                    <div class="title">
                        <p>Vertical △</p>
                    </div>
                    <div class="title">
                        <p>Base Direction</p>
                    </div>
                    <div class="select-wrapper">
                        <select name="od-vertical" id="od-vertical">
                            <option value="n/a">n/a</option>
                            <option value="0.50">0.50</option>
                            <option value="1.00">1.00</option>
                            <option value="1.50">1.50</option>
                            <option value="2.00">2.00</option>
                            <option value="2.50">2.50</option>
                            <option value="3.00">3.00</option>
                            <option value="3.50">3.50</option>
                            <option value="4.00">4.00</option>
                            <option value="4.50">4.50</option>
                            <option value="5.00">5.00</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="od-v-basedirection" id="od-v-basedirection">
                            <option value="n/a">n/a</option>
                            <option value="Up">Up</option>
                            <option value="Down">Down</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="os-vertical" id="os-vertical">
                            <option value="n/a">n/a</option>
                            <option value="0.50">0.50</option>
                            <option value="1.00">1.00</option>
                            <option value="1.50">1.50</option>
                            <option value="2.00">2.00</option>
                            <option value="2.50">2.50</option>
                            <option value="3.00">3.00</option>
                            <option value="3.50">3.50</option>
                            <option value="4.00">4.00</option>
                            <option value="4.50">4.50</option>
                            <option value="5.00">5.00</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="os-v-basedirection" id="os-v-basedirection">
                            <option value="n/a">n/a</option>
                            <option value="Up">Up</option>
                            <option value="Down">Down</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="prescription-table">
                <div class="title-column">
                    <div class="title title-row">
                        <p>OD</p>
                        <p>Right eye</p>
                    </div>
                    <div class="title title-row">
                        <p>OS</p>
                        <p>Left eye</p>
                    </div>
                </div>
                <div class="content-columns">
                    <div class="title">
                        <p>Horizontal △</p>
                    </div>
                    <div class="title">
                        <p>Base Direction</p>
                    </div>
                    <div class="select-wrapper">
                        <select name="od-horizontal" id="od-horizontal">
                            <option value="n/a">n/a</option>
                            <option value="0.50">0.50</option>
                            <option value="1.00">1.00</option>
                            <option value="1.50">1.50</option>
                            <option value="2.00">2.00</option>
                            <option value="2.50">2.50</option>
                            <option value="3.00">3.00</option>
                            <option value="3.50">3.50</option>
                            <option value="4.00">4.00</option>
                            <option value="4.50">4.50</option>
                            <option value="5.00">5.00</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="od-h-basedirection" id="od-h-basedirection">
                            <option value="n/a">n/a</option>
                            <option value="Up">In</option>
                            <option value="Down">Out</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="os-horizontal" id="os-horizontal">
                            <option value="n/a">n/a</option>
                            <option value="0.50">0.50</option>
                            <option value="1.00">1.00</option>
                            <option value="1.50">1.50</option>
                            <option value="2.00">2.00</option>
                            <option value="2.50">2.50</option>
                            <option value="3.00">3.00</option>
                            <option value="3.50">3.50</option>
                            <option value="4.00">4.00</option>
                            <option value="4.50">4.50</option>
                            <option value="5.00">5.00</option>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="os-h-basedirection" id="os-h-basedirection">
                            <option value="n/a">n/a</option>
                            <option value="Up">In</option>
                            <option value="Down">Out</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wizard-subtotal-mobile wizard2-subtotal">
        <p>Subtotal:</p>
        <p class="current-price"></p>
    </div>
    <div class="wizard2-bottom-buttons">
        <button class="wizard2-back-button-desktop">Back</button>
        <button class="continue-button wizard2-continue">Select and continue</button>
    </div>
</div>
