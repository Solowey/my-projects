<?php

?>

<div class="wizard3">
    <div class="step-heading-wrapper display-none">
        <p class="step-heading-text">Select lens color type</p>
    </div>
    <div class="sm-distancereading display-none step-wrapper step31">
        <div class="wizard-item">
            <input type="radio" id="clear" name="sm-distancereading" value="clear" class="radio-hide"
                <?php
                if (get_field('distance_reading_clear_price', 'options')) {
                    echo 'data-ad="' . get_field('distance_reading_clear_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="clear">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/clear.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Clear
                        <?php
                        if (get_field('distance_reading_clear_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('distance_reading_clear_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('distance_reading_clear_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_distance_reading_clear">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_distance_reading_clear">
                        <?php echo get_field('distance_reading_clear_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="blue-light" name="sm-distancereading" value="blue-light" class="radio-hide"
                <?php
                if (get_field('distance_reading_blue_light_price', 'options')) {
                    echo 'data-ad="' . get_field('distance_reading_blue_light_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="blue-light">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/bluelightblocking.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Blue light blocking
                        <?php
                        if (get_field('distance_reading_blue_light_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('distance_reading_blue_light_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('distance_reading_blue_light_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_distance_reading_blue_light">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_distance_reading_blue_light">
                        <?php echo get_field('distance_reading_blue_light_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="sun" name="sm-distancereading" value="sun" class="radio-hide"
                <?php
                if (get_field('distance_reading_sun_price', 'options')) {
                    echo 'data-ad="' . get_field('distance_reading_sun_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="sun">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/sun.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Sun
                        <?php
                        if (get_field('distance_reading_sun_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('distance_reading_sun_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('distance_reading_sun_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_distance_reading_sun">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_distance_reading_sun">
                        <?php echo get_field('distance_reading_sun_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="light-ajusting" name="sm-distancereading" value="light-ajusting" class="radio-hide"
                <?php
                if (get_field('distance_reading_light_ajusting_price', 'options')) {
                    echo 'data-ad="' . get_field('distance_reading_light_ajusting_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="light-ajusting">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/ajusting.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Light ajusting
                        <?php
                        if (get_field('distance_reading_light_ajusting_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('distance_reading_light_ajusting_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('distance_reading_light_ajusting_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_distance_reading_light_ajusting">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_distance_reading_light_ajusting">
                        <?php echo get_field('distance_reading_light_ajusting_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>
    </div>
    <div class="sm-multifocal display-none step-wrapper step32">
        <div class="wizard-item">
            <input type="radio" id="multifocal-clear" name="sm-multifocal" value="multifocal-clear" class="radio-hide"
                <?php
                if (get_field('multifocal_clear_price', 'options')) {
                    echo 'data-ad="' . get_field('multifocal_clear_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="multifocal-clear">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/clear.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Clear
                        <?php
                        if (get_field('multifocal_clear_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('multifocal_clear_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('multifocal_clear_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_clear">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_clear">
                        <?php echo get_field('multifocal_clear_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="multifocal-blue-light" name="sm-multifocal" value="multifocal-blue-light" class="radio-hide"
                <?php
                if (get_field('multifocal_blue_light_price', 'options')) {
                    echo 'data-ad="' . get_field('multifocal_blue_light_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="multifocal-blue-light">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/bluelightblocking.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Blue light blocking
                        <?php
                        if (get_field('multifocal_blue_light_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('multifocal_blue_light_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('multifocal_blue_light_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_blue_light">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_blue_light">
                        <?php echo get_field('multifocal_blue_light_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="multifocal-sun" name="sm-multifocal" value="multifocal-sun" class="radio-hide"
                <?php
                if (get_field('multifocal_sun_price', 'options')) {
                    echo 'data-ad="' . get_field('multifocal_sun_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="multifocal-sun">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/sun.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Sun
                        <?php
                        if (get_field('multifocal_sun_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('multifocal_sun_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('multifocal_sun_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_sun">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_sun">
                        <?php echo get_field('multifocal_sun_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="multifocal-light-ajusting" name="sm-multifocal" value="multifocal-light-ajusting" class="radio-hide"
                <?php
                if (get_field('multifocal_light_ajusting_price', 'options')) {
                    echo 'data-ad="' . get_field('multifocal_light_ajusting_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="multifocal-light-ajusting">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/ajusting.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Light ajusting
                        <?php
                        if (get_field('multifocal_light_ajusting_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('multifocal_light_ajusting_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('multifocal_light_ajusting_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_light_ajusting">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_light_ajusting">
                        <?php echo get_field('multifocal_light_ajusting_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>
    </div>
    <div class="sm-driving-single display-none step-wrapper step33">
        <div class="wizard-item">
            <input type="radio" id="driving-single-clear" name="sm-driving-single" value="driving-single-clear" class="radio-hide"
                <?php
                if (get_field('driving_clear_price', 'options')) {
                    echo 'data-ad="' . get_field('driving_clear_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="driving-single-clear">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/clear.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Clear
                        <?php
                        if (get_field('driving_clear_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('driving_clear_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('driving_clear_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving_clear">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_driving_clear">
                        <?php echo get_field('driving_clear_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="driving-single-sun" name="sm-driving-single" value="driving-single-sun" class="radio-hide"
                <?php
                if (get_field('driving_sun_price', 'options')) {
                    echo 'data-ad="' . get_field('driving_sun_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="driving-single-sun">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/sun.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Driving + Sun
                        <?php
                        if (get_field('driving_sun_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('driving_sun_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('driving_sun_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving_sun">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_driving_sun">
                        <?php echo get_field('driving_sun_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="driving-single-light-ajusting" name="sm-driving-single" value="driving-single-light-ajusting" class="radio-hide"
                <?php
                if (get_field('driving_light_ajusting_price', 'options')) {
                    echo 'data-ad="' . get_field('driving_light_ajusting_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="driving-single-light-ajusting">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/ajusting.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Driving + Light ajusting
                        <?php
                        if (get_field('driving_light_ajusting_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('driving_light_ajusting_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('driving_light_ajusting_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving_light_ajusting">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_driving_light_ajusting">
                        <?php echo get_field('driving_light_ajusting_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>
    </div>
    <div class="sm-non-prescriptions display-none step-wrapper step34">
        <div class="wizard-item">
            <input type="radio" id="clear3" name="sm-distancereading" value="clear" class="radio-hide"
                <?php
                if (get_field('non_prescriptions_clear_price', 'options')) {
                    echo 'data-ad="' . get_field('non_prescriptions_clear_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="clear3">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/clear.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Clear
                        <?php
                        if (get_field('non_prescriptions_clear_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('non_prescriptions_clear_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('non_prescriptions_clear_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_non_prescriptions">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_non_prescriptions">
                        <?php echo get_field('non_prescriptions_clear_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>

        <div class="wizard-item">
            <input type="radio" id="blue-light3" name="sm-distancereading" value="blue-light" class="radio-hide"
                <?php
                if (get_field('non_prescriptions_blue_light_price', 'options')) {
                    echo 'data-ad="' . get_field('non_prescriptions_blue_light_price', 'options') . '"';
                }
                ?>
            >
            <label class="wizard-item-label" for="blue-light3">
                <div class="label-image">
                    <img class="play-video-button"
                         src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/bluelightblocking.png"
                         alt="Clear">
                </div>
                <div class="label-info">
                    <p>Blue light blocking
                        <?php
                        if (get_field('non_prescriptions_blue_light_price', 'options')) {
                            ?>
                            <span> + <?php echo get_field('non_prescriptions_blue_light_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                    </p>
                    <p><?php echo get_field('non_prescriptions_blue_light_text', 'options'); ?></p>
                </div>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_non_prescriptions_blue_light">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_non_prescriptions_blue_light">
                        <?php echo get_field('non_prescriptions_blue_light_tooltip', 'options'); ?>
                    </span>
                </div>
            </label>
        </div>
    </div>
    <div class="wizard-subtotal-mobile wizard3-subtotal display-none">
        <p>Subtotal:</p>
        <p class="current-price"></p>
    </div>
    <div class="wizard2-bottom-buttons display-none">
        <button type="button" class="wizard2-back-button-desktop">Back</button>
    </div>
</div>
