<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<?php

global $post;
global $product;
global $wpdb;

$cate = get_queried_object();
$cateID = $cate->term_id;
$term = get_term_by('ID', $cateID, 'product_cat');

$loop = new WP_Query(array(
    'post_type' => 'product',
    'posts_per_page' => 32,
    'orderby' => 'date',
    'product_cat' => $term->slug,
));

$allProducts = $loop->found_posts;

$thumbnail_id = get_term_meta( $cateID, 'thumbnail_id', true );
$thumbnail_link = wp_get_attachment_url($thumbnail_id);


$c_fields = get_field('c_category', 'options');

foreach ($c_fields as $c_field) {
    if ($c_field['category'] === $cateID) {
        $img = $c_field['image'];
        $descr = $c_field['description'];
    }
}


//var_dump($term);
//var_dump($thumbnail_link);

//$query = new WP_Query(array(
//    'post_type' => 'post',
//    'posts_per_page' => 3,
//    'orderby' => 'date',
//));
?>
<main id="main" class="category-page" role="main">
    <section class="category-intro-section">
        <?php
            if (isset($img)) {
                ?>
                <div class="banner">
                    <img class="desktop-banner" src="<?php echo $img; ?>" alt="">
                    <img class="mobile-banner" src="<?php echo $img; ?>" alt="">
                    <div class="banner-info">
                        <div class="banner-text">
                            <?php
                            if (isset($descr)) {
                                echo $descr;
                            }
                            ?>
                            <!--                    <h1 class="banner-header">--><?php //echo get_field('banner_title') ?><!--</h1>-->
                            <!--                    <p class="description">--><?php //echo get_field('banner_description') ?><!--</p>-->
                            <!--                    <p class="quiz">-->
                            <!--                        <span class="quiz-bold">-->
                            <!--                            --><?php //echo get_field('banner_description_2') ?>
                            <!--                        </span>-->
                            <!--                    </p>-->
                        </div>
                    </div>
                </div>
        <?php
            }
        ?>
    </section>
    <div class="filter">
        <div class="filter-container">
            <div class="filter-wrap">
                <?php echo do_shortcode('[yith_wcan_filters slug="default-preset"]') ?>
            </div>
            <div class="reset">
                <?php echo do_shortcode('[yith_wcan_reset_button]') ?>
            </div>
        </div>
        <div class="filter-button-wrap">
            <button id="sortBy" class="sort-by main-modal-button">Sort by</button>
            <?php echo do_shortcode('[yith_wcan_mobile_modal_opener]') ?>
        </div>
    </div>
    <div class="page-container" data-category="<?php echo $term->slug; ?>">
        <div class="orderby">
            <?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?>
        </div>
        <div class="info-panel">

            <div class="headline breadcrumbs">
<!--                --><?php //if (function_exists('breadcrumbs')) breadcrumbs(); ?>
                <?php woocommerce_breadcrumb(); ?>
                <h2 class="page-title"><?php echo $term->name; ?></h2>
            </div>

            <div class="mobile-grid-button">
                <a class="toggle-grid col_2">
                    <span class="vertical-line"></span>
                    <span class="vertical-line"></span>
                </a>
                <a class="toggle-grid col_1 active-grid-button">
                    <span class="vertical-line"></span>
                </a>
            </div>
            <div class="product-sort">
                <div class="sort"><?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?></div>
                <div class="desktop grid-button">
                    <a class="toggle-grid col_4">
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                    </a>
                    <a class="toggle-grid col_3 active-grid-button">
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row product-row" data-all-posts="<?php echo $allProducts ?>"

             data-page="<?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>"
             data-max="<?php echo $loop->max_num_pages; ?>">
            <?php $counter = 0 ?>
            <?php while ($loop->have_posts()): $loop->the_post(); ?>

                <?php get_template_part('template-parts/content', 'category'); ?>

                <?php
                $counter++;
                if ($counter == 3) : ?>

                    <?php get_template_part('template-parts/try-on') ?>

                <?php endif ?>
            <?php endwhile ?>
        </div>

        <div class="load-more">
            <div class="load-more_text">You have viewed <span id="viewedPosts"></span> of
                <span><?php echo $allProducts ?></span> products
            </div>
            <div class="load-more_line">
                <div class="load-more_progress"></div>
            </div>
            <button id="load_more" class="load-more_btn">Load <?php echo ($allProducts - 3 >= 3) ? 3 : $allProducts % 3; ?> more</button>
        </div>
    </div>

    <section class="featured-article">
        <div class="page-container">
            <?php
            $post_query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'category_name' => 'Featured',
                'ignore_sticky_posts' => true,
            ));
            ?>
            <h2 class="featured-article-header">Featured articles</h2>
            <a class="featured-article-link" href="<?php echo get_site_url() ?>/blog/category/featured/">Good to know
                <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
            </a>
            <div class="featured-articles row">
                <?php if ($post_query->have_posts()) : ?>

                    <?php while ($post_query->have_posts()) : $post_query->the_post(); ?>

                        <div class="article col-md-4">
                            <div class="article-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div>
                                <p class="name">
                                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                </p>
                                <p class="description">
                                    <?php echo mb_strimwidth(get_the_excerpt(), 0, 145, '...') ?>
                                </p>
                            </div>
                        </div>

                    <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php endif; ?>

            </div>
            <a class="blog-link" href="/blog">View more articles</a>
        </div>
    </section>

    <section class="recently-product-section">
        <?php get_template_part('template-parts/content', 'recently-product') ?>
    </section>

    <div class="enter-you-prescription-modal-overlay">
        <div class="enter-you-prescription-modal">
            <div class="modal-headline">
                <a class="button-close-prescription-modal">
                    <svg class="right-close" width="24" height="24">
                        <use xlink:href="#cart-close">

                        </use>
                    </svg>
                    <svg class="left-close" width="8" height="14">
                        <use xlink:href="#chevron-left">

                        </use>
                    </svg>
                </a>

                <p class="text-heading">Enter Your Prescriptions</p>
                <p class="filter-prescription-preheader">We will show you only the frames available for your
                    prescription</p>
                <form method="post" class="add-prescription-form tab1">
                    <label for="prescription_name">Prescription name</label>
                    <input type="text" id="prescription_name" name="prescription_name"
                           placeholder="Reading, work, driving etc...">
                    <div class="top-button-wrapper">
                        <button class="prescription-modal-button tooltip-hover"
                                title="Tooltip1" type="button">
                            <svg viewBox="0 0 17 17" width="17" height="17" class="">
                                <use xlink:href="#modal-i"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="wizard2">
                        <div class="prescription-wrapper">

                            <div class="prescription-table">
                                <div class="title-column">
                                    <div class="title title-row">
                                        <p>OD</p>
                                        <p>Right eye</p>
                                    </div>
                                    <div class="title title-row">
                                        <p>OS</p>
                                        <p>Left eye</p>
                                    </div>
                                </div>
                                <div class="content-columns">
                                    <div class="title">
                                        <p>SPH</p>
                                        <p>&nbsp;(Sphere)</p>
                                    </div>
                                    <div class="title">
                                        <p>CYL</p>
                                        <p>&nbsp;(Cylinder)</p>
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="od-sph" id="od-sph">
                                            <option value="-16.00">-16.00</option>
                                            <option value="-15.75">-15.75</option>
                                            <option value="-15.50">-15.50</option>
                                            <option value="-15.25">-15.25</option>
                                            <option value="-15.00">-15.00</option>
                                            <option value="-14.75">-14.75</option>
                                            <option value="-14.50">-14.50</option>
                                            <option value="-14.25">-14.25</option>
                                            <option value="-14.00">-14.00</option>
                                            <option value="-13.75">-13.75</option>
                                            <option value="-13.50">-13.50</option>
                                            <option value="-13.25">-13.25</option>
                                            <option value="-13.00">-13.00</option>
                                            <option value="-12.75">-12.75</option>
                                            <option value="-12.50">-12.50</option>
                                            <option value="-12.25">-12.25</option>
                                            <option value="-12.00">-12.00</option>
                                            <option value="-11.75">-11.75</option>
                                            <option value="-11.50">-11.50</option>
                                            <option value="-11.25">-11.25</option>
                                            <option value="-11.00">-11.00</option>
                                            <option value="-10.75">-10.75</option>
                                            <option value="-10.50">-10.50</option>
                                            <option value="-10.25">-10.25</option>
                                            <option value="-10.00">-10.00</option>
                                            <option value="-9.75">-9.75</option>
                                            <option value="-9.50">-9.50</option>
                                            <option value="-9.25">-9.25</option>
                                            <option value="-9.00">-9.00</option>
                                            <option value="-8.75">-8.75</option>
                                            <option value="-8.50">-8.50</option>
                                            <option value="-8.25">-8.25</option>
                                            <option value="-8.00">-8.00</option>
                                            <option value="-7.75">-7.75</option>
                                            <option value="-7.50">-7.50</option>
                                            <option value="-7.25">-7.25</option>
                                            <option value="-7.00">-7.00</option>
                                            <option value="-6.75">-6.75</option>
                                            <option value="-6.50">-6.50</option>
                                            <option value="-6.25">-6.25</option>
                                            <option value="-6.00">-6.00</option>
                                            <option value="-5.75">-5.75</option>
                                            <option value="-5.50">-5.50</option>
                                            <option value="-5.25">-5.25</option>
                                            <option value="-5.00">-5.00</option>
                                            <option value="-4.75">-4.75</option>
                                            <option value="-4.50">-4.50</option>
                                            <option value="-4.25">-4.25</option>
                                            <option value="-4.00">-4.00</option>
                                            <option value="-3.75">-3.75</option>
                                            <option value="-3.50">-3.50</option>
                                            <option value="-3.25">-3.25</option>
                                            <option value="-3.00">-3.00</option>
                                            <option value="-2.75">-2.75</option>
                                            <option value="-2.50">-2.50</option>
                                            <option value="-2.25">-2.25</option>
                                            <option value="-2.00">-2.00</option>
                                            <option value="-1.75">-1.75</option>
                                            <option value="-1.50">-1.50</option>
                                            <option value="-1.25">-1.25</option>
                                            <option value="-1.00">-1.00</option>
                                            <option value="-0.75">-0.75</option>
                                            <option value="-0.50">-0.50</option>
                                            <option value="-0.25">-0.25</option>
                                            <option value="0.00" selected>0.00</option>
                                            <option value="Plano">Plano</option>
                                            <option value="0.25">0.25</option>
                                            <option value="0.50">0.50</option>
                                            <option value="0.75">0.75</option>
                                            <option value="1.00">1.00</option>
                                            <option value="1.25">1.25</option>
                                            <option value="1.50">1.50</option>
                                            <option value="1.75">1.75</option>
                                            <option value="2.00">2.00</option>
                                            <option value="2.25">2.25</option>
                                            <option value="2.50">2.50</option>
                                            <option value="2.75">2.75</option>
                                            <option value="3.00">3.00</option>
                                            <option value="3.25">3.25</option>
                                            <option value="3.50">3.50</option>
                                            <option value="3.75">3.75</option>
                                            <option value="4.00">4.00</option>
                                            <option value="4.25">4.25</option>
                                            <option value="4.50">4.50</option>
                                            <option value="4.75">4.75</option>
                                            <option value="5.00">5.00</option>
                                            <option value="5.25">5.25</option>
                                            <option value="5.50">5.50</option>
                                            <option value="5.75">5.75</option>
                                            <option value="6.00">6.00</option>
                                            <option value="6.25">6.25</option>
                                            <option value="6.50">6.50</option>
                                            <option value="6.75">6.75</option>
                                            <option value="7.00">7.00</option>
                                            <option value="7.25">7.25</option>
                                            <option value="7.50">7.50</option>
                                            <option value="7.75">7.75</option>
                                            <option value="8.00">8.00</option>
                                            <option value="8.25">8.25</option>
                                            <option value="8.50">8.50</option>
                                            <option value="8.75">8.75</option>
                                            <option value="9.00">9.00</option>
                                            <option value="9.25">9.25</option>
                                            <option value="9.50">9.50</option>
                                            <option value="9.75">9.75</option>
                                            <option value="10.00">10.00</option>
                                            <option value="10.25">10.25</option>
                                            <option value="10.50">10.50</option>
                                            <option value="10.75">10.75</option>
                                            <option value="11.00">11.00</option>
                                            <option value="11.25">11.25</option>
                                            <option value="11.50">11.50</option>
                                            <option value="11.75">11.75</option>
                                            <option value="12.00">12.00</option>
                                        </select>
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="od-cyl" id="od-cyl">
                                            <option value="-6.00">-6.00</option>
                                            <option value="-5.75">-5.75</option>
                                            <option value="-5.50">-5.50</option>
                                            <option value="-5.25">-5.25</option>
                                            <option value="-5.00">-5.00</option>
                                            <option value="-4.75">-4.75</option>
                                            <option value="-4.50">-4.50</option>
                                            <option value="-4.25">-4.25</option>
                                            <option value="-4.00">-4.00</option>
                                            <option value="-3.75">-3.75</option>
                                            <option value="-3.50">-3.50</option>
                                            <option value="-3.25">-3.25</option>
                                            <option value="-3.00">-3.00</option>
                                            <option value="-2.75">-2.75</option>
                                            <option value="-2.50">-2.50</option>
                                            <option value="-2.25">-2.25</option>
                                            <option value="-2.00">-2.00</option>
                                            <option value="-1.75">-1.75</option>
                                            <option value="-1.50">-1.50</option>
                                            <option value="-1.25">-1.25</option>
                                            <option value="-1.00">-1.00</option>
                                            <option value="-0.75">-0.75</option>
                                            <option value="-0.50">-0.50</option>
                                            <option value="-0.25">-0.25</option>
                                            <option value="0.00" selected>0.00</option>
                                            <option value="SPH/DS">SPH/DS</option>
                                            <option value="0.25">0.25</option>
                                            <option value="0.50">0.50</option>
                                            <option value="0.75">0.75</option>
                                            <option value="1.00">1.00</option>
                                            <option value="1.25">1.25</option>
                                            <option value="1.50">1.50</option>
                                            <option value="1.75">1.75</option>
                                            <option value="2.00">2.00</option>
                                            <option value="2.25">2.25</option>
                                            <option value="2.50">2.50</option>
                                            <option value="2.75">2.75</option>
                                            <option value="3.00">3.00</option>
                                            <option value="3.25">3.25</option>
                                            <option value="3.50">3.50</option>
                                            <option value="3.75">3.75</option>
                                            <option value="4.00">4.00</option>
                                            <option value="4.25">4.25</option>
                                            <option value="4.50">4.50</option>
                                            <option value="4.75">4.75</option>
                                            <option value="5.00">5.00</option>
                                            <option value="5.25">5.25</option>
                                            <option value="5.50">5.50</option>
                                            <option value="5.75">5.75</option>
                                            <option value="6.00">6.00</option>
                                        </select>
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="os-sph" id="os-sph">
                                            <option value="-16.00">-16.00</option>
                                            <option value="-15.75">-15.75</option>
                                            <option value="-15.50">-15.50</option>
                                            <option value="-15.25">-15.25</option>
                                            <option value="-15.00">-15.00</option>
                                            <option value="-14.75">-14.75</option>
                                            <option value="-14.50">-14.50</option>
                                            <option value="-14.25">-14.25</option>
                                            <option value="-14.00">-14.00</option>
                                            <option value="-13.75">-13.75</option>
                                            <option value="-13.50">-13.50</option>
                                            <option value="-13.25">-13.25</option>
                                            <option value="-13.00">-13.00</option>
                                            <option value="-12.75">-12.75</option>
                                            <option value="-12.50">-12.50</option>
                                            <option value="-12.25">-12.25</option>
                                            <option value="-12.00">-12.00</option>
                                            <option value="-11.75">-11.75</option>
                                            <option value="-11.50">-11.50</option>
                                            <option value="-11.25">-11.25</option>
                                            <option value="-11.00">-11.00</option>
                                            <option value="-10.75">-10.75</option>
                                            <option value="-10.50">-10.50</option>
                                            <option value="-10.25">-10.25</option>
                                            <option value="-10.00">-10.00</option>
                                            <option value="-9.75">-9.75</option>
                                            <option value="-9.50">-9.50</option>
                                            <option value="-9.25">-9.25</option>
                                            <option value="-9.00">-9.00</option>
                                            <option value="-8.75">-8.75</option>
                                            <option value="-8.50">-8.50</option>
                                            <option value="-8.25">-8.25</option>
                                            <option value="-8.00">-8.00</option>
                                            <option value="-7.75">-7.75</option>
                                            <option value="-7.50">-7.50</option>
                                            <option value="-7.25">-7.25</option>
                                            <option value="-7.00">-7.00</option>
                                            <option value="-6.75">-6.75</option>
                                            <option value="-6.50">-6.50</option>
                                            <option value="-6.25">-6.25</option>
                                            <option value="-6.00">-6.00</option>
                                            <option value="-5.75">-5.75</option>
                                            <option value="-5.50">-5.50</option>
                                            <option value="-5.25">-5.25</option>
                                            <option value="-5.00">-5.00</option>
                                            <option value="-4.75">-4.75</option>
                                            <option value="-4.50">-4.50</option>
                                            <option value="-4.25">-4.25</option>
                                            <option value="-4.00">-4.00</option>
                                            <option value="-3.75">-3.75</option>
                                            <option value="-3.50">-3.50</option>
                                            <option value="-3.25">-3.25</option>
                                            <option value="-3.00">-3.00</option>
                                            <option value="-2.75">-2.75</option>
                                            <option value="-2.50">-2.50</option>
                                            <option value="-2.25">-2.25</option>
                                            <option value="-2.00">-2.00</option>
                                            <option value="-1.75">-1.75</option>
                                            <option value="-1.50">-1.50</option>
                                            <option value="-1.25">-1.25</option>
                                            <option value="-1.00">-1.00</option>
                                            <option value="-0.75">-0.75</option>
                                            <option value="-0.50">-0.50</option>
                                            <option value="-0.25">-0.25</option>
                                            <option value="0.00" selected>0.00</option>
                                            <option value="Plano">Plano</option>
                                            <option value="0.25">0.25</option>
                                            <option value="0.50">0.50</option>
                                            <option value="0.75">0.75</option>
                                            <option value="1.00">1.00</option>
                                            <option value="1.25">1.25</option>
                                            <option value="1.50">1.50</option>
                                            <option value="1.75">1.75</option>
                                            <option value="2.00">2.00</option>
                                            <option value="2.25">2.25</option>
                                            <option value="2.50">2.50</option>
                                            <option value="2.75">2.75</option>
                                            <option value="3.00">3.00</option>
                                            <option value="3.25">3.25</option>
                                            <option value="3.50">3.50</option>
                                            <option value="3.75">3.75</option>
                                            <option value="4.00">4.00</option>
                                            <option value="4.25">4.25</option>
                                            <option value="4.50">4.50</option>
                                            <option value="4.75">4.75</option>
                                            <option value="5.00">5.00</option>
                                            <option value="5.25">5.25</option>
                                            <option value="5.50">5.50</option>
                                            <option value="5.75">5.75</option>
                                            <option value="6.00">6.00</option>
                                            <option value="6.25">6.25</option>
                                            <option value="6.50">6.50</option>
                                            <option value="6.75">6.75</option>
                                            <option value="7.00">7.00</option>
                                            <option value="7.25">7.25</option>
                                            <option value="7.50">7.50</option>
                                            <option value="7.75">7.75</option>
                                            <option value="8.00">8.00</option>
                                            <option value="8.25">8.25</option>
                                            <option value="8.50">8.50</option>
                                            <option value="8.75">8.75</option>
                                            <option value="9.00">9.00</option>
                                            <option value="9.25">9.25</option>
                                            <option value="9.50">9.50</option>
                                            <option value="9.75">9.75</option>
                                            <option value="10.00">10.00</option>
                                            <option value="10.25">10.25</option>
                                            <option value="10.50">10.50</option>
                                            <option value="10.75">10.75</option>
                                            <option value="11.00">11.00</option>
                                            <option value="11.25">11.25</option>
                                            <option value="11.50">11.50</option>
                                            <option value="11.75">11.75</option>
                                            <option value="12.00">12.00</option>
                                        </select>
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="os-cyl" id="os-cyl">
                                            <option value="-6.00">-6.00</option>
                                            <option value="-5.75">-5.75</option>
                                            <option value="-5.50">-5.50</option>
                                            <option value="-5.25">-5.25</option>
                                            <option value="-5.00">-5.00</option>
                                            <option value="-4.75">-4.75</option>
                                            <option value="-4.50">-4.50</option>
                                            <option value="-4.25">-4.25</option>
                                            <option value="-4.00">-4.00</option>
                                            <option value="-3.75">-3.75</option>
                                            <option value="-3.50">-3.50</option>
                                            <option value="-3.25">-3.25</option>
                                            <option value="-3.00">-3.00</option>
                                            <option value="-2.75">-2.75</option>
                                            <option value="-2.50">-2.50</option>
                                            <option value="-2.25">-2.25</option>
                                            <option value="-2.00">-2.00</option>
                                            <option value="-1.75">-1.75</option>
                                            <option value="-1.50">-1.50</option>
                                            <option value="-1.25">-1.25</option>
                                            <option value="-1.00">-1.00</option>
                                            <option value="-0.75">-0.75</option>
                                            <option value="-0.50">-0.50</option>
                                            <option value="-0.25">-0.25</option>
                                            <option value="0.00" selected>0.00</option>
                                            <option value="SPH/DS">SPH/DS</option>
                                            <option value="0.25">0.25</option>
                                            <option value="0.50">0.50</option>
                                            <option value="0.75">0.75</option>
                                            <option value="1.00">1.00</option>
                                            <option value="1.25">1.25</option>
                                            <option value="1.50">1.50</option>
                                            <option value="1.75">1.75</option>
                                            <option value="2.00">2.00</option>
                                            <option value="2.25">2.25</option>
                                            <option value="2.50">2.50</option>
                                            <option value="2.75">2.75</option>
                                            <option value="3.00">3.00</option>
                                            <option value="3.25">3.25</option>
                                            <option value="3.50">3.50</option>
                                            <option value="3.75">3.75</option>
                                            <option value="4.00">4.00</option>
                                            <option value="4.25">4.25</option>
                                            <option value="4.50">4.50</option>
                                            <option value="4.75">4.75</option>
                                            <option value="5.00">5.00</option>
                                            <option value="5.25">5.25</option>
                                            <option value="5.50">5.50</option>
                                            <option value="5.75">5.75</option>
                                            <option value="6.00">6.00</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="prescription-table">
                                <div class="title-column">
                                    <div class="title title-row">
                                        <p>OD</p>
                                        <p>Right eye</p>
                                    </div>
                                    <div class="title title-row">
                                        <p>OS</p>
                                        <p>Left eye</p>
                                    </div>
                                </div>
                                <div class="content-columns">
                                    <div class="title">
                                        <p>Axis</p>
                                    </div>
                                    <div class="title">
                                        <p>ADD</p>
                                    </div>
                                    <div class="input-wrapper">
                                        <input type="number" name="od-axis" id="od-axis" min="2" max="180">
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="od-ad" id="od-ad">
                                            <option value="n/a">n/a</option>
                                            <option value="+1.00">+1.00</option>
                                            <option value="+1.25">+1.25</option>
                                            <option value="+1.50">+1.50</option>
                                            <option value="+1.75">+1.75</option>
                                            <option value="+2.00">+2.00</option>
                                            <option value="+2.25">+2.25</option>
                                            <option value="+2.50">+2.50</option>
                                            <option value="+2.75">+2.75</option>
                                            <option value="+3.00">+3.00</option>
                                            <option value="+3.25">+3.25</option>
                                            <option value="+3.50">+3.50</option>
                                        </select>
                                    </div>
                                    <div class="input-wrapper">
                                        <input type="number" name="os-axis" id="os-axis" min="2" max="180">
                                    </div>
                                    <div class="select-wrapper">
                                        <select name="os-ad" id="os-ad">
                                            <option value="n/a">n/a</option>
                                            <option value="+1.00">+1.00</option>
                                            <option value="+1.25">+1.25</option>
                                            <option value="+1.50">+1.50</option>
                                            <option value="+1.75">+1.75</option>
                                            <option value="+2.00">+2.00</option>
                                            <option value="+2.25">+2.25</option>
                                            <option value="+2.50">+2.50</option>
                                            <option value="+2.75">+2.75</option>
                                            <option value="+3.00">+3.00</option>
                                            <option value="+3.25">+3.25</option>
                                            <option value="+3.50">+3.50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prescription-pupillary">
                            <!--                        <p>Pupillary Distance</p>-->
                            <button class="prescription-modal-button tooltip-hover"
                                    title="Tooltip12" type="button">
                                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                                    <use xlink:href="#modal-i"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="pupillary-info">
                            <p class="pupillary-title">PD<br><span class="pupillary-distance">Pupillary Distance</span>
                            </p>
                            <div class="pupillary-selects">
                                <div class="select-wrapper">
                                    <select name="pd-1" id="pd-1">
                                        <option value="Right PD">Right PD</option>
                                        <option value="23.0">23.0</option>
                                        <option value="23.5">23.5</option>
                                        <option value="24.0">24.0</option>
                                        <option value="24.5">24.5</option>
                                        <option value="25.0">25.0</option>
                                        <option value="25.5">25.5</option>
                                        <option value="26.0">26.0</option>
                                        <option value="26.5">26.5</option>
                                        <option value="27.0">27.0</option>
                                        <option value="27.5">27.5</option>
                                        <option value="28.0">28.0</option>
                                        <option value="28.5">28.5</option>
                                        <option value="29.0">29.0</option>
                                        <option value="29.5">29.5</option>
                                        <option value="30.0">30.0</option>
                                        <option value="30.5">30.5</option>
                                        <option value="31.0">31.0</option>
                                        <option value="31.5">31.5</option>
                                        <option value="32.0">32.0</option>
                                        <option value="32.5">32.5</option>
                                        <option value="33.0">33.0</option>
                                        <option value="33.5">33.5</option>
                                        <option value="34.0">34.0</option>
                                        <option value="34.5">34.5</option>
                                        <option value="35.0">35.0</option>
                                        <option value="35.5">35.5</option>
                                        <option value="36.0">36.0</option>
                                        <option value="36.5">36.5</option>
                                        <option value="37.0">37.0</option>
                                        <option value="37.5">37.5</option>
                                        <option value="38.0">38.0</option>
                                        <option value="38.5">38.5</option>
                                        <option value="39.0">39.0</option>
                                        <option value="39.5">39.5</option>
                                        <option value="40.0">40.0</option>
                                    </select>
                                </div>
                                <div class="select-wrapper">
                                    <select name="pd-2" id="pd-2" class="display-none">
                                        <option value="Left PD" selected>Left PD</option>
                                        <option value="25.0">25.0</option>
                                        <option value="25.5">25.5</option>
                                        <option value="26.0">26.0</option>
                                        <option value="26.5">26.5</option>
                                        <option value="27.0">27.0</option>
                                        <option value="27.5">27.5</option>
                                        <option value="28.0">28.0</option>
                                        <option value="28.5">28.5</option>
                                        <option value="29.0">29.0</option>
                                        <option value="29.5">29.5</option>
                                        <option value="30.0">30.0</option>
                                        <option value="30.5">30.5</option>
                                        <option value="31.0">31.0</option>
                                        <option value="31.5">31.5</option>
                                        <option value="32.0">32.0</option>
                                        <option value="32.5">32.5</option>
                                        <option value="33.0">33.0</option>
                                        <option value="33.5">33.5</option>
                                        <option value="34.0">34.0</option>
                                        <option value="34.5">34.5</option>
                                        <option value="35.0">35.0</option>
                                        <option value="35.5">35.5</option>
                                        <option value="36.0">36.0</option>
                                        <option value="36.5">36.5</option>
                                        <option value="37.0">37.0</option>
                                        <option value="37.5">37.5</option>
                                        <option value="38.0">38.0</option>
                                        <option value="38.5">38.5</option>
                                        <option value="39.0">39.0</option>
                                        <option value="39.5">39.5</option>
                                        <option value="40.0">40.0</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="pupillary-checkbox">
                            <label>
                                <input type="checkbox" name="two-pd" id="two-pd">
                                Two PD Numbers
                            </label>
                        </div>
                        <div class="lens-add-comment">
                            <p class="text">Show more options
                                <svg class="show-more-chevron" width="8" height="14">
                                    <use xlink:href="#chevron-left"></use>
                                </svg>
                            </p>
                        </div>
                        <div class="prescription-comment">
                            <p class="comment-text">Prescription comments</p>
                            <textarea name="comment" id="comment" placeholder="Enter Your comments"></textarea>
                            <div class="add-prism-checkbox">
                                <label class="add-prism-text">
                                    <input type="checkbox" name="add-prism" id="add-prism">
                                    Add prism
                                </label>
                            </div>
                            <div class="prescription-wrapper prism-wrapper display-none">
                                <div class="prescription-table">
                                    <div class="title-column">
                                        <div class="title title-row">
                                            <p>OD</p>
                                            <p>Right eye</p>
                                        </div>
                                        <div class="title title-row">
                                            <p>OS</p>
                                            <p>Left eye</p>
                                        </div>
                                    </div>
                                    <div class="content-columns">
                                        <div class="title">
                                            <p>Vertical △</p>
                                        </div>
                                        <div class="title">
                                            <p>Base Direction</p>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="od-vertical" id="od-vertical">
                                                <option value="n/a">n/a</option>
                                                <option value="0.50">0.50</option>
                                                <option value="1.00">1.00</option>
                                                <option value="1.50">1.50</option>
                                                <option value="2.00">2.00</option>
                                                <option value="2.50">2.50</option>
                                                <option value="3.00">3.00</option>
                                                <option value="3.50">3.50</option>
                                                <option value="4.00">4.00</option>
                                                <option value="4.50">4.50</option>
                                                <option value="5.00">5.00</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="od-v-basedirection" id="od-v-basedirection">
                                                <option value="n/a">n/a</option>
                                                <option value="Up">Up</option>
                                                <option value="Down">Down</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="os-vertical" id="os-vertical">
                                                <option value="n/a">n/a</option>
                                                <option value="0.50">0.50</option>
                                                <option value="1.00">1.00</option>
                                                <option value="1.50">1.50</option>
                                                <option value="2.00">2.00</option>
                                                <option value="2.50">2.50</option>
                                                <option value="3.00">3.00</option>
                                                <option value="3.50">3.50</option>
                                                <option value="4.00">4.00</option>
                                                <option value="4.50">4.50</option>
                                                <option value="5.00">5.00</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="os-v-basedirection" id="os-v-basedirection">
                                                <option value="n/a">n/a</option>
                                                <option value="Up">Up</option>
                                                <option value="Down">Down</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="prescription-table">
                                    <div class="title-column">
                                        <div class="title title-row">
                                            <p>OD</p>
                                            <p>Right eye</p>
                                        </div>
                                        <div class="title title-row">
                                            <p>OS</p>
                                            <p>Left eye</p>
                                        </div>
                                    </div>
                                    <div class="content-columns">
                                        <div class="title">
                                            <p>Horizontal △</p>
                                        </div>
                                        <div class="title">
                                            <p>Base Direction</p>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="od-horizontal" id="od-horizontal">
                                                <option value="n/a">n/a</option>
                                                <option value="0.50">0.50</option>
                                                <option value="1.00">1.00</option>
                                                <option value="1.50">1.50</option>
                                                <option value="2.00">2.00</option>
                                                <option value="2.50">2.50</option>
                                                <option value="3.00">3.00</option>
                                                <option value="3.50">3.50</option>
                                                <option value="4.00">4.00</option>
                                                <option value="4.50">4.50</option>
                                                <option value="5.00">5.00</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="od-h-basedirection" id="od-h-basedirection">
                                                <option value="n/a">n/a</option>
                                                <option value="Up">In</option>
                                                <option value="Down">Out</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="os-horizontal" id="os-horizontal">
                                                <option value="n/a">n/a</option>
                                                <option value="0.50">0.50</option>
                                                <option value="1.00">1.00</option>
                                                <option value="1.50">1.50</option>
                                                <option value="2.00">2.00</option>
                                                <option value="2.50">2.50</option>
                                                <option value="3.00">3.00</option>
                                                <option value="3.50">3.50</option>
                                                <option value="4.00">4.00</option>
                                                <option value="4.50">4.50</option>
                                                <option value="5.00">5.00</option>
                                            </select>
                                        </div>
                                        <div class="select-wrapper">
                                            <select name="os-h-basedirection" id="os-h-basedirection">
                                                <option value="n/a">n/a</option>
                                                <option value="Up">In</option>
                                                <option value="Down">Out</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="button-confirm-wrapper">
                            <button class="continue-button save-prescription-button save-filter-prescription"
                                    type="submit" disabled>Confirm
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</main>
