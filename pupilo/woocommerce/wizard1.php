<?php

?>

<div class="wizard1 usage display-none step-wrapper step1">
    <div class="step-heading-wrapper">
        <p class="step-heading-text">Select type of lenses</p>
    </div>
    <div class="wizard-item">
        <input type="radio" id="distance" name="usage" value="distance" class="radio-hide"
            <?php
            if (get_field('distance_price', 'options')) {
                echo 'data-ad="' . get_field('distance_price', 'options') . '"';
            }
            ?>
        >
        <label class="wizard-item-label" for="distance">
            <div class="label-image">
                <img class="play-video-button"
                     src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/distance.png"
                     alt="Distance">
            </div>
            <div class="label-info">
                <p>Distance
                    <?php
                    if (get_field('distance_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('distance_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </p>
                <p><?php echo get_field('distance_text', 'options'); ?></p>
            </div>
            <!--                <button class="label-tooltip" data-bs-toggle="modal" data-bs-target="#wizard1DistanceModal">-->
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_distance">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_distance">
                    <?php echo get_field('distance_tooltip', 'options'); ?>
                </span>
            </div>
        </label>
    </div>

    <div class="wizard-item">
        <input type="radio" id="reading" name="usage" value="reading" class="radio-hide"
            <?php
            if (get_field('reading_price', 'options')) {
                echo 'data-ad="' . get_field('reading_price', 'options') . '"';
            }
            ?>
        >
        <label class="wizard-item-label" for="reading">
            <div class="label-image">
                <img class="play-video-button"
                     src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/reading.png"
                     alt="Reading">
            </div>
            <div class="label-info">
                <p>Reading
                    <?php
                    if (get_field('reading_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('reading_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </p>
                <p><?php echo get_field('reading_text', 'options'); ?></p>
            </div>
            <!--                <button class="label-tooltip" data-bs-toggle="modal" data-bs-target="#wizard1ReadingModal">-->
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_reading">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_reading">
                    <?php echo get_field('reading_tooltip', 'options'); ?>
                </span>
            </div>
        </label>
    </div>

    <div class="wizard-item multifocal-item">
        <input type="radio" id="multifocal" name="usage" value="multifocal" class="radio-hide">
        <label class="wizard-item-label" for="multifocal">
            <div class="label-image">
                <img class="play-video-button"
                     src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/multifocal.png"
                     alt="Multifocal">
            </div>
            <div class="label-info">
                <p>Multifocal</p>
                <p><?php echo get_field('multifocal_text', 'options'); ?></p>
            </div>
            <!--                <button class="label-tooltip" data-bs-toggle="modal" data-bs-target="#wizard1MultifocalModal">-->
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_multifocal">
                    <?php echo get_field('multifocal_tooltip', 'options'); ?>
                </span>
            </div>
        </label>
        <div class="usage-multifocal display-none">
            <div class="sub-item">
                <input type="radio" id="progressive" name="usage-multifocal" value="progressive"
                    <?php
                    if (get_field('multifocal_progressive_price', 'options')) {
                        echo 'data-ad="' . get_field('multifocal_progressive_price', 'options') . '"';
                    }
                    ?>
                >
                <label class="wizard-item-label"
                       for="progressive">Progressive
                    <?php
                    if (get_field('multifocal_progressive_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('multifocal_progressive_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_progressive">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_progressive">
                        <?php echo get_field('multifocal_progressive_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
            <div class="sub-item">
                <input type="radio" id="premium-progressive" name="usage-multifocal" value="premium-progressive"
                    <?php
                    if (get_field('multifocal_premium_price', 'options')) {
                        echo 'data-ad="' . get_field('multifocal_premium_price', 'options') . '"';
                    }
                    ?>
                >
                <label class="wizard-item-label"
                       for="premium-progressive">Premium Progressive
                    <?php
                    if (get_field('multifocal_premium_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('multifocal_premium_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_multifocal_premium">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_multifocal_premium">
                        <?php echo get_field('multifocal_premium_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="wizard-item driving-item">
        <input type="radio" id="driving" name="usage" value="driving" class="radio-hide">
        <label class="wizard-item-label" for="driving">
            <div class="label-image">
                <img class="play-video-button"
                     src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/driving.png"
                     alt="Driving">
            </div>
            <div class="label-info">
                <p>Driving</p>
                <p><?php echo get_field('driving_text', 'options'); ?></p>
            </div>
            <!--                <button class="label-tooltip" data-bs-toggle="modal" data-bs-target="#wizard1DrivingModal">-->
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                    <span id="tooltip_driving">
                        <?php echo get_field('driving_tooltip', 'options'); ?>
                    </span>
            </div>
        </label>
        <div class="usage-driving display-none">
            <div class="sub-item">
                <input type="radio" id="driving-single" name="usage-driving" value="single-vision"
                    <?php
                    if (get_field('driving_single_price', 'options')) {
                        echo 'data-ad="' . get_field('driving_single_price', 'options') . '"';
                    }
                    ?>
                >
                <label class="wizard-item-label" for="driving-single">Single vision
                    <?php
                    if (get_field('driving_single_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('driving_single_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving_single">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_driving_single">
                        <?php echo get_field('driving_single_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
            <div class="sub-item">
                <input type="radio" id="driving-multifocal" name="usage-driving" value="multifocal"
                    <?php
                    if (get_field('driving_multi_price', 'options')) {
                        echo 'data-ad="' . get_field('driving_multi_price', 'options') . '"';
                    }
                    ?>
                >
                <label class="wizard-item-label" for="driving-multifocal">Multifocal
                    <?php
                    if (get_field('driving_multi_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('driving_multi_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_driving_multi">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_driving_multi">
                        <?php echo get_field('driving_multi_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="wizard-item">
        <input type="radio" id="non-prescription" name="usage" value="non-prescription" class="radio-hide">
        <label class="wizard-item-label" for="non-prescription">
            <div class="label-image">
                <img class="play-video-button"
                     src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/pupilo/dist/images/nonprescription.png"
                     alt="Non prescription">
            </div>
            <div class="label-info">
                <p>Non prescription
                    <?php
                    if (get_field('non_prescriptions_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('non_prescriptions_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </p>
                <p><?php echo get_field('non_prescriptions_text', 'options'); ?></p>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_non_prescription">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                    <span id="tooltip_non_prescription">
                        <?php echo get_field('non_prescriptions_tooltip', 'options'); ?>
                    </span>
            </div>
        </label>
    </div>
</div>
