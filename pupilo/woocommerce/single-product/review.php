<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

	<div id="comment-<?php comment_ID(); ?>" class="comment_container">

		<?php
		/**
		 * The woocommerce_review_before hook
		 *
		 * @hooked woocommerce_review_display_gravatar - 10
		 */
		do_action( 'woocommerce_review_before', $comment );
		?>

		<div class="comment-text">

			<?php
             $title = get_comment_meta( $comment->comment_ID, "title", true );
                echo '<h5>' . $title . '</h5>';

			/**
			 * The woocommerce_review_before_comment_meta hook.
			 *
			 * @hooked woocommerce_review_display_rating - 10
			 */
			do_action( 'woocommerce_review_before_comment_meta', $comment );

			/**
			 * The woocommerce_review_meta hook.
			 *
			 * @hooked woocommerce_review_display_meta - 10
			 */
			do_action( 'woocommerce_review_meta', $comment );

			do_action( 'woocommerce_review_before_comment_text', $comment );

			/**
			 * The woocommerce_review_comment_text hook
			 *
			 * @hooked woocommerce_review_display_comment_text - 10
			 */
			do_action( 'woocommerce_review_comment_text', $comment );

			do_action( 'woocommerce_review_after_comment_text', $comment );

			$comment_id = $comment->comment_ID;
            $user_id = get_current_user_id();

            $liked_comments = get_user_meta($user_id, 'likes', true);
            $liked_comments = explode(',', $liked_comments);
            $is_liked = in_array($comment_id, $liked_comments);

            $disliked_comments = get_user_meta($user_id, 'dislikes', true);
            $disliked_comments = explode(',', $disliked_comments);
            $is_disliked = in_array($comment_id, $disliked_comments);

            ?>
            <div class="like-dislike-wrap">
                <span class="question">Was this review helpful?</span>
                <div class="like-wrap">
                    <button class="like-button" type="button" data-id="<?php echo $comment_id; ?>" data-user="<?php echo get_current_user_id(); ?>">
                        <svg id="comment-like" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg" class="<?php if ($is_liked) { echo 'liked'; } ?>">
                            <path d="M5.83268 17.61H3.33268C2.89065 17.61 2.46673 17.4344 2.15417 17.1219C1.84161 16.8093 1.66602 16.3854 1.66602 15.9434V10.11C1.66602 9.668 1.84161 9.24408 2.15417 8.93151C2.46673 8.61895 2.89065 8.44336 3.33268 8.44336H5.83268M11.666 6.77669V3.44336C11.666 2.78032 11.4026 2.14443 10.9338 1.67559C10.4649 1.20675 9.82906 0.943359 9.16602 0.943359L5.83268 8.44336V17.61H15.2327C15.6346 17.6146 16.0247 17.4737 16.3309 17.2134C16.6372 16.953 16.8391 16.5908 16.8993 16.1934L18.0493 8.69336C18.0856 8.45449 18.0695 8.21059 18.0021 7.97857C17.9348 7.74654 17.8178 7.53194 17.6592 7.34962C17.5007 7.1673 17.3044 7.02164 17.084 6.92271C16.8636 6.82378 16.6243 6.77396 16.3827 6.77669H11.666Z"
                                  stroke-width="1.66667" stroke-linecap="round" stroke-linejoin="round" stroke="#59af9e"/>
                        </svg>
<!--                        <svg width="19" height="19">-->
<!--                            <use xlink:href="#comment-like"></use>-->
<!--                        </svg>-->
                    </button>

                    <span class="like-count">
                        <?php
                        if (get_comment_meta($comment_id, 'likes', true)) {
                            echo get_comment_meta($comment_id, 'likes', true);
                        } else {
                            echo 0;
                        }
                        ?>
                    </span>
                </div>
                <div class="dislike-wrap">
                    <button class="dislike-button" type="button" data-id="<?php echo $comment->comment_ID; ?>" data-user="<?php echo get_current_user_id(); ?>">
                        <svg id="comment-dislike" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg" class="<?php if ($is_disliked) { echo 'disliked'; } ?>">
                            <path d="M13.1672 0.943661H15.3922C15.8638 0.93532 16.3221 1.10044 16.68 1.40768C17.038 1.71491 17.2706 2.14288 17.3338 2.61033V8.44366C17.2706 8.91111 17.038 9.33908 16.68 9.64631C16.3221 9.95355 15.8638 10.1187 15.3922 10.1103H13.1672M7.33385 11.777V15.1103C7.33385 15.7734 7.59724 16.4093 8.06608 16.8781C8.53492 17.3469 9.17081 17.6103 9.83385 17.6103L13.1672 10.1103V0.943661H3.76718C3.36524 0.939117 2.9752 1.07999 2.66893 1.34032C2.36265 1.60065 2.16078 1.96291 2.10051 2.36033L0.950514 9.86033C0.914257 10.0992 0.930368 10.3431 0.997731 10.5751C1.06509 10.8071 1.1821 11.0217 1.34063 11.2041C1.49917 11.3864 1.69545 11.5321 1.91587 11.631C2.13629 11.7299 2.37559 11.7797 2.61718 11.777H7.33385Z"
                                  stroke="#EC423E" stroke-width="1.66667" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
<!--                        <svg width="19" height="19">-->
<!--                            <use xlink:href="#comment-dislike"></use>-->
<!--                        </svg>-->
                    </button>
                    <span class="dislike-count">
                        <?php
                        if (get_comment_meta($comment_id, 'dislikes', true)) {
                            echo get_comment_meta($comment_id, 'dislikes', true);
                        } else {
                            echo 0;
                        }
                        ?>
                    </span>
                </div>
            </div>
		</div>
	</div>
