<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>
<div>

    <?php
    global $product;
     ?>


    <div class="price-wrapper-with-sale-text">
        <p class="<?php echo esc_attr(apply_filters('woocommerce_product_price_class', 'price')); ?> basic-price"><?php echo $product->get_price_html(); ?></p>
        <?php
        if ($product->is_type('variable')) {
            $variations = $product->get_available_variations();
            foreach ($variations as $variation) {
                $id = $variation['variation_id'];
                $price = get_post_meta($variation['variation_id'], '_regular_price', true);
                $price_sale = get_post_meta($variation['variation_id'], '_sale_price', true);
            } ?>

            <?php if ($product->is_on_sale())  : ?>
                <div class="product-sale-text display-none">You Saved
                    <?php
//                    if ($price_sale !== "") {

                        $sale = 100 - ($price_sale * 100 / $price);
                        $discount = $price - $price_sale;
                        echo '<per>' . round($sale, 0) . '</per>';
//                    }
                    ?>
                    % (<dif><?php echo number_format((float)$discount, 2, '.', '') . '</dif>€' ?>)
                </div>

            <?php endif; ?>

            <?php
        } else { ?>
            <?php
            $price = get_post_meta(get_the_ID(), '_regular_price', true);
            $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
            if ($product->is_on_sale())  : ?>
                <div class="product-sale-text display-none">You Saved
                    <?php if ($price_sale !== "") {

                        $sale = 100 - ($price_sale * 100 / $price);
                        $discount = $price - $price_sale;
                        echo round($sale, 0);
                    } ?>
                    % (<?php echo number_format((float)$discount, 2, '.', '') . '€' ?>)
                </div>
            <?php endif; ?>

        <?php } ?>
    </div>
</div>
