<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
?>

<?php

    $product_id = wp_get_post_parent_id($item->get_variation_id());
    $basic_product = wc_get_product($product_id);
    $item_meta = $item->get_formatted_meta_data();

    foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
        if ($meta->display_key === 'Lenses configuration') {
            $lenses_configuration = $meta->display_value;
        }

        if ($meta->display_key === 'Prescriptions') {
            $prescriptions = $meta->display_value;
        }
    }
?>



<div class="product-image single-order-image">
    <?php echo $product->get_image(); ?>
</div>
<div class="product-info">
    <div class="product-main">
        <div class="product-name">
            <a href="<?php echo $product->get_permalink(); ?>">
                <?php
//                var_dump($item);

                if ($item->is_type( 'variable' ) ) {
                    echo $basic_product->get_title();
                } else {
                    echo $product->get_title();
                }
                ?>
            </a>
        </div>
        <div class="product-parameters">
            <div><p>Color: <span><?php echo get_post_meta($item->get_variation_id(), 'attribute_pa_color', true); ?></span></p></div>
            <div><p>Size: <span><?php echo get_post_meta($item->get_variation_id(), 'attribute_pa_size', true); ?></span></p></div>
        </div>
    </div>
    <div class="product-price-wrapper">
        <div class="product-quantity"><p class="product-quantity-text"><?php echo $item->get_quantity(); ?>pcs</p></div>
        <div class="product-price"><p class="product-price-text"><?php echo number_format($item->get_total(), 2, '.', ' '); ?>€</p></div>
    </div>
</div>
<?php

if (isset($prescriptions)) {
    $pre = str_replace("\\", "", $prescriptions);
    $pre = substr($pre, 4);
    $pre = substr($pre, 0, -6);
    $pre = explode(',', $pre);

    $new_pre = [];

    foreach ($pre as $pre_item) {
        $pre_item = str_replace("\"", "", $pre_item);
        $item = explode(":", $pre_item);
        $key = $item[0];

        if (isset($item[1])) {
            $value = $item[1];
        } else {
            $value = '';
        }

        $new_pre[$key] = $value;
    }
}

if (isset($lenses_configuration)) {
    $lens = str_replace("\\", "", $lenses_configuration);
    $lens = substr($lens, 4);
    $lens = substr($lens, 0, -6);
    $lens = explode('},{', $lens);

    $new_lens = [];

    foreach ($lens as $lens_item) {
        $lens_item = substr($lens_item, 1);
        $item_array = explode('","', $lens_item);

        $new_lens_item = [];

        foreach ($item_array as $single_item) {
            $single_item = str_replace("\"", "", $single_item);
            $single_item = explode(':', $single_item);
            $key = $single_item[0];
            $value = $single_item[1];
            $new_lens_item[$key] = $value;
        }

        array_push($new_lens, $new_lens_item);
    }
} ?>
<div class="order-item-pre">
    <?php

    if (get_the_category_by_id(wc_get_product_term_ids($product->get_id(), 'product_cat')[0]) !== 'Contact Lenses') {

        ?>

        <p class="order-item-pre-heading">
            Lens: <span><?php echo $new_lens[0]['fullName']; ?></span>
            <svg viewBox="0 0 12 12" width="12" height="12" class="order-product-open-lenses">
                <use xlink:href="#chevron-left"></use>
            </svg>
        </p>
        <div class="lenses-configuration">
            <table class="prescription-table-item">
                <tr>
                    <td></td>
                    <td>SPH</td>
                    <td>CYl</td>
                    <td>Axis</td>
                    <td>ADD</td>
                    <td>PD</td>
                </tr>
                <tr>
                    <td>OD<span>(R)</span></td>
                    <td><?php echo $new_pre['odSph']; ?></td>
                    <td><?php echo $new_pre['odCyl']; ?></td>
                    <td><?php echo $new_pre['odAxis']; ?></td>
                    <td><?php echo $new_pre['odAd']; ?></td>
                    <td><?php echo $new_pre['pd1']; ?></td>
                </tr>
                <tr>
                    <td>OS<span>(L)</span></td>
                    <td><?php echo $new_pre['osSph']; ?></td>
                    <td><?php echo $new_pre['osCyl']; ?></td>
                    <td><?php echo $new_pre['osAxis']; ?></td>
                    <td><?php echo $new_pre['osAd']; ?></td>
                    <td><?php echo $new_pre['pd2']; ?></td>
                </tr>
            </table>

            <?php
            foreach ($new_lens as $lens_step) {

                if ($lens_step['step'] !== 'step2') {
                    ?>
                    <p class="lenses-configuration-rows"><?php echo $lens_step['fullName']; ?></p>
                    <?php
                }
            }
            ?>
        </div>
    <?php
    } else {
        $current_lens_token_right = $cart_item['lenses_token'];
        $current_lens_side_right = $cart_item['lens_side'];

        $cl_bc = $product->get_attribute('pa_cl-bc');
        $cl_dia = $product->get_attribute('pa_cl-dia');
        $cl_right = $product->get_attribute('pa_cl-power');
        $lens_quantity = $cart_item['quantity'];

        $key_left = '';

        foreach (WC()->cart->get_cart() as $cart_item_key_left => $cart_item_left) {
            if ($current_lens_token_right === $cart_item_left['lenses_token'] && $cart_item_left['lens_side'] === 'left') {
                $product_left = $cart_item_left['data'];
                $cl_left = $product_left->get_attribute('pa_cl-power');
                $key_left = $cart_item_key_left;
            }
        }
        ?>
        <div class="checkout-item-wrapper">
            <div class="lens-config">
                <p class="order-lens-item-pre-heading">
                    Prescription: <span><?php echo $new_lens[0]['fullName']; ?></span>
                    <svg viewBox="0 0 12 12" width="12" height="12"
                         class="order-product-open-lenses rotate-270">
                        <use xlink:href="#chevron-left"></use>
                    </svg>
                </p>

                <div class="lenses-configuration">
                    <table class="prescription-table-item">
                        <tr>
                            <td></td>
                            <td>SPH</td>
                            <td>BC</td>
                            <td>DN</td>
                        </tr>
                        <tr>
                            <td>OD<span>(R)</span></td>
                            <td><?php echo $cl_right; ?></td>
                            <td><?php echo $cl_bc; ?></td>
                            <td><?php echo $cl_dia; ?></td>
                        </tr>
                        <tr>
                            <td>OS<span>(L)</span></td>
                            <td><?php echo $cl_left; ?></td>
                            <td><?php echo $cl_bc; ?></td>
                            <td><?php echo $cl_dia; ?></td>
                        </tr>
                    </table>

                </div>

            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php



?>

