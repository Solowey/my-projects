<?php
/**
 * Order Customer Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-customer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.4
 */

defined('ABSPATH') || exit;

$show_shipping = !wc_ship_to_billing_address_only() && $order->needs_shipping_address();
?>
<section class="woocommerce-customer-details">

    <?php if ($show_shipping) : ?>

    <section class="woocommerce-columns woocommerce-columns--2 woocommerce-columns--addresses col2-set addresses">
        <div class="woocommerce-column woocommerce-column--1 woocommerce-column--billing-address col-1">

            <?php endif; ?>

            <!--	<h2 class="woocommerce-column__title">-->
            <?php //esc_html_e( 'Billing address', 'woocommerce' ); ?><!--</h2>-->

            <div class="order-address-wrapper">
                <div class="order-single-delivery">
                    <p class="order-single-small-heading">Delivery</p>
                    <div class="shipping-summary">
                        <div class="shipping-main">
                            <div class="shipping-logo">
                                <?php

                                if (strripos($order->get_shipping_method(), 'omniva') === 0) {
                                    $method = 'Omniva';
                                    ?>
                                    <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                        <use xlink:href="#omniva"></use>
                                    </svg>
                                    <?php
                                } else if (strripos($order->get_shipping_method(), 'smartpost') === 0) {
                                    $method = 'Smartpost';
                                    ?>
                                    <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                        <use xlink:href="#smartpost"></use>
                                    </svg>
                                    <?php
                                } else {
                                    $method = 'DHL';
                                    ?>
                                    <svg viewBox="0 0 41 40" width="41" height="40" class="">
                                        <use xlink:href="#dhl"></use>
                                    </svg>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="shipping-name">
                                <p class="text"><?php echo $method; ?></p>
                                <p class="text-small"><?php echo $order->get_shipping_method(); ?></p>
                                <p class="text-small"><?php echo $order->get_formatted_shipping_address(); ?></p>
                                <p class="text-small"><?php echo $order->get_billing_email(); ?></p>
                                <?php
                                if ($method === 'Omniva' || $method === 'Smartpost') {
                                    ?>
                                    <p class="text-small"><?php echo $order->get_billing_first_name(); ?></p>
                                    <p class="text-small"><?php echo $order->get_billing_last_name(); ?></p>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="shipping-cost">
                                <p class="text"><?php if ($order->get_shipping_total() > 0) {
                                        echo $order->get_shipping_total() . '€';
                                    } else {
                                        echo 'Free';
                                    } ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order-single-payment">
                    <p class="order-single-small-heading">Payment</p>
                    <?php
                    if ($order->get_payment_method() === 'ppec_paypal') {
                        ?>
                        <div class="payment-method" data-payment-name="paypal">
                            <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                                <use xlink:href="#paypal-method"></use>
                            </svg>
                            <div class="payment-info">
                                <p class="heading">Paypal</p>
                                <p class="text">Safe payment online</p>
                            </div>
                        </div>
                        <?php
                    } else if ($order->get_payment_method() === 'everypay') {
                    ?>
                        <div class="payment-method" data-payment-name="montonio">
                            <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                                <use xlink:href="#credit-card"></use>
                            </svg>
                            <div class="payment-info">
                                <p class="heading">Everypay</p>
                                <p class="text">Visa, Mastercard</p>
                            </div>
                        </div>
                        <?php
                    } else {

                        ?>
                        <div class="payment-method" data-payment-name="montonio">
                            <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
                                <use xlink:href="#credit-card"></use>
                            </svg>
                            <div class="payment-info">
                                <p class="heading">Montonio</p>
                                <p class="text">Visa, Mastercard</p>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <!--        <address>-->
                <!--            --><?php //echo wp_kses_post( $order->get_formatted_billing_address( esc_html__( 'N/A', 'woocommerce' ) ) ); ?>
                <!---->
                <!--            --><?php //if ( $order->get_billing_phone() ) : ?>
                <!--                <p class="woocommerce-customer-details--phone">-->
                <?php //echo esc_html( $order->get_billing_phone() ); ?><!--</p>-->
                <!--            --><?php //endif; ?>
                <!---->
                <!--            --><?php //if ( $order->get_billing_email() ) : ?>
                <!--                <p class="woocommerce-customer-details--email">-->
                <?php //echo esc_html( $order->get_billing_email() ); ?><!--</p>-->
                <!--            --><?php //endif; ?>
                <!--        </address>-->
            </div>

            <div class="order-total-wrapper">
                <div class="subtotal">
                    <p class="subtotal-items">Subtotal (<?php echo $order->get_item_count(); ?> items)</p>
                    <p class="subtotal-price"><?php echo $order->get_subtotal(); ?>€</p>
                </div>
                <div class="subtotal subtotal-taxes">
                    <?php
                    $taxes = $order->get_taxes();
                    ?>
                    <p class="subtotal-items">Taxes</p>
                    <p class="subtotal-price subtotal-price-normal">
                        <?php
                        if (isset($taxes[0])) {
                            echo $taxes[0];
                        } else {
                            echo '0.00';
                        }

                        ?>
                        €</p>
                </div>
                <div class="subtotal subtotal-taxes subtotal-delivery">
                    <?php
                    $taxes = $order->get_taxes();
                    ?>
                    <p class="subtotal-items">Delivery</p>
                    <p class="subtotal-price subtotal-price-normal"><?php echo $order->get_shipping_total(); ?>€</p>
                </div>
                <div class="subtotal subtotal-delivery">
                    <p class="subtotal-items">Total</p>
                    <p class="subtotal-price subtotal-total-price"><?php echo $order->get_total(); ?>€</p>
                </div>
                <div class="need-help">
                    <p class="need-help-title">Need help?</p>
                    <a href="/faq" class="need-help-link">
                        <span>Help & FAQ’s</span>
                        <svg viewBox="0 0 12 12" width="12" height="12" class="">
                            <use xlink:href="#chevron-right"></use>
                        </svg>
                    </a>
                </div>
            </div>


            <?php if ($show_shipping) : ?>

        </div><!-- /.col-1 -->

        <div class="woocommerce-column woocommerce-column--2 woocommerce-column--shipping-address col-2">
            <!--			<h2 class="woocommerce-column__title">-->
            <?php //esc_html_e( 'Shipping address', 'woocommerce' ); ?><!--</h2>-->
            <!--			<address>-->
            <!--				--><?php //echo wp_kses_post( $order->get_formatted_shipping_address( esc_html__( 'N/A', 'woocommerce' ) ) ); ?>
            <!--			</address>-->
        </div><!-- /.col-2 -->

    </section><!-- /.col2-set -->

<?php endif; ?>

    <?php do_action('woocommerce_order_details_after_customer_details', $order); ?>

</section>
