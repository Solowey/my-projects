<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>

    <?php
    /**
     * Hook: woocommerce_before_single_product_summary.
     *
     * @hooked woocommerce_show_product_sale_flash - 10
     * @hooked woocommerce_show_product_images - 20
     */
    do_action('woocommerce_before_single_product_summary');
    ?>

    <div class="summary entry-summary">
        <?php
        /**
         * Hook: woocommerce_single_product_summary.
         *
         * @hooked woocommerce_template_single_title - 5
         * @hooked woocommerce_template_single_rating - 10
         * @hooked woocommerce_template_single_price - 10
         * @hooked woocommerce_template_single_excerpt - 20
         * @hooked woocommerce_template_single_add_to_cart - 30
         * @hooked woocommerce_template_single_meta - 40
         * @hooked woocommerce_template_single_sharing - 50
         * @hooked WC_Structured_Data::generate_product_data() - 60
         */
        do_action('woocommerce_single_product_summary');
        ?>


        <div class="button-wrap">
            <?php if (get_field('wizard_needed') !== 'No wizard' && get_field('wizard_needed') !== 'Lenses wizard') { ?>
                <button class="btn start-wizard" id="start-wizard" <?php if ($product->is_type('variable')) {
                    echo 'disabled';
                } ?>>Select lenses
                </button>
            <?php } elseif (get_field('wizard_needed') === 'Lenses wizard') { ?>
                <button type="submit" class="start-lenses-wizard add-prescription" disabled>Add Prescription</button>
            <?php } ?>

            <button type="submit" class="single_add_to_cart_button button alt add-to-cart-without-wizard
            <?php if (get_field('wizard_needed') !== 'No wizard') {
                echo ' display-none';
            } ?>
            "><?php echo esc_html($product->single_add_to_cart_text()); ?>
            </button>


            <a href="#" class="wish-single-product-link" data-id="<?php echo $product->get_id(); ?>">
                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                    <use xlink:href="#wish-list"></use>
                </svg>
            </a>
        </div>

        <?php if (get_field('wizard_needed') !== 'Lenses wizard') { ?>
            <button class="btn try-on" data-bs-toggle="modal" data-bs-target="#tryOnModal">
            <span>
                            <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                <use xlink:href="#try-on"></use>
            </svg>
            </span>
                <span>
                Try on
            </span>
            </button>
        <?php } ?>
        <div class="free-shipping">
            <span class="free-shipping-icon">
                <svg width="24" height="24">
                    <use xlink:href="#free-shipping"></use>
                </svg>
            </span>

            <span class="free-shipping-text"><?php echo get_field('free_shipping', 'option') ?></span>
            <span class="button-tooltip tooltip-hover" title="<?php echo get_field('free_shipping_tooltip_text', 'option') ?>"
            >!</span>
        </div>
    </div>

    <?php
    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 10
     * @hooked woocommerce_upsell_display - 15
     * @hooked woocommerce_output_related_products - 20
     */
    do_action('woocommerce_after_single_product_summary');
    ?>

</div>

<?php

if (get_field('wizard_needed') === 'Glasses wizard') {
    ?>
    <div id="wizard" class="wizard display-none">
        <div class="wizard-container">
            <div class="wizard-steps">
                <div class="wizard-roadmap">
                    <div class="wizard-heading">
                        <button class="wizard-back-button"><</button>
                        <p class="wizard-header">Usage</p>
                        <p class="wizard-header-desktop">Back to Frame Description</p>
                    </div>
                    <div class="wizard-circles">
                        <div class="circle circle-active">
                            <p class="circle-text">Usage</p>
                        </div>
                        <div class="circle">
                            <p class="circle-text">Prescription</p>
                        </div>
                        <div class="circle">
                            <p class="circle-text">Type</p>
                        </div>
                        <div class="circle">
                            <p class="circle-text">Lens</p>
                        </div>
                    </div>
                </div>
                <?php require 'wizard1.php'; ?>
                <?php require 'wizard2.php'; ?>
                <?php require 'wizard3.php'; ?>
                <?php require 'wizard4.php'; ?>
                <!--            --><?php //require 'wizard-review.php'; ?>
            </div>
            <div class="wizard-total-wrapper">
                <div class="total-brand--mobile">
                    <?php echo $product->get_attribute('Brand'); ?>
                </div>
                <div class="total-name-mobile">
                    <?php the_title(); ?>
                </div>
                <div class="total-img">
                    <img class="wizard-image" src="" alt="<?php the_title(); ?>">
                </div>
                <div class="total-brand">
                    <?php echo $product->get_attribute('Brand'); ?>
                </div>
                <div class="total-name">
                    <?php the_title(); ?>
                </div>
                <p class="frame">Frame</p>
                <div class="total-frame">
                    <div class="variations"></div>
                    <div class="price"></div>
                </div>
                <p class="table-name display-none">Your prescription
                    <svg viewBox="0 0 12 12" width="12" height="12" class="open-saved-pre rotate-90">
                        <use xlink:href="#chevron-right"></use>
                    </svg>
                </p>
                <div class="total-prescriptions">
                    <table>
                        <tr>
                            <td></td>
                            <td>SPH</td>
                            <td>CYl</td>
                            <td>Axis</td>
                            <td>ADD</td>
                            <td>PD</td>
                        </tr>
                        <tr>
                            <td>OD<span>(R)</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>OS<span>(L)</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="frame-options"></div>
                <div class="wizard-subtotal">
                    <p>Subtotal:</p>
                    <p class="current-price"></p>
                </div>
                <button class="add-to-cart display-none">Add to cart</button>
            </div>
            <div class="wizard-subtotal-mobile wizard1-subtotal">
                <p>Subtotal:</p>
                <p class="current-price"></p>
            </div>
        </div>

    </div>
    <?php

    require 'wizard-popups.php';
} else if (get_field('wizard_needed') === 'Lenses wizard') { ?>
    <div class="lenses-wizard display-none">
        <div class="headline">
            <div class="button-back-to-product">
                <svg class="chevron-left" width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg>
                Back to Description
            </div>
        </div>
        <div class="button-back-to-product-mobile">
            <svg class="chevron-left" width="8" height="14">
                <use xlink:href="#chevron-left"></use>
            </svg> Prescription</div>

        <div class="lenses-container">

            <div class="prescription-step">
                <div class="wizard-circles">
                    <div class="circle circle-active">
                        <p class="circle-text">Prescription</p>
                    </div>
                    <div class="circle">
                        <p class="circle-text">Quantity</p>
                    </div>
                </div>

                <div class="step-heading-wrapper">
                    <p class="step-heading-text">My Prescription</p>
                    <?php
                    if (!is_user_logged_in()) {
                    ?>
                    <p><span class="lens-sign-in">Sign in</span> to use previos prescription</p>
                    <?php
                    } else {
                        $user_id = get_current_user_id();

                        $prescriptions = get_user_meta($user_id, 'lens_prescriptions', true);

                    foreach ($prescriptions as $pre) {
                        $current = $pre[key($pre)];

//                        var_dump(key($pre));
                    }
                        ?>
                        <p class="saved-text">Saved prescriptions</p>
                        <div class="saved-select-wrapper">
                            <select class="saved-select-lenses">
                                <option>My prescriptions</option>
                                <?php

                                foreach ($prescriptions as $pre) {
                                    $current = $pre[key($pre)];
                                    ?>
                                    <option value="<?php echo key($pre); ?>"
                                            data-lens-both="<?php echo $current['lens_both']; ?>"
                                            data-lens-right="<?php echo str_replace('.', '-', $current['lens-right']); ?>"
                                            data-lens-left="<?php echo str_replace('.', '-', $current['lens-left']); ?>"
                                            data-lens-comment="<?php echo $current['lens_comment']; ?>"
                                    ><?php echo key($pre); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <button class="add-new-lenses-button">Add new</button>
                        </div>
                        <?php
                    } ?>
                </div>

                <hr>

                <div class="prescription-select-wrapper">
                    <p class="both-eyeglass-toggle">Same prescription for both eyes
                        <label class="both-eye switch">
                            <input id="both-eye-checkbox" type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </p>

                    <div class="right-eye-wrap">
                        <p class="eye-wrap-heading">Select your right eye(OD) prescription</p>
                        <p>Power/Sphere</p>
                        <select name="sph" id="lenses-right-sph"></select>
                    </div>

                    <div class="left-eye-wrap">
                        <p class="eye-wrap-heading">Select your left eye(OS) prescription</p>
                        <p>Power/Sphere</p>
                        <select name="sph" id="lenses-left-sph"></select>
                    </div>

<!--                </div>-->
                    <label class="save-new-pre-lens-label display-none" for="new-name-lens">
                        Save this prescription as:
                        <input type="text" id="new-name-lens" placeholder="">
                </div>
                <div class="button-lens-wrap button-lens-wrap-mobile">
                    <button class="wizard-lens-back-button-desktop" type="button">Back</button>
                    <button class="wizard-lens-btn-continue" type="button" disabled>Select and continue</button>
                </div>
            </div>


            <div class="wizard-total-wrapper">

                <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" alt="">
                <div class="total-brand">
                    <?php echo $product->get_attribute('Brand'); ?>
                </div>
                <div class="total-name">
                    <?php the_title(); ?>
                </div>
<!--                <p class="frame">Frame</p>-->
                <div class="total-frame">
                    <div class="variations"></div>
                    <div class="price"></div>
                </div>
                <div class="frame-options"></div>
                <div class="wizard-subtotal">
                    <p>Subtotal:</p>
                    <p class="current-price"></p>
                </div>

                <button class="add-to-cart display-none">Add to cart</button>

            </div>
        </div>
    </div>

    <div class="lenses-wizard2 display-none">
        <div class="headline">
            <div class="button-back-to-product">
                <svg class="chevron-left" width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg>
                Back to Description
            </div>
        </div>

        <div class="lenses-container">

            <div class="prescription-step">
                <div class="back-to-Prescription">
                    <svg class="chevron-left" width="8" height="14">
                        <use xlink:href="#chevron-left"></use>
                    </svg>Quantity
                </div>

                <div class="wizard-circles">
                    <div class="circle circle-completed">
                        <p class="circle-text">Prescription</p>
                    </div>
                    <div class="circle circle-active">
                        <p class="circle-text">Quantity</p>
                    </div>
                </div>

                <div class="step-heading-wrapper">
                    <p class="step-heading-text">Select your quantity</p>
                </div>

<!--                <hr>-->

                <div class="prescription-select-wrapper">

                    <?php

                    global $product;
                    $price = get_post_meta(get_the_ID(), '_regular_price', true);
                    $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);

                    $current_cost = 0;

                    if ($product->is_type('variable')) {
                        $variations = $product->get_available_variations();
                        foreach ($variations as $variation) {
                            $price = get_post_meta($variation['variation_id'], '_regular_price', true);
                            $price_sale = get_post_meta($variation['variation_id'], '_sale_price', true);
                        }
                        if ($product->is_on_sale())  : ?>
                            <?php if ($price_sale !== "") {
                                $current_cost = $price_sale;
                            } ?>

                        <?php else : ?>
                            <?php $current_cost = $price; ?>
                        <?php endif; ?>
                        <?php
                    } else { ?>
                        <?php
                        global $product;
                        $price = get_post_meta(get_the_ID(), '_regular_price', true);
                        $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
                        if ($product->is_on_sale())  : ?>
                            <?php if ($price_sale !== "") {
                                $current_cost = $price_sale;
                            } ?>
                        <?php else : ?>
                            <?php $current_cost = $price; ?>
                        <?php endif; ?>
                    <?php }

                    ?>


                    <input type="radio" name="quantity-step" value="3" class="choice-count" id="choice-count-3" data-month="3 month" data-cost="<?php echo $current_cost * 6; ?>€">
                    <label class="wizard-lenses-item" for="choice-count-3">

                        <div class="label-info">
                            <p class="label-info-first">3 months - <?php echo $current_cost * 6; ?>€</p>
                            <p class="label-info-second">3 monthly contact for right eye</p>
                        </div>
                    </label>

                    <input type="radio" name="quantity-step" value="6" class="choice-count" id="choice-count-6" data-month="6 month" data-cost="<?php echo $current_cost * 12; ?>€">
                    <label class="wizard-lenses-item" for="choice-count-6">

                        <div class="label-info">
                            <p class="label-info-first">6 months - <?php echo $current_cost * 12; ?>€</p>
                            <p class="label-info-second">6 monthly contact for right eye</p>
                        </div>
                    </label>

                    <input type="radio" name="quantity-step" value="12" class="choice-count" id="choice-count-12" data-month="12 month" data-cost="<?php echo $current_cost * 24; ?>€">
                    <label class="wizard-lenses-item" for="choice-count-12">

                        <div class="label-info">
                            <p class="label-info-first">1 year - <?php echo $current_cost * 24; ?>€</p>
                            <p class="label-info-second">12 monthly contact for right eye</p>
                        </div>

<!--                        <button class="label-tooltip" data-bs-toggle="modal" data-bs-target="#wizard1ReadingModal">-->
<!--                            <svg viewBox="0 0 17 17" width="17" height="17" class="">-->
<!--                                <use xlink:href="#modal-i"></use>-->
<!--                            </svg>-->
<!--                        </button>-->
                    </label>

                </div>

                <div class="button-lens-wrap button-lens-wrap-mobile">
                    <button class="wizard-lens-back-button-desktop">Back</button>
                    <button class="wizard-lens-btn-add-to-cart add-contact-lenses-to-cart add-contact-lenses-to-cart--mobile" disabled>Add to cart</button>

                    <button class="wizard-lens-mobile-step" type="button" disabled>Continue</button>
                </div>
            </div>

            <div class="wizard-total-wrapper wizard-total-wrapper--mobile">
                <div class="back-to-quantity">
                    <svg class="chevron-left" width="8" height="14">
                        <use xlink:href="#chevron-left"></use>
                    </svg>
                    Review
                </div>
                <div class="wizard-circles wizard-circles-mobile">
                    <div class="circle circle-completed">
                        <p>Prescription</p>
                    </div>
                    <div class="circle circle-completed">
                        <p>Quantity</p>
                    </div>
                </div>

                <div class="total-brand--mobile">
                    <?php echo $product->get_attribute('Brand'); ?>
                </div>
                <div class="total-name total-name-mobile">
                    <?php the_title(); ?>
                </div>
                <img src="<?php echo get_the_post_thumbnail_url($product_id); ?>" alt="">
                <div class="total-brand--desktop">
                    <?php echo $product->get_attribute('Brand'); ?>
                </div>
                <div class="total-name total-name-desktop">
                    <?php the_title(); ?>
                </div>

                <div class="total-frame">
                    <div class="variations"></div>
                    <div class="price"></div>
                </div>

                <div class="total-prescriptions">
                    <p class="order-item-pre-heading">

                        Your prescription
                        <svg viewBox="0 0 12 12" width="12" height="12" class="order-product-open-lenses">
                            <use xlink:href="#chevron-left"></use>
                        </svg>
                    </p>
                    <div class="lenses-configuration">
                    <table>
                        <tr>
                            <td></td>
                            <td>SPH</td>
                            <td>BC</td>
                            <td>DN</td>
                        </tr>
                        <tr>
                            <td>OD<span>(R)</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>OS<span>(L)</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    </div>
                    <p class="total-price-wrapper">
                        <span class="lens-count-total" id="lens-count-total">month</span>
                        <span class="total" id="lens-total"></span>
                    </p>
                </div>


                <div class="frame-options"></div>
                <div class="wizard-subtotal">
                    <p>Subtotal:</p>
                    <p class="current-price"></p>
                </div>

                <div class="button-lens-wrap-mobile">
                    <div class="wizard-subtotal">
                        <p>Subtotal:</p>
                        <p class="current-price"></p>
                    </div>
                <button class="wizard-lens-btn-add-to-cart add-contact-lenses-to-cart display-none" disabled>Add to cart</button>
                </div>
            </div>
        </div>
    </div>
<?php }

do_action('woocommerce_after_single_product'); ?>


