<?php

?>

<div class="wizard4">
    <div class="sm-distancereading-clear display-none step-wrapper last-step step41">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
            <div class="wizard-item-noimage" data-label="15s">
                <input type="radio" id="15s" name="sm-distancereading-clear" value="15s" class="radio-hide"
                    <?php
                    if (get_field('step41_1_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_1_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="15s">1,50 - Stock
                    <?php
                    if (get_field('step41_1_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_1_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_1">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_1">
                        <?php echo get_field('step41_1_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16s">
                <input type="radio" id="16s" name="sm-distancereading-clear" value="16s" class="radio-hide"
                    <?php
                    if (get_field('step41_2_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_2_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="16s">1,60 - Stock
                    <?php
                    if (get_field('step41_2_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_2_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_2">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_2">
                        <?php echo get_field('step41_2_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="167s">
                <input type="radio" id="167s" name="sm-distancereading-clear" value="167s" class="radio-hide"
                    <?php
                    if (get_field('step41_3_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_3_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="167s">1,67 - Stock
                    <?php
                    if (get_field('step41_3_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_3_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_3">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_3">
                        <?php echo get_field('step41_3_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="15l">
                <input type="radio" id="15l" name="sm-distancereading-clear" value="15l" class="radio-hide"
                    <?php
                    if (get_field('step41_4_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_4_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="15l">1,50 - Laboratory
                    <?php
                    if (get_field('step41_4_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_4_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_4">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_4">
                        <?php echo get_field('step41_4_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16l">
                <input type="radio" id="16l" name="sm-distancereading-clear" value="16l" class="radio-hide"
                    <?php
                    if (get_field('step41_5_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_5_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="16l">1,60 - Laboratory
                    <?php
                    if (get_field('step41_5_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_5_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_5">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_5">
                        <?php echo get_field('step41_5_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="167l">
                <input type="radio" id="167l" name="sm-distancereading-clear" value="167l" class="radio-hide"
                    <?php
                    if (get_field('step41_6_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_6_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="167l">1,67 - Laboratory
                    <?php
                    if (get_field('step41_6_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_6_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step41_6">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_6">
                        <?php echo get_field('step41_6_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="174l">
                <input type="radio" id="174l" name="sm-distancereading-clear" value="174l" class="radio-hide"
                    <?php
                    if (get_field('step41_7_price', 'options')) {
                        echo 'data-ad="' . get_field('step41_7_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="174l">1,74 - Laboratory
                    <?php
                    if (get_field('step41_7_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step41_7_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-hover="#tooltip_step41_7">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step41_7">
                        <?php echo get_field('step41_7_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
    </div>
    <div class="sm-distancereading-bluelight display-none step-wrapper last-step step42">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
            <div class="wizard-item-noimage" data-label="15s">
                <input type="radio" id="15s" name="sm-distancereading-bluelight" value="15s" class="radio-hide"
                    <?php
                    if (get_field('step42_1_price', 'options')) {
                        echo 'data-ad="' . get_field('step42_1_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="15s">1,50 - Stock
                    <?php
                    if (get_field('step42_1_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step42_1_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step42_1">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step42_1">
                        <?php echo get_field('step42_1_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16s">
                <input type="radio" id="16s" name="sm-distancereading-bluelight" value="16s" class="radio-hide"
                    <?php
                    if (get_field('step42_2_price', 'options')) {
                        echo 'data-ad="' . get_field('step42_2_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="16s">1,60 - Stock
                    <?php
                    if (get_field('step42_2_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step42_2_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step42_2">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step42_2">
                        <?php echo get_field('step42_2_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="15l">
                <input type="radio" id="15l" name="sm-distancereading-bluelight" value="15l" class="radio-hide"
                    <?php
                    if (get_field('step42_3_price', 'options')) {
                        echo 'data-ad="' . get_field('step42_3_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="15l">1,50 - Laboratory
                    <?php
                    if (get_field('step42_3_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step42_3_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step42_3">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step42_3">
                        <?php echo get_field('step42_3_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16l">
                <input type="radio" id="16l" name="sm-distancereading-bluelight" value="16l" class="radio-hide"
                    <?php
                    if (get_field('step42_4_price', 'options')) {
                        echo 'data-ad="' . get_field('step42_4_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="16l">1,60 - Laboratory
                    <?php
                    if (get_field('step42_4_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step42_4_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step42_4">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step42_4">
                        <?php echo get_field('step42_4_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="167l">
                <input type="radio" id="167l" name="sm-distancereading-bluelight" value="167l" class="radio-hide"
                    <?php
                    if (get_field('step42_5_price', 'options')) {
                        echo 'data-ad="' . get_field('step42_5_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="167l">1,67 - Laboratory
                    <?php
                    if (get_field('step42_5_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step42_5_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step42_5">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step42_5">
                        <?php echo get_field('step42_5_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
    </div>
    <div class="sm-distancereading-sun display-none step-wrapper step43">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Sun protection</p>
        </div>
        <div class="wizard-item-noimage basic">
            <input type="radio" id="basic" name="sm-distancereading-sun" value="basic" class="radio-hide">
            <label for="basic">
                <span>Basic</span>
                    <?php
                    if (get_field('step43_basic_price', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step43_basic_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step43_basic_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_basic_price', 'options')) {
                                    echo 'data-ad="' . get_field('step43_basic_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step43_basic_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step43_basic_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step43_basic_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_basic_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step43_basic_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step43_basic">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step43_basic">
                    <?php echo get_field('step43_basic_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage gradient">
            <input type="radio" id="gradient" name="sm-distancereading-sun" value="gradient" class="radio-hide">
            <label for="gradient">
                <span>Gradient</span>
                <?php
                if (get_field('step43_gradient_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step43_gradient_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step43_gradient_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_gradient_price', 'options')) {
                                    echo 'data-ad="' . get_field('step43_gradient_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step43_gradient_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step43_gradient_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step43_gradient_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_gradient_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step43_gradient_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step43_gradient">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step43_gradient">
                    <?php echo get_field('step43_gradient_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage polarized">
            <input type="radio" id="polarized" name="sm-distancereading-sun" value="polarized" class="radio-hide">
            <label for="polarized">
                <span>Polarized</span>
                <?php
                if (get_field('step43_polarized_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step43_polarized_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step43_polarized_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_polarized_price', 'options')) {
                                    echo 'data-ad="' . get_field('step43_polarized_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step43_polarized_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step43_polarized_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step43_polarized_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step43_polarized_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step43_polarized_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step43_polarized">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step43_polarized">
                    <?php echo get_field('step43_polarized_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage mirrored">
                <input type="radio" id="mirrored" name="sm-distancereading-sun" value="mirrored" class="radio-hide">
                <label for="mirrored">
                    <span>Mirrored</span>
                    <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                    <span class="tint-unavailable display-none">Unavailable</span>
                </label>
                <div class="tint-color-choose display-none">
                    <p>Tint Color
                        <?php
                        if (get_field('step43_mirrored_price', 'options')) {
                            ?>
                            <span class="prism-weight"> + <?php echo get_field('step43_mirrored_price', 'options'); ?>€</span>
                            <?php
                        }
                        ?>
                        : <span class="current-tint-color"></span></p>
                    <div class="tint-colors">
                        <?php
                        $colors = get_field('step43_mirrored_tint', 'options');

                        foreach ($colors as $color) {
                            ?>
                            <label style="background: <?php echo $color['color']; ?>">
                                <?php echo $color['name']; ?>
                                <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color" value="<?php echo $color['name']; ?>" class="tint-color-input"
                                    <?php
                                    if (get_field('step43_mirrored_price', 'options')) {
                                        echo 'data-ad="' . get_field('step43_mirrored_price', 'options') . '"';
                                    }
                                    ?>
                                >
                            </label>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step43_mirrored">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step43_mirrored">
                    <?php echo get_field('step43_mirrored_tooltip', 'options'); ?>
                </span>
            </div>
            </div>
        <div>
            <div class="wizard-subtotal-mobile wizard4-subtotal display-none">
                <p>Subtotal:</p>
                <p class="current-price"></p>
            </div>
            <button class="continue-button tint-continue" disabled>Continue</button>
        </div>
    </div>
    <div class="sm-distancereading-sun-thickness display-none step-wrapper last-step step44">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
            <div class="wizard-item-noimage" data-label="15s">
                <input type="radio" id="sun-15s" name="sm-distancereading-sun-thickness" value="sun-15s" class="radio-hide"
                    <?php
                    if (get_field('step44_1_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_1_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-15s">1,50 - Stock
                    <?php
                    if (get_field('step44_1_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_1_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_1">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_1">
                        <?php echo get_field('step44_1_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16s">
                <input type="radio" id="sun-16s" name="sm-distancereading-sun-thickness" value="sun-16s" class="radio-hide"
                    <?php
                    if (get_field('step44_2_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_2_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-16s">1,60 - Stock
                    <?php
                    if (get_field('step44_2_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_2_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_2">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_2">
                        <?php echo get_field('step44_2_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="15l">
                <input type="radio" id="sun-150l" name="sm-distancereading-sun-thickness" value="sun-150l" class="radio-hide"
                    <?php
                    if (get_field('step44_3_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_3_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-150l">1,50 - Laboratory + Tint
                    <?php
                    if (get_field('step44_3_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_3_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_3">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_3">
                        <?php echo get_field('step44_3_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16l">
                <input type="radio" id="sun-160l" name="sm-distancereading-sun-thickness" value="sun-160l" class="radio-hide"
                    <?php
                    if (get_field('step44_4_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_4_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-160l">1,60 - Laboratory + Tint
                    <?php
                    if (get_field('step44_4_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_4_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_4">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_4">
                        <?php echo get_field('step44_4_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="167l">
                <input type="radio" id="sun-167l" name="sm-distancereading-sun-thickness" value="sun-167l" class="radio-hide"
                    <?php
                    if (get_field('step44_5_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_5_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-167l">1,67 - Laboratory + Tint
                    <?php
                    if (get_field('step44_5_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_5_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_5">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_5">
                        <?php echo get_field('step44_5_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="15lp">
                <input type="radio" id="sun-150l-polar" name="sm-distancereading-sun-thickness" value="sun-150l-polar" class="radio-hide"
                    <?php
                    if (get_field('step44_6_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_6_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-150l-polar">1,50 - Laboratory - NuPolar
                    <?php
                    if (get_field('step44_6_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_6_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_6">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_6">
                        <?php echo get_field('step44_6_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>

            <div class="wizard-item-noimage" data-label="16lp">
                <input type="radio" id="sun-160l-polar" name="sm-distancereading-sun-thickness" value="sun-160l-polar" class="radio-hide"
                    <?php
                    if (get_field('step44_7_price', 'options')) {
                        echo 'data-ad="' . get_field('step44_7_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="sun-160l-polar">1,60 - Laboratory - NuPolar
                    <?php
                    if (get_field('step44_7_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step44_7_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
                <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step44_7">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
                <div class="tooltip_templates">
                    <span id="tooltip_step44_7">
                        <?php echo get_field('step44_7_tooltip', 'options'); ?>
                    </span>
                </div>
            </div>
    </div>
    <div class="sm-distancereading-ajusting display-none step-wrapper step45">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Light ajusting</p>
        </div>
        <div class="wizard-item-noimage">
            <input type="radio" id="transitions-gen-8" name="sm-distancereading-ajusting" value="transitions-gen-8" class="radio-hide">
            <label for="transitions-gen-8">
                <span>Transitions Gen 8</span>
                <?php
                if (get_field('step45_gen8_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step45_gen8_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step45_gen8_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step45_gen8_price', 'options')) {
                                    echo 'data-ad="' . get_field('step45_gen8_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step45_gen8_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step45_gen8_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step45_gen8_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step45_gen8_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step45_gen8_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step45_gen8">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step45_gen8">
                    <?php echo get_field('step45_gen8_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage">
            <input type="radio" id="transition-xtractive" name="sm-distancereading-ajusting" value="transition-xtractive" class="radio-hide">
            <label for="transition-xtractive">
                <span>Transition XTRActive</span>
                <?php
                if (get_field('step45_xtractive_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step45_xtractive_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step45_xtractive_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step45_xtractive_price', 'options')) {
                                    echo 'data-ad="' . get_field('step45_xtractive_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step45_xtractive_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step45_xtractive_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step45_xtractive_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step45_xtractive_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step45_xtractive_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step45_xtractive">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step45_xtractive">
                    <?php echo get_field('step45_xtractive_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div>
            <div class="wizard-subtotal-mobile wizard4-subtotal display-none">
                <p>Subtotal:</p>
                <p class="current-price"></p>
            </div>
            <button class="continue-button tint-continue" disabled>Select and continue</button>
        </div>
    </div>
    <div class="sm-distancereading-ajusting-thickness display-none step-wrapper last-step step46">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="16s">
            <input type="radio" id="ajust-16s" name="sm-distancereading-ajusting-thickness" value="ajust-16s"
                   class="radio-hide"
                <?php
                if (get_field('step46_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16s">1,60 - Stock - Gen8
                <?php
                if (get_field('step46_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_1">
                    <?php echo get_field('step46_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167s">
            <input type="radio" id="ajust-167s" name="sm-distancereading-ajusting-thickness" value="ajust-167s"
                   class="radio-hide"
                <?php
                if (get_field('step46_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167s">1,67 - Stock - Gen8
                <?php
                if (get_field('step46_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_2">
                    <?php echo get_field('step46_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="ajust-15l" name="sm-distancereading-ajusting-thickness" value="ajust-15l"
                   class="radio-hide"
                <?php
                if (get_field('step46_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15l">1,50 - Laboratory - Gen8
                <?php
                if (get_field('step46_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_3">
                    <?php echo get_field('step46_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="ajust-16l" name="sm-distancereading-ajusting-thickness" value="ajust-16l"
                   class="radio-hide"
                <?php
                if (get_field('step46_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16l">1,60 - Laboratory - Gen8
                <?php
                if (get_field('step46_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_4">
                    <?php echo get_field('step46_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="ajust-167l" name="sm-distancereading-ajusting-thickness" value="ajust-167l"
                   class="radio-hide"
                <?php
                if (get_field('step46_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167l">1,67 - Laboratory - Gen8
                <?php
                if (get_field('step46_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_5">
                    <?php echo get_field('step46_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="ajust-174l" name="sm-distancereading-ajusting-thickness" value="ajust-174l"
                   class="radio-hide"
                <?php
                if (get_field('step46_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step46_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-174l">1,74 - Laboratory - Gen8
                <?php
                if (get_field('step46_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step46_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step46_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step46_6">
                    <?php echo get_field('step46_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-distancereading-ajusting-thickness2 display-none step-wrapper last-step step47">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15sx">
            <input type="radio" id="ajust-15sx" name="sm-distancereading-ajusting-thickness" value="ajust-15sx" class="radio-hide"
                <?php
                if (get_field('step47_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step47_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15sx">1,50 - Stock - XTRA
                <?php
                if (get_field('step47_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step47_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_1">
                    <?php echo get_field('step47_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16sx">
            <input type="radio" id="ajust-16sx" name="sm-distancereading-ajusting-thickness" value="ajust-16sx" class="radio-hide"
                <?php
                if (get_field('step47_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step47_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16sx">1,60 - Stock - XTRA
                <?php
                if (get_field('step47_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step47_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_2">
                    <?php echo get_field('step47_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16asx">
            <input type="radio" id="ajust-16asx" name="sm-distancereading-ajusting-thickness" value="ajust-16asx" class="radio-hide"
                <?php
                if (get_field('step47_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step47_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16asx">1,60 - AS - Stock - XTRA
                <?php
                if (get_field('step47_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step47_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_3">
                    <?php echo get_field('step47_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lx">
            <input type="radio" id="ajust-15lx" name="sm-distancereading-ajusting-thickness" value="ajust-15lx" class="radio-hide"
                <?php
                if (get_field('step47_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step47_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15lx">1,50 - Laboratory - XTRA
                <?php
                if (get_field('step47_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step47_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_4">
                    <?php echo get_field('step47_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lx">
            <input type="radio" id="ajust-16lx" name="sm-distancereading-ajusting-thickness" value="ajust-16lx" class="radio-hide"
                <?php
                if (get_field('step47_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step47_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16lx">1,60 - Laboratory - XTRA
                <?php
                if (get_field('step47_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step47_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_5">
                    <?php echo get_field('step47_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lx">
                <input type="radio" id="ajust-167lx" name="sm-distancereading-ajusting-thickness" value="ajust-167lx" class="radio-hide"
                    <?php
                    if (get_field('step47_6_price', 'options')) {
                        echo 'data-ad="' . get_field('step47_6_price', 'options') . '"';
                    }
                    ?>
                >
                <label for="ajust-167lx">1,67 - Laboratory - XTRA
                    <?php
                    if (get_field('step47_6_price', 'options')) {
                        ?>
                        <span> + <?php echo get_field('step47_6_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step47_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step47_6">
                    <?php echo get_field('step47_6_tooltip', 'options'); ?>
                </span>
            </div>
            </div>
    </div>
    <div class="sm-multifocal-clear display-none step-wrapper last-step step48">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="multifocal-15l" name="sm-distancereading-clear" value="15l" class="radio-hide"
                <?php
                if (get_field('step48_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step48_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-15l">1,50 - Laboratory
                <?php
                if (get_field('step48_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step48_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step48_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step48_1">
                    <?php echo get_field('step48_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="multifocal-16l" name="sm-distancereading-clear" value="16l" class="radio-hide"
                <?php
                if (get_field('step48_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step48_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-16l">1,60 - Laboratory
                <?php
                if (get_field('step48_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step48_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step48_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step48_2">
                    <?php echo get_field('step48_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="multifocal-167l" name="sm-distancereading-clear" value="167l" class="radio-hide"
                <?php
                if (get_field('step48_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step48_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-167l">1,67 - Laboratory
                <?php
                if (get_field('step48_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step48_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step48_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step48_3">
                    <?php echo get_field('step48_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="multifocal-174l" name="sm-distancereading-clear" value="174l" class="radio-hide"
                <?php
                if (get_field('step48_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step48_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-174l">1,74 - Laboratory
                <?php
                if (get_field('step48_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step48_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step48_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step48_4">
                    <?php echo get_field('step48_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-multifocal-bluelight display-none step-wrapper last-step step49">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="sm-multifocal-bluelight-15l" name="sm-multifocal-bluelight" value="15l" class="radio-hide"
                <?php
                if (get_field('step49_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step49_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="sm-multifocal-bluelight-15l">1,50 - Laboratory
                <?php
                if (get_field('step49_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step49_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step49_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step49_1">
                    <?php echo get_field('step49_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="sm-multifocal-bluelight-16l" name="sm-multifocal-bluelight" value="16l" class="radio-hide"
                <?php
                if (get_field('step49_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step49_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="sm-multifocal-bluelight-16l">1,60 - Laboratory
                <?php
                if (get_field('step49_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step49_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step49_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step49_2">
                    <?php echo get_field('step49_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="sm-multifocal-bluelight-167l" name="sm-multifocal-bluelight" value="167l" class="radio-hide"
                <?php
                if (get_field('step49_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step49_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="sm-multifocal-bluelight-167l">1,67 - Laboratory
                <?php
                if (get_field('step49_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step49_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step49_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step49_3">
                    <?php echo get_field('step49_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-multifocal-sun display-none step-wrapper step410">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Sun protection</p>
        </div>
        <div class="wizard-item-noimage basic">
            <input type="radio" id="basic410" name="sm-multifocal-sun" value="basic410" class="radio-hide">
            <label for="basic410">
                <span>Basic</span>
                <?php
                if (get_field('step410_basic_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step410_basic_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step410_basic_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_basic_price', 'options')) {
                                    echo 'data-ad="' . get_field('step410_basic_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step410_basic_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step410_basic_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step410_basic_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_basic_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step410_basic_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step410_basic">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step410_basic">
                    <?php echo get_field('step410_basic_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage gradient">
            <input type="radio" id="gradient410" name="sm-multifocal-sun" value="gradient410" class="radio-hide">
            <label for="gradient410">
                <span>Gradient</span>
                <?php
                if (get_field('step410_gradient_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step410_gradient_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step410_gradient_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_gradient_price', 'options')) {
                                    echo 'data-ad="' . get_field('step410_gradient_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step410_gradient_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step410_gradient_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step410_gradient_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_gradient_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step410_gradient_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step410_gradient">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step410_gradient">
                    <?php echo get_field('step410_gradient_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage polarized">
            <input type="radio" id="polarized410" name="sm-multifocal-sun" value="polarized410" class="radio-hide">
            <label for="polarized410">
                <span>Polarized</span>
                <?php
                if (get_field('step410_polarized_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step410_polarized_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step410_polarized_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_polarized_price', 'options')) {
                                    echo 'data-ad="' . get_field('step410_polarized_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step410_polarized_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step410_polarized_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step410_polarized_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_polarized_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step410_polarized_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step410_polarized">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step410_polarized">
                    <?php echo get_field('step410_polarized_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage mirrored">
            <input type="radio" id="mirrored410" name="sm-multifocal-sun" value="mirrored410" class="radio-hide">
            <label for="mirrored410">
                <span>Mirrored</span>
                <span class="tint-text">Choose from these classic colors to make your own sunglasses</span>
                <span class="tint-unavailable display-none">Unavailable</span>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color
                    <?php
                    if (get_field('step410_mirrored_price', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step410_mirrored_price', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step410_mirrored_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color" value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step410_mirrored_price', 'options')) {
                                    echo 'data-ad="' . get_field('step410_mirrored_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step410_mirrored">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step410_mirrored">
                    <?php echo get_field('step410_mirrored_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
        <div>
            <div class="wizard-subtotal-mobile wizard4-subtotal display-none">
                <p>Subtotal:</p>
                <p class="current-price"></p>
            </div>
            <button class="continue-button tint-continue" disabled>Continue</button>
        </div>
    </div>
    <div class="sm-multifocal-sun-thickness display-none step-wrapper last-step step411">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="multifocal-sun-15l1" name="sm-multifocal-sun-thickness" value="sun-15l" class="radio-hide"
                <?php
                if (get_field('step411_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step411_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-15l1">1,50 - Laboratory + Tint
                <?php
                if (get_field('step411_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step411_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step411_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step411_1">
                    <?php echo get_field('step411_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="multifocal-sun-16l1" name="sm-multifocal-sun-thickness" value="sun-16l" class="radio-hide"
                <?php
                if (get_field('step411_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step411_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-16l1">1,60 - Laboratory + Tint
                <?php
                if (get_field('step411_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step411_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step411_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step411_2">
                    <?php echo get_field('step411_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="multifocal-sun-167l1" name="sm-multifocal-sun-thickness" value="sun-167l" class="radio-hide"
                <?php
                if (get_field('step411_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step411_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-167l1">1,67 - Laboratory + Tint
                <?php
                if (get_field('step411_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step411_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step411_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step411_3">
                    <?php echo get_field('step411_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lp">
            <input type="radio" id="multifocal-sun-15l-polar1" name="sm-multifocal-sun-thickness" value="sun-15l-polar" class="radio-hide"
                <?php
                if (get_field('step411_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step411_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-15l-polar1">1,50 - Laboratory - NuPolar
                <?php
                if (get_field('step411_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step411_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step411_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step411_4">
                    <?php echo get_field('step411_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lp">
            <input type="radio" id="multifocal-sun-16l-polar1" name="sm-multifocal-sun-thickness" value="sun-16l-polar" class="radio-hide"
                <?php
                if (get_field('step411_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step411_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-16l-polar1">1,60 - Laboratory - NuPolar
                <?php
                if (get_field('step411_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step411_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step411_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step411_5">
                    <?php echo get_field('step411_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-multifocal-ajusting display-none step-wrapper step412">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Light ajusting</p>
        </div>

        <div class="wizard-item-noimage">
            <input type="radio" id="transitions-gen-8412" name="sm-distancereading-ajusting412" value="transitions-gen-8412" class="radio-hide">
            <label for="transitions-gen-8412">
                <span>Transitions Gen 8</span>
                <?php
                if (get_field('step412_gen8_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step412_gen8_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step412_gen8_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step412_gen8_price', 'options')) {
                                    echo 'data-ad="' . get_field('step412_gen8_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step412_gen8_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step412_gen8_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step412_gen8_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step412_gen8_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step412_gen8_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step412_gen8">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step412_gen8">
                    <?php echo get_field('step412_gen8_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage">
            <input type="radio" id="transition-xtractive412" name="sm-distancereading-ajusting412" value="transition-xtractive412" class="radio-hide">
            <label for="transition-xtractive412">
                <span>Transition XTRActive</span>
                <?php
                if (get_field('step412_xtractive_price', 'options')) {
                    ?>
                    <span class="prism-weight"> + <?php echo get_field('step412_xtractive_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <div class="tint-color-choose display-none">
                <p>Tint Color: <span class="current-tint-color"></span></p>
                <div class="tint-colors">
                    <?php
                    $colors = get_field('step412_xtractive_tint', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-color"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step412_xtractive_price', 'options')) {
                                    echo 'data-ad="' . get_field('step412_xtractive_price', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tint-color-mirrored display-none">
                <p>Tint Mirror (optional)
                    <?php
                    if (get_field('step412_xtractive_price_mirror', 'options')) {
                        ?>
                        <span class="prism-weight"> + <?php echo get_field('step412_xtractive_price_mirror', 'options'); ?>€</span>
                        <?php
                    }
                    ?>
                    : <span class="current-tint-color"></span></p>
                <div class="tint-mirror">
                    <?php
                    $colors = get_field('step412_xtractive_tint_mirror', 'options');

                    foreach ($colors as $color) {
                        ?>
                        <label style="background: <?php echo $color['color']; ?>">
                            <?php echo $color['name']; ?>
                            <input type="radio" id="<?php echo $color['name']; ?>" name="tint-mirror"
                                   value="<?php echo $color['name']; ?>" class="tint-color-input"
                                <?php
                                if (get_field('step412_xtractive_price_mirror', 'options')) {
                                    echo 'data-ad="' . get_field('step412_xtractive_price_mirror', 'options') . '"';
                                }
                                ?>
                            >
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step412_xtractive">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step412_xtractive">
                    <?php echo get_field('step412_xtractive_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div>
            <div class="wizard-subtotal-mobile wizard4-subtotal display-none">
                <p>Subtotal:</p>
                <p class="current-price"></p>
            </div>
            <button class="continue-button tint-continue" disabled>Select and continue</button>
        </div>
    </div>
    <div class="sm-multifocal-ajusting-thickness display-none step-wrapper last-step step413">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="ajust-15l2" name="sm-distancereading-ajusting-thickness" value="ajust-15l" class="radio-hide"
                <?php
                if (get_field('step413_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step413_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15l2">1,50 - Laboratory - Gen8
                <?php
                if (get_field('step413_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step413_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step413_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step413_1">
                    <?php echo get_field('step413_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="ajust-16l2" name="sm-distancereading-ajusting-thickness" value="ajust-16l" class="radio-hide"
                <?php
                if (get_field('step413_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step413_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16l2">1,60 - Laboratory - Gen8
                <?php
                if (get_field('step413_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step413_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step413_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step413_2">
                    <?php echo get_field('step413_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="ajust-167l2" name="sm-distancereading-ajusting-thickness" value="ajust-167l" class="radio-hide"
                <?php
                if (get_field('step413_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step413_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167l2">1,67 - Laboratory - Gen8
                <?php
                if (get_field('step413_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step413_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step413_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step413_3">
                    <?php echo get_field('step413_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="ajust-174l2" name="sm-distancereading-ajusting-thickness" value="ajust-174l" class="radio-hide"
                <?php
                if (get_field('step413_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step413_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-174l2">1,74 - Laboratory - Gen8
                <?php
                if (get_field('step413_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step413_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step413_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step413_4">
                    <?php echo get_field('step413_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-multifocal-ajusting-thickness2 display-none step-wrapper last-step step414">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15lx">
            <input type="radio" id="ajust-15lx2" name="sm-distancereading-ajusting-thickness" value="ajust-15lx" class="radio-hide"
                <?php
                if (get_field('step414_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step414_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15lx2">1,50 - Laboratory - XTRA
                <?php
                if (get_field('step414_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step414_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step414_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step414_1">
                    <?php echo get_field('step414_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lx">
            <input type="radio" id="ajust-16lx2" name="sm-distancereading-ajusting-thickness" value="ajust-16lx" class="radio-hide"
                <?php
                if (get_field('step414_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step414_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16lx2">1,60 - Laboratory - XTRA
                <?php
                if (get_field('step414_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step414_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step414_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step414_2">
                    <?php echo get_field('step414_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lx">
            <input type="radio" id="ajust-167lx2" name="sm-distancereading-ajusting-thickness" value="ajust-167lx" class="radio-hide"
                <?php
                if (get_field('step414_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step414_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167lx2">1,67 - Laboratory - XTRA
                <?php
                if (get_field('step414_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step414_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step414_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step414_3">
                    <?php echo get_field('step414_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-single-clear display-none step-wrapper last-step step415">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-15l" name="sm-driving-single-clear" value="driving-single-15l" class="radio-hide"
                <?php
                if (get_field('step415_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step415_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-15l">1,50 - Laboratory
                <?php
                if (get_field('step415_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step415_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step415_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step415_1">
                    <?php echo get_field('step415_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-16l" name="sm-driving-single-clear" value="driving-single-16l" class="radio-hide"
                <?php
                if (get_field('step415_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step415_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-16l">1,60 - Laboratory
                <?php
                if (get_field('step415_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step415_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step415_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step415_2">
                    <?php echo get_field('step415_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-167l" name="sm-driving-single-clear" value="driving-single-167l" class="radio-hide"
                <?php
                if (get_field('step415_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step415_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-167l">1,67 - Laboratory
                <?php
                if (get_field('step415_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step415_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step415_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step415_3">
                    <?php echo get_field('step415_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="driving-single-174l" name="sm-driving-single-clear" value="driving-single-174l" class="radio-hide"
                <?php
                if (get_field('step415_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step415_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-174l">1,74 - Laboratory
                <?php
                if (get_field('step415_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step415_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step415_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step415_4">
                    <?php echo get_field('step415_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-single-sun display-none step-wrapper last-step step416">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-sun-15l" name="sm-driving-single-sun" value="driving-single-sun-15l" class="radio-hide"
                <?php
                if (get_field('step416_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15l">1,50 - Laboratory + Tinting
                <?php
                if (get_field('step416_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_1">
                    <?php echo get_field('step416_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-sun-16l" name="sm-driving-single-sun" value="driving-single-sun-16l" class="radio-hide"
                <?php
                if (get_field('step416_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16l">1,60 - Laboratory + Tinting
                <?php
                if (get_field('step416_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_2">
                    <?php echo get_field('step416_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-sun-167l" name="sm-driving-single-sun" value="driving-single-sun-167l" class="radio-hide"
                <?php
                if (get_field('step416_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-167l">1,67 - Laboratory + Tinting
                <?php
                if (get_field('step416_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_3">
                    <?php echo get_field('step416_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ln">
            <input type="radio" id="driving-single-sun-15ln" name="sm-driving-single-sun" value="driving-single-sun-15ln" class="radio-hide"
                <?php
                if (get_field('step416_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ln">1,50 - Laboratory + NuPolar
                <?php
                if (get_field('step416_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_4">
                    <?php echo get_field('step416_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16ln">
            <input type="radio" id="driving-single-sun-16ln" name="sm-driving-single-sun" value="driving-single-sun-16ln" class="radio-hide"
                <?php
                if (get_field('step416_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16ln">1,60 - Laboratory + NuPolar
                <?php
                if (get_field('step416_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_5">
                    <?php echo get_field('step416_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ld">
            <input type="radio" id="driving-single-sun-15ld" name="sm-driving-single-sun" value="driving-single-sun-15ld" class="radio-hide"
                <?php
                if (get_field('step416_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step416_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ld">1,50 - Laboratory + DriveWear
                <?php
                if (get_field('step416_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step416_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step416_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step416_6">
                    <?php echo get_field('step416_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-single-ajusting display-none step-wrapper last-step step417">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-ajusting-15l" name="sm-driving-single-ajusting" value="driving-single-ajusting-15l" class="radio-hide"
                <?php
                if (get_field('step417_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15l">1,50 - Laboratory XTRA
                <?php
                if (get_field('step417_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_1">
                    <?php echo get_field('step417_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-ajusting-16l" name="sm-driving-single-ajusting" value="driving-single-ajusting-16l" class="radio-hide"
                <?php
                if (get_field('step417_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l">1,60 - Laboratory XTRA
                <?php
                if (get_field('step417_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_2">
                    <?php echo get_field('step417_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-ajusting-167l" name="sm-driving-single-ajusting" value="driving-single-ajusting-167l" class="radio-hide"
                <?php
                if (get_field('step417_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167l">1,67 - Laboratory XTRA
                <?php
                if (get_field('step417_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_3">
                    <?php echo get_field('step417_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lxp">
            <input type="radio" id="driving-single-ajusting-15lxp" name="sm-driving-single-ajusting" value="driving-single-ajusting-15lxp" class="radio-hide"
                <?php
                if (get_field('step417_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15lxp">1,50 - Laboratory XTRA Polar
                <?php
                if (get_field('step417_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_4">
                    <?php echo get_field('step417_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lxp">
            <input type="radio" id="driving-single-ajusting-16lxp" name="sm-driving-single-ajusting" value="driving-single-ajusting-16lxp" class="radio-hide"
                <?php
                if (get_field('step417_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16lxp">1,60 - Laboratory XTRA Polar
                <?php
                if (get_field('step417_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_5">
                    <?php echo get_field('step417_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lxp">
            <input type="radio" id="driving-single-ajusting-167lxp" name="sm-driving-single-ajusting" value="driving-single-ajusting-167lxp" class="radio-hide"
                <?php
                if (get_field('step417_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step417_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167lxp">1,67 - Laboratory XTRA Polar
                <?php
                if (get_field('step417_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step417_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step417_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step417_6">
                    <?php echo get_field('step417_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-multifocal-clear display-none step-wrapper last-step step420">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-15l-2" name="sm-driving-single-clear" value="driving-single-15l" class="radio-hide"
                <?php
                if (get_field('step420_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step420_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-15l-2">1,50 - Laboratory
                <?php
                if (get_field('step420_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step420_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step420_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step420_1">
                    <?php echo get_field('step420_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-16l-2" name="sm-driving-single-clear" value="driving-single-16l" class="radio-hide"
                <?php
                if (get_field('step420_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step420_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-16l-2">1,60 - Laboratory
                <?php
                if (get_field('step420_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step420_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step420_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step420_2">
                    <?php echo get_field('step420_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-167l-2" name="sm-driving-single-clear" value="driving-single-167l" class="radio-hide"
                <?php
                if (get_field('step420_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step420_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-167l-2">1,67 - Laboratory
                <?php
                if (get_field('step420_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step420_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step420_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step420_3">
                    <?php echo get_field('step420_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="driving-single-174l" name="sm-driving-single-clear" value="driving-single-174l" class="radio-hide"
                <?php
                if (get_field('step420_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step420_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-174l">1,74 - Laboratory
                <?php
                if (get_field('step420_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step420_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step420_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step420_4">
                    <?php echo get_field('step420_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-multifocal-sun display-none step-wrapper last-step step421">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-sun-15l-3" name="sm-driving-single-sun" value="driving-single-sun-15l" class="radio-hide"
                <?php
                if (get_field('step421_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15l-3">1,50 - Laboratory + Tinting
                <?php
                if (get_field('step421_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_1">
                    <?php echo get_field('step421_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-sun-16l-3" name="sm-driving-single-sun" value="driving-single-sun-16l" class="radio-hide"
                <?php
                if (get_field('step421_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16l-3">1,60 - Laboratory + Tinting
                <?php
                if (get_field('step421_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_2">
                    <?php echo get_field('step421_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-sun-167l-3" name="sm-driving-single-sun" value="driving-single-sun-167l" class="radio-hide"
                <?php
                if (get_field('step421_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-167l-3">1,67 - Laboratory + Tinting
                <?php
                if (get_field('step421_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_3">
                    <?php echo get_field('step421_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ln">
            <input type="radio" id="driving-single-sun-15ln-3" name="sm-driving-single-sun" value="driving-single-sun-15ln" class="radio-hide"
                <?php
                if (get_field('step421_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ln-3">1,50 - Laboratory + NuPolar
                <?php
                if (get_field('step421_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_4">
                    <?php echo get_field('step421_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16ln">
            <input type="radio" id="driving-single-sun-16ln-3" name="sm-driving-single-sun" value="driving-single-sun-16ln" class="radio-hide"
                <?php
                if (get_field('step421_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16ln-3">1,60 - Laboratory + NuPolar
                <?php
                if (get_field('step421_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_5">
                    <?php echo get_field('step421_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ld">
            <input type="radio" id="driving-single-sun-15ld-3" name="sm-driving-single-sun" value="driving-single-sun-15ld" class="radio-hide"
                <?php
                if (get_field('step421_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step421_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ld-3">1,50 - Laboratory + DriveWear
                <?php
                if (get_field('step421_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step421_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step421_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step421_6">
                    <?php echo get_field('step421_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-driving-multifocal-ajusting display-none step-wrapper last-step step422">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness422</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-ajusting-15l-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-15l" class="radio-hide"
                <?php
                if (get_field('step422_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15l-4">1,50 - Laboratory XTRA
                <?php
                if (get_field('step422_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_1">
                    <?php echo get_field('step422_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-ajusting-16l-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-16l" class="radio-hide"
                <?php
                if (get_field('step422_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l-4">1,60 - Laboratory XTRA
                <?php
                if (get_field('step422_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_2">
                    <?php echo get_field('step422_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-ajusting-167l-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-167l" class="radio-hide"
                <?php
                if (get_field('step422_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167l-4">1,67 - Laboratory XTRA
                <?php
                if (get_field('step422_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_3">
                    <?php echo get_field('step422_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lxp">
            <input type="radio" id="driving-single-ajusting-15lxp-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-15lxp" class="radio-hide"
                <?php
                if (get_field('step422_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15lxp-4">1,50 - Laboratory XTRA Polar
                <?php
                if (get_field('step422_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_4">
                    <?php echo get_field('step422_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lxp">
            <input type="radio" id="driving-single-ajusting-16lxp-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-16lxp" class="radio-hide"
                <?php
                if (get_field('step422_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16lxp-4">1,60 - Laboratory XTRA Polar
                <?php
                if (get_field('step422_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_5">
                    <?php echo get_field('step422_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lxp">
            <input type="radio" id="driving-single-ajusting-16l7xp-4" name="sm-driving-single-ajusting" value="driving-single-ajusting-167lxp" class="radio-hide"
                <?php
                if (get_field('step422_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step422_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l7xp-4">1,67 - Laboratory XTRA Polar
                <?php
                if (get_field('step422_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step422_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step422_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step422_6">
                    <?php echo get_field('step422_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-distancereading-clear display-none step-wrapper last-step step423">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="15l-1" name="sm-distancereading-clear" value="15l" class="radio-hide"
                <?php
                if (get_field('step423_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step423_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="15l-1">1,50 - Laboratory
                <?php
                if (get_field('step423_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step423_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step423_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step423_1">
                    <?php echo get_field('step423_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="16l-1" name="sm-distancereading-clear" value="16l" class="radio-hide"
                <?php
                if (get_field('step423_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step423_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="16l-1">1,60 - Laboratory
                <?php
                if (get_field('step423_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step423_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step423_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step423_2">
                    <?php echo get_field('step423_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="167l-1" name="sm-distancereading-clear" value="167l" class="radio-hide"
                <?php
                if (get_field('step423_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step423_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="167l-1">1,67 - Laboratory
                <?php
                if (get_field('step423_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step423_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step423_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step423_3">
                    <?php echo get_field('step423_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="174l-1" name="sm-distancereading-clear" value="174l" class="radio-hide"
                <?php
                if (get_field('step423_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step423_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="174l-1">1,74 - Laboratory
                <?php
                if (get_field('step423_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step423_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step423_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step423_4">
                    <?php echo get_field('step423_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-distancereading-bluelight display-none step-wrapper last-step step424">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="15l-2" name="sm-distancereading-bluelight" value="15l" class="radio-hide"
                <?php
                if (get_field('step424_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step424_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="15l-2">1,50 - Laboratory
                <?php
                if (get_field('step424_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step424_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step424_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step424_1">
                    <?php echo get_field('step424_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="16l-2" name="sm-distancereading-bluelight" value="16l" class="radio-hide"
                <?php
                if (get_field('step424_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step424_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="16l-2">1,60 - Laboratory
                <?php
                if (get_field('step424_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step424_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step424_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step424_2">
                    <?php echo get_field('step424_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="167l-2" name="sm-distancereading-bluelight" value="167l" class="radio-hide"
                <?php
                if (get_field('step424_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step424_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="167l-2">1,67 - Laboratory
                <?php
                if (get_field('step424_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step424_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step424_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step424_3">
                    <?php echo get_field('step424_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-distancereading-sun-thickness display-none step-wrapper last-step step425">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="sun-150l-3" name="sm-distancereading-sun-thickness" value="sun-150l" class="radio-hide"
                <?php
                if (get_field('step425_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step425_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="sun-150l-3">1,50 - Laboratory + Tint
                <?php
                if (get_field('step425_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step425_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step425_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step425_1">
                    <?php echo get_field('step425_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="sun-160l-3" name="sm-distancereading-sun-thickness" value="sun-160l" class="radio-hide"
                <?php
                if (get_field('step425_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step425_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="sun-160l-3">1,60 - Laboratory + Tint
                <?php
                if (get_field('step425_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step425_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#step425_2_tooltip">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step425_2">
                    <?php echo get_field('step425_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="sun-167l-3" name="sm-distancereading-sun-thickness" value="sun-167l" class="radio-hide"
                <?php
                if (get_field('step425_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step425_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="sun-167l-3">1,67 - Laboratory + Tint
                <?php
                if (get_field('step425_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step425_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step425_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step425_3">
                    <?php echo get_field('step425_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lp">
            <input type="radio" id="sun-150l-polar-3" name="sm-distancereading-sun-thickness" value="sun-150l-polar" class="radio-hide"
                <?php
                if (get_field('step425_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step425_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="sun-150l-polar-3">1,50 - Laboratory - NuPolar
                <?php
                if (get_field('step425_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step425_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step425_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step425_4">
                    <?php echo get_field('step425_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lp">
            <input type="radio" id="sun-160l-polar-3" name="sm-distancereading-sun-thickness" value="sun-160l-polar" class="radio-hide"
                <?php
                if (get_field('step425_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step425_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="sun-160l-polar-3">1,60 - Laboratory - NuPolar
                <?php
                if (get_field('step425_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step425_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step425_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step425_5">
                    <?php echo get_field('step425_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-distancereading-ajusting-thickness display-none step-wrapper last-step step426">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="ajust-15l-4" name="sm-distancereading-ajusting-thickness" value="ajust-15l" class="radio-hide"
                <?php
                if (get_field('step426_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step426_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15l-4">1,50 - Laboratory - Gen8
                <?php
                if (get_field('step426_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step426_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step426_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step426_1">
                    <?php echo get_field('step426_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="ajust-16l-4" name="sm-distancereading-ajusting-thickness" value="ajust-16l" class="radio-hide"
                <?php
                if (get_field('step426_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step426_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16l-4">1,60 - Laboratory - Gen8
                <?php
                if (get_field('step426_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step426_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step426_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step426_2">
                    <?php echo get_field('step426_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="ajust-167l-4" name="sm-distancereading-ajusting-thickness" value="ajust-167l" class="radio-hide"
                <?php
                if (get_field('step426_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step426_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167l-4">1,67 - Laboratory - Gen8
                <?php
                if (get_field('step426_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step426_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step426_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step426_3">
                    <?php echo get_field('step426_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="ajust-174l-4" name="sm-distancereading-ajusting-thickness" value="ajust-174l" class="radio-hide"
                <?php
                if (get_field('step426_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step426_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-174l-4">1,74 - Laboratory - Gen8
                <?php
                if (get_field('step426_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step426_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step426_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step426_4">
                    <?php echo get_field('step426_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-distancereading-ajusting-thickness2 display-none step-wrapper last-step step427">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>

        <div class="wizard-item-noimage" data-label="15lx">
            <input type="radio" id="ajust-15lx-5" name="sm-distancereading-ajusting-thickness" value="ajust-15lx" class="radio-hide"
                <?php
                if (get_field('step427_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step427_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15lx-5">1,50 - Laboratory - XTRA
                <?php
                if (get_field('step427_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step427_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step427_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step427_1">
                    <?php echo get_field('step427_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lx">
            <input type="radio" id="ajust-16lx-5" name="sm-distancereading-ajusting-thickness" value="ajust-16lx" class="radio-hide"
                <?php
                if (get_field('step427_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step427_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16lx-5">1,60 - Laboratory - XTRA
                <?php
                if (get_field('step427_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step427_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step427_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step427_2">
                    <?php echo get_field('step427_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lx">
            <input type="radio" id="ajust-167lx-5" name="sm-distancereading-ajusting-thickness" value="ajust-167lx" class="radio-hide"
                <?php
                if (get_field('step427_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step427_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167lx-5">1,67 - Laboratory - XTRA
                <?php
                if (get_field('step427_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step427_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step427_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step427_3">
                    <?php echo get_field('step427_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-multifocal-clear display-none step-wrapper last-step step428">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="multifocal-15l-6" name="sm-distancereading-clear" value="15l" class="radio-hide"
                <?php
                if (get_field('step428_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step428_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-15l-6">1,50 - Laboratory
                <?php
                if (get_field('step428_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step428_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step428_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step428_1">
                    <?php echo get_field('step428_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="multifocal-16l-6" name="sm-distancereading-clear" value="16l" class="radio-hide"
                <?php
                if (get_field('step428_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step428_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-16l-6">1,60 - Laboratory
                <?php
                if (get_field('step428_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step428_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step428_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step428_2">
                    <?php echo get_field('step428_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="multifocal-167l-6" name="sm-distancereading-clear" value="167l" class="radio-hide"
                <?php
                if (get_field('step428_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step428_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-167l-6">1,67 - Laboratory
                <?php
                if (get_field('step428_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step428_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step428_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step428_3">
                    <?php echo get_field('step428_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="multifocal-174l-6" name="sm-distancereading-clear" value="174l" class="radio-hide"
                <?php
                if (get_field('step428_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step428_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-174l-6">1,74 - Laboratory
                <?php
                if (get_field('step428_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step428_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step428_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step428_4">
                    <?php echo get_field('step428_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-multifocal-bluelight display-none step-wrapper last-step step429">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="multifocal-15l-7" name="sm-multifocal-bluelight" value="15l" class="radio-hide"
                <?php
                if (get_field('step429_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step429_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-15l-7">1,50 - Laboratory
                <?php
                if (get_field('step429_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step429_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step429_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step429_1">
                    <?php echo get_field('step429_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="multifocal-16l-7" name="sm-multifocal-bluelight" value="16l" class="radio-hide"
                <?php
                if (get_field('step429_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step429_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-16l-7">1,60 - Laboratory
                <?php
                if (get_field('step429_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step429_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step429_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step429_2">
                    <?php echo get_field('step429_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="multifocal-167l-7" name="sm-multifocal-bluelight" value="167l" class="radio-hide"
                <?php
                if (get_field('step429_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step429_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-167l-7">1,67 - Laboratory
                <?php
                if (get_field('step429_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step429_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step429_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step429_3">
                    <?php echo get_field('step429_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-multifocal-sun-thickness display-none step-wrapper last-step step430">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="multifocal-sun-15l1-8" name="sm-multifocal-sun-thickness" value="sun-15l" class="radio-hide"
                <?php
                if (get_field('step430_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step430_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-15l1-8">1,50 - Laboratory + Tint
                <?php
                if (get_field('step430_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step430_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step430_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step430_1">
                    <?php echo get_field('step430_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="multifocal-sun-16l1-8" name="sm-multifocal-sun-thickness" value="sun-16l" class="radio-hide"
                <?php
                if (get_field('step430_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step430_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-16l1-8">1,60 - Laboratory + Tint
                <?php
                if (get_field('step430_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step430_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step430_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step430_2">
                    <?php echo get_field('step430_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="multifocal-sun-167l1-8" name="sm-multifocal-sun-thickness" value="sun-167l" class="radio-hide"
                <?php
                if (get_field('step430_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step430_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-167l1-8">1,67 - Laboratory + Tint
                <?php
                if (get_field('step430_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step430_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step430_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step430_3">
                    <?php echo get_field('step430_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lp">
            <input type="radio" id="multifocal-sun-15l-polar1-8" name="sm-multifocal-sun-thickness" value="sun-15l-polar" class="radio-hide"
                <?php
                if (get_field('step430_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step430_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-15l-polar1-8">1,50 - Laboratory - NuPolar
                <?php
                if (get_field('step430_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step430_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step430_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step430_4">
                    <?php echo get_field('step430_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lp">
            <input type="radio" id="multifocal-sun-16l-polar1-8" name="sm-multifocal-sun-thickness" value="sun-16l-polar" class="radio-hide"
                <?php
                if (get_field('step430_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step430_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="multifocal-sun-16l-polar1-8">1,60 - Laboratory - NuPolar
                <?php
                if (get_field('step430_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step430_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step430_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step430_5">
                    <?php echo get_field('step430_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-multifocal-ajusting-thickness display-none step-wrapper last-step step431">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="ajust-15l2431" name="sm-distancereading-ajusting-thickness" value="ajust-15l431" class="radio-hide"
                <?php
                if (get_field('step431_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step431_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15l2431">1,50 - Laboratory - Gen8
                <?php
                if (get_field('step431_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step431_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step431_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step431_1">
                    <?php echo get_field('step431_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="ajust-16l2431" name="sm-distancereading-ajusting-thickness" value="ajust-16l431" class="radio-hide"
                <?php
                if (get_field('step431_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step431_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16l2431">1,60 - Laboratory - Gen8
                <?php
                if (get_field('step431_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step431_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step431_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step431_2">
                    <?php echo get_field('step431_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="ajust-167l2431" name="sm-distancereading-ajusting-thickness" value="ajust-167l431" class="radio-hide"
                <?php
                if (get_field('step431_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step431_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167l2431">1,67 - Laboratory - Gen8
                <?php
                if (get_field('step431_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step431_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step431_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step431_3">
                    <?php echo get_field('step431_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="ajust-174l2431" name="sm-distancereading-ajusting-thickness" value="ajust-174l431" class="radio-hide"
                <?php
                if (get_field('step431_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step431_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-174l2431">1,74 - Laboratory - Gen8
                <?php
                if (get_field('step431_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step431_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step431_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step431_4">
                    <?php echo get_field('step431_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-multifocal-ajusting-thickness2 display-none step-wrapper last-step step432">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15lx">
            <input type="radio" id="ajust-15lx2432" name="sm-distancereading-ajusting-thickness" value="ajust-15lx432" class="radio-hide"
                <?php
                if (get_field('step432_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step432_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-15lx2432">1,50 - Laboratory - XTRA
                <?php
                if (get_field('step432_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step432_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step432_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step432_1">
                    <?php echo get_field('step432_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lx">
            <input type="radio" id="ajust-16lx2432" name="sm-distancereading-ajusting-thickness" value="ajust-16lx432" class="radio-hide"
                <?php
                if (get_field('step432_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step432_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-16lx2432">1,60 - Laboratory - XTRA
                <?php
                if (get_field('step432_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step432_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step432_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step432_2">
                    <?php echo get_field('step432_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lx">
            <input type="radio" id="ajust-167lx2432" name="sm-distancereading-ajusting-thickness" value="ajust-167lx432" class="radio-hide"
                <?php
                if (get_field('step432_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step432_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="ajust-167lx2432">1,67 - Laboratory - XTRA
                <?php
                if (get_field('step432_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step432_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step432_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step432_3">
                    <?php echo get_field('step432_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-single-clear display-none step-wrapper last-step step433">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-15l-9" name="sm-driving-single-clear" value="driving-single-15l" class="radio-hide"
                <?php
                if (get_field('step433_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step433_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-15l-9">1,50 - Laboratory
                <?php
                if (get_field('step433_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step433_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step433_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step433_1">
                    <?php echo get_field('step433_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-16l-9" name="sm-driving-single-clear" value="driving-single-16l" class="radio-hide"
                <?php
                if (get_field('step433_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step433_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-16l-9">1,60 - Laboratory
                <?php
                if (get_field('step433_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step433_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step433_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step433_2">
                    <?php echo get_field('step433_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-167l-9" name="sm-driving-single-clear" value="driving-single-167l" class="radio-hide"
                <?php
                if (get_field('step433_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step433_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-167l-9">1,67 - Laboratory
                <?php
                if (get_field('step433_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step433_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step433_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step433_3">
                    <?php echo get_field('step433_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="driving-single-174l-9" name="sm-driving-single-clear" value="driving-single-174l" class="radio-hide"
                <?php
                if (get_field('step433_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step433_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-174l-9">1,74 - Laboratory
                <?php
                if (get_field('step433_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step433_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step433_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step433_4">
                    <?php echo get_field('step433_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-single-sun display-none step-wrapper last-step step434">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-sun-15l-10" name="sm-driving-single-sun" value="driving-single-sun-15l" class="radio-hide"
                <?php
                if (get_field('step434_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15l-10">1,50 - Laboratory + Tinting
                <?php
                if (get_field('step434_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_1">
                    <?php echo get_field('step434_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-sun-16l-10" name="sm-driving-single-sun" value="driving-single-sun-16l" class="radio-hide"
                <?php
                if (get_field('step434_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16l-10">1,60 - Laboratory + Tinting
                <?php
                if (get_field('step434_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_2">
                    <?php echo get_field('step434_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-sun-167l-10" name="sm-driving-single-sun" value="driving-single-sun-167l" class="radio-hide"
                <?php
                if (get_field('step434_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-167l-10">1,67 - Laboratory + Tinting
                <?php
                if (get_field('step434_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_3">
                    <?php echo get_field('step434_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ln">
            <input type="radio" id="driving-single-sun-15ln-10" name="sm-driving-single-sun" value="driving-single-sun-15ln" class="radio-hide"
                <?php
                if (get_field('step434_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ln-10">1,50 - Laboratory + NuPolar
                <?php
                if (get_field('step434_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_4">
                    <?php echo get_field('step434_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16ln">
            <input type="radio" id="driving-single-sun-16ln-10" name="sm-driving-single-sun" value="driving-single-sun-16ln" class="radio-hide"
                <?php
                if (get_field('step434_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16ln-10">1,60 - Laboratory + NuPolar
                <?php
                if (get_field('step434_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_5">
                    <?php echo get_field('step434_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ld">
            <input type="radio" id="driving-single-sun-15ld-10" name="sm-driving-single-sun" value="driving-single-sun-15ld" class="radio-hide"
                <?php
                if (get_field('step434_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step434_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ld-10">1,50 - Laboratory + DriveWear
                <?php
                if (get_field('step434_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step434_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step434_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step434_6">
                    <?php echo get_field('step434_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-single-ajusting display-none step-wrapper last-step step435">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-ajusting-15l-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-15l" class="radio-hide"
                <?php
                if (get_field('step435_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15l-11">1,50 - Laboratory XTRA
                <?php
                if (get_field('step435_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_1">
                    <?php echo get_field('step435_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-ajusting-16l-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-16l" class="radio-hide"
                <?php
                if (get_field('step435_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l-11">1,60 - Laboratory XTRA
                <?php
                if (get_field('step435_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_2">
                    <?php echo get_field('step435_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-ajusting-167l-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-167l" class="radio-hide"
                <?php
                if (get_field('step435_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167l-11">1,67 - Laboratory XTRA
                <?php
                if (get_field('step435_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_3">
                    <?php echo get_field('step435_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lxp">
            <input type="radio" id="driving-single-ajusting-15lxp-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-15lxp" class="radio-hide"
                <?php
                if (get_field('step435_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15lxp-11">1,50 - Laboratory XTRA Polar
                <?php
                if (get_field('step435_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_4">
                    <?php echo get_field('step435_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lxp">
            <input type="radio" id="driving-single-ajusting-16lxp-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-16lxp" class="radio-hide"
                <?php
                if (get_field('step435_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16lxp-11">1,60 - Laboratory XTRA Polar
                <?php
                if (get_field('step435_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_5">
                    <?php echo get_field('step435_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lxp">
            <input type="radio" id="driving-single-ajusting-167lxp-11" name="sm-driving-single-ajusting" value="driving-single-ajusting-167lxp" class="radio-hide"
                <?php
                if (get_field('step435_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step435_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167lxp-11">1,67 - Laboratory XTRA Polar
                <?php
                if (get_field('step435_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step435_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step435_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step435_6">
                    <?php echo get_field('step435_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-multifocal-clear display-none step-wrapper last-step step436">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-15l-12" name="sm-driving-single-clear" value="driving-single-15l" class="radio-hide"
                <?php
                if (get_field('step436_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step436_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-15l-12">1,50 - Laboratory
                <?php
                if (get_field('step436_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step436_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step436_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step436_1">
                    <?php echo get_field('step436_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-16l-12" name="sm-driving-single-clear" value="driving-single-16l" class="radio-hide"
                <?php
                if (get_field('step436_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step436_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-16l-12">1,60 - Laboratory
                <?php
                if (get_field('step436_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step436_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step436_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step436_2">
                    <?php echo get_field('step436_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-167l-12" name="sm-driving-single-clear" value="driving-single-167l" class="radio-hide"
                <?php
                if (get_field('step436_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step436_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-167l-12">1,67 - Laboratory
                <?php
                if (get_field('step436_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step436_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step436_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step436_3">
                    <?php echo get_field('step436_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="driving-single-174l-12" name="sm-driving-single-clear" value="driving-single-174l" class="radio-hide"
                <?php
                if (get_field('step436_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step436_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-174l-12">1,74 - Laboratory
                <?php
                if (get_field('step436_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step436_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step436_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step436_4">
                    <?php echo get_field('step436_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-multifocal-sun display-none step-wrapper last-step step437">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-sun-15l-13" name="sm-driving-single-sun" value="driving-single-sun-15l" class="radio-hide"
                <?php
                if (get_field('step437_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15l-3">1,50 - Laboratory + Tinting
                <?php
                if (get_field('step437_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_1">
                    <?php echo get_field('step437_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-sun-16l-13" name="sm-driving-single-sun" value="driving-single-sun-16l" class="radio-hide"
                <?php
                if (get_field('step437_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16l-3">1,60 - Laboratory + Tinting
                <?php
                if (get_field('step437_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_2">
                    <?php echo get_field('step437_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-sun-167l-13" name="sm-driving-single-sun" value="driving-single-sun-167l" class="radio-hide"
                <?php
                if (get_field('step437_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-167l-13">1,67 - Laboratory + Tinting
                <?php
                if (get_field('step437_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_3">
                    <?php echo get_field('step437_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ln">
            <input type="radio" id="driving-single-sun-15ln-13" name="sm-driving-single-sun" value="driving-single-sun-15ln" class="radio-hide"
                <?php
                if (get_field('step437_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ln-13">1,50 - Laboratory + NuPolar
                <?php
                if (get_field('step437_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_4">
                    <?php echo get_field('step437_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16ln">
            <input type="radio" id="driving-single-sun-16ln-13" name="sm-driving-single-sun" value="driving-single-sun-16ln" class="radio-hide"
                <?php
                if (get_field('step437_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-16ln-13">1,60 - Laboratory + NuPolar
                <?php
                if (get_field('step437_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_5">
                    <?php echo get_field('step437_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15ld">
            <input type="radio" id="driving-single-sun-15ld-13" name="sm-driving-single-sun" value="driving-single-sun-15ld" class="radio-hide"
                <?php
                if (get_field('step437_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step437_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-sun-15ld-13">1,50 - Laboratory + DriveWear
                <?php
                if (get_field('step437_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step437_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step437_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step437_6">
                    <?php echo get_field('step437_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="l-driving-multifocal-ajusting display-none step-wrapper last-step step438">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness422</p>
        </div>
        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="driving-single-ajusting-15l-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-15l" class="radio-hide"
                <?php
                if (get_field('step438_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15l-14">1,50 - Laboratory XTRA
                <?php
                if (get_field('step438_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_1">
                    <?php echo get_field('step438_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="driving-single-ajusting-16l-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-16l" class="radio-hide"
                <?php
                if (get_field('step438_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l-14">1,60 - Laboratory XTRA
                <?php
                if (get_field('step438_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_2">
                    <?php echo get_field('step438_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="driving-single-ajusting-167l-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-167l" class="radio-hide"
                <?php
                if (get_field('step438_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-167l-14">1,67 - Laboratory XTRA
                <?php
                if (get_field('step438_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_3">
                    <?php echo get_field('step438_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15lxp">
            <input type="radio" id="driving-single-ajusting-15lxp-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-15lxp" class="radio-hide"
                <?php
                if (get_field('step438_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-15lxp-14">1,50 - Laboratory XTRA Polar
                <?php
                if (get_field('step438_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_4">
                    <?php echo get_field('step438_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16lxp">
            <input type="radio" id="driving-single-ajusting-16lxp-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-16lxp" class="radio-hide"
                <?php
                if (get_field('step438_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16lxp-14">1,60 - Laboratory XTRA Polar
                <?php
                if (get_field('step438_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_5">
                    <?php echo get_field('step438_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167lxp">
            <input type="radio" id="driving-single-ajusting-16l7xp-14" name="sm-driving-single-ajusting" value="driving-single-ajusting-167lxp" class="radio-hide"
                <?php
                if (get_field('step438_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step438_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="driving-single-ajusting-16l7xp-14">1,67 - Laboratory XTRA Polar
                <?php
                if (get_field('step438_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step438_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step438_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step438_6">
                    <?php echo get_field('step438_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-non-prescriptions-clear display-none step-wrapper last-step step418">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15s">
            <input type="radio" id="15s3" name="sm-distancereading-clear" value="15s" class="radio-hide"
                <?php
                if (get_field('step418_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="15s3">1,50 - Stock
                <?php
                if (get_field('step418_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_1">
                    <?php echo get_field('step418_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16s">
            <input type="radio" id="16s3" name="sm-distancereading-clear" value="16s" class="radio-hide"
                <?php
                if (get_field('step418_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="16s3">1,60 - Stock
                <?php
                if (get_field('step418_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip toolip-hover" data-tooltip-content="#tooltip_step418_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_2">
                    <?php echo get_field('step418_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167s">
            <input type="radio" id="167s3" name="sm-distancereading-clear" value="167s" class="radio-hide"
                <?php
                if (get_field('step418_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="167s3">1,67 - Stock
                <?php
                if (get_field('step418_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_3">
                    <?php echo get_field('step418_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="15l3" name="sm-distancereading-clear" value="15l" class="radio-hide"
                <?php
                if (get_field('step418_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="15l3">1,50 - Laboratory
                <?php
                if (get_field('step418_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_4">
                    <?php echo get_field('step418_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="16l3" name="sm-distancereading-clear" value="16l" class="radio-hide"
                <?php
                if (get_field('step418_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="16l3">1,60 - Laboratory
                <?php
                if (get_field('step418_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_5">
                    <?php echo get_field('step418_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="167l3" name="sm-distancereading-clear" value="167l" class="radio-hide"
                <?php
                if (get_field('step418_6_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_6_price', 'options') . '"';
                }
                ?>
            >
            <label for="167l3">1,67 - Laboratory
                <?php
                if (get_field('step418_6_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_6_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_6">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_6">
                    <?php echo get_field('step418_6_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="174l">
            <input type="radio" id="174l3" name="sm-distancereading-clear" value="174l" class="radio-hide"
                <?php
                if (get_field('step418_7_price', 'options')) {
                    echo 'data-ad="' . get_field('step418_7_price', 'options') . '"';
                }
                ?>
            >
            <label for="174l3">1,74 - Laboratory
                <?php
                if (get_field('step418_7_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step418_7_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step418_7">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step418_7">
                    <?php echo get_field('step418_7_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="sm-non-prescriptions-bluelight display-none step-wrapper last-step step419">
        <div class="step-heading-wrapper">
            <p class="step-heading-text">Select lenses thickness</p>
        </div>
        <div class="wizard-item-noimage" data-label="15s">
            <input type="radio" id="15s4" name="sm-distancereading-bluelight" value="15s" class="radio-hide"
                <?php
                if (get_field('step419_1_price', 'options')) {
                    echo 'data-ad="' . get_field('step419_1_price', 'options') . '"';
                }
                ?>
            >
            <label for="15s4">1,50 - Stock
                <?php
                if (get_field('step419_1_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step419_1_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step419_1">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step419_1">
                    <?php echo get_field('step419_1_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16s">
            <input type="radio" id="16s4" name="sm-distancereading-bluelight" value="16s" class="radio-hide"
                <?php
                if (get_field('step419_2_price', 'options')) {
                    echo 'data-ad="' . get_field('step419_2_price', 'options') . '"';
                }
                ?>
            >
            <label for="16s4">1,60 - Stock
                <?php
                if (get_field('step419_2_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step419_2_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step419_2">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step419_2">
                    <?php echo get_field('step419_2_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="15l">
            <input type="radio" id="15l4" name="sm-distancereading-bluelight" value="15l" class="radio-hide"
                <?php
                if (get_field('step419_3_price', 'options')) {
                    echo 'data-ad="' . get_field('step419_3_price', 'options') . '"';
                }
                ?>
            >
            <label for="15l4">1,50 - Laboratory
                <?php
                if (get_field('step419_3_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step419_3_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step419_3">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step419_3">
                    <?php echo get_field('step419_3_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="16l">
            <input type="radio" id="16l4" name="sm-distancereading-bluelight" value="16l" class="radio-hide"
                <?php
                if (get_field('step419_4_price', 'options')) {
                    echo 'data-ad="' . get_field('step419_4_price', 'options') . '"';
                }
                ?>
            >
            <label for="16l4">1,60 - Laboratory
                <?php
                if (get_field('step419_4_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step419_4_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step419_4">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step419_4">
                    <?php echo get_field('step419_4_tooltip', 'options'); ?>
                </span>
            </div>
        </div>

        <div class="wizard-item-noimage" data-label="167l">
            <input type="radio" id="167l4" name="sm-distancereading-bluelight" value="167l" class="radio-hide"
                <?php
                if (get_field('step419_5_price', 'options')) {
                    echo 'data-ad="' . get_field('step419_5_price', 'options') . '"';
                }
                ?>
            >
            <label for="167l4">1,67 - Laboratory
                <?php
                if (get_field('step419_5_price', 'options')) {
                    ?>
                    <span> + <?php echo get_field('step419_5_price', 'options'); ?>€</span>
                    <?php
                }
                ?>
            </label>
            <button class="label-tooltip tooltip-hover" data-tooltip-content="#tooltip_step419_5">
                <svg viewBox="0 0 17 17" width="17" height="17" class="">
                    <use xlink:href="#modal-i"></use>
                </svg>
            </button>
            <div class="tooltip_templates">
                <span id="tooltip_step419_5">
                    <?php echo get_field('step419_5_tooltip', 'options'); ?>
                </span>
            </div>
        </div>
    </div>

    <div class="wizard-subtotal-mobile wizard4-subtotal display-none">
        <p>Subtotal:</p>
        <p class="current-price"></p>
    </div>
    <div class="wizard2-bottom-buttons display-none">
        <button type="button" class="wizard2-back-button-desktop">Back</button>
    </div>
</div>
