<?php
/*
 * Template Name: Home Template
 */
?>
<?php get_header(); ?>

<main id="main-<?php the_ID(); ?>" class="page-home" role="main">

    <section class="intro-section intro-slider">

        <?php while (have_rows('home_page_banner')): the_row(); ?>

            <div class="intro-banner">

                <div class="page-container">
                    <div class="intro-banner-text">
                        <p class="intro-banner-description">New collection 2021</p>
                        <h1 class="intro-banner-header">Time to find those glasses</h1>
                        <div class="intro-banner-buttons">
                            <?php while (have_rows('banner_buttons')): the_row(); ?>

                                <a class="btn" href="<?php echo get_sub_field('link') ?>">
                                    <?php echo get_sub_field('button') ?>
                                </a>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
                <img class="intro-banner__image" src="<?php echo get_sub_field('banner_image')['url'] ?>"
                     alt="<?php echo get_sub_field('banner_image')['alt'] ?>">

                <img class="intro-banner__image--mobile" src="<?php echo get_sub_field('banner_image_mobile')['url'] ?>"
                     alt="<?php echo get_sub_field('banner_image_mobile')['alt'] ?>">

            </div>

        <?php endwhile; ?>

    </section>

    <section class="separate-section">
        <div class="page-container">
            <div class="separate-item">
                <?php while (have_rows('separate_section')): the_row(); ?>
                    <svg width="135" height="30" viewBox="0 0 135 30" class="separate-item__icon">
                        <use xlink:href="<?php echo get_sub_field('separate_image') ?>"></use>
                    </svg>
                    <h3 class="separate-item__head">
                        <?php echo get_sub_field('separate_head') ?>
                    </h3>
                    <p class="separate-item__description">
                        <?php echo get_sub_field('separate_description') ?>
                    </p>
                <?php endwhile; ?>
            </div>
        </div>
    </section>

    <section class="categories-section">
        <div class="page-container">
            <h2 class="headline">Top Categories</h2>
            <div class="row multiple-items">
                <?php while (have_rows('product_categories')): the_row(); ?>
                        <a  href="<?php echo get_sub_field('category_link')['url'] ?>" class="<?php echo get_sub_field('category_item_class') ?> category-item"
                                style="background-image: url('<?php echo get_sub_field('category_image')['url'] ?>')">
                                <img class="desktop-cat-image" src="<?php echo get_sub_field('category_image')['url'] ?>"
                                     alt="<?php echo get_sub_field('category_image')['title'] ?>">

                                <span class="categories_link">
                                        <?php echo get_sub_field('category_link')['title'] ?>
                                    <span>
                                    <svg class="categories_link--arrow">
                                            <use xlink:href="#arrow"></use>
                                    </svg>
                                    </span>
                                </span>
                         </a>
                <?php endwhile; ?>
            </div>

        </div>
    </section>

    <section class="quiz-section">
        <div class="page-container">
            <?php while (have_rows('quiz_section')): the_row(); ?>

                <div class="quiz-section__item"
                     style="background-image: url('<?php echo get_sub_field('quiz_banner')['url'] ?>')">
                    <h3 class="quiz-header"><?php echo get_sub_field('quiz_header') ?></h3>
                    <a class="btn btn--quiz" href="<?php echo get_sub_field('quiz_button')['url'] ?>">
                        <?php echo get_sub_field('quiz_button')['title'] ?>
                    </a>
                </div>

                <div class="quiz-section__item quiz-section__item--mobile"
                     style="background-image: url('<?php echo get_sub_field('quiz_banner_mobile')['url'] ?>')">
                    <h3 class="quiz-title"><?php echo get_sub_field('quiz_header') ?></h3>
                    <a class="btn btn--quiz" href="<?php echo get_sub_field('quiz_button')['url'] ?>">
                        <?php echo get_sub_field('quiz_button')['title'] ?>
                    </a>
                </div>

            <?php endwhile; ?>
        </div>
    </section>

    <section class="best-selling-product">
        <div class="page-container home-slider-container">
            <div class="best-selling-headline">
                <h2 class="best-selling-header"><?php echo get_field('title')?></h2>
                <a class="best-selling-link" href="<?php echo get_field('link_1')['url']?>"><?php echo get_field('link_1')['title']?></a>

                <a class="best-selling-mobile-link" href="<?php echo get_field('link_1')['url']?>"><?php echo get_field('title')?>
                    <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
                </a>

            </div>
            <div class="nav-container"></div>
            <div class="best-selling eye-slider">
                <?php while( have_rows('best_selling') ): the_row(); ?>

                    <?php $post_object = get_sub_field('best_selling_product'); ?>

                    <?php if( $post_object ): ?>

                        <?php $post = $post_object; setup_postdata( $post ); ?>

                        <?php get_template_part('template-parts/content', 'category') ?>

                        <?php wp_reset_postdata(); ?>

                    <?php endif; ?>

                <?php endwhile; ?>
            </div>

        </div>
        <div class="page-container home-slider-container">

            <div class="best-selling-headline">
                <h2 class="best-selling-header"><?php echo get_field('title_2')?></h2>
                <a class="best-selling-link" href="<?php echo get_field('link_2')['url']?>"><?php echo get_field('link_2')['title']?></a>
                <a class="best-selling-mobile-link" href="<?php echo get_field('link_2')['url']?>"><?php echo get_field('title')?>
                    <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
                </a>
            </div>

            <div class="sun-nav-container"></div>
            <div class="best-selling sun-slider">
                <?php while( have_rows('best_selling_2') ): the_row(); ?>

                    <?php $post_object = get_sub_field('best_selling_product'); ?>

                    <?php if( $post_object ): ?>

                        <?php $post = $post_object; setup_postdata( $post ); ?>

                        <?php get_template_part('template-parts/content', 'category') ?>

                        <?php wp_reset_postdata(); ?>

                    <?php endif; ?>

                <?php endwhile; ?>
            </div>

        </div>


        <div class="page-container home-slider-container">

            <div class="best-selling-headline">
                <h2 class="best-selling-header"><?php echo get_field('title_3')?></h2>
                <a class="best-selling-link" href="<?php echo get_field('link_3')['url']?>"><?php echo get_field('link_3')['title']?></a>
                <a class="best-selling-mobile-link" href="<?php echo get_field('link_3')['url']?>"><?php echo get_field('title_3')?>
                    <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
                </a>
            </div>

            <div class="special-sun-nav-container"></div>
            <div class="best-selling special-sun-slider">
                <?php while( have_rows('best_selling_3') ): the_row(); ?>

                    <?php $post_object = get_sub_field('best_selling_product'); ?>

                    <?php if( $post_object ): ?>

                        <?php $post = $post_object; setup_postdata( $post ); ?>

                        <?php get_template_part('template-parts/content', 'category') ?>

                        <?php wp_reset_postdata(); ?>

                    <?php endif; ?>

                <?php endwhile; ?>
            </div>

        </div>



        <div class="page-container home-slider-container">
            <div class="lenses-nav-container"></div>

            <div class="best-selling-headline">
                <h2 class="best-selling-header"><?php echo get_field('title_4')?></h2>
                <a class="best-selling-link" href="<?php echo get_field('link_4')['url']?>"><?php echo get_field('link_4')['title']?></a>
                <a class="best-selling-mobile-link" href="<?php echo get_field('link_4')['url']?>"><?php echo get_field('title_4')?>
                    <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
                </a>

            </div>

            <div class="best-selling lenses-slider">
                <?php while( have_rows('best_selling_4') ): the_row(); ?>

                    <?php $post_object = get_sub_field('best_selling_product'); ?>

                    <?php if( $post_object ): ?>

                        <?php $post = $post_object; setup_postdata( $post ); ?>

                        <?php get_template_part('template-parts/content', 'category') ?>

                        <?php wp_reset_postdata(); ?>

                    <?php endif; ?>

                <?php endwhile; ?>
            </div>
        </div>


    </section>

    <section class="testimonials">

        <div class="page-container slider-container">
            <div class="testimonial-nav-container"></div>
            <div class="testimonial-slider">
                <?php
                $testimonial_query = new WP_Query(array(
                    'post_type' => 'testimonial',
                    'posts_per_page' => -1,
                ));
                ?>
                <?php if ($testimonial_query->have_posts()) : ?>

                    <?php while ($testimonial_query->have_posts()) : $testimonial_query->the_post(); ?>

                        <?php get_template_part('template-parts/content-testimonial') ?>

                    <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php endif; ?>
            </div>
        </div>
    </section>
    <div class="page-container">
        <?php get_template_part('template-parts/try-on') ?>
    </div>
    <section class="featured-article">
        <div class="page-container">
            <h2 class="featured-article-header">Featured articles</h2>
            <a class="featured-article-link" href="<?php echo get_site_url() ?>/blog/">Featured articles
                <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
            </a>
            <?php
            $post_query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'taxonomy' => 'featured',
                'ignore_sticky_posts' => true,
            ));
            ?>

            <div class="featured-articles row">
                <?php if ($post_query->have_posts()) : ?>

                    <?php while ($post_query->have_posts()) : $post_query->the_post(); ?>

                        <div class="article col-md-4">
                            <a href="<?php echo get_the_permalink(); ?>" class="article-img">
                                <?php the_post_thumbnail(); ?>
                            </a>
                            <div>
                                <p class="name">
                                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                </p>
                                <p class="description">
                                    <?php echo mb_strimwidth(get_the_excerpt(), 0, 145, '...') ?>
                                </p>
                            </div>
                        </div>

                    <?php endwhile;
                    wp_reset_postdata(); ?>

                <?php endif; ?>
                <a class="blog-link" href="/blog">View more articles</a>
            </div>
        </div>
    </section>

    <section class="stories">
        <div class="page-container">
            <?php while (have_rows('stories_section')): the_row(); ?>
                <div class="stories-info">
                    <div class="stories-image">
                        <img src="<?php echo get_sub_field('stories_image')['url'] ?>"
                             alt="<?php echo get_sub_field('stories_image')['alt'] ?>">
                    </div>

                    <div class="stories-text-content">

                        <p class="stories-headline"><?php echo get_sub_field('stories_headline')?></p>
                        <h2 class="stories-header"><?php echo get_sub_field('stories_header') ?></h2>
                        <div class="stories-image-mobile">
                            <img src="<?php echo get_sub_field('stories_image')['url'] ?>"
                                 alt="<?php echo get_sub_field('stories_image')['alt'] ?>">
                        </div>
                        <p class="stories-description">
                            <?php echo get_sub_field('stories_description') ?>
                        </p>
                        <a class="stories-link" href="<?php echo get_sub_field('stories_link')['url'] ?>">
                            <?php echo get_sub_field('stories_link')['title'] ?>
                            <span class="arrow-right">
                                <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                                    <use xlink:href="#arrow_right"></use>
                                </svg>
                            </span>
                        </a>
                    </div>
                </div>
            <?php endwhile; ?>

        </div>

    </section>

    <section class="seo">
        <div class="page-container">
            <div class="seo-content">
                <?php while (have_rows('seo_section')): the_row(); ?>
                <h2 class="seo-title"><?php echo get_sub_field('seo_title') ?></h2>
                <p class="seo-text">
                    <?php echo get_sub_field('seo_description') ?>
                </p>
                <a class="seo-link" href="<?php echo get_sub_field('seo_link')['url'] ?>">
                    <?php echo get_sub_field('seo_link')['title'] ?>
                    <span class="arrow-down">
                        <svg width="18" height="10" viewBox="0 0 18 10" class="categories_link--arrow">
                            <use xlink:href="#arrow-down"></use>
                        </svg>
                    </span>
                </a>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <?php get_template_part('template-parts/subscribe') ?>
</main>

<?php get_footer(); ?>
