<?php
/**
 * The template for displaying all single posts
 */

$related = new WP_Query(array('category__in'   => wp_get_post_categories( $post->ID ),'posts_per_page' => 3,'post__not_in'   => array( $post->ID )));
global $post;
?>

<?php get_header(); ?>

<main class="single-main">
    <div class="wrap">
        <div class="main_text">
            <div class="category_name"><?php the_category(' '); ?></div>
            <p class="post_name"><?php echo get_the_title(); ?></p>
        </div>
    </div>
    <div class="menu-blog-category-container">
        <?php wp_nav_menu(array(
            'theme_location' => 'Categories-menu',
            'menu_id' => 'menu-blog-category',
            'container' => 'ul'
        )); ?>
    </div>
    <div class="page-container">
        <div class="single-container">
            <?php the_content(); ?>
            <div class="author">
                <img class="author_img"
                     src="<?php echo get_avatar_url(get_the_author()); ?>"
                     alt="">
                <div class="author_text">
                    <div class="written">Written by</div>
                    <p class="author_name">
                        <?php echo get_the_author_meta('display_name', $post->post_author) ?>
                    </p>
                    <p class="author_description">
                        <?php echo get_the_author_meta('user_description', $post->post_author) ?>
                    </p>
                </div>
            </div>

            <div class="share">
                <div class="share_text">
                    <p>Did you like the article?</p>
                    <p>Here you can like or share.</p>
                </div>
                <div class="share_buttons">
                    <div class="like">
                        <?php echo getPostLikeLink(get_the_ID()); ?>
                    </div>

                    <?php echo do_shortcode('[TheChamp-Sharing]')?>
                </div>
            </div>

            <div class="links">
                <a class="back_link" href="<?php echo get_site_url() ?>/blog">
                    <svg width="21" height="17" class="button-arrow-left">
                        <use xlink:href="#arrow">

                        </use>
                    </svg>
                    Back to all articles
                </a>
                <?php
                next_post_link('%link',
                    'Next article
                    <svg width="21" height="20">
                    <use xlink:href="#arrow"></use>
                   </svg>', true);
                ?>
            </div>
        </div>
        <div class="related">
            <h2>You may also be interested in</h2>
            <div class="related_grid">
                <?php if( $related->have_posts() ) { while( $related->have_posts() ) { $related->the_post(); ?>
                    <div class="articles_item" >
                        <div class="articles_img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div>
                            <div class="category_name category_name-mob"><?php the_category(' '); ?></div>
                            <p class="post_name">
                                <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                            </p>
                        </div>
                    </div>
                <?php } wp_reset_postdata(); } ?>
            </div>
        </div>
    </div>
</main>
<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>
<?php get_template_part('template-parts/subscribe') ?>

<?php get_footer(); ?>
