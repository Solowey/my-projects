<?php
/*
 * Template Name: Blog Template
 */

$query = new WP_Query(['posts_per_page' => 3, 'ignore_sticky_posts' => true]);
$popularPost = new WP_Query(array(
    'posts_per_page' => 3,
    'meta_key' => 'wpb_post_views_count',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'ignore_sticky_posts' => true
));
$sticky = get_option('sticky_posts');
$stickyPost = new WP_Query('p=' . $sticky[0]);
$allPosts = $query->found_posts;
?>

<?php get_header(); ?>
<main class="blog-main">

    <div class="menu-blog-category-container">
        <?php wp_nav_menu(array(
            'theme_location' => 'Categories-menu',
            'menu_id' => 'menu-blog-category',
            'container' => 'ul'
        )); ?>
    </div>
    <div class="page-container">
        <div class="blog_top">
            <?php while ($stickyPost->have_posts()) : $stickyPost->the_post(); ?>
                <div class="blog_main"
                     style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
                    <div class="category_name"><?php the_category(' '); ?></div>
                    <p class="category_title"><a
                            href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></p>
                    <p class="category_subtitle"><?php echo mb_strimwidth(get_the_excerpt(), 0, 75, '...') ?> </p>
                </div>
            <?php endwhile; ?>
            <div class="blog_popular">
                <h3 class="popular_title">Most popular</h3>
                <div class="popular_grid">
                    <?php while ($popularPost->have_posts()) : $popularPost->the_post(); ?>
                        <div class="grid_item">
                            <div class="popular_img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="popular_text">
                                <div class="category_name category_name-popular"><?php the_category(' '); ?></div>
                                <p class="category_title"><a
                                        href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></p>
                                <p class="category_subtitle"><?php echo mb_strimwidth(get_the_excerpt(), 0, 40, '...') ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>

        <div class="blog_articles">
            <p class="articles_title">All articles</p>
            <div class="articles_grid" data-all-posts="<?php echo $allPosts ?>"
                 data-page="<?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>"
                 data-max="<?php echo $query->max_num_pages; ?>">

                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <?php get_template_part('template-parts/blog', 'item') ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>


        <div class="load-more">
            <div class="load-more_text">You have viewed <span id="viewedPosts"></span> of
                <span><?php echo $allPosts ?></span> posts
            </div>
            <div class="load-more_line">
                <div class="load-more_progress"></div>
            </div>
            <button id="load_more" class="load-more_btn">Load 3 more</button>
        </div>


    </div>
</main>
<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>
<?php get_template_part('template-parts/subscribe') ?>

<?php get_footer(); ?>
