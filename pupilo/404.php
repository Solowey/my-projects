<?php get_header(); ?>
<div class="page-container">
    <div class="page-404">

        <a href="<?php echo get_home_url(null, '/', 'http'); ?>">
            <img src="<?php echo get_field('logo', 'option')['url'] ?>"
                 alt="<?php echo get_field('logo', 'option')['alt'] ?>" class="logo-404">
        </a>
        <h1 class="main-page-title">ERROR</h1>
        <img src="<?php echo wp_get_attachment_image_url(332, 'full'); ?>" alt="404">
        <h3 class="third-head lite">This page not found </h3>
        <a href="<?php echo get_home_url(null, '/', 'http'); ?>" class="btn">Back to home page</a>

    </div>
</div>
<?php get_footer(); ?>
