<?php
/*
 * Template Name: Cart Template
 */
?>
<?php get_header('cart'); ?>

<main id="primary" class="site-main">
    <div class="page-container">
        <?php echo do_shortcode('[woocommerce_cart]')?>
    </div>
</main>

<?php
    if (is_page('cart')) : ?>
        <?php get_footer(); ?>

<?php endif; ?>
