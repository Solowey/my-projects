<head>
    <?php get_template_part('template-parts/head'); ?>
</head>

<body <?php body_class("page-body"); ?>>
<div class="wrapper" id="app">
    <?php get_template_part('template-parts/sprite') ?>
    <div class="content">
        <header class="page-header header-cart">
            <div class="page-container top-wrapper">

                <a class="header-logo-link" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                    <svg viewBox="0 0 138 30" class="header-logo">
                        <use xlink:href="
                                          <?php echo get_field('site_logo', 'option') ?>"></use>
                    </svg>
                </a>
                <div class="header-cart-menu">
                    <a class="cart-menu-link" href="#">Help</a>
                    <div class="cart-localization">
                        <span class="localization-lang">EN</span>
<!--                        <span class="localization-lang">ET</span>-->
<!--                        <span class="localization-lang">RU</span>-->
                    </div>
                </div>

            </div>
            <div class="header-cart-info">
                <div class="product-step one active decor">
                    <p class="product-step__description">
                        Bag <span class="aside-headline"></span>
                    </p>

                </div>
                <div class="product-step two <?php if (is_page('checkout')) echo 'active'; ?> decor">
                    <p class="product-step__description">
                        Details & Delivery
                    </p>
                </div>
                <div class="product-step three">
                    <p class="product-step__description">
                        Pay
                    </p>
                </div>
            </div>
        </header>

<?php get_template_part('template-parts/aside', 'login') ?>

        <?php if (!is_user_logged_in()) { ?>
        <div class="control-button">

            <div class="cart-subtotal">
                <span><?php esc_html_e( 'Total:', 'woocommerce' ); ?></span>
<!--                <p>--><?php //wc_cart_totals_order_total_html(); ?><!--</p>-->
                <p class="cost"> </p>
            </div>

            <div class="radio-item">
                <input type="radio" id="checkoutGuest" name="checkout" value="checked" checked>
                <label for="checkoutGuest">Checkout as a guest</label>
            </div>

            <div class="radio-item">
                <input type="radio" id="checkoutAccount" name="checkout" value="checked2">
                <label for="checkoutAccount">Checkout with account</label>
            </div>
            <div class="choice">
                <div class="go-to-login">
                    <button type="button" class="cart-sign-in">Sign In</button>
                    <button type="button" class="cart-sign-up">Sign up</button>
                </div>
                <div class="go-to-checkout">
                    <?php do_action('woocommerce_proceed_to_checkout'); ?>
                </div>
            </div>
        </div>
       <?php } else { ?>
        <div class="control-button">
        <div class="go-to-checkout">
            <div class="cart-subtotal ss">
                <p><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
<!--                <p>--><?php //wc_cart_totals_order_total_html(); ?><!--</p>-->
                <p class="cost"> </p>
            </div>
            <?php do_action('woocommerce_proceed_to_checkout'); ?>
        </div>
        </div>
        <?php  } ?>

        <div class="loginCheck" data-login-check <?php
        if ( is_user_logged_in() ) {
            echo 'registered';
        } else {
            echo 'visitor';
        }
        ?>></div>
        <div class="modal fade suggestions-modal" id="suggestionsOpen" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">

                    <button type="button" class="btn-close btn-close-suggestions" data-bs-dismiss="modal"
                            aria-label="Close"></button>

                    <div class="modal-body">
                        <h3 class="suggestions-modal-title"><?php echo get_field('our_suggestions_title', 'option')?></h3>
                        <div class="our-suggestions suggestions-slider">

                            <?php get_template_part('template-parts/content', 'suggestions'); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

