<?php
/*
 * Template Name: Sitemap Template
 */
?>

<?php get_header(); ?>
<main class="sitemap-main">
    <div class="page-container">
        <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
        <h1>Sitemap</h1>

        <div class="sitemap-container">
            <?php while (have_rows('sitemap_items')): the_row(); ?>
                <div class="item-wrap <?php echo get_sub_field('sitemap__heading') ?>">
                    <h2><?php echo the_sub_field('sitemap__heading') ?></h2>
                    <div class="item-block">
                        <?php while (have_rows('sitemap_links')): the_row(); ?>
                            <div class="sitemap-item">
                                <a class="sitemap-item-link" href="<?php echo get_sub_field('sitemap_item_link')?>">
                                    <p class="sitemap-item-label"><?php echo get_sub_field('sitemap_item_label')?></p>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</main>
<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>

<?php get_template_part('template-parts/subscribe') ?>

<?php get_footer(); ?>
