<?php
/*
* Template Name: Search Template
*/

 get_header(); ?>

<?php

$text = $_GET['search_text'];

$args = array(
    'post_type' => 'product',
    'posts_per_page' => 32,
    's' => $text
);

$query3 = new WP_Query($args);
$allPosts = $query3 ->found_posts;

?>
<main id="page-<?php the_ID(); ?>" class="site-main" role="main">

    <div class="search-post-wrap">

        <div class="filter">
            <div class="filter-container">
                <div class="filter-wrap">
                    <?php echo do_shortcode('[yith_wcan_filters slug="default-preset"]') ?>
                </div>
                <div class="reset">
                    <?php echo do_shortcode('[yith_wcan_reset_button]') ?>
                </div>
            </div>
            <div class="filter-button-wrap">
                <button id="sortBy" class="sort-by main-modal-button">Sort by</button>
                <?php echo do_shortcode('[yith_wcan_mobile_modal_opener]') ?>
            </div>
        </div>

        <div class="page-container container-flex-col">
            <div class="orderby">
                <?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?>
            </div>
            <div class="info-panel">

                <div class="headline breadcrumbs">

                    <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>

                    <h2 class="page-title">Products(<?php echo $allPosts ?>)</h2>

                </div>

                <div class="mobile-grid-button">
                    <a class="toggle-grid col_2">
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                    </a>
                    <a class="toggle-grid col_1 active-grid-button">
                        <span class="vertical-line"></span>
                    </a>
                </div>
                <div class="product-sort">
                    <div class="sort"><?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?></div>
                    <div class="desktop grid-button">
                        <a class="toggle-grid col_4">
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                        </a>
                        <a class="toggle-grid col_3 active-grid-button">
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                        </a>
                    </div>
                </div>
            </div>


            <div class="container-flex-row row"
                 data-all-posts="<?php echo $allPosts ?>"
                 data-page="<?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>"
                 data-max="<?php echo $query3->max_num_pages; ?>"
                 data-searched="<?php echo $text?>"
            >

               <?php

                if ($query3->have_posts()) :
                    while ($query3 -> have_posts()) : $query3 -> the_post();
                        ?>

                        <?php get_template_part('template-parts/content', 'category'); ?>

                    <?php endwhile;?>
                <?php else : ?>

                    <?php get_template_part('template-parts/content', 'none'); ?>
                <?php endif; ?>

        </div>
            <div class="load-more search-load-more-button">
                <div class="load-more_text">You have viewed <span id="viewedPosts" class="viewedPosts"></span> of
                    <span><?php echo $allPosts ?></span> products
                </div>
                <div class="load-more_line">
                    <div class="load-more_progress"></div>
                </div>
                <button id="load_moress" class="load-more_btn">Load <?php echo ($allPosts - 32 >= 32) ? 32 : $allPosts % 32; ?> more</button>
            </div>
    </div>


            <?php get_template_part('template-parts/content', 'recently-product')?>

        <?php get_template_part('template-parts/subscribe') ?>
</main>

<?php get_footer(); ?>
