<?php
/**
 * theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme
 */

if ( ! function_exists( 'theme_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function theme_setup() {
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu().

        register_nav_menus( array(
            'Icon-menu' => esc_html__( 'Icon-menu', 'theme' ),
            'Account-menu' => esc_html__( 'Account-menu', 'theme' ),
            'menu-1' => esc_html__( 'Primary', 'theme' ),
            'menu-top' => esc_html__( 'Secondary', 'theme' ),
            'Navigation' => esc_html__( 'Navigation', 'theme' ),
            'Eyeglasses-1' => esc_html__( 'Eyeglasses-1', 'theme' ),
            'Eyeglasses-2' => esc_html__( 'Eyeglasses-2', 'theme' ),

            'Categories-menu' => esc_html__( 'Categories-menu', 'theme' ),

            'Sunglasses-1' => esc_html__( 'Sunglasses-1', 'theme' ),
            'Sunglasses-2' => esc_html__( 'Sunglasses-2', 'theme' ),

            'Contact-lenses-1' => esc_html__( 'Contact-lenses-1', 'theme' ),
            'Contact-lenses-2' => esc_html__( 'Contact-lenses-2', 'theme' ),

            'Special-glasses-1' => esc_html__( 'Special-glasses-1', 'theme' ),
            'Special-glasses-2' => esc_html__( 'Special-glasses-2', 'theme' ),

            'footer-menu' => esc_html__( 'Footer', 'theme' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );
    }
endif;
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'theme' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'theme' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function theme_scripts() {

    wp_enqueue_style( 'theme-main-style', get_template_directory_uri() . '/dist/css/bundle.css' );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/dist/js/bundle.js', array("jquery"), false, true );
    wp_enqueue_script('single-product-fitting-script', get_template_directory_uri() . '/dist/js/singleProductFitting.js', array("jquery"), false, true);
    wp_enqueue_script('single-product-fitting-api', 'https://static.fittingbox.com/api/v1/fitmix.js', array("jquery"), false, true);
    wp_enqueue_script('cart-lenses-script', get_template_directory_uri() . '/dist/js/cartLenses.js', array("jquery"), false, true);

    add_action('wp_enqueue_scripts', function () {
        wp_localize_script('theme-script', 'myajax',
            array(
                'url' => admin_url('admin-ajax.php'),
                'nonce' => wp_create_nonce('myajax-nonce')
            )
        );
    }, 99);

    global $post;

    switch ($post->post_name) {
        case 'homepage':
            wp_enqueue_style( 'home-page-style', get_template_directory_uri() . '/dist/css/home.css' );
            wp_enqueue_script( 'home-page-script', get_template_directory_uri() . '/dist/js/home.js', array("jquery"), false, true );
            break;

        case 'blog':
            wp_enqueue_style( 'blog-page-style', get_template_directory_uri() . '/dist/css/blog.css' );
            wp_enqueue_script( 'blog-page-script', get_template_directory_uri() . '/dist/js/blog.js', array("jquery"), false, true );
            break;

        case 'terms':
            wp_enqueue_style( 'terms-page-style', get_template_directory_uri() . '/dist/css/terms.css' );
            wp_enqueue_script( 'terms-page-script', get_template_directory_uri() . '/dist/js/terms.js', array("jquery"), false, true );
            break;
        case 'faq':
            wp_enqueue_style( 'faq-page-style', get_template_directory_uri() . '/dist/css/faq.css' );
            wp_enqueue_script( 'faq-page-script', get_template_directory_uri() . '/dist/js/faq.js', array("jquery"), false, true );
            break;

        case 'sitemap':
            wp_enqueue_style( 'sitemap-style', get_template_directory_uri() . '/dist/css/faq.css' );
            wp_enqueue_script( 'sitemap-script', get_template_directory_uri() . '/dist/js/faq.js', array("jquery"), false, true );
            break;

        case 'contacts':
            wp_enqueue_style( 'contacts-page-style', get_template_directory_uri() . '/dist/css/contacts.css' );
            wp_enqueue_script( 'contacts-page-script', get_template_directory_uri() . '/dist/js/contacts.js', array("jquery"), false, true );
            break;

        case 'confirmation-guest':
            wp_enqueue_style( 'confirmation-guest-page-style', get_template_directory_uri() . '/dist/css/confirmationGuest.css' );
            wp_enqueue_script( 'confirmation-guest-page-script', get_template_directory_uri() . '/dist/js/confirmationGuest.js', array("jquery"), false, true );
            break;

        case 'confirmation':
            wp_enqueue_style( 'confirmation-member-page-style', get_template_directory_uri() . '/dist/css/confirmationMember.css' );
            wp_enqueue_script( 'confirmation-member-page-script', get_template_directory_uri() . '/dist/js/confirmationMember.js', array("jquery"), false, true );
            break;
        case 'brands':
            wp_enqueue_style( 'brands-page-style', get_template_directory_uri() . '/dist/css/brands.css' );
            wp_enqueue_script( 'brands-page-script', get_template_directory_uri() . '/dist/js/brands.js', array("jquery"), false, true );
            break;

        case is_singular('post') :
            wp_enqueue_style('single-page-style', get_template_directory_uri() . '/dist/css/single.css');
            wp_enqueue_script('single-page-script', get_template_directory_uri() . '/dist/js/single.js', array("jquery"), false, true);
            wp_enqueue_script('like_post', get_template_directory_uri() . '/dist/js/postLike.js', array('jquery'), false, true);
            wp_localize_script('like_post', 'ajax_var', array(
                'url' => admin_url('admin-ajax.php'),
                'nonce' => wp_create_nonce('ajax-nonce')
            ));
            break;

        case is_singular('brand')   :
            wp_enqueue_style('brand-page-style', get_template_directory_uri() . '/dist/css/brand.css');
            wp_enqueue_script('brand-page-script', get_template_directory_uri() . '/dist/js/brand.js', array("jquery"), false, true);
            break;

        case is_category() && 'category' :
            wp_enqueue_style( 'bootstrap-css-category', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
            wp_enqueue_script( 'bootstrap-js-category', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );
            wp_enqueue_style('blog-category-page-style', get_template_directory_uri() . '/dist/css/blog.css');
            wp_enqueue_script('blog-category-page-script', get_template_directory_uri() . '/dist/js/blog.js', array("jquery"), false, true);
            break;

        case is_product_category() :
            wp_enqueue_style( 'bootstrap-css-category', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
            wp_enqueue_script( 'bootstrap-js-category', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );
            wp_enqueue_style('category-page-style1', get_template_directory_uri() . '/dist/css/productCategory.css');
            wp_enqueue_script('category-page-script1', get_template_directory_uri() . '/dist/js/productCategory.js', array("jquery"), false, true);
            wp_enqueue_style('category-page-style12', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('category-page-script12', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;

        case 'sunglasses' :
            wp_enqueue_style('category-page-style', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('category-page-script', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;
        case 'eyeglasses' :
            wp_enqueue_style('category-page-style2', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('category-page-script2', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;
        case 'contact-lenses' :
            wp_enqueue_style('category-page-style3', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('category-page-script3', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;

        case 'accessories' :
            wp_enqueue_style('category-page-style4', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('category-page-script4', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;

        case 'wish-list' :
            wp_enqueue_style('wish-list-style', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('wish-list-script', get_template_directory_uri() . '/dist/js/category.js', array("jquery"), false, true);
            break;

        case 'search' :
            wp_enqueue_style('search-page-style', get_template_directory_uri() . '/dist/css/category.css');
            wp_enqueue_script('search-page-script', get_template_directory_uri() . '/dist/js/search.js', array("jquery"), false, true);
            break;

        case 'my-account' :
            wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
            wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );
            wp_enqueue_style('my-account-style', get_template_directory_uri() . '/dist/css/myAccount.css');
            wp_enqueue_script('my-account-script', get_template_directory_uri() . '/dist/js/myAccount.js', array("jquery"), false, true);
            break;

        case 'cart' :
            wp_enqueue_style('cart-style', get_template_directory_uri() . '/dist/css/cart.css');
            wp_enqueue_script('cart-script', get_template_directory_uri() . '/dist/js/cart.js', array("jquery"), false, true);
//            wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
//            wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );
            break;

        case 'checkout' :
            wp_enqueue_style('checkout-style', get_template_directory_uri() . '/dist/css/checkout.css');
            wp_enqueue_script('checkout-script', get_template_directory_uri() . '/dist/js/checkout.js', array("jquery"), false, true);
            wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
            wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );
            break;

        default :
//            echo 'false';
    }

    switch ($post->post_type) {
        case 'product':
            wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' );
            wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', array("jquery"), false, true );

            wp_enqueue_style( 'single-product-style', get_template_directory_uri() . '/dist/css/singleProduct.css' );
            wp_enqueue_script( 'single-product-script', get_template_directory_uri() . '/dist/js/singleProduct.js', array("jquery"), false, true );
            wp_enqueue_script('single-product-tab-script', get_template_directory_uri() . '/dist/js/singleProductTab.js', array("jquery"), false, true);


            break;
    }

    if (get_field('wizard_needed') === 'Lenses wizard') {
        wp_enqueue_style( 'single-product-lenses', get_template_directory_uri() . '/dist/css/singleProductLenses.css' );
        wp_enqueue_script( 'single-product-lenses-script', get_template_directory_uri() . '/dist/js/singleProductLenses.js', array("jquery"), false, true );

    }

}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

function admin_panel_js($hook) {
    if ('post.php' !== $hook) {
        return;
    }
    wp_enqueue_script('admin-panel-glasses', get_template_directory_uri() . '/dist/js/adminPanel.js');
}
add_action('admin_enqueue_scripts', 'admin_panel_js');

function create_post_type()
{
    register_post_type('brand',
        array(
            'supports' => array('title', 'excerpt', 'thumbnail', 'author', 'editor', 'custom-fields'),
            'labels' => array(
                'name' => ('Brand'),
                'singular_name' => ('Brand')
            ),
            'public' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-format-quote',
            'rewrite' => array('slug' => 'brand'),
            'has_archive' => true,
        )
    );

    register_post_type('testimonial',
        array(
            'supports' => array('title'),
            'labels' => array(
                'name' => __('Testimonial'),
                'singular_name' => __('Testimonial')
            ),
            'public' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-admin-comments',
            'rewrite' => array('slug' => 'testimonial'),
            'has_archive' => true,
        )
    );
}

add_action('init', 'create_post_type');


add_action('wp_ajax_nopriv_post-like', 'post_like');
add_action('wp_ajax_post-like', 'post_like');

function post_like()
{
    $nonce = $_POST['nonce'];
    if (!wp_verify_nonce($nonce, 'ajax-nonce')) die ('Error!');
    if (isset($_POST['post_like'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $post_id = $_POST['post_id'];
        $meta_IP = get_post_meta($post_id, "voted_IP");
        $voted_IP = $meta_IP[0] ?? null;
        $meta_count = get_post_meta($post_id, "votes_count", true);
        if (!is_array($voted_IP)) $voted_IP = array();
        if (!hasAlreadyVoted($post_id)) {
            $voted_IP[$ip] = time();
            update_post_meta($post_id, "voted_IP", $voted_IP);
            update_post_meta($post_id, "votes_count", ++$meta_count);
            echo $meta_count;
        } else echo "already";
    }
    exit;
}

function hasAlreadyVoted($post_id)
{
    global $timebeforerevote;
    $timebeforerevote = 120;
    $meta_IP = get_post_meta($post_id, "voted_IP");
    $voted_IP = $meta_IP[0] ?? null;
    if (!is_array($voted_IP)) $voted_IP = array();
    $ip = $_SERVER['REMOTE_ADDR'];
    if (in_array($ip, array_keys($voted_IP))) {
        $time = $voted_IP[$ip];
        $now = time();
        if (round(($now - $time) / 60) > $timebeforerevote) return false;
        return true;
    }
    return false;
}

//function getPostLikeLink($post_id)
//{
//    $vote_count = get_post_meta($post_id, "votes_count", true);
//    $output = '';
//
//    if (hasAlreadyVoted($post_id)) {
//        $output .= '
//                    <div class="svg_bottom_ico">
//                            <div class="like_ico is-active">
//                                <svg width="22" height="20">
//                                    <use xlink:href="#like"></use>
//                                </svg>
//                            </div>
//                    </div>
//                    <span class="likecount"> ' . $vote_count . '</span>';
//
//    } else {
//        $output .= '
//                    <div class="svg_bottom_ico">
//                        <div class="like_ico noactive_svg" data-post_id="' . $post_id . '">
//                            <svg width="22" height="20">
//                                <use xlink:href="#like"></use>
//                            </svg>
//                        </div>
//                    </div>
//                    <div>|</div>
//                    <span class="likecount"> ' . $vote_count . '</span>';
//    }
//    return $output;
//}

//function wpb_set_post_views($postID)
//{
//    $count_key = 'wpb_post_views_count';
//    $count = get_post_meta($postID, $count_key, true);
//    if ($count == '') {
//        $count = 0;
//        delete_post_meta($postID, $count_key);
//        add_post_meta($postID, $count_key, '0');
//    } else {
//        $count++;
//        update_post_meta($postID, $count_key, $count);
//    }
//}
//
//remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
//
//
//function wpb_track_post_views($post_id)
//{
//    if (!is_single()) return;
//    if (empty ($post_id)) {
//        global $post;
//        $post_id = $post->ID;
//    }
//    wpb_set_post_views($post_id);
//}
//
//add_action('wp_head', 'wpb_track_post_views');


























function load_more_posts()
{
    $next_page = $_POST['currentPage'] + 1;
    $query = new WP_Query(['posts_per_page' => 3, 'paged' => $next_page]);
    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
        get_template_part('template-parts/blog', 'item');
    endwhile;
    endif;
    die();
}

add_action('wp_ajax_load_more_posts', 'load_more_posts');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');
function load_more_posts_current_category()
{
    $next_page = $_POST['current_page'] + 1;
    $query = new WP_Query(['posts_per_page' => 3, 'paged' => $next_page, 'cat' => $_POST['current_category']]);
    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
        get_template_part('template-parts/blog', 'item');
    endwhile;
    endif;
    die();
}

add_action('wp_ajax_load_more_posts_current_category', 'load_more_posts_current_category');
add_action('wp_ajax_nopriv_load_more_posts_current_category', 'load_more_posts_current_category');

add_filter( 'woocommerce_get_breadcrumb', 'custom_get_breadcrumb', 20, 2 );
function custom_get_breadcrumb( $crumbs, $breadcrumb ){
    $new_crumbs = [];

    foreach ($crumbs as $crumb) {
//        $crumb[1] = str_replace('/product-category/sunglasses/', '/sunglasses/', $crumb[1]);
//        $crumb[1] = str_replace('/product-category/eyeglasses/', '/eyeglasses/', $crumb[1]);
//        $crumb[1] = str_replace('/product-category/contact-lenses/', '/contact-lenses/', $crumb[1]);
        array_push($new_crumbs, $crumb);
    }

    if( ! is_shop() ) return $new_crumbs; // Only shop page

    // The Crump item to target
    $target = __( 'Home', 'woocommerce' );

    foreach($new_crumbs as $key => $crumb){
        if( $target === $crumb[0] ){
            // 1. Change name
            $new_crumbs[$key][0] = __( 'Name', 'woocommerce' );

            // 2. Change URL (you can also use get_permalink( $id ) with the post Id
            $new_crumbs[$key][1] = home_url( '/my-link/' );
        }
    }
    return $new_crumbs;
}

function breadcrumbs()
{

    /* === ОПЦИИ === */
    $text['home']     = 'Home'; // текст ссылки
    $text['category'] = '%s'; // текст для страницы рубрики
    $text['searchs']   = 'Search results'; // текст для страницы с результатами поиска
    $text['tag']      = 'Записи с тегом "%s"'; // текст для страницы тега
    $text['author']   = 'Статьи автора %s'; // текст для страницы автора
    $text['404']      = 'Ошибка 404'; // текст для страницы 404
    $text['page']     = 'Страница %s'; // текст 'Страница N'
    $text['cpage']    = 'Страница комментариев %s'; // текст 'Страница комментариев N'

    $wrap_before    = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
    $wrap_after     = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
    $sep            = '<span class="breadcrumbs__separator"> > </span>'; // разделитель между "крошками"
    $before         = '<span class="breadcrumbs__current">'; // тег перед текущей "крошкой"
    $after          = '</span>'; // тег после текущей "крошки"

    $show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
    $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
    $show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
    $show_last_sep  = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
    /* === КОНЕЦ ОПЦИЙ === */

    global $post;
    $home_url       = home_url('/');
    $link           = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
    $link          .= '<meta itemprop="position" content="%3$s" />';
    $link          .= '</span>';
    $parent_id      = ( $post ) ? $post->post_parent : '';
    $home_link      = sprintf( $link, $home_url, $text['home'], 1 );

    if ( is_home() || is_front_page() ) {

        if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

    } else {

        $position = 0;

        echo $wrap_before;

        if ( $show_home_link ) {
            $position += 1;
            echo $home_link;
        }

        if ( is_category() ) {
            $parents = get_ancestors( get_query_var('cat'), 'category' );
            foreach ( array_reverse( $parents ) as $cat ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $cat = get_query_var('cat');
                echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 ) echo $sep;
                    echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
                } elseif ( $show_last_sep ) echo $sep;
            }

        } elseif (  is_page('search') ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $show_home_link ) echo $sep;
                echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) );
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 ) echo $sep;
                    echo $before . 'Search results';
                } elseif ( $show_last_sep ) echo $sep;
            }

        } elseif ( is_year() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . get_the_time('Y') . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

        } elseif ( is_month() ) {
            if ( $show_home_link ) echo $sep;
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
            if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_day() ) {
            if ( $show_home_link ) echo $sep;
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
            $position += 1;
            echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
            if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_single() && ! is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $position += 1;
                $post_type = get_post_type_object( get_post_type() );
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
                if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                elseif ( $show_last_sep ) echo $sep;
            } else {
                $cat = get_the_category(); $catID = $cat[0]->cat_ID;
                $parents = get_ancestors( $catID, 'category' );
                $parents = array_reverse( $parents );
                $parents[] = $catID;
                foreach ( $parents as $cat ) {
                    $position += 1;
                    if ( $position > 1 ) echo $sep;
                    echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                }
                if ( get_query_var( 'cpage' ) ) {
                    $position += 1;
                    echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
                    echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
                } else {
                    if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                    elseif ( $show_last_sep ) echo $sep;
                }
            }

        } elseif ( is_post_type_archive() ) {
            $post_type = get_post_type_object( get_post_type() );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . $post_type->label . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_attachment() ) {
            $parent = get_post( $parent_id );
            $cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
            $parents = get_ancestors( $catID, 'category' );
            $parents = array_reverse( $parents );
            $parents[] = $catID;
            foreach ( $parents as $cat ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            $position += 1;
            echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
            if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_page() && ! $parent_id ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

        } elseif ( is_page() && $parent_id ) {
            $parents = get_post_ancestors( get_the_ID() );
            foreach ( array_reverse( $parents ) as $pageID ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
            }
            if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_tag() ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $tagID = get_query_var( 'tag_id' );
                echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_author() ) {
            $author = get_userdata( get_query_var( 'author' ) );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_404() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . $text['404'] . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( has_post_format() && ! is_singular() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            echo get_post_format_string( get_post_format() );
        }

        echo $wrap_after;

    }
}

add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
function wcc_change_breadcrumb_delimiter( $defaults ) {
    // Change the breadcrumb delimeter from '/' to '>'
    $defaults['delimiter'] = ' &gt; ';
    return $defaults;
}

/**
 * Clear WP HEAD
 */
require get_template_directory() . '/include/clear-wp-head.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/include/customizer.php';

/**
 * Create custom post types and taxonomy.
 */
require get_template_directory() . '/include/setup.php';
/**
 * Php helpers
 */
require get_template_directory() . "/helpers/index.php";

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'nav_menu_submenu_css_class', 'change_wp_nav_menu', 10, 3 );

function change_wp_nav_menu( $classes, $args, $depth ) {
    $classes[] = 'sub-menu-depth-1';

    return $classes;
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));
}

add_filter( 'woocommerce_product_tabs', 'woo_custom_product_tabs' );
function woo_custom_product_tabs( $tabs ) {

    // 1) Removing tabs

    unset( $tabs['description'] );              // Remove the description tab
    // unset( $tabs['reviews'] );               // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab


    // 2 Adding new tabs and set the right order

    //Attribute Description tab
    $tabs['attrib_desc_tab'] = array(
        'title'     => __( 'Description', 'woocommerce' ),
        'priority'  => 100,
        'callback'  => 'woo_attrib_desc_tab_content'
    );

    // Adds the qty pricing  tab
    $tabs['details_tab'] = array(
        'title'     => __( 'Details', 'woocommerce' ),
        'priority'  => 110,
        'callback'  => 'woo_details_tab_content'
    );

    // Adds the other products tab
    $tabs['sizes_tab'] = array(
        'title'     => __( 'Sizes', 'woocommerce' ),
        'priority'  => 120,
        'callback'  => 'woo_sizes_products_tab_content'
    );

    return $tabs;

}

// New Tab contents

function woo_attrib_desc_tab_content()
{
    // The attribute description tab content

    $content = get_the_content();

    echo '<p>' . $content . '</p>';

}
remove_filter('term_description','wpautop'); //remove paragraph on tooltips
function woo_details_tab_content()
{
    // The Details tab content
    // Create the object

    global $product;

    $attributes = $product->get_attributes();

    $res = '';

    foreach ($attributes as $attribute) {
        $attr_name = wc_get_attribute($attribute['id'])->name;

        $res .= '<div class="tax-head"><span>' . $attr_name . ': </span>';

        $attr_array = get_the_terms($product->get_id(), $attribute['name']);

        $res .= '<span class="tax-name">';
        $attr_val = '';

        foreach ($attr_array as $attr) {
            $tooltip = '';
            if ($attr->description) {
                $tooltip = '&nbsp;<span class="button-tooltip tooltip-hover" title="' . $attr->description . '">!</span>';
            }

            $attr_val .= $attr->name . $tooltip . ', ';
        }

        $res .= substr($attr_val, 0, -2) . '</span></div>';
    }


    echo $res;


//    $size = get_the_terms($product->get_id(), 'pa_size');
//    if ($size) {
//        echo '<p class="tax-head">Size: <span class="tax-name">';
//        foreach ($size as $value) {
//            echo '<span>' . $value->name . '&nbsp;' . '</span>';
//        }
//        echo '</span></p>';
//    }
//    $color = get_the_terms($product->get_id(), 'pa_color');
//    if ($color) {
//        echo '<p class="tax-head">Color: <span class="tax-name">';
//        foreach ($color as $value) {
//            echo '<span>' . $value->name . '&nbsp;' . '</span>';
//        }
//        echo '</span></p>';
//    }
//    $material = get_the_terms($product->get_id(), 'pa_material');
//
//    if ($material) {
//        echo '<div class="tax-head">Material: <span class="tax-name">';
//        foreach ($material as $value) {
//            echo '<div class="tax-item-group">' . $value->name  . '&nbsp;' .  '<span class="button-tooltip"
//data-bs-toggle="tooltip" data-bs-placement="bottom" title="'. term_description( $value->term_id ) .'"
//>!</span></div>';
//
//        }
//
//        echo '</span> </div>';
//    }
//
//    $shape = get_the_terms($product->get_id(), 'pa_shape');
//
//    if ($shape) {
//        echo '<div class="tax-head">Shape: <span class="tax-name">';
//        foreach ($shape as $value) {
//            echo '<div class="tax-item-group">' . $value->name  . '&nbsp;' .  '<span class="button-tooltip"
//data-bs-toggle="tooltip" data-bs-placement="bottom" title="'. term_description( $value->term_id ) .'"
//>!</span></div>';
//
//        }
//
//        echo '</span> </div>';
//    }
//    $coating = get_the_terms($product->get_id(), 'pa_coating');
//    if ($coating) {
//        echo '<p>';
//        foreach ($coating as $value) {
//            echo '<span>' . $value->name . '&nbsp;' . '</span>';
//        }
//        echo '</p>';
//    }
}

function woo_sizes_products_tab_content()
{
    // The Sizes tab content

    echo get_field('size_block', 'option');
}

function set_user_visited_product_cookie() {
    global $post;

    if ( is_product() )
    {
        $existing_product_id = $_COOKIE['woocommerce_recently_viewed'];
        $cookie_arr = explode('|', $existing_product_id );
        if (!in_array($post->ID, $cookie_arr ) ) {
            array_push($cookie_arr, $post->ID);
        }

        if (count($cookie_arr) > 5) {
            array_shift($cookie_arr);
        }

        $existing_product_id = implode('|', $cookie_arr);

        wc_setcookie( 'woocommerce_recently_viewed',  $existing_product_id );
    }
}

add_action( 'wp', 'set_user_visited_product_cookie' );

/*
*
 * Register the [woocommerce_recently_viewed_products per_page="5"] shortcode
 *
 * This shortcode displays recently viewed products using WooCommerce default cookie
 * It only has one parameter "per_page" to choose number of items to show
 *
 * @access      public
 * @return      $content
 * @since       1.0
 */
function rc_woocommerce_recently_viewed_products($atts, $content = null)
{

    // Get shortcode parameters
    extract(shortcode_atts(array(
        "per_page" => '5'
    ), $atts));

    // Get WooCommerce Global
    global $woocommerce;

    // Get recently viewed product cookies data
    $viewed_products = !empty($_COOKIE['woocommerce_recently_viewed']) ? (array)explode('|', $_COOKIE['woocommerce_recently_viewed']) : array();
    $viewed_products = array_filter(array_map('absint', $viewed_products));

    // If no data, quit
    if (empty($viewed_products))
        return __('', 'rc_wc_rvp');
//    return __('You have not viewed any product yet!', 'rc_wc_rvp');

    // Create the object
    ob_start();

    // Get products per page
    if (!isset($per_page) ? $number = 5 : $number = $per_page)

        // Create query arguments array
        $query_args = array(
            'posts_per_page' => $number,
            'no_found_rows' => 1,
            'post_status' => 'publish',
            'post_type' => 'product',
            'post__in' => $viewed_products,
            'orderby' => 'rand'
        );

    // Add meta_query to query args
    $query_args['meta_query'] = array();

    // Check products stock status
    $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();

    // Create a new query
    $r = new WP_Query($query_args);

    // If query return results
    if ($r->have_posts()) {

        $content = '<div class="recently-headline">
            <h2 class="recently-header">Recently Viewed</h2>
            <button class="clear-cookie">Clear all</button>
                    </div><div class="recently-nav-container"></div><div class="recently-slider">';
        // Start the loop
        while ($r->have_posts()) {
            $r->the_post();
            global $product;
            $content .= get_template_part("template-parts/content", "category");
        }
    }

    // Get clean object
    $content .= ob_get_clean();

    // Return whole content
    return $content;
}
// Register the shortcode
add_shortcode("woocommerce_recently_viewed_products", "rc_woocommerce_recently_viewed_products");

function clear_cookie(){
    wc_setcookie( 'woocommerce_recently_viewed',  "", time()-3600);
    echo 'test';
}

add_action('wp_ajax_clear', 'clear_cookie');
add_action('wp_ajax_nopriv_clear', 'clear_cookie');

// change woocommerce thumbnail image size
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'override_woocommerce_image_size_gallery_thumbnail' );
function override_woocommerce_image_size_gallery_thumbnail( $size ) {
    // Gallery thumbnails: proportional, max width 200px
    return array(
        'width'  => 560,
        'height' => 560,
        'crop'   => 0,
    );
}
function add_review_title_field_on_comment_form() {
    echo '<p class="comment-form-title uk-margin-top"><label for="title">' . __( 'Title', 'text-domain' ) . '</label><input class="uk-input uk-width-large uk-display-block" type="text" name="title" id="title" placeholder="Enter title"/></p>';
}
add_action( 'comment_form_logged_in_after', 'add_review_title_field_on_comment_form' );
add_action( 'comment_form_after_fields', 'add_review_title_field_on_comment_form' );

add_action( 'comment_post', 'save_comment_review_title_field' );
function save_comment_review_title_field( $comment_id ){
    if( isset( $_POST['title'] ) )
        update_comment_meta( $comment_id, 'title', esc_attr( $_POST['title'] ) );
}
add_action('init','tw_create_my_template');
function tw_create_my_template(){
    if( isset($_POST['tw_new_checklist_nonce'])
        && wp_verify_nonce($_POST['tw_new_checklist_nonce'],'tw-new-checklist-nonce'))
    {
        return 'Worked!';
    }
}

function wc_custom_user_redirect( $redirect, $user ) {
    // Get the first of all the roles assigned to the user
    $role = $user->roles[0];

    $dashboard = admin_url();
    $myaccount = get_permalink( wc_get_page_id(12) );

    if( $role == 'administrator' ) {
        //Redirect administrators to the dashboard
        $redirect = $myaccount . '/?loggined';

    } elseif ( $role == 'shop-manager' ) {
        //Redirect shop managers to the dashboard
        $redirect = $dashboard;
    } elseif ( $role == 'editor' ) {
        //Redirect editors to the dashboard
        $redirect = $dashboard;
    } elseif ( $role == 'author' ) {
        //Redirect authors to the dashboard
        $redirect = $dashboard;
    } elseif ( $role == 'customer' || $role == 'subscriber' ) {
        //Redirect customers and subscribers to the "My Account" page
        $redirect = $myaccount . '/?loggined';
    } else {
        //Redirect any other role to the previous visited page or, if not available, to the home
        $redirect = wp_get_referer() ? wp_get_referer() : home_url();
    }

    return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );

add_filter( 'woocommerce_coupons_enabled', 'truemisha_coupon_field_on_checkout' );

function truemisha_coupon_field_on_checkout( $enabled ) {
    $enabled = false; // купоны отключены

    return $enabled;

}

function filter_woocommerce_registration_redirect( $redirect ) {

    $myaccount = get_permalink( wc_get_page_id(12) );
    $redirect = $myaccount . '/?loggined';
    return $redirect;
}
add_filter( 'woocommerce_registration_redirect', 'filter_woocommerce_registration_redirect', 10, 1 );

function add_accessories()
{
    $product_id = $_POST['product_id'];
    $var_id = $_POST['product_var_id'];

    if ($var_id ) {
        WC()->cart->add_to_cart( $product_id, 1, $var_id );
    } else {
        WC()->cart->add_to_cart( $product_id );
    }

    exit();
}

add_action('wp_ajax_add_accessories', 'add_accessories');
add_action('wp_ajax_nopriv_add_accessories', 'add_accessories');

function my_account_redirect() {
    if(!is_user_logged_in() && is_page('my-account')) {
        wp_redirect( home_url() );
    }
}
add_action( 'wp', 'my_account_redirect' );

include 'template-parts/functions-parts/wizard-logic-parts.php';
include 'template-parts/functions-parts/account-tabs.php';
include 'template-parts/functions-parts/like-dislike.php';
include 'template-parts/functions-parts/add-account-fields.php';
include 'template-parts/functions-parts/delete-user-button.php';
include 'template-parts/functions-parts/account-address.php';
include 'template-parts/functions-parts/wish-list.php';
include 'template-parts/functions-parts/checkout.php';
include 'template-parts/functions-parts/save-new-prescriptions.php';
include 'template-parts/functions-parts/save-new-lens-prescriptions.php';
include 'template-parts/functions-parts/contact-lenses-add-to-cart.php';
include 'template-parts/functions-parts/product_ajax.php';
include 'template-parts/functions-parts/search_load_more_product.php';
include 'template-parts/functions-parts/lenses-ajax.php';
include 'template-parts/functions-parts/receive-dhl-rates.php';

//add_filter('woocommerce_product_review_list_args', 'newest_reviews_first');
//function newest_reviews_first($args)
//{
//    $args['reverse_top_level'] = true;
//    return $args;
//}

add_action('wp_ajax_cloadmore', 'review_comments_loadmore_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_cloadmore', 'review_comments_loadmore_handler'); // wp_ajax_nopriv_{action}

function review_comments_loadmore_handler(){

    global $comment;
    $comment_id = $comment->comment_ID;
    $next_page = $_POST['page'] + 1;
    $comment_arr = array(
        'callback' => 'woocommerce_comments',
        'per_page' => get_option('comments_per_page'),
        'page' => $_POST['page'],
        'style' => 'ol',
        'paged' => $next_page,
        'order' => 'DESC'
    );

    wp_list_comments(
        apply_filters(
            'woocommerce_product_review_list_args',
            $comment_arr
        )
    );

    ?>

    <?php
    die;
}

?>
