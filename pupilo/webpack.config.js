/* eslint-disable */
const config = require("./config");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const TerserJSPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require("webpack");
const path = require("path");

module.exports = (env, options) => {
    return {
        entry: {
            // include js
            bundle: path.resolve(__dirname, 'src/main.js'),
        },

        output: {
            filename: './js/[name].js',
            path: __dirname + '/dist'
        },
        optimization: {
            minimizer: [
                new TerserJSPlugin({
                    terserOptions: {
                        output: {comments: false}
                    }
                }),
            ]
        },
        externals: {
            "jquery": 'jQuery'
        },
        devtool: "source-map",
        resolve: {
            alias: {
                "@": path.resolve(__dirname, 'src'),
                "@js": path.resolve(__dirname, 'src/assets/js'),
                "@scss": path.resolve(__dirname, 'src/assets/sass'),
                // "@fonts": path.resolve(__dirname, '../src/assets/fonts'),
                // "@images": path.resolve(__dirname, '../src/assets/images'),
                // "@icons": path.resolve(__dirname, '../src/assets/icons'),
                // "@img": path.resolve(__dirname, '../src/assets/images'),
                // "@public": path.resolve(__dirname, '../src/assets'),
                // "@lib/js": path.resolve(__dirname, '../src/assets/lib/js'),
                // "@lib/scss": path.resolve(__dirname, '../src/assets/lib/scss'),
                // "@frame": path.resolve(__dirname, '../src/framework'),
                // "@components": path.resolve(__dirname, '../src/framework/components'),
                // "@helpers": path.resolve(__dirname, "../src/assets/helpers")
            },
            extensions: [".scss", ".sass", ".js", ".css"]
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: ["babel-loader"]
                },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true,
                            },
                        },

                    ]
                },
                {
                    test: /\.(png|jpe?g|svg|gif)/i,
                    exclude: [/fonts/],
                    use: [
                        {
                            loader: "url-loader",
                            options: {
                                name: "[name].[ext]",
                                limit: 10000,
                                outputPath: './images/',
                                publicPath: '../images/'
                            }
                        },
                        {
                            loader: "img-loader"
                        }
                    ]
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    include: [/fonts/],
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: './fonts/',
                            publicPath: '../fonts/'
                        }
                    }]
                }
            ]
        },
        devServer: {
            host: 'localhost',
            port: '8081'
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "css/[name].css",
                chunkFilename: "[id].css",
            }),
            new BrowserSyncPlugin({
                proxy: config.localUrl,
                host: config.devServer.host,
                port: config.devServer.port,
                files: [
                    {
                        match: ['{helpers,include,template-parts,woocommerce}/**/*.php', '*.php'],
                        fn: function (event) {
                            if (event === "change") {
                                const bs = require("browser-sync").get("bs-webpack-plugin");
                                bs.reload();
                            }
                        }
                    }
                ]
            }, {
                reload: true
            }),
            new CopyWebpackPlugin({
                patterns: [
                    { from: path.resolve(__dirname, 'src/assets/icons'), to: './icons' },
                    { from: path.resolve(__dirname, 'src/assets/images'), to: './images' }
                ],
            }),

            new webpack.DefinePlugin({
                isDev: options.mode !== "production"
            }),
            new webpack.ProvidePlugin({
                dl: path.resolve(__dirname, "./src/assets/lib/js/devLogger"),
                "$": "jquery",
                "jQuery": "jquery"
            })
        ]
    }
};
