<section class="pre-footer-section">

    <?php while (have_rows('subscribe', 'option')): the_row(); ?>
        <div class="subscribe-item-mobile"
             style="background-image: url('<?php echo get_sub_field('subscribe_banner_mobile')['url'] ?>')">
            <div class="page-container content-container">
                <img class="subscribe-desktop-bg" src="<?php echo get_sub_field('subscribe_banner')['url'] ?>" alt="">

                <div class="subscribe-item-mobile-info">
                    <div class="subscribe-info-text">
                        <h3 class="subscribe-info-title"><?php echo get_sub_field('subscribe_header') ?></h3>
                        <p class="info-text-style"><?php echo get_sub_field('subscribe_description') ?></p>
                    </div>
                    <div class="subscribe-group">
                        <input class="subscribe-email" type="email" placeholder="Enter your Email ">
                        <button class="btn btn--subscribe"></button>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; ?>

</section>
