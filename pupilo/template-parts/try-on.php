<section class="try-on">
    <img class="try-on-banner" src="<?php echo get_field('try_on_banner', 'option')['url']?>"
         alt="<?php echo get_field('try_on_banner', 'option')['url']?>">
    <img class="try-on-mobile-banner" src="<?php echo get_field('try_on_mobile_banner', 'option')['url']?>"
         alt="<?php echo get_field('try_on_mobile_banner', 'option')['url']?>">
    <div class="try-on-info">
        <h2 class="try-on-info-title"><?php echo get_field('try_on_title', 'option')?></h2>
        <p class="try-on-info-description"><?php echo get_field('try_on_description', 'option')?></p>
        <a class="try-now" href="<?php echo get_field('try_on_button', 'option')['url']?>">
            <?php echo get_field('try_on_button', 'option')['title']?>
        </a>
    </div>
</section>
