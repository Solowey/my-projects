<div class="login-wrap">
    <div class="login-info">
        <h5 class="login-header">
            <button  type="button" class="button-back login-info-popup-close">
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg>
                Login or Sign up
            </button>
        </h5>
        <div class="login-text">
            <h5>Welcome back! </h5>
            <p>Log in to earn <b>5%</b> bonus money and save time on checkout. Enjoy this and that too.</p>
        </div>
        <?php echo do_shortcode('[TheChamp-Login]')?>

        <button type="button" class="log-button" data-login-open>Log in with email</button>
        <div class="login-text">
            <h5>Don’t have an account? </h5>
            <p>Sign up to earn <b>5%</b> bonus money and save time on checkout. Enjoy this and that too.</p>
        </div>
        <button  type="button" class="log-button log-button-type-2" data-sign-up-open>Sign up</button>

    </div>

    <div class="login-modal">
        <h5 class="login-header">

            <button  type="button" class="button-back" data-return-to-info>
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg> Log in via email
            </button>

        </h5>

        <div class="login-text">
            <h5>Welcome back! </h5>
            <p>Log in to earn <b>5%</b> bonus money and save time on checkout. Enjoy this and that too.</p>
        </div>
        <?php wc_get_template_part( 'woocommerce/myaccount/form', 'login' );?>
        <p class="forgot" data-forgot-open>Forgot password?</p>
    </div>
    <div class="sign-up-modal">
        <h5 class="login-header">
            <button  type="button" class="button-back" data-return-to-info>
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg> Login or Sign up
            </button>

        </h5>

        <div class="login-text">
            <h5>Hello hello! </h5>
            <p>Sign in to earn 5% bonus money and save time on checkout. Enjoy this and that too.</p>
        </div>

        <?php echo do_shortcode('[TheChamp-Login]')?>
        <button  type="button" class="log-button" data-sign-up-modal-open>Sign up with email</button>

        <div class="login-text">
            <h5>Have an account? </h5>
            <p>Log in to earn 5% bonus money and save time on checkout. Enjoy this and that too.</p>
        </div>

        <button  type="button" class="log-button log-button-type-2" data-return-to-info>Log in</button>

    </div>

    <div class="sign-up-with-email-modal">
        <h5 class="login-header">
            <button  type="button" class="button-back" data-return-to-info>
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg> Sign up via email
            </button>

        </h5>
        <div class="login-text">
            <h5>Save time and earn bonus</h5>
            <p>Sign up to earn 5% bonus money and save time on checkout. Enjoy this and that too. It will be amazing, just need to write better copy.</p>
        </div>
        <?php wc_get_template_part( 'woocommerce/myaccount/form', 'login' );?>
    </div>
    <div class="recover-password-modal">
        <h5 class="login-header">
            <button  type="button" class="button-back" data-return-to-login>
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg> Recover Password
            </button>
        </h5>

        <?php echo do_shortcode('[reset_password]')?>
    </div>

    <div class="log-in-user-modal" data-<?php if ( is_user_logged_in() ) {
        echo 'registered-user';
    }?>>
        <h5 class="login-header" data-close-popup>
            <button  type="button" class="button-close">
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg>Back
            </button>
        </h5>
        <svg class="separateItem" width="182" height="57">
            <use xlink:href="#separateIcon"></use>
        </svg>
        <div class="login-text">
            <h5>Hi, <?php echo get_userdata(get_current_user_id())->display_name; ?></h5>
            <p>
                Welcome to our community. Now with every purchase you will receive a bonus Let's find something special for you!
            </p>
        </div>
        
        <a class="btn" href="<?php echo esc_url(home_url('/')); ?>">Start shopping</a>
    </div>

    <div class="reset-password-modal">
        <h5 class="login-header">
            <button  type="button" class="button-back" data-return-to-info>
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg> Reset Password
            </button>

        </h5>

        <?php echo do_shortcode('[reset_password]')?>
        <p class="sent-status-message"></p>
    </div>

    <div class="reset-pass-confirmation">
        <h5 class="login-header" data-close-confirm>
            <button  type="button" class="button-back">
                <svg width="8" height="14">
                    <use xlink:href="#chevron-left"></use>
                </svg>Recover Password
            </button>
        </h5>
        <div class="recovered-confirm">
            <svg width="80" height="80">
                <use xlink:href="#recovered"></use>
            </svg>
        </div>
        <h5>All good!</h5>
        <p>You have successfully recovered your password and can now</p>
        <button class="log-button log-button-type-2" data-close-confirm>Login</button>
    </div>

</div>
