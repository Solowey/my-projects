<?php while (have_rows('all_our_suggestions', 'option')): the_row(); ?>

    <?php $post_object = get_sub_field('similar_product'); ?>

    <?php if ($post_object): ?>

        <?php $post = $post_object;
        setup_postdata($post); ?>

        <div class="product-item col-12 col-sm-6 col-lg-4">
            <?php

            global $post;
            global $product;?>


                <div class="product-thumbnail" data-product-id="<?php echo $product->get_id(); ?>">
                    <div class="product-sale-tooltip">
                        SALE -60%
                    </div>
                    <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                        <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                            <use xlink:href="#wish-list"></use>
                        </svg>
                    </a>
                    <?php
                    $image_id = $product->get_image_id();
                    $image_url = wp_get_attachment_image_url($image_id, 'full');
                    ?>

                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo $image_url ?>" alt="">
                    </a>

                </div>


            <a class="product-link" href="<?php the_permalink(); ?>">
                <h3 class="product-title">
                    <?php the_title(); ?>
                </h3>
                <div class="product-rait-info">
                    <p class="product-price">
                        <?php woocommerce_template_loop_price(); ?>

                    </p>
                    <div class="rating">
                        <?php wc_get_template_part( 'single-product/rating' ); ?>
                    </div>
                </div>
            </a>
            <span class="add-to-cart-suggestion-product">
            Add to cart
        </span>

        </div>

        <?php wp_reset_postdata(); ?>

    <?php endif; ?>

<?php endwhile; ?>

