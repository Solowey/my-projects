<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="review-info">
        <h3 class="review-info-title">
            <?php echo get_field('title'); ?>
        </h3>

        <p class="review-text">
            <?php echo get_field('description'); ?>
        </p>

        <div class="reviewer-group">
            <span class="name"><?php echo get_field('name'); ?></span>
            <span class="raiting">
                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                    <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                        <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                            <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                </span>
            </span>
        </div>

        <div class="verified">
            <p><?php echo get_field('verified'); ?></p>
        </div>
    </div>

</article>
