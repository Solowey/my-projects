<div class="articles_item" >
    <div class="articles_img">
        <?php the_post_thumbnail(); ?>
    </div>
    <div>
        <div class="category_name"><?php the_category(' '); ?></div>
        <div class="author">
            <img class="author_img"
                 src="<?php echo get_avatar_url(get_the_author()); ?>"
                 alt="">
            <p class="author_name">
                <?php echo get_the_author(); ?>
            </p>
        </div>
        <p class="post_name">
            <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
        </p>
        <p class="post_description">
            <?php  echo mb_strimwidth(get_the_excerpt(), 0, 145, '...')?>
        </p>
    </div>
</div>
