<?php
global $post;
global $product;

?>

<div class="product-item col-12 col-sm-6 col-lg-4" data-prod-id="<?php echo $product->get_id(); ?>">
    <?php
    if ($product) : ?>
        <?php
        global $wpdb;
        $taxes = get_the_terms($product->get_id(), 'pa_size');
        $taxes2 = get_the_terms($product->get_id(), 'pa_color');

        if ($product->is_type('variable')) {
            $variations = $product->get_available_variations();

            $variation_array = [];

            foreach ($variations as $variation) {
                $meta = get_post_meta($variation['variation_id'], 'attribute_pa_color', true);
                $term = get_term_by('slug', $meta, 'pa_color')->term_id;
                $id = $variation['variation_id'];
                $table = $wpdb->get_blog_prefix() . 'termmeta';

                $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$term} AND meta_key='pa_color_yith_wccl_value'");

                $price = get_post_meta($variation['variation_id'], '_regular_price', true);
                $price_sale = get_post_meta($variation['variation_id'], '_sale_price', true);


                $item = [
                    'color_name' => $variation['attributes']['attribute_pa_color'],
                    'color_id' => $variation['variation_id'],
                    'color_code' => $color,
                    'variation_id' => $term,
                    'image_url' => $variation['image']['url']
                ];

                array_push($variation_array, $item);
            }

            ?>
            <div class="product-thumbnail-wrapper">
                <div class="product-thumbnail">

                    <?php global $product;
                    if ($product->is_on_sale())  : ?>
                        <div class="product-sale-tooltip red"> SALE
                            <?php if ($price_sale !== "") {

                                $sale = 100 - ($price_sale * 100 / $price);

                                echo round($sale, 0);
                            } ?>
                            %
                        </div>
                    <?php else : ?>

                        <?php
                        $new = get_field('tooltip_for_new_product');
                        if ($new) : ?>
                            <div class="product-new-tooltip green">
                                <?php echo "NEW"; ?>
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>

                    <?php
                    if (is_page('wish-list')) {
                        ?>
                        <button class="wish-product-link-del remove-from-wishlist"
                                data-prod-id="<?php echo $product->get_id(); ?>">
                            <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                <use xlink:href="#wish-del"></use>
                            </svg>
                        </button>
                        <?php
                    } else {
                        ?>
                        <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                            <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                <use xlink:href="#wish-list"></use>
                            </svg>
                        </a>
                        <?php
                    }
                    ?>

                    <a href="<?php the_permalink(); ?>">
                        <?php
                        $flag = 0;

                        foreach ($variation_array as $var) {

                            ?>

                            <img src="<?php echo $var['image_url']; ?>"
                                 alt="<?php the_title(); ?>"
                                 data-varid="<?php echo $var['color_id']; ?>"
                                 class="<?php if ($flag) {
                                     echo 'display-none';
                                 } ?>">
                            <?php
                            $flag = 1;
                        }
                        ?>
                    </a>
                    <?php
                    if (!is_page('wish-list')) {
                        ?>
                        <div class="product-attribute">
                            <?php $count2 = -2;
                            if ($taxes): ?>
                                <p class="attribute-size">
                                    <?php foreach ($taxes as $tax): ?>
                                        <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                        <?php $count2++; endforeach; ?>
                                </p>
                                <?php if ($count2 > 0) { ?>
                                    <span class="attribute-count">+<?php echo $count2 ?></span>
                                    <?php
                                } ?>

                            <?php endif; ?>

                            <?php if ($taxes): ?>
                                <div class="attribute-color">
                                    <?php
                                    $flag = 1;
                                    $count = -1;
                                    foreach ($variation_array as $var) {
                                        ?>
                                        <span class="attribute-color-item <?php if ($flag) {
                                            echo 'attribute-color-after';
                                        } ?>"
                                              data-itemcolor="<?php echo $var['color_name']; ?>"
                                              data-varid="<?php echo $var['color_id']; ?>"
                                              style="background-color: <?php echo $var['color_code']; ?>"

                                        ></span>
                                        <?php

                                        $flag = 0;

                                        $count++;
                                    }
                                    ?>
                                </div>
                                <?php if ($count > 0) { ?>
                                    <span class="attribute-count">+<?php echo $count ?></span>
                                    <?php
                                } ?>
                            <?php endif; ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="product-thumbnail-wrapper">
                <div class="product-thumbnail">

                    <?php global $product;
                    $price = get_post_meta(get_the_ID(), '_regular_price', true);
                    $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
                    if ($product->is_on_sale())  : ?>
                        <div class="product-sale-tooltip red"> SALE
                            <?php if ($price_sale !== "") {

                                $sale = 100 - ($price_sale * 100 / $price);

                                echo round($sale, 0);
                            } ?>
                            %
                        </div>
                    <?php else : ?>

                        <?php
                        $new = get_field('tooltip_for_new_product');
                        if ($new) : ?>
                            <div class="product-new-tooltip green">
                                <?php echo "NEW"; ?>
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>
                    <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                        <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                            <use xlink:href="#wish-list"></use>
                        </svg>
                    </a>
                    <?php
                    $image_id = $product->get_image_id();
                    $image_url = wp_get_attachment_image_url($image_id, 'full');
                    ?>

                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo $image_url ?>" alt="">
                    </a>
                    <div class="product-attribute">
                        <?php if ($taxes): ?>
                            <p class="attribute-size">
                                <?php foreach ($taxes as $tax): ?>
                                    <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                <?php endforeach; ?>
                            </p>
                        <?php endif; ?>

                        <p class="attribute-color">
                            <?php if ($taxes2): ?>
                                <?php

                                foreach ($taxes2 as $tax2):

                                    $table = $wpdb->get_blog_prefix() . 'termmeta';
                                    $id = $tax2->term_id;

                                    $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$id} AND meta_key='pa_color_yith_wccl_value'");
                                    ?>
                                    <span class="attribute-color-item "
                                          data-itemcolor="<?php echo $tax2->name; ?>"
                                          style="background-color: <?php echo $color; ?>"></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
        }

        ?>

        <a class="product-link" href="<?php the_permalink(); ?>">
            <h3 class="product-title">
                <?php the_title(); ?>
            </h3>
            <div class="product-rait-info">
                <p class="product-price">
                    <?php woocommerce_template_loop_price(); ?>

                </p>
                <div class="rating">
                    <?php wc_get_template_part('single-product/rating'); ?>
                </div>
            </div>
        </a>

        <div class="product-btns">
            <?php if ($product->get_sku()) : ?>
                <button class="btn btn--try-on" data-show="<?php echo $product->get_sku() ?>">
        <span class="try-on-icon">
            <svg width="20" height="20" viewBox="0 0 20 20">
                 <use xlink:href="#try-on"></use>
            </svg>
            <span> Try On</span>
        </span>
                </button>
            <?php endif; ?>
            <a class="btn btn--product-view" href="<?php the_permalink(); ?>">View</a>
        </div>
        <?php
        if (is_page('wish-list')) {
            ?>
            <a href="<?php the_permalink(); ?>" class="wishlist-more-details-button">More Details</a>
            <?php
        }

    endif; ?>
</div>
