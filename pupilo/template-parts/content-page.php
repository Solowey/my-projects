<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    global $post;

    if ($post->post_name === 'my-account') {
    ?>

        <div class="title-wrapper">
            <a class="my-account-link" href="/my-account">
                <svg viewBox="0 0 12 12" width="12" height="12" class="chevron-icon">
                    <use xlink:href="#chevron-left"></use>
                </svg>
                <!--            --><?php //$title = get_the_title(); ?>
                <!--            <h1 class="page-title">--><?php //echo $title; ?><!--</h1>-->
                <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
            </a>
            <button class="button-pdf display-none">PDF</button>
        </div>

    <?php
    }
    ?>

    <div class="page-content">
        <?php the_content(); ?>
    </div>

</article>
