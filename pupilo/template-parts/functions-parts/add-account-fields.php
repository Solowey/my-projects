<?php


add_action( 'woocommerce_edit_account_form', 'pupilo_add_field_edit_account_form' );

function pupilo_add_field_edit_account_form() {

    return woocommerce_form_field(
        'gender',
        array(
            'type'        => 'select',
            'class' => array('my-details-input-wrapper'),
            'required'    => true,
            'label'       => 'Gender',
            'priority' => 1,
            'placeholder' => 'Choose your gender',
            'options' => array(
//                '' => 'Choose your gender',
                1  => 'Male',
                2  => 'Female',
            ),
        ),
        get_user_meta( get_current_user_id(), 'gender', true ) // get the data
    );

}

add_action( 'woocommerce_save_account_details', 'pupilo_save_account_details' );
function pupilo_save_account_details( $user_id ) {

    update_user_meta( $user_id, 'gender', sanitize_text_field( $_POST['gender'] ) );

}

function pupilo_print_user_admin_fields() {
    $fields = pupilo_add_field_edit_account_form();
    ?>
    <h2>Additional Information</h2>
    <table class="form-table" id="iconic-additional-information">
        <tbody>
        <?php foreach ( $fields as $key => $field_args ) { ?>
            <tr>
                <th>
                    <label for="<?php echo $key; ?>"><?php echo $field_args['label']; ?></label>
                </th>
                <td>
                    <?php $field_args['label'] = false; ?>
                    <?php woocommerce_form_field( $key, $field_args ); ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
}

add_action( 'show_user_profile', 'pupilo_print_user_admin_fields', 30 );
add_action( 'edit_user_profile', 'pupilo_print_user_admin_fields', 30 );


///////////////////////////////////////////////////
///
///


add_action( 'woocommerce_edit_account_form', 'pupilo_add_field_edit_account_form2' );
function pupilo_add_field_edit_account_form2() {

    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );

    if ( !$user )
        return;

    $birthdate = get_user_meta( $user_id, 'birthdate', true );

    ?>
    <div class="my-details-input-wrapper">
        <label for="birthday">Birthday</label>
        <input type="date" id="birthday" name="birthdate" value="<?php echo esc_attr( $birthdate ); ?>" class="input-text my-details-input" />
    </div>

    <?php

}

add_action( 'woocommerce_save_account_details', 'pupilo_save_account_details2' );
function pupilo_save_account_details2( $user_id ) {
    update_user_meta( $user_id, 'birthdate', htmlentities( $_POST[ 'birthdate' ] ) );
}

function pupilo_print_user_admin_fields2() {
    $fields = pupilo_add_field_edit_account_form2();
    ?>
    <table class="form-table" id="iconic-additional-information">
        <tbody>
        <?php foreach ( $fields as $key => $field_args ) { ?>
            <tr>
                <th>
                    <label for="<?php echo $key; ?>"><?php echo $field_args['label']; ?></label>
                </th>
                <td>
                    <?php $field_args['label'] = false; ?>
                    <?php woocommerce_form_field( $key, $field_args ); ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
}
add_action( 'show_user_profile', 'pupilo_print_user_admin_fields2', 30 );
add_action( 'edit_user_profile', 'pupilo_print_user_admin_fields2', 30 );

///////////////////////////////////////////

add_action( 'woocommerce_edit_account_form', 'pupilo_add_field_edit_account_form3' );
function pupilo_add_field_edit_account_form3() {

    return woocommerce_form_field(
        'tel',
        array(
            'type'  => 'tel',
            'label' => 'Phone',
            'required' => true,
            'class' => array('my-details-input-wrapper', 'input-phone'),
            'placeholder' => 'Enter your phone number',
        ),
        get_user_meta( get_current_user_id(), 'tel', true ) // get the data
    );

}

add_action( 'woocommerce_save_account_details', 'pupilo_save_account_details3' );
function pupilo_save_account_details3( $user_id ) {
    update_user_meta( $user_id, 'tel', htmlentities( $_POST[ 'tel' ] ) );
}

function pupilo_print_user_admin_fields3() {
    $fields = pupilo_add_field_edit_account_form3();
    ?>
    <table class="form-table" id="iconic-additional-information">
        <tbody>
        <?php foreach ( $fields as $key => $field_args ) { ?>
            <tr>
                <th>
                    <label for="<?php echo $key; ?>"><?php echo $field_args['label']; ?></label>
                </th>
                <td>
                    <?php $field_args['label'] = false; ?>
                    <?php woocommerce_form_field( $key, $field_args ); ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
}
add_action( 'show_user_profile', 'pupilo_print_user_admin_fields3', 30 );
add_action( 'edit_user_profile', 'pupilo_print_user_admin_fields3', 30 );
