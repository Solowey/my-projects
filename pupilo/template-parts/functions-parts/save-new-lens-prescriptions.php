<?php

add_action('wp_ajax_save_lens_prescriptions', 'save_lens_prescriptions');
function save_lens_prescriptions() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'save_lens_prescriptions')) {
        $user_id = get_current_user_id();
        add_user_meta($user_id, 'lens_prescriptions', '', true);
        $saved_prescriptions = get_user_meta($user_id, 'lens_prescriptions', true);

        $lens_right = str_replace('-', '.', $_POST['lens-right']);
        $lens_left = str_replace('-', '.', $_POST['lens-left']);

        $new_prescription = array(
            $_POST['prescription_name'] => array(
                'lens-both' => $_POST['lens-both'],
                'lens-right' => $lens_right,
                'lens-left' => $lens_left,
            )
        );

        if ($saved_prescriptions != '') {
            array_push($saved_prescriptions, $new_prescription);
        } else {
            $saved_prescriptions = array($new_prescription);
        }

        update_user_meta($user_id, 'lens_prescriptions', $saved_prescriptions);

        echo $_POST['prescription_name'];
//        echo $saved_prescriptions;

        die;
    }
}

