<?php
$user_id = get_current_user_id();
$saved_prescriptions = get_user_meta($user_id, 'prescriptions', true);
$saved_lens = get_user_meta($user_id, 'lens_prescriptions', true);
$updated = false;
$updated_lens = false;

if (isset($_POST['delete'])) {
    $new_array = [];

    for ($i = 0; $i < count($saved_prescriptions); $i++) {
        if (key($saved_prescriptions[$i]) !== $_POST['delete']) {
            array_push($new_array, $saved_prescriptions[$i]);
        }
    }

//    $saved_prescriptions = $new_array;

    update_user_meta($user_id, 'prescriptions', $new_array, $saved_prescriptions);
}

if (isset($_POST['lens_delete'])) {
    $new_array = [];

    for ($i = 0; $i < count($saved_lens); $i++) {
        if (key($saved_lens[$i]) !== $_POST['lens_delete']) {
            array_push($new_array, $saved_lens[$i]);
        }
    }

    $saved_lens = $new_array;

    update_user_meta($user_id, 'lens_prescriptions', $saved_lens);
}

if (isset($_POST['prescription_name'])) {
    $new_prescription = array(
        $_POST['prescription_name'] => array(
            'od-sph' => $_POST['od-sph'],
            'od-cyl' => $_POST['od-cyl'],
            'os-sph' => $_POST['os-sph'],
            'os-cyl' => $_POST['os-cyl'],
            'od-axis' => $_POST['od-axis'],
            'od-ad' => $_POST['od-ad'],
            'os-axis' => $_POST['os-axis'],
            'os-ad' => $_POST['os-ad'],
            'pd-1' => $_POST['pd-1'],
            'pd-2' => $_POST['pd-2'],
            'comment' => $_POST['comment'],
            'od-vertical' => $_POST['od-vertical'],
            'od-v-basedirection' => $_POST['od-v-basedirection'],
            'os-vertical' => $_POST['os-vertical'],
            'os-v-basedirection' => $_POST['os-v-basedirection'],
            'od-horizontal' => $_POST['od-horizontal'],
            'od-h-basedirection' => $_POST['od-h-basedirection'],
            'os-horizontal' => $_POST['os-horizontal'],
            'os-h-basedirection' => $_POST['os-h-basedirection'],
        )
    );

    for ($i = 0; $i < count($saved_prescriptions); $i++) {
        if (key($saved_prescriptions[$i]) == $_POST['prescription_name']) {
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-sph'] = $_POST['od-sph'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-cyl'] = $_POST['od-cyl'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-sph'] = $_POST['os-sph'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-cyl'] = $_POST['os-cyl'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-axis'] = $_POST['od-axis'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-ad'] = $_POST['od-ad'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-axis'] = $_POST['os-axis'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-ad'] = $_POST['os-ad'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['pd-1'] = $_POST['pd-1'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['pd-2'] = $_POST['pd-2'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-vertical'] = $_POST['od-vertical'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-v-basedirection'] = $_POST['od-v-basedirection'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-vertical'] = $_POST['os-vertical'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-v-basedirection'] = $_POST['os-v-basedirection'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-horizontal'] = $_POST['od-horizontal'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['od-h-basedirection'] = $_POST['od-h-basedirection'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-horizontal'] = $_POST['os-horizontal'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['os-h-basedirection'] = $_POST['os-h-basedirection'];
            $saved_prescriptions[$i][key($saved_prescriptions[$i])]['comment'] = $_POST['comment'];
            $updated = true;
            break;
        }
    }

    if (!$saved_prescriptions) {
        update_user_meta($user_id, 'prescriptions', array($new_prescription));
    } else {
        if (!$updated) {
            array_push($saved_prescriptions, $new_prescription);
        }

        update_user_meta($user_id, 'prescriptions', $saved_prescriptions);
    }
}

if (isset($_POST['lens_name'])) {
    $left = '';

    if ($_POST['lens_both']) {
        $left = $_POST['lens_right'];
    } else {
        $left = $_POST['lens_left'];
    }

    $new_lens = array(
        $_POST['lens_name'] => array(
            'lens_both' => $_POST['lens_both'],
            'lens_right' => $_POST['lens_right'],
            'lens_left' => $left,
            'lens_comment' => $_POST['lens_comment'],
        )
    );

    for ($i = 0; $i < count($saved_lens); $i++) {
        if (key($saved_lens[$i]) == $_POST['lens_name']) {
            $saved_lens[$i][key($saved_lens[$i])]['lens_both'] = $_POST['lens_both'];
            $saved_lens[$i][key($saved_lens[$i])]['lens_right'] = $_POST['lens_right'];
            $saved_lens[$i][key($saved_lens[$i])]['lens_left'] = $_POST['lens_left'];
            $saved_lens[$i][key($saved_lens[$i])]['lens_comment'] = $_POST['lens_comment'];
            $updated_lens = true;
            break;
        }
    }

    if (!$saved_lens) {
        update_user_meta($user_id, 'lens_prescriptions', array($new_lens));
    } else {
        if (!$updated_lens) {
            array_push($saved_lens, $new_lens);
        }

        update_user_meta($user_id, 'lens_prescriptions', $saved_lens);
    }
}

if (($saved_prescriptions || $saved_lens) && !isset($_POST['lens_edit']) && !isset($_POST['edit'])) {
    ?>
    <button class="mobile-button-add-prescriptions">Add</button>
    <button class="desktop-button-add-prescriptions">+ Add new</button>
    <?php
}
?>


<div class="save-prescriptions-wrapper">
    <?php
    if ((!isset($_POST['edit']) && !isset($_POST['lens_edit'])) || isset($_POST['prescription_name'])) {
        $saved_prescriptions = get_user_meta($user_id, 'prescriptions', true);
        foreach ($saved_prescriptions as $prescription_item) {
            ?>
            <div class="saved_prescription">
                <div class="saved-first-row">
                    <div class="saved-title">
                        <p><?php echo key($prescription_item); ?></p>
                        <svg viewBox="0 0 12 12" width="12" height="12" class="open-saved-pre">
                            <use xlink:href="#chevron-right"></use>
                        </svg>
                    </div>
                    <form method="post">
                        <input type="text" value="<?php echo key($prescription_item); ?>" id="edit" name="edit"
                               class="display-none">
                        <button type="submit" class="edit-prescriptions-button">Edit</button>
                    </form>
                </div>
                <div class="saved-second-row">
                    <?php
                    $saved_pre = $prescription_item[key($prescription_item)];
                    ?>
                    <table class="prescription-table-item">
                        <tr>
                            <td></td>
                            <td>SPH</td>
                            <td>CYl</td>
                            <td>Axis</td>
                            <td>ADD</td>
                            <td>PD</td>
                        </tr>
                        <tr>
                            <td>OD<span>(R)</span></td>
                            <td><?php echo $saved_pre['od-sph']; ?></td>
                            <td><?php echo $saved_pre['od-cyl']; ?></td>
                            <td><?php echo $saved_pre['od-axis']; ?></td>
                            <td><?php echo $saved_pre['od-ad']; ?></td>
                            <td><?php echo $saved_pre['pd-1']; ?></td>
                        </tr>
                        <tr>
                            <td>OS<span>(L)</span></td>
                            <td><?php echo $saved_pre['os-sph']; ?></td>
                            <td><?php echo $saved_pre['os-cyl']; ?></td>
                            <td><?php echo $saved_pre['os-axis']; ?></td>
                            <td><?php echo $saved_pre['os-ad']; ?></td>
                            <td><?php echo $saved_pre['pd-2']; ?></td>
                        </tr>
                    </table>
                </div>
                <?php
                if ($prescription_item[key($prescription_item)]['comment']) {
                    ?>
                    <p class="saved-comment"><?php echo $prescription_item[key($prescription_item)]['comment']; ?></p>
                    <?php
                }
                ?>
            </div>
            <?php
        }

        foreach ($saved_lens as $prescription_item) {
            $saved_pre = $prescription_item[key($prescription_item)];

            ?>
            <div class="saved_prescription">
                <div class="saved-first-row">
                    <div class="saved-title">
                        <p><?php echo key($prescription_item); ?></p>
                        <svg viewBox="0 0 12 12" width="12" height="12" class="open-saved-pre">
                            <use xlink:href="#chevron-right"></use>
                        </svg>
                    </div>
                    <form method="post">
                        <input type="text" value="<?php echo key($prescription_item); ?>" id="lens_edit"
                               name="lens_edit"
                               class="display-none">
                        <button type="submit" class="edit-prescriptions-button">Edit</button>
                    </form>
                </div>
                <div class="saved-second-row">
                    <table class="prescription-table-item">
                        <tr>
                            <td></td>
                            <td>SPH</td>
                        </tr>
                        <tr>
                            <td>OD<span>(R)</span></td>
                            <td><?php echo $saved_pre['lens-right']; ?></td>
                        </tr>
                        <tr>
                            <td>OS<span>(L)</span></td>
                            <td><?php echo $saved_pre['lens-left']; ?></td>
                        </tr>
                    </table>
                </div>
                <?php
                if ($prescription_item[key($prescription_item)]['lens_comment']) {
                    ?>
                    <p class="saved-comment"><?php echo $prescription_item[key($prescription_item)]['lens_comment']; ?></p>
                    <?php
                }
                ?>
            </div>
            <?php
        }
    }
    ?>

</div>
<?php

if (isset($_POST['edit'])) {
    $prescription_item = array();

    for ($i = 0; $i < count($saved_prescriptions); $i++) {
        if (key($saved_prescriptions[$i]) == $_POST['edit']) {
            $prescription_item = $saved_prescriptions[$i];
            break;
        }
    }

    ?>
    <form method="post" class="add-prescription-form edit">
        <label for="prescription_name">Prescription name</label>
        <input type="text" id="prescription_name" name="prescription_name" placeholder="Reading, work, driving etc..."
               value="<?php echo key($prescription_item); ?>">
        <div class="wizard2">
            <div class="prescription-wrapper">
                <div class="prescription-table">
                    <div class="title-column">
                        <div class="title title-row">
                            <p>OD</p>
                            <p>Right eye</p>
                        </div>
                        <div class="title title-row">
                            <p>OS</p>
                            <p>Left eye</p>
                        </div>
                    </div>
                    <div class="content-columns">
                        <div class="title">
                            <p>SPH</p>
                            <p><span>(</span>Sphere<span>)</span></p>
                        </div>
                        <div class="title">
                            <p>CYL</p>
                            <p><span>(</span>Cylinder<span>)</span></p>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-sph" id="od-sph">
                                <?php
                                $i = -16;
                                while ($i <= 12) {
                                    ?>
                                    <option value="<?php echo number_format($i, 2); ?>"
                                        <?php if ($prescription_item[key($prescription_item)]['od-sph'] == $i) {
                                            echo 'selected';
                                        } ?>
                                    >
                                        <?php echo number_format($i, 2); ?>
                                    </option>
                                    <?php
                                    $i += 0.25;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-cyl" id="od-cyl">
                                <?php
                                $i = -6;
                                while ($i <= 6) {
                                    ?>
                                    <option value="<?php echo number_format($i, 2); ?>"
                                        <?php if ($prescription_item[key($prescription_item)]['od-cyl'] == $i) {
                                            echo 'selected';
                                        } ?>
                                    >
                                        <?php echo number_format($i, 2); ?>
                                    </option>
                                    <?php
                                    $i += 0.25;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-sph" id="os-sph">
                                <?php
                                $i = -16;
                                while ($i <= 12) {
                                    ?>
                                    <option value="<?php echo number_format($i, 2); ?>"
                                        <?php if ($prescription_item[key($prescription_item)]['os-sph'] == $i) {
                                            echo 'selected';
                                        } ?>
                                    >
                                        <?php echo number_format($i, 2); ?>
                                    </option>
                                    <?php
                                    $i += 0.25;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-cyl" id="os-cyl">
                                <?php
                                $i = -6;
                                while ($i <= 6) {
                                    ?>
                                    <option value="<?php echo number_format($i, 2); ?>"
                                        <?php if ($prescription_item[key($prescription_item)]['os-cyl'] == $i) {
                                            echo 'selected';
                                        } ?>
                                    >
                                        <?php echo number_format($i, 2); ?>
                                    </option>
                                    <?php
                                    $i += 0.25;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="prescription-table">
                    <button type="button" class="prescription-modal-button top-button" data-bs-toggle="modal" data-bs-target="#descriptionPrescription">
                        <svg viewBox="0 0 17 17" width="17" height="17" class="">
                            <use xlink:href="#modal-i"></use>
                        </svg>
                    </button>
                    <div class="title-column">
                        <div class="title title-row">
                            <p>OD</p>
                            <p>Right eye</p>
                        </div>
                        <div class="title title-row">
                            <p>OS</p>
                            <p>Left eye</p>
                        </div>
                    </div>
                    <div class="content-columns">
                        <div class="title">
                            <p>Axis</p>
                        </div>
                        <div class="title">
                            <p>ADD</p>
                        </div>
                        <div class="input-wrapper">
                            <input type="text" name="od-axis" id="od-axis" inputmode="numeric" pattern="[0-9]*"
                                   value="<?php echo $prescription_item[key($prescription_item)]['od-axis'] ?>">
                        </div>
                        <div class="select-wrapper">
                            <select name="od-ad" id="od-ad">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="+1.00" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.00
                                </option>
                                <option
                                    value="+1.25" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.25
                                </option>
                                <option
                                    value="+1.50" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.50
                                </option>
                                <option
                                    value="+1.75" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.75
                                </option>
                                <option
                                    value="+2.00" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.00
                                </option>
                                <option
                                    value="+2.25" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.25
                                </option>
                                <option
                                    value="+2.50" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.50
                                </option>
                                <option
                                    value="+2.75" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.75
                                </option>
                                <option
                                    value="+3.00" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.00
                                </option>
                                <option
                                    value="+3.25" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.25
                                </option>
                                <option
                                    value="+3.50" <?php if ($prescription_item[key($prescription_item)]['od-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.50
                                </option>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <input type="text" name="os-axis" id="os-axis" inputmode="numeric" pattern="[0-9]*"
                                   value="<?php echo $prescription_item[key($prescription_item)]['os-axis'] ?>">
                        </div>
                        <div class="select-wrapper">
                            <select name="os-ad" id="os-ad">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="+1.00" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.00
                                </option>
                                <option
                                    value="+1.25" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.25
                                </option>
                                <option
                                    value="+1.50" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.50
                                </option>
                                <option
                                    value="+1.75" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+1.75
                                </option>
                                <option
                                    value="+2.00" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.00
                                </option>
                                <option
                                    value="+2.25" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.25
                                </option>
                                <option
                                    value="+2.50" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.50
                                </option>
                                <option
                                    value="+2.75" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+2.75
                                </option>
                                <option
                                    value="+3.00" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.00
                                </option>
                                <option
                                    value="+3.25" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.25
                                </option>
                                <option
                                    value="+3.50" <?php if ($prescription_item[key($prescription_item)]['os-ad'] == $i) {
                                    echo 'selected';
                                } ?>>+3.50
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prescription-pupillary">
                <p class="pupillary-heading">Pupillary Distance</p>
                <button type="button" class="prescription-modal-button" data-bs-toggle="modal" data-bs-target="#pupillaryDistance">
                    <svg viewBox="0 0 17 17" width="17" height="17" class="">
                        <use xlink:href="#modal-i"></use>
                    </svg>
                </button>
            </div>
            <div class="pupillary-info">
                <p class="pupillary-title">PD<span class="pupillary-span">Pupillary Distance</span></p>
                <div class="pupillary-selects">
                    <div class="select-wrapper">
                        <select name="pd-1" id="pd-1">
                            <?php
                            $i = 23;
                            while ($i <= 40) {
                                ?>
                                <option value="<?php echo number_format($i, 2); ?>"
                                    <?php if ($prescription_item[key($prescription_item)]['pd-1'] == $i) {
                                        echo 'selected';
                                    } ?>
                                >
                                    <?php echo number_format($i, 2); ?>
                                </option>
                                <?php
                                $i += 0.5;
                            }
                            ?>
                        </select>
                    </div>
                    <div class="select-wrapper">
                        <select name="pd-2" id="pd-2" class="display-none">
                            <?php
                            $i = 25;
                            while ($i <= 40) {
                                ?>
                                <option value="<?php echo number_format($i, 2); ?>"
                                    <?php if ($prescription_item[key($prescription_item)]['pd-2'] == $i) {
                                        echo 'selected';
                                    } ?>
                                >
                                    <?php echo number_format($i, 2); ?>
                                </option>
                                <?php
                                $i += 0.5;
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="pupillary-checkbox">
<!--                <label>-->
<!--                    <input type="checkbox" name="two-pd" id="two-pd">-->
<!--                    Two PD Numbers-->
<!--                </label>-->
                <label class="custom-checkbox order-check-wrapper">
                    <input type="checkbox" name="two-pd" id="two-pd">
                    <span class="text">Two PD Numbers</span>
                </label>
            </div>
            <div class="prescription-comment edit-comment-display">
                <p class="comment-text">Prescription comments</p>
                <textarea name="comment" id="comment"
                          placeholder="Enter Your comments"><?php echo $prescription_item[key($prescription_item)]['comment'] ?></textarea>
            </div>
            <div class="add-prism-checkbox">
                <label class="custom-checkbox order-check-wrapper">
                    <input type="checkbox" name="add-prism" id="add-prism">
                    <span class="text">Add prism&nbsp;<b class="prism-weight">+19€</b></span>
                </label>
            </div>
            <div class="prescription-wrapper prism-wrapper display-none">
                <div class="prescription-table">
                    <div class="title-column">
                        <div class="title title-row">
                            <p>OD</p>
                            <p>Right eye</p>
                        </div>
                        <div class="title title-row">
                            <p>OS</p>
                            <p>Left eye</p>
                        </div>
                    </div>
                    <div class="content-columns">
                        <div class="title">
                            <p>Vertical △</p>
                        </div>
                        <div class="title">
                            <p>Base Direction</p>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-vertical" id="od-vertical">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="0.50" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '0.50') {
                                    echo 'selected';
                                } ?>>0.50
                                </option>
                                <option
                                    value="1.00" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '1.00') {
                                    echo 'selected';
                                } ?>>1.00
                                </option>
                                <option
                                    value="1.50" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '1.50') {
                                    echo 'selected';
                                } ?>>1.50
                                </option>
                                <option
                                    value="2.00" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '2.00') {
                                    echo 'selected';
                                } ?>>2.00
                                </option>
                                <option
                                    value="2.50" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '2.50') {
                                    echo 'selected';
                                } ?>>2.50
                                </option>
                                <option
                                    value="3.00" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '3.00') {
                                    echo 'selected';
                                } ?>>3.00
                                </option>
                                <option
                                    value="3.50" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '3.50') {
                                    echo 'selected';
                                } ?>>3.50
                                </option>
                                <option
                                    value="4.00" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '4.00') {
                                    echo 'selected';
                                } ?>>4.00
                                </option>
                                <option
                                    value="4.50" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '4.50') {
                                    echo 'selected';
                                } ?>>4.50
                                </option>
                                <option
                                    value="5.00" <?php if ($prescription_item[key($prescription_item)]['od-vertical'] == '5.00') {
                                    echo 'selected';
                                } ?>>5.00
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-v-basedirection" id="od-v-basedirection">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['od-v-basedirection'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="Up" <?php if ($prescription_item[key($prescription_item)]['od-v-basedirection'] == 'Up') {
                                    echo 'selected';
                                } ?>>Up
                                </option>
                                <option
                                    value="Down" <?php if ($prescription_item[key($prescription_item)]['od-v-basedirection'] == 'Down') {
                                    echo 'selected';
                                } ?>>Down
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-vertical" id="os-vertical">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="0.50" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '0.50') {
                                    echo 'selected';
                                } ?>>0.50
                                </option>
                                <option
                                    value="1.00" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '1.00') {
                                    echo 'selected';
                                } ?>>1.00
                                </option>
                                <option
                                    value="1.50" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '1.50') {
                                    echo 'selected';
                                } ?>>1.50
                                </option>
                                <option
                                    value="2.00" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '2.00') {
                                    echo 'selected';
                                } ?>>2.00
                                </option>
                                <option
                                    value="2.50" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '2.50') {
                                    echo 'selected';
                                } ?>>2.50
                                </option>
                                <option
                                    value="3.00" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '3.00') {
                                    echo 'selected';
                                } ?>>3.00
                                </option>
                                <option
                                    value="3.50" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '3.50') {
                                    echo 'selected';
                                } ?>>3.50
                                </option>
                                <option
                                    value="4.00" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '4.00') {
                                    echo 'selected';
                                } ?>>4.00
                                </option>
                                <option
                                    value="4.50" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '4.50') {
                                    echo 'selected';
                                } ?>>4.50
                                </option>
                                <option
                                    value="5.00" <?php if ($prescription_item[key($prescription_item)]['os-vertical'] == '5.00') {
                                    echo 'selected';
                                } ?>>5.00
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-v-basedirection" id="os-v-basedirection">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['os-v-basedirection'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="Up" <?php if ($prescription_item[key($prescription_item)]['os-v-basedirection'] == 'Up') {
                                    echo 'selected';
                                } ?>>Up
                                </option>
                                <option
                                    value="Down" <?php if ($prescription_item[key($prescription_item)]['os-v-basedirection'] == 'Down') {
                                    echo 'selected';
                                } ?>>Down
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="prescription-table">
                    <div class="title-column">
                        <div class="title title-row">
                            <p>OD</p>
                            <p>Right eye</p>
                        </div>
                        <div class="title title-row">
                            <p>OS</p>
                            <p>Left eye</p>
                        </div>
                    </div>
                    <div class="content-columns">
                        <div class="title">
                            <p>Horizontal △</p>
                        </div>
                        <div class="title">
                            <p>Base Direction</p>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-horizontal" id="od-horizontal">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="0.50" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '0.50') {
                                    echo 'selected';
                                } ?>>0.50
                                </option>
                                <option
                                    value="1.00" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '1.00') {
                                    echo 'selected';
                                } ?>>1.00
                                </option>
                                <option
                                    value="1.50" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '1.50') {
                                    echo 'selected';
                                } ?>>1.50
                                </option>
                                <option
                                    value="2.00" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '2.00') {
                                    echo 'selected';
                                } ?>>2.00
                                </option>
                                <option
                                    value="2.50" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '2.50') {
                                    echo 'selected';
                                } ?>>2.50
                                </option>
                                <option
                                    value="3.00" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '3.00') {
                                    echo 'selected';
                                } ?>>3.00
                                </option>
                                <option
                                    value="3.50" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '3.50') {
                                    echo 'selected';
                                } ?>>3.50
                                </option>
                                <option
                                    value="4.00" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '4.00') {
                                    echo 'selected';
                                } ?>>4.00
                                </option>
                                <option
                                    value="4.50" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '4.50') {
                                    echo 'selected';
                                } ?>>4.50
                                </option>
                                <option
                                    value="5.00" <?php if ($prescription_item[key($prescription_item)]['od-horizontal'] == '5.00') {
                                    echo 'selected';
                                } ?>>5.00
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="od-h-basedirection" id="od-h-basedirection">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['od-h-basedirection'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="Up" <?php if ($prescription_item[key($prescription_item)]['od-h-basedirection'] == 'Up') {
                                    echo 'selected';
                                } ?>>In
                                </option>
                                <option
                                    value="Down" <?php if ($prescription_item[key($prescription_item)]['od-h-basedirection'] == 'Down') {
                                    echo 'selected';
                                } ?>>Out
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-horizontal" id="os-horizontal">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="0.50" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '0.50') {
                                    echo 'selected';
                                } ?>>0.50
                                </option>
                                <option
                                    value="1.00" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '1.00') {
                                    echo 'selected';
                                } ?>>1.00
                                </option>
                                <option
                                    value="1.50" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '1.50') {
                                    echo 'selected';
                                } ?>>1.50
                                </option>
                                <option
                                    value="2.00" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '2.00') {
                                    echo 'selected';
                                } ?>>2.00
                                </option>
                                <option
                                    value="2.50" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '2.50') {
                                    echo 'selected';
                                } ?>>2.50
                                </option>
                                <option
                                    value="3.00" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '3.00') {
                                    echo 'selected';
                                } ?>>3.00
                                </option>
                                <option
                                    value="3.50" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '3.50') {
                                    echo 'selected';
                                } ?>>3.50
                                </option>
                                <option
                                    value="4.00" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '4.00') {
                                    echo 'selected';
                                } ?>>4.00
                                </option>
                                <option
                                    value="4.50" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '4.50') {
                                    echo 'selected';
                                } ?>>4.50
                                </option>
                                <option
                                    value="5.00" <?php if ($prescription_item[key($prescription_item)]['os-horizontal'] == '5.00') {
                                    echo 'selected';
                                } ?>>5.00
                                </option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="os-h-basedirection" id="os-h-basedirection">
                                <option
                                    value="n/a" <?php if ($prescription_item[key($prescription_item)]['os-h-basedirection'] == 'n/a') {
                                    echo 'selected';
                                } ?>>n/a
                                </option>
                                <option
                                    value="Up" <?php if ($prescription_item[key($prescription_item)]['os-h-basedirection'] == 'Up') {
                                    echo 'selected';
                                } ?>>In
                                </option>
                                <option
                                    value="Down" <?php if ($prescription_item[key($prescription_item)]['os-h-basedirection'] == 'Down') {
                                    echo 'selected';
                                } ?>>Out
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <button class="continue-button save-prescription-button save-changes" type="submit">Save changes</button>
        </div>
    </form>
    <form method="post">
        <input type="text" class="display-none" id="delete" name="delete" value="<?php echo key($prescription_item); ?>">
        <button class="prescriptions-cancel-button" type="submit">Delete</button>
    </form>
    <?php
} else if (isset($_POST['lens_edit'])) {
    $lens_item = array();

    for ($i = 0; $i < count($saved_lens); $i++) {
        if (key($saved_lens[$i]) == $_POST['lens_edit']) {
            $lens_item = $saved_lens[$i];
            break;
        }
    }

    ?>
    <form method="post" class="add-lens-form edit">
        <label for="prescription_name">Prescription name</label>
        <input type="text" id="lens_name" name="lens_name" placeholder="Reading, work, driving etc..."
               value="<?php echo key($lens_item); ?>">
        <div class="lens-prescriptions-wrapper">
            <div class="lens-prescriptions-both">
                Same prescriptions for both eyes
                <label class="switch lens-switch">
                    <input type="checkbox" name="lens_both" id="lens_both">
                    <span class="slider round"></span>
                </label>
            </div>
            <div>
                <p class="lens-text">Select your right eye(OD) prescription</p>
                <label class="lens-text lens-label">
                    Power/Sphere
                    <select name="lens_right" id="lens_right">
                        <?php
                        $i = -17;
                        while ($i <= 17) {
                            ?>
                            <option value="<?php echo number_format($i, 2); ?>"
                                <?php if ($lens_item[key($lens_item)]['lens-right'] == $i) {
                                    echo 'selected';
                                } ?>
                            >
                                <?php echo number_format($i, 2); ?>
                            </option>
                            <?php
                            $i += 0.5;
                        }
                        ?>
                    </select>
                </label>
            </div>
            <div>
                <p class="lens-text">Select your left eye(OS) prescription</p>
                <label class="lens-text lens-label">
                    Power/Sphere
                    <select name="lens_left" id="lens_left">
                        <?php
                        $i = -17;
                        while ($i <= 17) {
                            ?>
                            <option value="<?php echo number_format($i, 2); ?>"
                                <?php if ($lens_item[key($lens_item)]['lens-left'] == $i) {
                                    echo 'selected';
                                } ?>
                            >
                                <?php echo number_format($i, 2); ?>
                            </option>
                            <?php
                            $i += 0.5;
                        }
                        ?>
                    </select>
                </label>
            </div>
        </div>
        <div class="lens-add-comment">
            <p class="text">Add comments</p>
        </div>
        <div class="prescription-comment lens-comment">
            <p class="comment-text">Prescription comments</p>
            <textarea name="lens_comment" id="lens_comment"
                      placeholder="Enter Your comments"><?php echo $lens_item[key($lens_item)]['lens_comment'] ?></textarea>
        </div>
        <button class="continue-button save-prescription-button" type="submit" >Save prescription</button>
    </form>
    <form method="post">
        <input type="text" class="display-none" id="lens_delete" name="lens_delete" value="<?php echo key($lens_item); ?>">
        <button class="prescriptions-cancel-button" type="submit">Delete</button>
    </form>
    <?php
} else {
    if (!$saved_prescriptions && !$saved_lens) {
        ?>

        <div class="my-account-prescriptions-wrapper">
            <div class="prescriptions-icon-wrapper">
                <svg viewBox="0 0 80 80" width="80" height="80" class="">
                    <use xlink:href="#prescriptions-icon"></use>
                </svg>
                <p>No prescriptions have been added to this account yet.</p>
            </div>
            <button id="add-prescription" class="button-add-prescriptions">+ Add Prescriptions</button>
        </div>

        <?php

    }
    ?>
    <div class="prescriptions-tabs display-none">
        <div class="prescriptions-tabs-buttons">
            <div class="prescriptions-tab prescriptions-tab1">
                <p class="text">Eyeglasses</p>
            </div>
            <div class="prescriptions-tab not-active prescriptions-tab2">
                <p class="text">Contact Lenses</p>
            </div>
        </div>

        <form method="post" class="add-prescription-form tab1">
            <label for="prescription_name">Prescription name</label>
            <input type="text" id="prescription_name" name="prescription_name"
                   placeholder="Reading, work, driving etc...">
            <div class="wizard2">
                <div class="prescription-wrapper">
                    <div class="prescription-table">
                        <div class="title-column">
                            <div class="title title-row">
                                <p>OD</p>
                                <p>Right eye</p>
                            </div>
                            <div class="title title-row">
                                <p>OS</p>
                                <p>Left eye</p>
                            </div>
                        </div>
                        <div class="content-columns">
                            <div class="title">
                                <p>SPH</p>
                                <p>Sphere</p>
                            </div>
                            <div class="title">
                                <p>CYL</p>
                                <p>Cylinder</p>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-sph" id="od-sph">
                                    <option value="-16.00">-16.00</option>
                                    <option value="-15.75">-15.75</option>
                                    <option value="-15.50">-15.50</option>
                                    <option value="-15.25">-15.25</option>
                                    <option value="-15.00">-15.00</option>
                                    <option value="-14.75">-14.75</option>
                                    <option value="-14.50">-14.50</option>
                                    <option value="-14.25">-14.25</option>
                                    <option value="-14.00">-14.00</option>
                                    <option value="-13.75">-13.75</option>
                                    <option value="-13.50">-13.50</option>
                                    <option value="-13.25">-13.25</option>
                                    <option value="-13.00">-13.00</option>
                                    <option value="-12.75">-12.75</option>
                                    <option value="-12.50">-12.50</option>
                                    <option value="-12.25">-12.25</option>
                                    <option value="-12.00">-12.00</option>
                                    <option value="-11.75">-11.75</option>
                                    <option value="-11.50">-11.50</option>
                                    <option value="-11.25">-11.25</option>
                                    <option value="-11.00">-11.00</option>
                                    <option value="-10.75">-10.75</option>
                                    <option value="-10.50">-10.50</option>
                                    <option value="-10.25">-10.25</option>
                                    <option value="-10.00">-10.00</option>
                                    <option value="-9.75">-9.75</option>
                                    <option value="-9.50">-9.50</option>
                                    <option value="-9.25">-9.25</option>
                                    <option value="-9.00">-9.00</option>
                                    <option value="-8.75">-8.75</option>
                                    <option value="-8.50">-8.50</option>
                                    <option value="-8.25">-8.25</option>
                                    <option value="-8.00">-8.00</option>
                                    <option value="-7.75">-7.75</option>
                                    <option value="-7.50">-7.50</option>
                                    <option value="-7.25">-7.25</option>
                                    <option value="-7.00">-7.00</option>
                                    <option value="-6.75">-6.75</option>
                                    <option value="-6.50">-6.50</option>
                                    <option value="-6.25">-6.25</option>
                                    <option value="-6.00">-6.00</option>
                                    <option value="-5.75">-5.75</option>
                                    <option value="-5.50">-5.50</option>
                                    <option value="-5.25">-5.25</option>
                                    <option value="-5.00">-5.00</option>
                                    <option value="-4.75">-4.75</option>
                                    <option value="-4.50">-4.50</option>
                                    <option value="-4.25">-4.25</option>
                                    <option value="-4.00">-4.00</option>
                                    <option value="-3.75">-3.75</option>
                                    <option value="-3.50">-3.50</option>
                                    <option value="-3.25">-3.25</option>
                                    <option value="-3.00">-3.00</option>
                                    <option value="-2.75">-2.75</option>
                                    <option value="-2.50">-2.50</option>
                                    <option value="-2.25">-2.25</option>
                                    <option value="-2.00">-2.00</option>
                                    <option value="-1.75">-1.75</option>
                                    <option value="-1.50">-1.50</option>
                                    <option value="-1.25">-1.25</option>
                                    <option value="-1.00">-1.00</option>
                                    <option value="-0.75">-0.75</option>
                                    <option value="-0.50">-0.50</option>
                                    <option value="-0.25">-0.25</option>
                                    <option value="0.00" selected>0.00</option>
                                    <option value="Plano">Plano</option>
                                    <option value="0.25">0.25</option>
                                    <option value="0.50">0.50</option>
                                    <option value="0.75">0.75</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.25">1.25</option>
                                    <option value="1.50">1.50</option>
                                    <option value="1.75">1.75</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.25">2.25</option>
                                    <option value="2.50">2.50</option>
                                    <option value="2.75">2.75</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.25">3.25</option>
                                    <option value="3.50">3.50</option>
                                    <option value="3.75">3.75</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.25">4.25</option>
                                    <option value="4.50">4.50</option>
                                    <option value="4.75">4.75</option>
                                    <option value="5.00">5.00</option>
                                    <option value="5.25">5.25</option>
                                    <option value="5.50">5.50</option>
                                    <option value="5.75">5.75</option>
                                    <option value="6.00">6.00</option>
                                    <option value="6.25">6.25</option>
                                    <option value="6.50">6.50</option>
                                    <option value="6.75">6.75</option>
                                    <option value="7.00">7.00</option>
                                    <option value="7.25">7.25</option>
                                    <option value="7.50">7.50</option>
                                    <option value="7.75">7.75</option>
                                    <option value="8.00">8.00</option>
                                    <option value="8.25">8.25</option>
                                    <option value="8.50">8.50</option>
                                    <option value="8.75">8.75</option>
                                    <option value="9.00">9.00</option>
                                    <option value="9.25">9.25</option>
                                    <option value="9.50">9.50</option>
                                    <option value="9.75">9.75</option>
                                    <option value="10.00">10.00</option>
                                    <option value="10.25">10.25</option>
                                    <option value="10.50">10.50</option>
                                    <option value="10.75">10.75</option>
                                    <option value="11.00">11.00</option>
                                    <option value="11.25">11.25</option>
                                    <option value="11.50">11.50</option>
                                    <option value="11.75">11.75</option>
                                    <option value="12.00">12.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-cyl" id="od-cyl">
                                    <option value="-6.00">-6.00</option>
                                    <option value="-5.75">-5.75</option>
                                    <option value="-5.50">-5.50</option>
                                    <option value="-5.25">-5.25</option>
                                    <option value="-5.00">-5.00</option>
                                    <option value="-4.75">-4.75</option>
                                    <option value="-4.50">-4.50</option>
                                    <option value="-4.25">-4.25</option>
                                    <option value="-4.00">-4.00</option>
                                    <option value="-3.75">-3.75</option>
                                    <option value="-3.50">-3.50</option>
                                    <option value="-3.25">-3.25</option>
                                    <option value="-3.00">-3.00</option>
                                    <option value="-2.75">-2.75</option>
                                    <option value="-2.50">-2.50</option>
                                    <option value="-2.25">-2.25</option>
                                    <option value="-2.00">-2.00</option>
                                    <option value="-1.75">-1.75</option>
                                    <option value="-1.50">-1.50</option>
                                    <option value="-1.25">-1.25</option>
                                    <option value="-1.00">-1.00</option>
                                    <option value="-0.75">-0.75</option>
                                    <option value="-0.50">-0.50</option>
                                    <option value="-0.25">-0.25</option>
                                    <option value="0.00" selected>0.00</option>
                                    <option value="SPH/DS">SPH/DS</option>
                                    <option value="0.25">0.25</option>
                                    <option value="0.50">0.50</option>
                                    <option value="0.75">0.75</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.25">1.25</option>
                                    <option value="1.50">1.50</option>
                                    <option value="1.75">1.75</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.25">2.25</option>
                                    <option value="2.50">2.50</option>
                                    <option value="2.75">2.75</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.25">3.25</option>
                                    <option value="3.50">3.50</option>
                                    <option value="3.75">3.75</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.25">4.25</option>
                                    <option value="4.50">4.50</option>
                                    <option value="4.75">4.75</option>
                                    <option value="5.00">5.00</option>
                                    <option value="5.25">5.25</option>
                                    <option value="5.50">5.50</option>
                                    <option value="5.75">5.75</option>
                                    <option value="6.00">6.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-sph" id="os-sph">
                                    <option value="-16.00">-16.00</option>
                                    <option value="-15.75">-15.75</option>
                                    <option value="-15.50">-15.50</option>
                                    <option value="-15.25">-15.25</option>
                                    <option value="-15.00">-15.00</option>
                                    <option value="-14.75">-14.75</option>
                                    <option value="-14.50">-14.50</option>
                                    <option value="-14.25">-14.25</option>
                                    <option value="-14.00">-14.00</option>
                                    <option value="-13.75">-13.75</option>
                                    <option value="-13.50">-13.50</option>
                                    <option value="-13.25">-13.25</option>
                                    <option value="-13.00">-13.00</option>
                                    <option value="-12.75">-12.75</option>
                                    <option value="-12.50">-12.50</option>
                                    <option value="-12.25">-12.25</option>
                                    <option value="-12.00">-12.00</option>
                                    <option value="-11.75">-11.75</option>
                                    <option value="-11.50">-11.50</option>
                                    <option value="-11.25">-11.25</option>
                                    <option value="-11.00">-11.00</option>
                                    <option value="-10.75">-10.75</option>
                                    <option value="-10.50">-10.50</option>
                                    <option value="-10.25">-10.25</option>
                                    <option value="-10.00">-10.00</option>
                                    <option value="-9.75">-9.75</option>
                                    <option value="-9.50">-9.50</option>
                                    <option value="-9.25">-9.25</option>
                                    <option value="-9.00">-9.00</option>
                                    <option value="-8.75">-8.75</option>
                                    <option value="-8.50">-8.50</option>
                                    <option value="-8.25">-8.25</option>
                                    <option value="-8.00">-8.00</option>
                                    <option value="-7.75">-7.75</option>
                                    <option value="-7.50">-7.50</option>
                                    <option value="-7.25">-7.25</option>
                                    <option value="-7.00">-7.00</option>
                                    <option value="-6.75">-6.75</option>
                                    <option value="-6.50">-6.50</option>
                                    <option value="-6.25">-6.25</option>
                                    <option value="-6.00">-6.00</option>
                                    <option value="-5.75">-5.75</option>
                                    <option value="-5.50">-5.50</option>
                                    <option value="-5.25">-5.25</option>
                                    <option value="-5.00">-5.00</option>
                                    <option value="-4.75">-4.75</option>
                                    <option value="-4.50">-4.50</option>
                                    <option value="-4.25">-4.25</option>
                                    <option value="-4.00">-4.00</option>
                                    <option value="-3.75">-3.75</option>
                                    <option value="-3.50">-3.50</option>
                                    <option value="-3.25">-3.25</option>
                                    <option value="-3.00">-3.00</option>
                                    <option value="-2.75">-2.75</option>
                                    <option value="-2.50">-2.50</option>
                                    <option value="-2.25">-2.25</option>
                                    <option value="-2.00">-2.00</option>
                                    <option value="-1.75">-1.75</option>
                                    <option value="-1.50">-1.50</option>
                                    <option value="-1.25">-1.25</option>
                                    <option value="-1.00">-1.00</option>
                                    <option value="-0.75">-0.75</option>
                                    <option value="-0.50">-0.50</option>
                                    <option value="-0.25">-0.25</option>
                                    <option value="0.00" selected>0.00</option>
                                    <option value="Plano">Plano</option>
                                    <option value="0.25">0.25</option>
                                    <option value="0.50">0.50</option>
                                    <option value="0.75">0.75</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.25">1.25</option>
                                    <option value="1.50">1.50</option>
                                    <option value="1.75">1.75</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.25">2.25</option>
                                    <option value="2.50">2.50</option>
                                    <option value="2.75">2.75</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.25">3.25</option>
                                    <option value="3.50">3.50</option>
                                    <option value="3.75">3.75</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.25">4.25</option>
                                    <option value="4.50">4.50</option>
                                    <option value="4.75">4.75</option>
                                    <option value="5.00">5.00</option>
                                    <option value="5.25">5.25</option>
                                    <option value="5.50">5.50</option>
                                    <option value="5.75">5.75</option>
                                    <option value="6.00">6.00</option>
                                    <option value="6.25">6.25</option>
                                    <option value="6.50">6.50</option>
                                    <option value="6.75">6.75</option>
                                    <option value="7.00">7.00</option>
                                    <option value="7.25">7.25</option>
                                    <option value="7.50">7.50</option>
                                    <option value="7.75">7.75</option>
                                    <option value="8.00">8.00</option>
                                    <option value="8.25">8.25</option>
                                    <option value="8.50">8.50</option>
                                    <option value="8.75">8.75</option>
                                    <option value="9.00">9.00</option>
                                    <option value="9.25">9.25</option>
                                    <option value="9.50">9.50</option>
                                    <option value="9.75">9.75</option>
                                    <option value="10.00">10.00</option>
                                    <option value="10.25">10.25</option>
                                    <option value="10.50">10.50</option>
                                    <option value="10.75">10.75</option>
                                    <option value="11.00">11.00</option>
                                    <option value="11.25">11.25</option>
                                    <option value="11.50">11.50</option>
                                    <option value="11.75">11.75</option>
                                    <option value="12.00">12.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-cyl" id="os-cyl">
                                    <option value="-6.00">-6.00</option>
                                    <option value="-5.75">-5.75</option>
                                    <option value="-5.50">-5.50</option>
                                    <option value="-5.25">-5.25</option>
                                    <option value="-5.00">-5.00</option>
                                    <option value="-4.75">-4.75</option>
                                    <option value="-4.50">-4.50</option>
                                    <option value="-4.25">-4.25</option>
                                    <option value="-4.00">-4.00</option>
                                    <option value="-3.75">-3.75</option>
                                    <option value="-3.50">-3.50</option>
                                    <option value="-3.25">-3.25</option>
                                    <option value="-3.00">-3.00</option>
                                    <option value="-2.75">-2.75</option>
                                    <option value="-2.50">-2.50</option>
                                    <option value="-2.25">-2.25</option>
                                    <option value="-2.00">-2.00</option>
                                    <option value="-1.75">-1.75</option>
                                    <option value="-1.50">-1.50</option>
                                    <option value="-1.25">-1.25</option>
                                    <option value="-1.00">-1.00</option>
                                    <option value="-0.75">-0.75</option>
                                    <option value="-0.50">-0.50</option>
                                    <option value="-0.25">-0.25</option>
                                    <option value="0.00" selected>0.00</option>
                                    <option value="SPH/DS">SPH/DS</option>
                                    <option value="0.25">0.25</option>
                                    <option value="0.50">0.50</option>
                                    <option value="0.75">0.75</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.25">1.25</option>
                                    <option value="1.50">1.50</option>
                                    <option value="1.75">1.75</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.25">2.25</option>
                                    <option value="2.50">2.50</option>
                                    <option value="2.75">2.75</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.25">3.25</option>
                                    <option value="3.50">3.50</option>
                                    <option value="3.75">3.75</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.25">4.25</option>
                                    <option value="4.50">4.50</option>
                                    <option value="4.75">4.75</option>
                                    <option value="5.00">5.00</option>
                                    <option value="5.25">5.25</option>
                                    <option value="5.50">5.50</option>
                                    <option value="5.75">5.75</option>
                                    <option value="6.00">6.00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="prescription-table">
                        <button type="button" class="prescription-modal-button top-button" data-bs-toggle="modal" data-bs-target="#descriptionPrescription">
                            <svg viewBox="0 0 17 17" width="17" height="17" class="">
                                <use xlink:href="#modal-i"></use>
                            </svg>
                        </button>
                        <div class="title-column">
                            <div class="title title-row">
                                <p>OD</p>
                                <p>Right eye</p>
                            </div>
                            <div class="title title-row">
                                <p>OS</p>
                                <p>Left eye</p>
                            </div>
                        </div>
                        <div class="content-columns">
                            <div class="title">
                                <p>Axis</p>
                            </div>
                            <div class="title">
                                <p>ADD</p>
                            </div>
                            <div class="input-wrapper">
                                <input type="text" name="od-axis" id="od-axis" inputmode="numeric" pattern="[0-9]*">
                            </div>
                            <div class="select-wrapper">
                                <select name="od-ad" id="od-ad">
                                    <option value="n/a">n/a</option>
                                    <option value="+1.00">+1.00</option>
                                    <option value="+1.25">+1.25</option>
                                    <option value="+1.50">+1.50</option>
                                    <option value="+1.75">+1.75</option>
                                    <option value="+2.00">+2.00</option>
                                    <option value="+2.25">+2.25</option>
                                    <option value="+2.50">+2.50</option>
                                    <option value="+2.75">+2.75</option>
                                    <option value="+3.00">+3.00</option>
                                    <option value="+3.25">+3.25</option>
                                    <option value="+3.50">+3.50</option>
                                </select>
                            </div>
                            <div class="input-wrapper">
                                <input type="text" name="os-axis" id="os-axis" inputmode="numeric" pattern="[0-9]*">
                            </div>
                            <div class="select-wrapper">
                                <select name="os-ad" id="os-ad">
                                    <option value="n/a">n/a</option>
                                    <option value="+1.00">+1.00</option>
                                    <option value="+1.25">+1.25</option>
                                    <option value="+1.50">+1.50</option>
                                    <option value="+1.75">+1.75</option>
                                    <option value="+2.00">+2.00</option>
                                    <option value="+2.25">+2.25</option>
                                    <option value="+2.50">+2.50</option>
                                    <option value="+2.75">+2.75</option>
                                    <option value="+3.00">+3.00</option>
                                    <option value="+3.25">+3.25</option>
                                    <option value="+3.50">+3.50</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="prescription-pupillary">
                    <p class="pupillary-heading">Pupillary Distance</p>
                    <button class="prescription-modal-button" data-bs-toggle="modal" data-bs-target="#pupillaryDistance" type="button">
                        <svg viewBox="0 0 17 17" width="17" height="17" class="">
                            <use xlink:href="#modal-i"></use>
                        </svg>
                    </button>
                </div>
                <div class="pupillary-info">
                    <p class="pupillary-title">PD<span class="pupillary-span">Pupillary Distance</span></p>
                    <div class="pupillary-selects">
                        <div class="select-wrapper">
                            <select name="pd-1" id="pd-1">
                                <option value="Right PD">Right PD</option>
                                <option value="23.0">23.0</option>
                                <option value="23.5">23.5</option>
                                <option value="24.0">24.0</option>
                                <option value="24.5">24.5</option>
                                <option value="25.0">25.0</option>
                                <option value="25.5">25.5</option>
                                <option value="26.0">26.0</option>
                                <option value="26.5">26.5</option>
                                <option value="27.0">27.0</option>
                                <option value="27.5">27.5</option>
                                <option value="28.0">28.0</option>
                                <option value="28.5">28.5</option>
                                <option value="29.0">29.0</option>
                                <option value="29.5">29.5</option>
                                <option value="30.0">30.0</option>
                                <option value="30.5">30.5</option>
                                <option value="31.0">31.0</option>
                                <option value="31.5">31.5</option>
                                <option value="32.0">32.0</option>
                                <option value="32.5">32.5</option>
                                <option value="33.0">33.0</option>
                                <option value="33.5">33.5</option>
                                <option value="34.0">34.0</option>
                                <option value="34.5">34.5</option>
                                <option value="35.0">35.0</option>
                                <option value="35.5">35.5</option>
                                <option value="36.0">36.0</option>
                                <option value="36.5">36.5</option>
                                <option value="37.0">37.0</option>
                                <option value="37.5">37.5</option>
                                <option value="38.0">38.0</option>
                                <option value="38.5">38.5</option>
                                <option value="39.0">39.0</option>
                                <option value="39.5">39.5</option>
                                <option value="40.0">40.0</option>
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="pd-2" id="pd-2" class="display-none">
                                <option value="Left PD" selected>Left PD</option>
                                <option value="25.0">25.0</option>
                                <option value="25.5">25.5</option>
                                <option value="26.0">26.0</option>
                                <option value="26.5">26.5</option>
                                <option value="27.0">27.0</option>
                                <option value="27.5">27.5</option>
                                <option value="28.0">28.0</option>
                                <option value="28.5">28.5</option>
                                <option value="29.0">29.0</option>
                                <option value="29.5">29.5</option>
                                <option value="30.0">30.0</option>
                                <option value="30.5">30.5</option>
                                <option value="31.0">31.0</option>
                                <option value="31.5">31.5</option>
                                <option value="32.0">32.0</option>
                                <option value="32.5">32.5</option>
                                <option value="33.0">33.0</option>
                                <option value="33.5">33.5</option>
                                <option value="34.0">34.0</option>
                                <option value="34.5">34.5</option>
                                <option value="35.0">35.0</option>
                                <option value="35.5">35.5</option>
                                <option value="36.0">36.0</option>
                                <option value="36.5">36.5</option>
                                <option value="37.0">37.0</option>
                                <option value="37.5">37.5</option>
                                <option value="38.0">38.0</option>
                                <option value="38.5">38.5</option>
                                <option value="39.0">39.0</option>
                                <option value="39.5">39.5</option>
                                <option value="40.0">40.0</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="pupillary-checkbox">
<!--                    <label>-->
<!--                        <input type="checkbox" name="two-pd" id="two-pd">-->
<!--                        Two PD Numbers-->
<!--                    </label>-->
                    <label class="custom-checkbox order-check-wrapper">
                        <input type="checkbox" name="two-pd" id="two-pd">
                        <span class="text">Two PD Numbers</span>
                    </label>
                </div>
                <div class="lens-add-comment">
                    <p class="text">Add comments</p>
                </div>
                <div class="prescription-comment">
                    <p class="comment-text">Prescription comments</p>
                    <textarea name="comment" id="comment" placeholder="Enter Your comments"></textarea>
                </div>
                <div class="add-prism-checkbox">
                    <label class="custom-checkbox order-check-wrapper">
                        <input type="checkbox" name="add-prism" id="add-prism">
                        <span class="text">Add prism&nbsp;<b class="prism-weight">+19€</b></span>
                    </label>
                </div>
                <div class="prescription-wrapper prism-wrapper display-none">
                    <div class="prescription-table">
                        <div class="title-column">
                            <div class="title title-row">
                                <p>OD</p>
                                <p>Right eye</p>
                            </div>
                            <div class="title title-row">
                                <p>OS</p>
                                <p>Left eye</p>
                            </div>
                        </div>
                        <div class="content-columns">
                            <div class="title">
                                <p>Vertical △</p>
                            </div>
                            <div class="title">
                                <p>Base Direction</p>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-vertical" id="od-vertical">
                                    <option value="n/a">n/a</option>
                                    <option value="0.50">0.50</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.50">1.50</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.50">2.50</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.50">3.50</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.50">4.50</option>
                                    <option value="5.00">5.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-v-basedirection" id="od-v-basedirection">
                                    <option value="n/a">n/a</option>
                                    <option value="Up">Up</option>
                                    <option value="Down">Down</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-vertical" id="os-vertical">
                                    <option value="n/a">n/a</option>
                                    <option value="0.50">0.50</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.50">1.50</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.50">2.50</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.50">3.50</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.50">4.50</option>
                                    <option value="5.00">5.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-v-basedirection" id="os-v-basedirection">
                                    <option value="n/a">n/a</option>
                                    <option value="Up">Up</option>
                                    <option value="Down">Down</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="prescription-table">
                        <div class="title-column">
                            <div class="title title-row">
                                <p>OD</p>
                                <p>Right eye</p>
                            </div>
                            <div class="title title-row">
                                <p>OS</p>
                                <p>Left eye</p>
                            </div>
                        </div>
                        <div class="content-columns">
                            <div class="title">
                                <p>Horizontal △</p>
                            </div>
                            <div class="title">
                                <p>Base Direction</p>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-horizontal" id="od-horizontal">
                                    <option value="n/a">n/a</option>
                                    <option value="0.50">0.50</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.50">1.50</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.50">2.50</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.50">3.50</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.50">4.50</option>
                                    <option value="5.00">5.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="od-h-basedirection" id="od-h-basedirection">
                                    <option value="n/a">n/a</option>
                                    <option value="Up">In</option>
                                    <option value="Down">Out</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-horizontal" id="os-horizontal">
                                    <option value="n/a">n/a</option>
                                    <option value="0.50">0.50</option>
                                    <option value="1.00">1.00</option>
                                    <option value="1.50">1.50</option>
                                    <option value="2.00">2.00</option>
                                    <option value="2.50">2.50</option>
                                    <option value="3.00">3.00</option>
                                    <option value="3.50">3.50</option>
                                    <option value="4.00">4.00</option>
                                    <option value="4.50">4.50</option>
                                    <option value="5.00">5.00</option>
                                </select>
                            </div>
                            <div class="select-wrapper">
                                <select name="os-h-basedirection" id="os-h-basedirection">
                                    <option value="n/a">n/a</option>
                                    <option value="Up">In</option>
                                    <option value="Down">Out</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="continue-button save-prescription-button" type="submit" disabled>Save prescription
                </button>
            </div>
        </form>
        <form method="post" class="add-lens-form tab2 display-none">
            <label for="prescription_name">Prescription name</label>
            <input type="text" id="lens_name" name="lens_name" placeholder="Reading, work, driving etc..."
                   value="">
            <div class="lens-prescriptions-wrapper">
                <div class="lens-prescriptions-both">
                    Same prescriptions for both eyes
                    <label class="switch lens-switch">
                        <input type="checkbox" name="lens_both" id="lens_both">
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="right_eye_select_wrapper">
                    <p class="lens-text">Select your right eye(OD) prescription</p>
                    <label class="lens-text lens-label">
                        Power/Sphere
                        <select name="lens_right" id="lens_right">
                            <option value="-12.0">-12.0</option>
                            <option value="-11.5">-11.5</option>
                            <option value="-11.0">-11.0</option>
                            <option value="-10.5">-10.5</option>
                            <option value="-10.0">-10.0</option>
                            <option value="-9.5">-9.5</option>
                            <option value="-9.0">-9.0</option>
                            <option value="-8.5">-8.5</option>
                            <option value="-8.0">-8.0</option>
                            <option value="-7.5">-7.5</option>
                            <option value="-7.0">-7.0</option>
                            <option value="-6.5">-6.5</option>
                            <option value="-6.0">-6.0</option>
                            <option value="-5.5">-5.5</option>
                            <option value="-5.0">-5.0</option>
                            <option value="-4.5">-4.5</option>
                            <option value="-4.0">-4.0</option>
                            <option value="-3.5">-3.5</option>
                            <option value="-3.0">-3.0</option>
                            <option value="-2.5">-2.5</option>
                            <option value="-2.0">-2.0</option>
                            <option value="-1.5">-1.5</option>
                            <option value="-1.0">-1.0</option>
                            <option value="-0.5">-0.5</option>
                            <option value="0.5">0.5</option>
                            <option value="1.0">1.0</option>
                            <option value="1.5">1.5</option>
                            <option value="2.0">2.0</option>
                            <option value="2.5">2.5</option>
                            <option value="3.0">3.0</option>
                            <option value="3.5">3.5</option>
                            <option value="4.0">4.0</option>
                            <option value="4.5">4.5</option>
                            <option value="5.0">5.0</option>
                            <option value="5.5">5.5</option>
                            <option value="6.0">6.0</option>
                            <option value="6.5">6.5</option>
                            <option value="7.0">7.0</option>
                            <option value="7.5">7.5</option>
                            <option value="8.0">8.0</option>
                        </select>
                    </label>
                </div>
                <div class="left_eye_select_wrapper">
                    <p class="lens-text">Select your left eye(OS) prescription</p>
                    <label class="lens-text lens-label">
                        Power/Sphere
                        <select name="lens_left" id="lens_left">
                            <option value="-12.0">-12.0</option>
                            <option value="-11.5">-11.5</option>
                            <option value="-11.0">-11.0</option>
                            <option value="-10.5">-10.5</option>
                            <option value="-10.0">-10.0</option>
                            <option value="-9.5">-9.5</option>
                            <option value="-9.0">-9.0</option>
                            <option value="-8.5">-8.5</option>
                            <option value="-8.0">-8.0</option>
                            <option value="-7.5">-7.5</option>
                            <option value="-7.0">-7.0</option>
                            <option value="-6.5">-6.5</option>
                            <option value="-6.0">-6.0</option>
                            <option value="-5.5">-5.5</option>
                            <option value="-5.0">-5.0</option>
                            <option value="-4.5">-4.5</option>
                            <option value="-4.0">-4.0</option>
                            <option value="-3.5">-3.5</option>
                            <option value="-3.0">-3.0</option>
                            <option value="-2.5">-2.5</option>
                            <option value="-2.0">-2.0</option>
                            <option value="-1.5">-1.5</option>
                            <option value="-1.0">-1.0</option>
                            <option value="-0.5">-0.5</option>
                            <option value="0.5">0.5</option>
                            <option value="1.0">1.0</option>
                            <option value="1.5">1.5</option>
                            <option value="2.0">2.0</option>
                            <option value="2.5">2.5</option>
                            <option value="3.0">3.0</option>
                            <option value="3.5">3.5</option>
                            <option value="4.0">4.0</option>
                            <option value="4.5">4.5</option>
                            <option value="5.0">5.0</option>
                            <option value="5.5">5.5</option>
                            <option value="6.0">6.0</option>
                            <option value="6.5">6.5</option>
                            <option value="7.0">7.0</option>
                            <option value="7.5">7.5</option>
                            <option value="8.0">8.0</option>
                        </select>
                    </label>
                </div>
            </div>
            <div class="lens-add-comment">
                <p class="text">Add comments</p>
            </div>
            <div class="prescription-comment lens-comment">
                <p class="comment-text">Prescription comments</p>
                <textarea name="lens_comment" id="lens_comment" placeholder="Enter Your comments"></textarea>
            </div>
            <button class="continue-button save-prescription-button" type="submit" disabled>Save prescription</button>
        </form>
        <form method="post">
            <button class="prescriptions-cancel-button" type="submit">Cancel</button>
        </form>
    </div>

    <?php
}
?>

<div class="modal fade" id="pupillaryDistance" tabindex="-1" aria-labelledby="pupillaryDistanceLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                This text will be added from admin panel!!!
                <br>
                <br>

                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                Why do we use it?
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                Where does it come from?
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

                Where can I get some?
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modal-button" data-bs-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="descriptionPrescription" tabindex="-1" aria-labelledby="descriptionPrescriptionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <h5 class="modal-title" id="descriptionPrescriptionLabel">Modal title</h5>-->
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                This text will be added from admin panel!!!111
                <br>
                <br>

                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                Why do we use it?
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                Where does it come from?
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

                Where can I get some?
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modal-button" data-bs-dismiss="modal">Ok</button>
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
