<?php

add_action( 'wp_ajax_contact_lenses_delete_from_cart', 'contact_lenses_delete_from_cart' );
add_action( 'wp_ajax_nopriv_contact_lenses_delete_from_cart', 'contact_lenses_delete_from_cart' );

function contact_lenses_delete_from_cart() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'contact_lenses_delete_from_cart')) {
        WC()->cart->remove_cart_item($_POST['keyRight']);
        WC()->cart->remove_cart_item($_POST['keyLeft']);

        echo 'deleted';
        die;
    }
}

add_action( 'wp_ajax_contact_lenses_plus_minus', 'contact_lenses_plus_minus' );
add_action( 'wp_ajax_nopriv_contact_lenses_plus_minus', 'contact_lenses_plus_minus' );

function contact_lenses_plus_minus() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'contact_lenses_plus_minus')) {

        if ($_POST['sign'] === 'minus') {
            WC()->cart->set_quantity($_POST['keyRight'], $_POST['currentQty'] - $_POST['qty']);
            WC()->cart->set_quantity($_POST['keyLeft'], $_POST['currentQty'] - $_POST['qty']);
        } else {
            WC()->cart->set_quantity($_POST['keyRight'], $_POST['currentQty'] + $_POST['qty']);
            WC()->cart->set_quantity($_POST['keyLeft'], $_POST['currentQty'] + $_POST['qty']);
        }

        echo $_POST['sign'];
        die;
    }
}
