<?php
//Custom fields for Wizard - start

add_action('woocommerce_before_add_to_cart_button','pupilo_add_custom_fields_to_product');
function pupilo_add_custom_fields_to_product()
{
    global $product;

    ob_start();

    ?>
    <div class="product-custom-fields">
        <textarea name="lenses_configuration"></textarea>
        <textarea name="prescriptions"></textarea>
        <input type="text" name="additional_price">
        <input name="lens_side">
        <input name="lenses_token">
    </div>
    <div style="border: 1px solid green;" class="display-none">
        <label>
            Both eyes
            <input type="checkbox" name="both-eyes" id="both-eyes">
        </label>
        <select name="right-lens" id="right-lens"></select>
        <select name="left-lens" id="left-lens" disabled></select>
        <label>
            <input type="radio" name="quantity-lenses" value="3" id="val-3">
            3 month
        </label>
        <label>
            <input type="radio" name="quantity-lenses" value="6" id="val-6">
            6 month
        </label>
        <label>
            <input type="radio" name="quantity-lenses" value="12" id="val-12">
            12 month
        </label>
<!--        <button type="button" class="add-contact-lenses-to-cart">Add to cart</button>-->
    </div>
    <div class="clear"></div>

    <?php

    $content = ob_get_contents();
    ob_end_flush();

    return $content;
}

add_filter('woocommerce_add_cart_item_data','pupilo_add_item_data',10,3);
function pupilo_add_item_data($cart_item_data, $product_id, $variation_id)
{
    if(isset($_REQUEST['lenses_configuration']))
    {
        $cart_item_data['lenses_configuration'] = sanitize_text_field($_REQUEST['lenses_configuration']);
    }

    if(isset($_REQUEST['prescriptions']))
    {
        $cart_item_data['prescriptions'] = sanitize_text_field($_REQUEST['prescriptions']);
    }

    if(isset($_REQUEST['additional_price']))
    {
        $cart_item_data['additional_price'] = sanitize_text_field($_REQUEST['additional_price']);
    }

    if(isset($_REQUEST['lenses_token']))
    {
        $cart_item_data['lenses_token'] = sanitize_text_field($_REQUEST['lenses_token']);
    }

    if(isset($_REQUEST['lens_side']))
    {
        $cart_item_data['lens_side'] = sanitize_text_field($_REQUEST['lens_side']);
    }

    return $cart_item_data;
}

add_filter('woocommerce_get_item_data','pupilo_add_item_meta',10,2);
add_filter('woocommerce_get_item_data','pupilo_add_item_meta2',10,2);
add_filter('woocommerce_get_item_data','pupilo_add_item_meta3',10,2);
add_filter('woocommerce_get_item_data','pupilo_add_item_meta4',10,2);
add_filter('woocommerce_get_item_data','pupilo_add_item_meta5',10,2);

function pupilo_add_item_meta($item_data, $cart_item)
{
    if(array_key_exists('lenses_configuration', $cart_item))
    {
        $custom_details = $cart_item['lenses_configuration'];

        $item_data[] = array(
            'key'   => 'Lenses configuration',
            'value' => $custom_details
        );
    }

    return $item_data;
}

function pupilo_add_item_meta2($item_data, $cart_item)
{
    if(array_key_exists('additional_price', $cart_item))
    {
        $custom_details = $cart_item['additional_price'];

        $item_data[] = array(
            'key'   => 'Additional price',
            'value' => $custom_details
        );
    }

    return $item_data;
}

function pupilo_add_item_meta3($item_data, $cart_item)
{
    if(array_key_exists('prescriptions', $cart_item))
    {
        $custom_details = $cart_item['prescriptions'];

        $item_data[] = array(
            'key'   => 'Prescriptions',
            'value' => $custom_details
        );
    }

    return $item_data;
}

function pupilo_add_item_meta4($item_data, $cart_item)
{
    if(array_key_exists('lenses_token', $cart_item))
    {
        $custom_details = $cart_item['lenses_token'];

        $item_data[] = array(
            'key'   => 'Lenses token',
            'value' => $custom_details
        );
    }

    return $item_data;
}

function pupilo_add_item_meta5($item_data, $cart_item)
{
    if(array_key_exists('lens_side', $cart_item))
    {
        $custom_details = $cart_item['lens_side'];

        $item_data[] = array(
            'key'   => 'Lens side',
            'value' => $custom_details
        );
    }

    return $item_data;
}

add_action( 'woocommerce_checkout_create_order_line_item', 'pupilo_add_custom_order_line_item_meta',10,4 );
function pupilo_add_custom_order_line_item_meta($item, $cart_item_key, $values, $order)
{
    if(array_key_exists('lenses_configuration', $values))
    {
        $item->add_meta_data('Lenses configuration',$values['lenses_configuration']);
    }

    if(array_key_exists('prescriptions', $values))
    {
        $item->add_meta_data('Prescriptions',$values['prescriptions']);
    }

    if(array_key_exists('additional_price', $values))
    {
        $item->add_meta_data('Additional price',$values['additional_price']);
    }

    if(array_key_exists('lenses_token', $values))
    {
        $item->add_meta_data('Lenses token',$values['lenses_token']);
    }

    if(array_key_exists('lens_side', $values))
    {
        $item->add_meta_data('Lens side',$values['lens_side']);
    }
}

add_action( 'woocommerce_before_calculate_totals', 'calculate_additional_price', 10, 1 );
function calculate_additional_price( $cart ) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;

    foreach ( $cart->get_cart() as $cart_item ) {
        $added_price = number_format( floatval($cart_item['additional_price']), 2 );
        $product = $cart_item['data'];
        $product_price = method_exists( $product, 'get_price' ) ? floatval($product->get_price()) : floatval($product->price);
        $new_price = $product_price + $added_price;
        method_exists( $product, 'set_price' ) ? $product->set_price( $new_price ) : $product->price = $new_price;
    }
}

//Custom fields for Wizard - finish
