<?php

?>

<div class="linked-shortcode-wrapper">

    <?php echo do_shortcode('[TheChamp-Social-Linking]'); ?>

</div>
<div class="linked-visual-wrapper">
    <div class="linked-item-wrapper">
        <svg viewBox="0 0 64 64" width="64" height="64" class="">
            <use xlink:href="#facebook-account"></use>
        </svg>
        <div class="text-wrapper">
            <p class="heading">Facebook</p>
            <p class="text facebook-text">Connect your Facebook account</p>
        </div>
        <label class="facebook-label switch">
            <input id="facebook-checkbox" type="checkbox">
            <span class="slider round"></span>
        </label>
    </div>
    <div class="linked-item-wrapper">
        <svg viewBox="0 0 64 64" width="64" height="64" class="">
            <use xlink:href="#google-account"></use>
        </svg>
        <div class="text-wrapper">
            <p class="heading">Google</p>
            <p class="text google-text">Connect your Google account</p>
        </div>
        <label class="google-label switch">
            <input id="google-checkbox" type="checkbox">
            <span class="slider round"></span>
        </label>
    </div>
</div>

