<?php

add_action('wp_ajax_save_prescriptions', 'save_prescriptions');
function save_prescriptions() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'save_prescriptions')) {
        $user_id = get_current_user_id();
        add_user_meta($user_id, 'prescriptions', '', true);
        $saved_prescriptions = get_user_meta($user_id, 'prescriptions', true);

        $is_new = true;

        for ($i = 0; $i < count($saved_prescriptions); $i++) {
            if (key($saved_prescriptions[$i]) === $_POST['prescription_name']) {
                $is_new = false;
            }
        }

//        if ($is_new) {
//            $new_prescription = array(
//                $_POST['prescription_name'] => array(
//                    'od-sph' => $_POST['od-sph'],
//                    'od-cyl' => $_POST['od-cyl'],
//                    'os-sph' => $_POST['os-sph'],
//                    'os-cyl' => $_POST['os-cyl'],
//                    'od-axis' => $_POST['od-axis'],
//                    'od-ad' => $_POST['od-ad'],
//                    'os-axis' => $_POST['os-axis'],
//                    'os-ad' => $_POST['os-ad'],
//                    'pd-1' => $_POST['pd-1'],
//                    'pd-2' => $_POST['pd-2'],
//                    'comment' => $_POST['comment'],
//                    'od-vertical' => $_POST['od-vertical'],
//                    'od-v-basedirection' => $_POST['od-v-basedirection'],
//                    'os-vertical' => $_POST['os-vertical'],
//                    'os-v-basedirection' => $_POST['os-v-basedirection'],
//                    'od-horizontal' => $_POST['od-horizontal'],
//                    'od-h-basedirection' => $_POST['od-h-basedirection'],
//                    'os-horizontal' => $_POST['os-horizontal'],
//                    'os-h-basedirection' => $_POST['os-h-basedirection'],
//                )
//            );
//
//            if ($saved_prescriptions != '') {
//                array_push($saved_prescriptions, $new_prescription);
//            } else {
//                $saved_prescriptions = array($new_prescription);
//            }
//
//            array_push($saved_prescriptions, $new_prescription);
//
//            update_user_meta($user_id, 'prescriptions', $saved_prescriptions);
//        }

        echo $_POST['od-sph'];
        die;
    }
}
