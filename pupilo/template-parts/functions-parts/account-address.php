<?php

add_filter( 'woocommerce_shipping_fields', 'pupilo_remove_company_fields' );
function pupilo_remove_company_fields( $fields ) {

    unset( $fields[ 'shipping_company' ] );
    return $fields;
}

add_filter( 'woocommerce_shipping_fields', 'pupilo_remove_address2_fields' );
function pupilo_remove_address2_fields( $fields ) {

    unset( $fields[ 'shipping_address_2' ] );
    return $fields;
}

//add_filter( 'woocommerce_shipping_fields', 'pupilo_remove_city_fields' );
//function pupilo_remove_city_fields( $fields ) {
//
//    unset( $fields[ 'shipping_city' ] );
//    return $fields;
//}

add_filter( 'woocommerce_default_address_fields' , 'pupilo_change_address_field' );
function pupilo_change_address_field( $fields ) {

    $fields['address_1']['label'] = 'Address';
    $fields['address_1']['placeholder'] = 'Enter address';

    return $fields;
}

add_filter( 'woocommerce_default_address_fields' , 'pupilo_change_address_state' );
function pupilo_change_address_state( $fields ) {

    $fields['state']['priority'] = 50;

    return $fields;
}

add_filter( 'woocommerce_default_address_fields' , 'pupilo_change_address_city' );
function pupilo_change_address_city( $fields ) {

    $fields['city']['priority'] = 40;

    return $fields;
}

add_filter( 'woocommerce_default_address_fields', 'pupilo_add_address_email' );
function pupilo_add_address_email( $fields ) {

    $fields['fav_color']   = array(
        'label'        => 'Email',
        'required'     => false,
        'class'        => array( 'form-row-wide', 'my-custom-class' ),
        'priority'     => 1,
        'placeholder'  => 'Enter email',
    );

    return $fields;
}

add_filter( 'woocommerce_default_address_fields', 'pupilo_add_address_city' );
function pupilo_add_address_city( $fields ) {

    $fields['city']   = array(
        'label'        => 'City',
        'required'     => false,
        'class'        => array( 'form-row-wide', 'my-custom-class' ),
        'priority'     => 40,
        'placeholder'  => 'Enter city',
    );

    return $fields;
}

add_filter( 'woocommerce_default_address_fields', 'pupilo_add_address_phone' );
function pupilo_add_address_phone( $fields ) {

    $fields['phone']   = array(
        'label'        => 'Phone',
        'required'     => false,
        'class'        => array( 'form-row-wide', 'my-custom-class' ),
        'priority'     => 20,
        'placeholder'  => 'Enter contact phone',
    );

    return $fields;
}

add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1
);
function wc_npr_filter_phone( $address_fields ) {
    $address_fields['billing_phone']['required'] = false;
    $address_fields['billing_country']['required'] = false;
    $address_fields['billing_last_name']['required'] = false;
    $address_fields['billing_city']['required'] = false;
    $address_fields['billing_postcode']['required'] = false;
    $address_fields['billing_email']['required'] = false;
    $address_fields['billing_state']['required'] = false;
    $address_fields['billing_address_1']['required'] = false;
    $address_fields['billing_address_2']['required'] = false;
    return $address_fields;

}

//add_filter( 'woocommerce_default_address_fields', 'pupilo_add_address_state' );
//function pupilo_add_address_state( $fields ) {
//
//    $fields['province']   = array(
//        'label'        => 'State',
//        'required'     => false,
//        'class'        => array( 'form-row-wide', 'my-custom-class' ),
//        'priority'     => 20,
//        'placeholder'  => 'State',
//    );
//
//    return $fields;
//}
