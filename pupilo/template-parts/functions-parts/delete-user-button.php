<?php

add_action( 'woocommerce_after_my_account', 'woo_delete_account_button' );
function woo_delete_account_button() {
    ?>
    <a href="<?php echo add_query_arg( 'wc-api', 'wc-delete-account', home_url( '/' ) ) ?>" class="close-account-button">Close Account</a>
    <?php
}

add_action( 'woocommerce_api_' . strtolower( 'wc-delete-account' ), 'woo_handle_account_delete' );
function woo_handle_account_delete() {
    // we do not want the admin to delete their account
    // advised to add more checks here to ensure you delete the correct account.
    if ( ! is_admin() ) {
//        require('./wp-admin/includes/user.php');
//        wp_delete_user(get_current_user_id());
//        wp_redirect( home_url() ); die();

        wp_delete_user( get_current_user_id() );
        wp_redirect( home_url() ); die();
    }
}
