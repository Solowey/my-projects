<?php
function ba_ajax_search_load_more()
{
    $next_page = $_POST['currentPage'] + 1;
    $product_class = $_POST['productClass'];
    $args = array(
        's' => $_POST['term'],
        'posts_per_page' => 32,
        'paged' => $next_page,
        'post_type' => 'product',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) : ?>

        <?php while ($query->have_posts()) : ?>

            <?php $query->the_post() ?>

            <div class="product-item col-sm-6 <?php echo $product_class ?>">
                <?php

                global $post;
                global $product;
                if ($product) : ?>
                    <?php
                    global $wpdb;
                    $taxes = get_the_terms($product->get_id(), 'pa_size');
                    $taxes2 = get_the_terms($product->get_id(), 'pa_color');

                    if ($product->is_type('variable')) {
                        $variations = $product->get_available_variations();

                        $variation_array = [];

                        foreach ($variations as $variation) {
                            $meta = get_post_meta($variation['variation_id'], 'attribute_pa_color', true);
                            $term = get_term_by('slug', $meta, 'pa_color')->term_id;
                            $id = $variation['variation_id'];
                            $table = $wpdb->get_blog_prefix() . 'termmeta';

                            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$term} AND meta_key='pa_color_yith_wccl_value'");

                            $item = [
                                'color_name' => $variation['attributes']['attribute_pa_color'],
                                'color_id' => $variation['variation_id'],
                                'color_code' => $color,
                                'variation_id' => $term,
                                'image_url' => $variation['image']['url']
                            ];

                            array_push($variation_array, $item);
                        }

                        ?>
                        <div class="product-thumbnail">
                            <div class="product-sale-tooltip">
                                SALE -60%
                            </div>
                            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                    <use xlink:href="#wish-list"></use>
                                </svg>
                            </a>
                            <a href="<?php the_permalink(); ?>">
                                <?php
                                $flag = 0;

                                foreach ($variation_array as $var) {

                                    ?>

                                    <img src="<?php echo $var['image_url']; ?>"
                                         alt="<?php the_title(); ?>"
                                         data-varid="<?php echo $var['color_id']; ?>"
                                         class="<?php if ($flag) {
                                             echo 'display-none';
                                         } ?>">
                                    <?php
                                    $flag = 1;
                                }
                                ?>
                            </a>
                            <div class="product-attribute">
                                <?php if ($taxes): ?>
                                    <p class="attribute-size">
                                        <?php foreach ($taxes as $tax): ?>
                                            <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                        <?php endforeach; ?>
                                    </p>
                                <?php endif; ?>

                                <?php if ($taxes): ?>
                                    <div class="attribute-color">
                                        <?php
                                        $flag = 1;

                                        foreach ($variation_array as $var) {
                                            ?>
                                            <span class="attribute-color-item <?php if ($flag) {
                                                echo 'attribute-color-after';
                                            } ?>"
                                                  data-itemcolor="<?php echo $var['color_name']; ?>"
                                                  data-varid="<?php echo $var['color_id']; ?>"
                                                  style="background-color: <?php echo $var['color_code']; ?>"></span>
                                            <?php

                                            $flag = 0;
                                        }
                                        ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php
                    } else {
                        ?>
                        <div class="product-thumbnail">
                            <div class="product-sale-tooltip">
                                SALE -60%
                            </div>
                            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                    <use xlink:href="#wish-list"></use>
                                </svg>
                            </a>
                            <?php
                            $image_id  = $product->get_image_id();
                            $image_url = wp_get_attachment_image_url( $image_id, 'full' );
                            ?>

                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php echo $image_url?>" alt="">
                            </a>
                            <div class="product-attribute">
                                <?php if ($taxes): ?>
                                    <p class="attribute-size">
                                        <?php foreach ($taxes as $tax): ?>
                                            <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                        <?php endforeach; ?>
                                    </p>
                                <?php endif; ?>

                                <p class="attribute-color">
                                    <?php if ($taxes2): ?>
                                        <?php

                                        foreach ($taxes2 as $tax2):

                                            $table = $wpdb->get_blog_prefix() . 'termmeta';
                                            $id = $tax2->term_id;

                                            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$id} AND meta_key='pa_color_yith_wccl_value'");
                                            ?>
                                            <span class="attribute-color-item "
                                                  data-itemcolor="<?php echo $tax2->name; ?>"
                                                  style="background-color: <?php echo $color; ?>"></span>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>

                        <?php
                    }

                    ?>

                    <a class="product-link" href="<?php the_permalink(); ?>">
                        <h3 class="product-title">
                            <t><?php the_title(); ?></t>
                        </h3>
                        <div class="product-rait-info">
                            <p class="product-price">
                                <?php woocommerce_template_loop_price(); ?>

                            </p>
                            <div class="rating">
                                <?php wc_get_template_part( 'single-product/rating' ); ?>
                            </div>
                        </div>
                    </a>

                    <div class="product-btns">
                        <?php if ($product->get_sku()) : ?>
                            <button class="btn btn--try-on" data-show="<?php echo $product->get_sku()?>">
            <span class="try-on-icon">
                <svg width="20" height="20" viewBox="0 0 20 20">
                     <use xlink:href="#try-on"></use>
                </svg>
                <span> Try On</span>
            </span>
                            </button>
                        <?php endif; ?>
                        <a class="btn btn--product-view" href="<?php the_permalink();?>">View</a>
                    </div>
                <?php endif;?>
            </div>


        <?php endwhile;?>

    <?php endif;

    wp_reset_query();
    die();
}

add_action('wp_ajax_ba_ajax_search_load_more', 'ba_ajax_search_load_more');
add_action('wp_ajax_nopriv_ba_ajax_search_load_more', 'ba_ajax_search_load_more');

function ba_ajax_search(){
    $next_page = $_POST['current_page'] + 1;
    $args = array(
        's' => $_POST['term'],
        'posts_per_page' => 4,
        'post_type' => 'product',
        'paged' => $next_page,
    );

    global $wp;
    $the_query = new WP_Query($args);
    $allProducts = $the_query->found_posts;

    if ($the_query->have_posts()) : ?>
    <div class="wrapper">
        <a class="search-link" href="<?php echo esc_url(home_url('/search') . '/?search_text=' . $_POST['term'] ); ?>">
            <span>
            Products
            <span class="product-count">(<?php echo $allProducts ?>)</span>
            </span>
            <span>

                <svg width="21" height="17" class="product-link-arrow">
                    <use xlink:href="#arrow"></use>
                </svg>
            </span>
        </a>
        <div class="product-ajax-grid">

        <?php while ($the_query->have_posts()) : ?>

            <?php $the_query->the_post() ?>

            <div class="result_item clear col-6" data-all-posts="<?php echo $allProducts ?>">
                <?php

                global $post;
                global $product;

                if ($product) : ?>

                    <?php
                    global $wpdb;
                    $taxes = get_the_terms($product->get_id(), 'pa_size');
                    $taxes2 = get_the_terms($product->get_id(), 'pa_color');

                    if ($product->is_type('variable')) {
                        $variations = $product->get_available_variations();

                        $variation_array = [];

                        foreach ($variations as $variation) {
                            $meta = get_post_meta($variation['variation_id'], 'attribute_pa_color', true);
                            $term = get_term_by('slug', $meta, 'pa_color')->term_id;
                            $id = $variation['variation_id'];
                            $table = $wpdb->get_blog_prefix() . 'termmeta';

                            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$term} AND meta_key='pa_color_yith_wccl_value'");

                            $item = [
                                'color_name' => $variation['attributes']['attribute_pa_color'],
                                'color_id' => $variation['variation_id'],
                                'color_code' => $color,
                                'variation_id' => $term,
                                'image_url' => $variation['image']['url']
                            ];

                            array_push($variation_array, $item);
                        }

                        ?>
                        <div class="product-thumbnail">

                            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                    <use xlink:href="#wish-list"></use>
                                </svg>
                            </a>
                            <a href="<?php the_permalink(); ?>">
                                <?php
                                $flag = 0;

                                foreach ($variation_array as $var) {

                                    ?>

                                    <img src="<?php echo $var['image_url']; ?>"
                                         alt="<?php the_title(); ?>"
                                         data-varid="<?php echo $var['color_id']; ?>"
                                         class="<?php if ($flag) {
                                             echo 'display-none';
                                         } ?>">
                                    <?php
                                    $flag = 1;
                                }
                                ?>
                            </a>
                            <div class="product-attribute">
                                <?php $count2 = -2; if ($taxes): ?>
                                    <p class="attribute-size">
                                        <?php foreach ($taxes as $tax): ?>
                                            <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                            <?php $count2++; endforeach;  ?>
                                    </p><?php if ($count2 > 0 ){?>
                                        <span class="attribute-count">+<?php echo $count2?></span>
                                        <?php
                                    }?>
                                <?php endif; ?>

                                <?php if ($taxes): ?>
                                    <div class="attribute-color">
                                        <?php
                                        $flag = 1;
                                        $count = -1;
                                        foreach ($variation_array as $var) {
                                            ?>
                                            <span class="attribute-color-item <?php if ($flag) {
                                                echo 'attribute-color-after';
                                            } ?>"
                                                  data-itemcolor="<?php echo $var['color_name']; ?>"
                                                  data-varid="<?php echo $var['color_id']; ?>"
                                                  style="background-color: <?php echo $var['color_code']; ?>"

                                            ></span>
                                            <?php

                                            $flag = 0;

                                            $count++;
                                        }
                                        ?>
                                    </div>
                                    <?php if ($count > 0 ){?>
                                        <span class="attribute-count">+<?php echo $count ?></span>
                                        <?php
                                    }?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php
                    } else {
                        ?>
                        <div class="product-thumbnail">

                            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                    <use xlink:href="#wish-list"></use>
                                </svg>
                            </a>
                            <?php
                            $image_id  = $product->get_image_id();
                            $image_url = wp_get_attachment_image_url( $image_id, 'full' );
                            ?>

                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php echo $image_url?>" alt="">
                            </a>
                            <div class="product-attribute">
                                <?php if ($taxes): ?>
                                    <p class="attribute-size">
                                        <?php foreach ($taxes as $tax): ?>
                                            <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                        <?php endforeach; ?>
                                    </p>
                                <?php endif; ?>

                                <p class="attribute-color">
                                    <?php if ($taxes2): ?>
                                        <?php

                                        foreach ($taxes2 as $tax2):

                                            $table = $wpdb->get_blog_prefix() . 'termmeta';
                                            $id = $tax2->term_id;

                                            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$id} AND meta_key='pa_color_yith_wccl_value'");
                                            ?>
                                            <span class="attribute-color-item "
                                                  data-itemcolor="<?php echo $tax2->name; ?>"
                                                  style="background-color: <?php echo $color; ?>"></span>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>

                        <?php
                    }

                    ?>

                    <a class="product-link" href="<?php the_permalink(); ?>">
                        <h3 class="product-title">
                            <t><?php the_title(); ?></t>
                        </h3>
                        <div class="product-rait-info">
                            <p class="product-price">
                                <?php woocommerce_template_loop_price(); ?>

                            </p>
                            <div class="rating">
                                <?php wc_get_template_part( 'single-product/rating' ); ?>
                            </div>
                        </div>
                    </a>

                <?php endif;?>
            </div>

        <?php endwhile;?>

        <a class="blog-link product-link-load-more" href="<?php echo esc_url(home_url('/search') ); ?>">Load  more</a>
    <?php else : ?>
        <div class="post-ajax-grid flex flex-column">
            <div class="non-result">
                <span class="not_found">Unfortunately, nothing was found for your query.</span>
            </div>
        </div>
        </div>
    <?php endif;?>

    </div>
    <?php
    wp_reset_query();

    $args2 = array(
        's' => $_POST['term'],
        'posts_per_page' => 2,
        'post_type' => 'post',
        'ignore_sticky_posts' => true,
    );

    $the_query2 = new WP_Query($args2);
    $allProducts = $the_query2->found_posts;
    if ($the_query2->have_posts()) : ?>
        <a class="search-link" href="<?php echo esc_url(home_url('/blog') ); ?>">
            <span>
            Articles
            <span class="product-count">(<?php echo $allProducts ?>)</span>
                </span>
                <span>
                <svg width="21" height="17" class="article-link-arrow">
                    <use xlink:href="#arrow"></use>
                </svg>
            </span>
        </a>
        <div class="post-ajax-grid">

        <?php while ($the_query2->have_posts()) : ?>

            <?php $the_query2->the_post() ?>
            <div class="result_item clear col-6" >

                <?php if(has_post_thumbnail()) : ?>
                <div class="post-image">
                    <?php the_post_thumbnail('full'); ?>
                </div>
                    <a class="title-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <p class="category_subtitle"><?php echo mb_strimwidth(get_the_excerpt(), 0, 75, '...') ?> </p>

                <?php endif;?>
            </div>

        <?php endwhile;?>
            <a class="blog-link" href="<?php echo esc_url(home_url('/blog') ); ?>">View more articles</a>
        </div>

    <?php endif;?>
    </div>
    <?php  wp_reset_query();
    die();
}
add_action('wp_ajax_nopriv_ba_ajax_search','ba_ajax_search');
add_action('wp_ajax_ba_ajax_search','ba_ajax_search');
?>
