<?php

add_action('wp_login', 'pupilo_move_wish_cookie_to_usermeta', 100, 2);
function pupilo_move_wish_cookie_to_usermeta($user_login, $user) {
    update_user_meta( $user->ID, 'last_login_time', time() );
    $user_id = $user->ID;
    $current_cookie = $_COOKIE['wishlist'];
    $current_wishlist = get_user_meta($user_id, 'wishlist', true);

    $current_cookie = explode(',', $current_cookie);
    $current_wishlist = explode(',', $current_wishlist);

    $new_wishlist = implode(',', array_unique(array_merge($current_cookie, $current_wishlist)));

    wc_setcookie('wishlist', '', time()+259200);
    update_user_meta($user_id, 'wishlist', $new_wishlist);
}

add_action( 'wp_ajax_wishlist', 'wishlist_logged' );
function wishlist_logged() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'wishlist_logged')) {
        $user_id = get_current_user_id();

        $current_wishlist = get_user_meta($user_id, 'wishlist', true);
        $current_wishlist .= ',' . $_POST['product_id'];

        update_user_meta($user_id, 'wishlist', $current_wishlist);

        echo $current_wishlist;
        die;
    }
}

add_action( 'wp_ajax_nopriv_wishlist', 'wishlist_nopriv' );
function wishlist_nopriv() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'wishlist_nopriv')) {
        $current_cookie = $_COOKIE['wishlist'];
        $current_cookie .= ',' . $_POST['product_id'];

        wc_setcookie('wishlist', $current_cookie, time()+259200);

        echo $current_cookie;
        die;
    }
}

add_action( 'wp_ajax_remove_wishlist', 'remove_wishlist_logged' );
function remove_wishlist_logged() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'remove_wishlist_logged')) {
        $user_id = get_current_user_id();

        $current_wishlist = get_user_meta($user_id, 'wishlist', true);
        $array_wishlist = explode(',', $current_wishlist);

        $new_array = [];

        foreach ($array_wishlist as $item) {
            if ($item !== $_POST['product_id']) {
                array_push($new_array, $item);
            }
        }
        $res = implode(',', $new_array);
        update_user_meta($user_id, 'wishlist', $res);

        echo 'deleted';
        die;
    }
}

add_action( 'wp_ajax_nopriv_remove_wishlist', 'remove_wishlist_nopriv' );
function remove_wishlist_nopriv() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'remove_wishlist_nopriv')) {
        $current_cookie = $_COOKIE['wishlist'];

        $array_cookie = explode(',', $current_cookie);

        $new_array = [];

        foreach ($array_cookie as $item) {
            if ($item !== $_POST['product_id']) {
                array_push($new_array, $item);
            }
        }
        $res = implode(',', $new_array);

        wc_setcookie('wishlist', $res, time()+259200);

        echo $res;
        die;
    }
}
