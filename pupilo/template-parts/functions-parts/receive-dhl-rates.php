<?php
add_action('wp_ajax_dhl_rates', 'dhl_rates');
add_action('wp_ajax_nopriv_dhl_rates', 'dhl_rates');

function dhl_rates()
{
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'dhl_rates')) {
        global $woocommerce;

        $itemList = '{
                "name": "Total Items",
                "sku": null,
                "quantity": 1,
                "grams": ' . $woocommerce->cart->cart_contents_weight . ' ,
                "price": ' . $woocommerce->cart->subtotal . ',
                "vendor": null,
                "requires_shipping": true,
                "taxable": true,
                "fulfillment_service": "manual"
              }';

        $url = 'https://api.starshipit.com/api/rates/shopify?apiKey=d2c8c0d14207493896130ae2ec7277f9&integration_type=woocommerce&version=3.0&format=json&source=DHL';
        $post_data = '{
                        "rate": {
                          "destination":{
                            "country": "' . $_POST['country'] . '",
                            "postal_code": "' . $_POST['postcode'] . '",
                            "province": null,
                            "city": "' . $_POST['city'] . '",
                            "name": null,
                            "address1": "' . $_POST['address'] . '",
                            "address2": null,
                            "address3": null,
                            "phone": null,
                            "fax": null,
                            "address_type": null,
                            "company_name": null
                          },
                          "items":[' .
                            "$itemList" .
                            ']
                          }
                        }';

        $response = wp_remote_post($url, array(
                'headers' => array('Content-Type' => 'application/json; charset=utf-8'),
                'method' => 'POST',
                'body' => $post_data,
                'timeout' => 75,
                'sslverify' => 0
            )
        );

        $response_code = wp_remote_retrieve_response_code($response);
        $response_body = wp_remote_retrieve_body($response);

        $json_obj = json_decode($response_body);
        $rates_obj = $json_obj->{'rates'};



//        if (sizeof($rates_obj) > 0) {
//            foreach($rates_obj as $rate) {
//                if (is_object($rate)) {
//                    $shipping_rate = array(
//                        'id' => 'shipping_method_0_' . $rate -> {'service_code'},
//                        'label' => $rate -> {'service_name'},
//                        'cost' => $rate -> {'total_price'},
//                        'calc_tax' => 'per_order'
//                    );
//
//                    WC()->session->set('chosen_shipping_methods', array( 'flat_rate:1' ) );
//                }
//            }
//        }



        $output = '';
        $arr = [];

        if (sizeof($rates_obj) > 0) {
            foreach($rates_obj as $rate) {
                $output .= '<li><input type="radio" name="shipping_method[0]" data-index="0" id="shipping_method_0_dhlexpress_' . $rate->service_code . '" value="dhlexpress_' . $rate->service_code . '" class="shipping_method"><label for="shipping_method_0_dhlexpress_' . $rate->service_code . '">' . $rate->service_name . ': <span class="woocommerce-Price-amount amount"><bdi>' . $rate->total_price . '<span class="woocommerce-Price-currencySymbol">€</span></bdi></span></label></li>';
                array_push($arr, $rate);
            }
        }


//        echo $output;


        echo json_encode($arr);
        die;
    }
}
