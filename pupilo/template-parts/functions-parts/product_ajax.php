<?php function load_more_products()
{
    global $product;
    $next_page = $_POST['currentPage'] + 1;
    $product_class = $_POST['productClass'];
//    $product_attr = $_POST['productAttr'];
    $product_category = $_POST['productCategory'];
    $loop = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => 32,
        'orderby' => 'date',
        'product_cat' => $product_category,
        'paged' => $next_page,
//        'taxonomy' => $product_attr,
    ));
    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>

        <div class="product-item col-sm-6 <?php echo $product_class ?>">
            <?php

            global $post;
            global $product;
            if ($product) : ?>
                <?php
                global $wpdb;
                $taxes = get_the_terms($product->get_id(), 'pa_size');
                $taxes2 = get_the_terms($product->get_id(), 'pa_color');

                if ($product->is_type('variable')) {
                    $variations = $product->get_available_variations();

                    $variation_array = [];

                    foreach ($variations as $variation) {
                        $meta = get_post_meta($variation['variation_id'], 'attribute_pa_color', true);
                        $term = get_term_by('slug', $meta, 'pa_color')->term_id;
                        $id = $variation['variation_id'];
                        $table = $wpdb->get_blog_prefix() . 'termmeta';

                        $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$term} AND meta_key='pa_color_yith_wccl_value'");

                        $item = [
                            'color_name' => $variation['attributes']['attribute_pa_color'],
                            'color_id' => $variation['variation_id'],
                            'color_code' => $color,
                            'variation_id' => $term,
                            'image_url' => $variation['image']['url']
                        ];

                        array_push($variation_array, $item);
                    }

                    ?>
                    <div class="product-thumbnail">
                        <div class="product-sale-tooltip">
                            SALE -60%
                        </div>
                        <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                            <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                <use xlink:href="#wish-list"></use>
                            </svg>
                        </a>
                        <a href="<?php the_permalink(); ?>">
                            <?php
                            $flag = 0;

                            foreach ($variation_array as $var) {

                                ?>

                                <img src="<?php echo $var['image_url']; ?>"
                                     alt="<?php the_title(); ?>"
                                     data-varid="<?php echo $var['color_id']; ?>"
                                     class="<?php if ($flag) {
                                         echo 'display-none';
                                     } ?>">
                                <?php
                                $flag = 1;
                            }
                            ?>
                        </a>
                        <div class="product-attribute">
                            <?php if ($taxes): ?>
                                <p class="attribute-size">
                                    <?php foreach ($taxes as $tax): ?>
                                        <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                    <?php endforeach; ?>
                                </p>
                            <?php endif; ?>

                            <?php if ($taxes): ?>
                                <div class="attribute-color">
                                    <?php
                                    $flag = 1;

                                    foreach ($variation_array as $var) {
                                        ?>
                                        <span class="attribute-color-item <?php if ($flag) {
                                            echo 'attribute-color-after';
                                        } ?>"
                                              data-itemcolor="<?php echo $var['color_name']; ?>"
                                              data-varid="<?php echo $var['color_id']; ?>"
                                              style="background-color: <?php echo $var['color_code']; ?>"></span>
                                        <?php

                                        $flag = 0;
                                    }
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php
                } else {
                    ?>
                    <div class="product-thumbnail">
                        <div class="product-sale-tooltip">
                            SALE -60%
                        </div>
                        <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                            <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                                <use xlink:href="#wish-list"></use>
                            </svg>
                        </a>
                        <?php
                        $image_id  = $product->get_image_id();
                        $image_url = wp_get_attachment_image_url( $image_id, 'full' );
                        ?>

                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo $image_url?>" alt="">
                        </a>
                        <div class="product-attribute">
                            <?php if ($taxes): ?>
                                <p class="attribute-size">
                                    <?php foreach ($taxes as $tax): ?>
                                        <span class="attribute-size-item"><?php echo $tax->slug; ?></span>
                                    <?php endforeach; ?>
                                </p>
                            <?php endif; ?>

                            <p class="attribute-color">
                                <?php if ($taxes2): ?>
                                    <?php

                                    foreach ($taxes2 as $tax2):

                                        $table = $wpdb->get_blog_prefix() . 'termmeta';
                                        $id = $tax2->term_id;

                                        $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$id} AND meta_key='pa_color_yith_wccl_value'");
                                        ?>
                                        <span class="attribute-color-item "
                                              data-itemcolor="<?php echo $tax2->name; ?>"
                                              style="background-color: <?php echo $color; ?>"></span>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>

                    <?php
                }

                ?>

                <a class="product-link" href="<?php the_permalink(); ?>">
                    <h3 class="product-title">
                        <?php the_title(); ?>
                    </h3>
                    <p class="product-price">
                        <?php woocommerce_template_loop_price(); ?>
                        <span class="raiting">
                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                    <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                        <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                            <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                </span>
            </span>

                    </p>
                </a>

                <div class="product-btns">
                    <?php if ($product->get_sku()) : ?>
                        <button class="btn btn--try-on" data-show="<?php echo $product->get_sku()?>">
            <span class="try-on-icon">
                <svg width="20" height="20" viewBox="0 0 20 20">
                     <use xlink:href="#try-on"></use>
                </svg>
                <span> Try On</span>
            </span>
                        </button>
                    <?php endif; ?>
                    <a class="btn btn--product-view" href="<?php the_permalink();?>">View</a>
                </div>
            <?php endif;?>
        </div>
    <?php

    endwhile;
    endif;
    die();
}

add_action('wp_ajax_load_more_products', 'load_more_products');
add_action('wp_ajax_nopriv_load_more_products', 'load_more_products');
?>
