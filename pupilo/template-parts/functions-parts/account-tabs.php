<?php

add_filter ( 'woocommerce_account_menu_items', 'pupilo_remove_my_account_links' );
function pupilo_remove_my_account_links( $menu_links ){
//    unset( $menu_links['edit-address'] ); // Addresses
    unset( $menu_links['dashboard'] ); // Remove Dashboard
    //unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    //unset( $menu_links['orders'] ); // Remove Orders
    unset( $menu_links['downloads'] ); // Disable Downloads
    //unset( $menu_links['edit-account'] ); // Remove Account details tab
    unset( $menu_links['customer-logout'] ); // Remove Logout link

    return $menu_links;
}

add_filter ( 'woocommerce_account_menu_items', 'pupilo_rename_tabs' );
function pupilo_rename_tabs( $menu_links ){

    $menu_links = array(
        'orders' => 'My Orders',
        'edit-account' => 'My Details',
        'my-prescriptions' => 'My Prescriptions',
        'linked-accounts' => 'Linked accounts',
        'change-password' => 'Change password',
        'edit-address' => 'Delivery addresses and methods',
        'payment-methods' => 'Payment methods',
//        'customer-logout' => 'Logout'
    );
//    $menu_links['orders'] = 'My Orders';
//    $menu_links['edit-account'] = 'My Details';
//    $menu_links['edit-address'] = 'Delivery addresses';

    return $menu_links;
}

add_action( 'after_switch_theme', 'my_custom_flush_rewrite_rules' );

function my_custom_endpoints() {
    add_rewrite_endpoint( 'my-prescriptions', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'linked-accounts', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'change-password', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'payment-methods', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'my_custom_endpoints' );

function my_custom_query_vars( $vars ) {
    $vars[] = 'my-prescriptions';
    array_push($vars, 'change-password');

    return $vars;
}
add_filter( 'query_vars', 'my_custom_query_vars', 0 );

function my_custom_my_account_menu_items( $items ) {
    // Remove the logout menu item.
    $logout = $items['customer-logout'];
    unset( $items['customer-logout'] );

    // Insert your custom endpoint.
    $items['my-prescriptions'] = 'My Prescriptions';
    $items['linked-accounts'] = 'Linked Accounts';
    $items['change-password'] = 'Change Password';
    $items['payment-methods'] = 'Payment methods';

    // Insert back the logout item.
    $items['customer-logout'] = $logout;

    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );

function my_custom_endpoint_content() {
    @include 'my-prescriptions-tab.php';
}
add_action( 'woocommerce_account_my-prescriptions_endpoint', 'my_custom_endpoint_content' );

function my_custom_endpoint_content2() {
    @include 'change-password-tab.php';
}
add_action( 'woocommerce_account_change-password_endpoint', 'my_custom_endpoint_content2' );

function my_custom_endpoint_content3() {
    @include 'linked-accounts-tab.php';
}
add_action( 'woocommerce_account_linked-accounts_endpoint', 'my_custom_endpoint_content3' );

function my_custom_endpoint_content4() {
    @include 'payment-methods.php';
}
add_action( 'woocommerce_account_payment-methods_endpoint', 'my_custom_endpoint_content4' );

add_filter( 'woocommerce_endpoint_orders_title', 'change_my_account_orders_title', 10, 2 );
function change_my_account_orders_title( $title, $endpoint ) {
    $title = 'My Orders';

    return $title;
}

add_filter( 'woocommerce_endpoint_edit-account_title', 'change_my_account_edit_account_title', 10, 2 );
function change_my_account_edit_account_title( $title, $endpoint ) {
    $title = 'My Details';

    return $title;
}

add_filter( 'the_title', 'custom_account_endpoint_titles' );
function custom_account_endpoint_titles( $title ) {
    global $wp_query;

    if ( isset( $wp_query->query_vars['my-prescriptions'] ) && in_the_loop() ) {
        return 'My Prescriptions';
    }

    if ( isset( $wp_query->query_vars['linked-accounts'] ) && in_the_loop() ) {
        return 'Linked accounts';
    }

    if ( isset( $wp_query->query_vars['change-password'] ) && in_the_loop() ) {
        return 'Change password';
    }

    if ( isset( $wp_query->query_vars['edit-address'] ) && in_the_loop() ) {
        return 'Delivery methods';
    }

    if ( isset( $wp_query->query_vars['payment-methods'] ) && in_the_loop() ) {
        return 'Payment methods';
    }

    return $title;
}

add_action('wp_ajax_change_payment_method', 'change_payment_method');
function change_payment_method() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'change_payment_method')) {
        $user_id = get_current_user_id();

        update_user_meta($user_id, 'preferred_payment', $_POST['data']);

        echo $_POST['data'];
        die;
    }
}
