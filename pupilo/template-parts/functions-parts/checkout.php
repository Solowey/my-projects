<?php

add_filter('pre_option_woocommerce_default_gateway' . '__return_false', 99);
add_filter('woocommerce_shipping_chosen_method', '__return_false', 99);

//add_filter('woocommerce_update_order_review_fragments', 'awoohc_add_update_form_billing', 99);
function awoohc_add_update_form_billing($fragments)
{

    $checkout = WC()->checkout();
    ob_start();

    echo '<div class="woocommerce-billing-fields__field-wrapper">';

    $fields = $checkout->get_checkout_fields('billing');
    foreach ($fields as $key => $field) {
        if (isset($field['country_field'], $fields[$field['country_field']])) {
            $field['country'] = $checkout->get_value($field['country_field']);
        }
        woocommerce_form_field($key, $field, $checkout->get_value($key));
    }

    echo '</div>';

    $art_add_update_form_billing = ob_get_clean();
    $fragments['.woocommerce-billing-fields'] = $art_add_update_form_billing;

    return $fragments;
}

//add_filter('woocommerce_checkout_fields', 'awoohc_override_checkout_fields');
function awoohc_override_checkout_fields($fields)
{
    $chosen_methods = WC()->session->get('chosen_shipping_methods');

    if ('multiparcels_omniva_ee_pickup_point:5' === $chosen_methods[0] || 'multiparcels_smartpost_pickup_point:3' === $chosen_methods[0]) {
//        unset($fields['billing']['billing_company']);
//        unset($fields['billing']['billing_address_1']);
//        unset($fields['billing']['billing_address_2']);
//        unset($fields['billing']['billing_city']);
//        unset($fields['billing']['billing_postcode']);
//        unset( $fields['billing']['billing_country'] );
//        unset($fields['billing']['billing_state']);
//        unset( $fields['billing']['billing_phone'] );
//        unset($fields['billing']['billing_email']);
    }

    return $fields;
}


add_action('wp_footer', 'awoohc_add_script_update_shipping_method');
function awoohc_add_script_update_shipping_method()
{
    if (is_checkout()) {
        ?>
<!--        <style>-->
<!--            #billing_country_field {-->
<!--                display: none !important;-->
<!--            }-->
<!--        </style>-->
        <script>
            jQuery(document).ready(function ($) {

                $(document.body).on('updated_checkout updated_shipping_method', function (event, xhr, data) {
                    $('input[name^="shipping_method"]').on('change', function () {
                        $('.woocommerce-billing-fields__field-wrapper').block({
                            message: null,
                            overlayCSS: {
                                background: '#fff',
                                'z-index': 1000000,
                                opacity: 0.3
                            }
                        });
                    });
                    var first_name = $('#billing_first_name').val(),
                        last_name = $('#billing_last_name').val(),
                        phone = $('#billing_phone').val(),
                        email = $('#billing_email').val();

                    $(".woocommerce-billing-fields__field-wrapper").html(xhr.fragments[".woocommerce-billing-fields"]);
                    $(".woocommerce-billing-fields__field-wrapper").find('input[name="billing_first_name"]').val(first_name);
                    $(".woocommerce-billing-fields__field-wrapper").find('input[name="billing_last_name"]').val(last_name);
                    $(".woocommerce-billing-fields__field-wrapper").find('input[name="billing_phone"]').val(phone);
                    $(".woocommerce-billing-fields__field-wrapper").find('input[name="billing_email"]').val(email);
                    $('.woocommerce-billing-fields__field-wrapper').unblock();
                });
            });

        </script>
        <?php
    }
}

//
//add_filter('woocommerce_available_payment_gateways', 'pupilo_gateway_by_country');
//function pupilo_gateway_by_country($gateways)
//{
//
//    if (isset($gateways['montonio_card_payments']) && ('EE' !== WC()->customer->get_billing_country() && 'LT' !== WC()->customer->get_billing_country() && 'LV' !== WC()->customer->get_billing_country())) {
//        unset($gateways['montonio_card_payments']);
//    }
//    return $gateways;
//}


//add_filter('woocommerce_checkout_fields', 'pupilo_remove_fields', 9999);

function pupilo_remove_fields($woo_checkout_fields_array)
{

    // she wanted me to leave these fields in checkout
    unset($woo_checkout_fields_array['billing']['billing_first_name']);
    unset($woo_checkout_fields_array['billing']['billing_last_name']);
    unset($woo_checkout_fields_array['billing']['billing_phone']);
    unset($woo_checkout_fields_array['billing']['billing_email']);
//    unset($woo_checkout_fields_array['billing']['billing_fav_color']);
//     unset( $woo_checkout_fields_array['order']['order_comments'] ); // remove order notes

    // and to remove the billing fields below
    unset($woo_checkout_fields_array['billing']['billing_company']); // remove company field
    unset($woo_checkout_fields_array['billing']['billing_country']);
    unset($woo_checkout_fields_array['billing']['billing_address_1']);
    unset($woo_checkout_fields_array['billing']['billing_address_2']);
    unset($woo_checkout_fields_array['billing']['billing_city']);
    unset($woo_checkout_fields_array['billing']['billing_state']); // remove state field
    unset($woo_checkout_fields_array['billing']['billing_postcode']); // remove zip code field

    return $woo_checkout_fields_array;
}


//edit address from checkout to usermeta

add_action('wp_ajax_edit_address', 'edit_address');
function edit_address()
{
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'edit_address')) {
        $new_address = $_POST['data'];
        $user_id = get_current_user_id();

        update_user_meta($user_id, $new_address['id'] . '_address_nickname', $new_address['name']);
        update_user_meta($user_id, $new_address['id'] . '_email', $new_address['email']);
        update_user_meta($user_id, $new_address['id'] . '_first_name', $new_address['firstName']);
        update_user_meta($user_id, $new_address['id'] . '_last_name', $new_address['lastName']);
        update_user_meta($user_id, $new_address['id'] . '_phone', $new_address['phone']);

        echo $_POST['data']['id'];
        die;
    }
}

add_action('wp_ajax_edit_european_address', 'edit_european_address');
function edit_european_address()
{
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'edit_european_address')) {
        $new_address = $_POST['data'];
        $user_id = get_current_user_id();

        update_user_meta($user_id, $new_address['id'] . '_address_nickname', $new_address['name']);
        update_user_meta($user_id, $new_address['id'] . '_email', $new_address['email']);
        update_user_meta($user_id, $new_address['id'] . '_first_name', $new_address['firstName']);
        update_user_meta($user_id, $new_address['id'] . '_last_name', $new_address['lastName']);
        update_user_meta($user_id, $new_address['id'] . '_phone', $new_address['phone']);
        update_user_meta($user_id, $new_address['id'] . '_city', $new_address['city']);
        update_user_meta($user_id, $new_address['id'] . '_address_1', $new_address['address']);
        update_user_meta($user_id, $new_address['id'] . '_postcode', $new_address['postcode']);

        echo $_POST['data']['id'];
        die;
    }
}

add_action('wp_ajax_add_address', 'add_address');
function add_address() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'add_address')) {
        $new_address = $_POST['data'];
        $user_id = get_current_user_id();

        $array_shipping = get_user_meta($user_id, 'wc_address_book', true);
        $last_address = end($array_shipping);

        $last = substr($last_address, -1);

        if (is_numeric($last)) {
            $last += 1;
            $name = substr($last_address, 0, -1);
            $new_name = $name . $last;
        } else {
            $new_name = $last_address . '2';
        }

        array_push($array_shipping, $new_name);
        update_user_meta($user_id, 'wc_address_book', $array_shipping);

        update_user_meta($user_id, $new_name . '_address_nickname', $new_address['name']);
        update_user_meta($user_id, $new_name . '_email', $new_address['email']);
        update_user_meta($user_id, $new_name . '_first_name', $new_address['firstName']);
        update_user_meta($user_id, $new_name . '_last_name', $new_address['lastName']);
        update_user_meta($user_id, $new_name . '_phone', $new_address['phone']);

        echo $new_name;

        die;
    }
}

add_action('wp_ajax_add_european_address', 'add_european_address');
function add_european_address() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'add_european_address')) {
        $new_address = $_POST['data'];
        $user_id = get_current_user_id();

        $array_shipping = get_user_meta($user_id, 'wc_address_book', true);
        $last_address = end($array_shipping);

        $last = substr($last_address, -1);

        if (is_numeric($last)) {
            $last += 1;
            $name = substr($last_address, 0, -1);
            $new_name = $name . $last;
        } else {
            $new_name = $last_address . '2';
        }

        array_push($array_shipping, $new_name);
        update_user_meta($user_id, 'wc_address_book', $array_shipping);

        update_user_meta($user_id, $new_name . '_address_nickname', $new_address['name']);
        update_user_meta($user_id, $new_name . '_email', $new_address['email']);
        update_user_meta($user_id, $new_name . '_first_name', $new_address['firstName']);
        update_user_meta($user_id, $new_name . '_last_name', $new_address['lastName']);
        update_user_meta($user_id, $new_name . '_phone', $new_address['phone']);
        update_user_meta($user_id, $new_name . '_city', $new_address['city']);
        update_user_meta($user_id, $new_name . '_address_1', $new_address['address']);
        update_user_meta($user_id, $new_name . '_postcode', $new_address['postcode']);

//        echo $new_name;
        var_dump($_POST['data']);

        die;
    }
}

add_action('woocommerce_thankyou', 'pupilo_redirectcustom');
function pupilo_redirectcustom($order_id)
{

    $order = wc_get_order($order_id);

//    if(!is_user_logged_in()) {
//        setcookie('order', $order_id, time()+360000);
//    }

    $url = '/confirmation/?order=' . $order_id;

    if (!$order->has_status('failed')) {

        wp_safe_redirect($url);

        exit;

    }

}


add_filter('woocommerce_billing_fields', 'pupilo_billing_fields');
function pupilo_billing_fields($fields) {
//    unset($fields['billing_fav_color']['required']);
    unset($fields['billing_city']['required']);

    return $fields;
}

add_filter('woocommerce_shipping_fields','wpb_custom_billing_fields');
// remove some fields from billing form
// ref - https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
function wpb_custom_billing_fields( $fields = array() ) {

//    unset($fields['shipping_first_name']);
//    unset($fields['billing_address_1']);
//    unset($fields['billing_address_2']);
//    unset($fields['billing_state']);
//    unset($fields['billing_city']);
//    unset($fields['billing_phone']);
//    unset($fields['billing_postcode']);
//    unset($fields['billing_country']);

    return $fields;
}

add_filter( 'woocommerce_shipping_fields', 'pupilo_remove_fields2' );
function pupilo_remove_fields2( $fields ) {

    unset( $fields[ 'shipping_fav_color' ]['required']);

    unset( $fields[ 'shipping_first_name' ]['required'] );
    unset( $fields[ 'shipping_last_name' ]['required'] );
    unset( $fields[ 'shipping_phone' ]['required'] );
    unset( $fields[ 'shipping_country' ]['required'] );
    unset( $fields[ 'shipping_state' ]['required'] );
    unset( $fields[ 'shipping_address_1' ]['required'] );
    unset( $fields[ 'shipping_postcode' ]['required'] );
    unset( $fields[ 'shipping_email' ]['required'] );

    return $fields;

}


add_filter( 'woocommerce_default_address_fields', 'pupilo_add_field3' );
function pupilo_add_field3( $fields ) {
    $fields['fav_color'] = array(
        'label'        => 'Address name',
        'required'     => false,
        'class'        => array('display-none'),
        'priority'     => 1,
        'placeholder'  => 'Home, work, parents etc...',
    );

    $fields['address_nickname'] = array(
        'label'        => 'Address name',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 1,
        'placeholder'  => 'Home, work, parents etc...',
    );

    $fields['email']   = array(
        'label'        => 'Email',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 2,
        'placeholder'  => 'Enter email',
    );

    $fields['first_name']   = array(
        'label'        => 'First name',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 3,
        'placeholder'  => 'Enter First name',
    );

    $fields['last_name']   = array(
        'label'        => 'Last name',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 4,
        'placeholder'  => 'Enter Last name',
    );

    $fields['phone']   = array(
        'label'        => 'Phone',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 5,
        'placeholder'  => 'Enter contact phone',
    );

    $fields['address_1']   = array(
        'label'        => 'Address',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 50,
        'placeholder'  => 'Enter address',
    );

    $fields['postcode']   = array(
        'label'        => 'Postal code',
        'required'     => false,
        'class'        => array('add-address-row'),
        'priority'     => 55,
        'placeholder'  => 'XXXXXX',
    );

    return $fields;

}
