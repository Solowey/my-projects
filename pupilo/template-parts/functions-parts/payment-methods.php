<?php
    $user_id = get_current_user_id();
    $preferred_payment = get_user_meta($user_id, 'preferred_payment', true);
?>

<div class="payment-methods-wrapper">
    <div class="payment-method">
        <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
            <use xlink:href="#credit-card"></use>
        </svg>
        <div class="payment-info">
            <p class="heading">Montonio</p>
            <p class="text">Visa, Mastercard</p>
        </div>
        <div class="payment-check" data-method="montonio">
            <svg viewBox="0 0 24 24" width="24" height="24" class="unchecked <?php if ($preferred_payment === 'montonio') {echo 'display-none';} ?>">
                <use xlink:href="#unchecked"></use>
            </svg>
            <svg viewBox="0 0 24 24" width="24" height="24" class="montonio checked <?php if ($preferred_payment !== 'montonio') {echo 'display-none';} ?>">
                <use xlink:href="#checked"></use>
            </svg>
        </div>
    </div>
    <div class="payment-method">
        <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">
            <use xlink:href="#paypal-method"></use>
        </svg>
        <div class="payment-info">
            <p class="heading">Paypal</p>
            <p class="text">Safe payment online</p>
        </div>
        <div class="payment-check" data-method="paypal">
            <svg viewBox="0 0 24 24" width="24" height="24" class="unchecked <?php if ($preferred_payment === 'paypal') {echo 'display-none';} ?>">
                <use xlink:href="#unchecked"></use>
            </svg>
            <svg viewBox="0 0 24 24" width="24" height="24" class="paypal checked <?php if ($preferred_payment !== 'paypal') {echo 'display-none';} ?>">
                <use xlink:href="#checked"></use>
            </svg>
        </div>
    </div>
<!--    <div class="payment-method">-->
<!--        <svg viewBox="0 0 41 40" width="41" height="40" class="payment-logo">-->
<!--            <use xlink:href="#credit-card"></use>-->
<!--        </svg>-->
<!--        <div class="payment-info">-->
<!--            <p class="heading">Stripe</p>-->
<!--            <p class="text">Visa, Mastercard, American Express</p>-->
<!--        </div>-->
<!--        <div class="payment-check" data-method="stripe">-->
<!--            <svg viewBox="0 0 24 24" width="24" height="24" class="unchecked --><?php //if ($preferred_payment === 'stripe') {echo 'display-none';} ?><!--">-->
<!--                <use xlink:href="#unchecked"></use>-->
<!--            </svg>-->
<!--            <svg viewBox="0 0 24 24" width="24" height="24" class="stripe checked --><?php //if ($preferred_payment !== 'stripe') {echo 'display-none';} ?><!--">-->
<!--                <use xlink:href="#checked"></use>-->
<!--            </svg>-->
<!--        </div>-->
<!--    </div>-->
</div>
