<?php
function change_password_form()
{ ?>
    <div class="change-password-form">
        <form action="" method="post">
            <div class="change-password-input-wrapper">
                <label for="current_password">Your current password:</label>
                <div class="input-wrapper">
                    <input id="current_password" type="password" name="current_password" title="current_password"
                           placeholder="Enter your current password" required>
                    <svg viewBox="0 0 24 24" width="24" height="24" class="show-pass">
                        <use xlink:href="#show-password"></use>
                    </svg>
                </div>
            </div>
            <div class="change-password-input-wrapper">
                <label for="new_password">New password:</label>
                <div class="input-wrapper">
                    <input id="new_password" type="password" name="new_password" title="new_password"
                           placeholder="Enter new password" required autocomplete="new-password">
                    <svg viewBox="0 0 24 24" width="24" height="24" class="show-pass">
                        <use xlink:href="#show-password"></use>
                    </svg>
                </div>
            </div>
            <div class="change-password-input-wrapper">
                <label for="confirm_new_password">Confirm new password:</label>
                <div class="input-wrapper">
                    <input id="confirm_new_password" type="password" name="confirm_new_password"
                           title="confirm_new_password" placeholder="Confirm new password" required
                           autocomplete="new-password">
                    <svg viewBox="0 0 24 24" width="24" height="24" class="show-pass">
                        <use xlink:href="#show-password"></use>
                    </svg>
                </div>
            </div>
            <div class="password-condition condition1">
                <svg viewBox="0 0 16 11" width="16" height="11" class="show-pass">
                    <use xlink:href="#check-password"></use>
                </svg>
                <p>Add 1 uppercase letter</p>
            </div>
            <div class="password-condition condition2">
                <svg viewBox="0 0 16 11" width="16" height="11" class="show-pass">
                    <use xlink:href="#check-password"></use>
                </svg>
                <p>Add minimum of 8 characters</p>
            </div>
            <div class="password-condition condition3">
                <svg viewBox="0 0 16 11" width="16" height="11" class="show-pass">
                    <use xlink:href="#check-password"></use>
                </svg>
                <p>Add one number</p>
            </div>
            <input type="submit" value="Change Password" disabled>
        </form>
    </div>
<?php }

function change_password()
{
    if (isset($_POST['current_password'])) {
        $_POST = array_map('stripslashes_deep', $_POST);
        $current_password = sanitize_text_field($_POST['current_password']);
        $new_password = sanitize_text_field($_POST['new_password']);
        $confirm_new_password = sanitize_text_field($_POST['confirm_new_password']);
        $user_id = get_current_user_id();
        $errors = array();
        $current_user = get_user_by('id', $user_id);
    }

    // Check for errors
    if (empty($current_password) && empty($new_password) && empty($confirm_new_password)) {
        $errors[] = 'All fields are required';
    }
    if ($current_user && wp_check_password($current_password, $current_user->data->user_pass, $current_user->ID)) {
//match
    } else {
        $errors[] = 'Password is incorrect';
    }
    if ($new_password != $confirm_new_password) {
        $errors[] = 'Password does not match';
    }
    if (strlen($new_password) < 8) {
        $errors[] = 'Password is too short, minimum of 8 characters';
    }

    if (empty($errors)) {
        wp_set_password($new_password, $current_user->ID);
        echo '<h2>Password successfully changed!</h2>';
    } else {
        // Echo Errors
        if (!empty($_POST)) {
            echo '<h3>Errors:</h3>';
            foreach ($errors as $error) {
                echo '<p>';
                echo "<strong>$error</strong>";
                echo '</p>';
            }
        }
    }
}

function cp_form_shortcode()
{
    echo '<div class="change-password-result-wrapper">';
    change_password();
    echo '</div>';
    change_password_form();
}

add_shortcode('changepassword_form', 'cp_form_shortcode');


?>

<?php echo do_shortcode('[changepassword_form]'); ?>
