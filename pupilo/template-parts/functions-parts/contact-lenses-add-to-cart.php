<?php

add_action( 'wp_ajax_contact-lenses-add-to-cart', 'contact_lenses_add_to_cart' );
add_action( 'wp_ajax_nopriv_contact-lenses-add-to-cart', 'contact_lenses_add_to_cart' );

function contact_lenses_add_to_cart() {
    check_ajax_referer('myajax-nonce', 'nonce_code');

    if (!wp_verify_nonce($_POST['nonce_code'], 'contact-lenses-add-to-cart')) {
        $timestamp = time();

        $product_id = $_POST['productId'];
        global $post, $woocommerce;

        //----------------------------

        $match_attributes =  array(
            "attribute_pa_cl-bc" => $_POST['clBc'],
            "attribute_pa_cl-dia" => $_POST['clDia'],
            "attribute_pa_cl-power" => $_POST['rightLens']
        );

        $data_store   = WC_Data_Store::load( 'product' );
        $variation_id = $data_store->find_matching_product_variation(
            new \WC_Product( $product_id),$match_attributes
        );

        $variation = array(
            'lens_side' => 'right',
            'lenses_token' => $timestamp,
            'qty' =>$_POST['quantity']
        );

        $woocommerce->cart->add_to_cart( $product_id, $_POST['quantity'], $variation_id, '', $variation);

        //----------------------------

        $match_attributes =  array(
            "attribute_pa_cl-bc" => $_POST['clBc'],
            "attribute_pa_cl-dia" => $_POST['clDia'],
            "attribute_pa_cl-power" => $_POST['leftLens']
        );

        $data_store   = WC_Data_Store::load( 'product' );
        $variation_id = $data_store->find_matching_product_variation(
            new \WC_Product( $product_id),$match_attributes
        );

        $variation = array(
            'lens_side' => 'left',
            'lenses_token' => $timestamp,
            'qty' =>$_POST['quantity']
        );

        $woocommerce->cart->add_to_cart( $product_id, $_POST['quantity'], $variation_id, '', $variation);

        //----------------------------

        echo $timestamp;
        die;
    }
}
