<?php

add_action('wp_ajax_add_like', 'add_like');
//add_action('wp_ajax_nopriv_add_like_dislike', 'add_like_dislike');

function add_like() {
    $comment_id = $_POST['comment_id'];
    $user_id = $_POST['user_id'];

    $likes = get_comment_meta($comment_id, 'likes', true);

    $liked_comments = get_user_meta($user_id, 'likes', true);
    $liked_comments = explode(',', $liked_comments);

    if (!in_array($comment_id, $liked_comments)) {
        $likes += 1;
        update_comment_meta($comment_id, 'likes', $likes);

        array_push($liked_comments, $comment_id);
        $liked_comments = implode(',', $liked_comments);
        update_user_meta($user_id, 'likes', $liked_comments);

        echo $likes;
    } else {
        echo '-1';
    }

    die;
}

add_action('wp_ajax_add_dislike', 'add_dislike');
//add_action('wp_ajax_nopriv_add_like_dislike', 'add_like_dislike');

function add_dislike() {
    $comment_id = $_POST['comment_id'];
    $user_id = $_POST['user_id'];

    $dislikes = get_comment_meta($comment_id, 'dislikes', true);

    $disliked_comments = get_user_meta($user_id, 'dislikes', true);
    $disliked_comments = explode(',', $disliked_comments);

    if (!in_array($comment_id, $disliked_comments)) {
        $dislikes += 1;
        update_comment_meta($comment_id, 'dislikes', $dislikes);

        array_push($disliked_comments, $comment_id);
        $disliked_comments = implode(',', $disliked_comments);
        update_user_meta($user_id, 'dislikes', $disliked_comments);

        echo $dislikes;
    } else {
        echo '-1';
    }

    die;
}
