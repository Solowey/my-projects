<section class="recently-product-section">
    <div class="page-container">
        <?php echo do_shortcode('[woocommerce_recently_viewed_products per_page="5"]')?>
    </div>
</section>
