<div class="product-item col-12 col-sm-6 col-lg-4">
    <?php

    global $post;
    global $product;
    global $wpdb;
    $taxes = get_the_terms($product->get_id(), 'pa_color');

    if ($product->is_type('variable')) {
        $variations = $product->get_available_variations();

        $variation_array = [];

        foreach ($variations as $variation) {
            $meta = get_post_meta($variation['variation_id'], 'attribute_pa_color', true);
            $term = get_term_by('slug', $meta, 'pa_color')->term_id;
            $id = $variation['variation_id'];
            $table = $wpdb->get_blog_prefix() . 'termmeta';

            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$term} AND meta_key='pa_color_yith_wccl_value'");

            $item = [
                'color_name' => $variation['attributes']['attribute_pa_color'],
                'color_id' => $variation['variation_id'],
                'color_code' => $color,
                'variation_id' => $term,
                'image_url' => $variation['image']['url']
            ];

            array_push($variation_array, $item);
        }

        ?>
        <div class="product-thumbnail"
             data-product-id="<?php echo $product->get_id(); ?>"
             data-cariation-product-id=""
        >
            <div class="product-sale-tooltip">
                SALE -60%
            </div>
            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                    <use xlink:href="#wish-list"></use>
                </svg>
            </a>
            <a href="<?php the_permalink(); ?>">
                <?php
                $flag = 0;

                foreach ($variation_array as $var) {

                    ?>

                    <img src="<?php echo $var['image_url']; ?>"
                         alt="<?php the_title(); ?>"
                         data-varid="<?php echo $var['color_id']; ?>"
                         class="<?php if ($flag) {
                             echo 'display-none';
                         } ?>">
                    <?php
                    $flag = 1;
                }
                ?>
            </a>
            <div class="product-attribute">

                <?php if ($taxes): ?>
                    <div class="attribute-color">
                        <?php
                        $flag = 1;

                        foreach ($variation_array as $var) {
                            ?>
                            <span class="attribute-color-item <?php if ($flag) {
                                echo 'attribute-color-after';
                            } ?>"
                                  data-itemcolor="<?php echo $var['color_name']; ?>"
                                  data-varid="<?php echo $var['color_id']; ?>"
                                  style="background-color: <?php echo $var['color_code']; ?>">

                            </span>

                            <?php

                            $flag = 0;
                        }

                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php
    } else {
        ?>
        <div class="product-thumbnail" data-product-id="<?php echo $product->get_id(); ?>" >
            <div class="product-sale-tooltip">
                SALE -60%
            </div>
            <a href="#" class="wish-product-link" data-id="<?php echo $product->get_id(); ?>">
                <svg viewBox="0 0 24 24" width="24" height="24" class="wish-product-icon">
                    <use xlink:href="#wish-list"></use>
                </svg>
            </a>
            <?php
            $image_id  = $product->get_image_id();
            $image_url = wp_get_attachment_image_url( $image_id, 'full' );
            ?>

            <a href="<?php the_permalink(); ?>">
                <img src="<?php echo $image_url?>" alt="">
            </a>
            <div class="product-attribute">

                <p class="attribute-color">
                    <?php if ($taxes): ?>
                        <?php

                        foreach ($taxes as $tax):

                            $table = $wpdb->get_blog_prefix() . 'termmeta';
                            $id = $tax->term_id;

                            $color = $wpdb->get_var("SELECT meta_value FROM {$table} WHERE term_id={$id} AND meta_key='pa_color_yith_wccl_value'");
                            ?>
                            <span class="attribute-color-item "
                                  data-itemcolor="<?php echo $tax->name; ?>"
                                  style="background-color: <?php echo $color; ?>"></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </p>
            </div>
        </div>

        <?php
    }

    ?>


    <a class="product-link" href="<?php the_permalink(); ?>">
        <h3 class="product-title">
            <?php the_title(); ?>
        </h3>
        <p class="product-price">
            <?php woocommerce_template_loop_price(); ?>
            <span class="raiting">
                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                    <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                        <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                            <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                                <span class="star">
                    <svg width="16" height="15" viewBox="0 0 16 15">
                        <use xlink:href="#star"></use>
                    </svg>
                </span>
            </span>

        </p>
    </a>

        <button type="button" class="add-to-cart-accessories">
            Add to cart
        </button>

</div>
