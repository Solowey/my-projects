<!DOCTYPE html>
<html <?php language_attributes(); ?> id="html--hidden">
<head>
    <?php get_template_part('template-parts/head'); ?>
</head>

<body <?php body_class("page-body"); ?>>
<div class="wrapper" id="app">
    <?php get_template_part('template-parts/sprite') ?>
    <div class="content">
        <header class="page-header">
            <div class="page-header-top">
                <div class="page-container">
                    <div class="header-free-shipping">
                        <svg viewBox="0 0 24 24" class="header-free-shipping--icon">
                            <use xlink:href="#free-shipping"></use>
                        </svg>
                        <p class="header-free-shipping--title"><?php echo get_field('header_delivery', 'option') ?></p>
                    </div>
                    <a class="header-logo-link" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <svg viewBox="0 0 138 30" class="header-logo">
                            <use xlink:href="<?php echo get_field('site_logo', 'option') ?>"></use>
                        </svg>
                    </a>

                    <div class="iconic-menu">
                        <?php wp_nav_menu(array('theme_location' => 'Icon-menu', 'menu_class' => 'main-nav__icon-list', 'container' => false)); ?>

                    </div>
                </div>
            </div>
            <div class="account-menu">
                <?php wp_nav_menu(array('theme_location' => 'Account-menu', 'menu_class' => 'account-menu-list', 'container' => false)); ?>
            </div>
            <div class="page-container">
                <div class="page-header-bottom">
                    <div id="toggle" class="btn-burger">
                        <span class="span1 line"></span>
                        <span class="span2 line"></span>
                        <span class="span3 line"></span>
                    </div>

                    <div class="searched-icon">
                    <svg class="menu-icon menu-icon--search">
                        <use xlink:href="#search"></use>
                    </svg>
                    </div>
                    <a class="header-logo-link" href="<?php echo esc_url(home_url('/')); ?>" rel="home" style='display:none'>
                        <svg viewBox="0 0 138 30" class="header-logo">
                            <use xlink:href="<?php echo get_field('site_logo', 'option') ?>"></use>
                        </svg>

                    </a>

                    <nav class="main-nav" id="main-nav" role="navigation">
                        <div class="localization">
                            <p class="localization-lang">EN</p>
                            <p class="localization-lang">ET</p>
                            <p class="localization-lang">RU</p>
                        </div>
                        <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'main-nav__list', 'container' => false)); ?>
                        <div class="nav-bottom">
                            <p>
                                <?php the_field('header_info_text', 'option'); ?></p>
                            <p>
                                <?php the_field('header_info_contact', 'option'); ?>
                                <a class="mail-style" href="mailto:<?php the_field('mail_to', 'option') ?>">

                                    <?php the_field('mail_to', 'option'); ?>
                                </a>
                            </p>
                            <div class="header-social">
                                <?php while (have_rows('social_group', 'option')): the_row(); ?>
                                    <a class="header-social__icon"
                                       href="<?php echo the_sub_field('social_link') ?>">

                                        <svg class="social-icon">
                                            <use xlink:href="
                                                <?php echo the_sub_field('social_icon') ?>"></use>
                                        </svg>
                                    </a>
                                <?php endwhile; ?>

                            </div>
                        </div>
                    </nav>
                    <a href="<?php echo esc_url(home_url('/wish-list')); ?>">
                        <svg class="menu-icon menu-icon--wish-list">
                            <use xlink:href="#wish-list"></use>
                        </svg>
                    </a>
                    <a class="bag-link" href="<?php echo esc_url(home_url('/cart')); ?>">
                        <svg height="24px" width="35px" class="menu-icon menu-icon--bag">
                            <use xlink:href="#bag"></use>
                        </svg>

                    </a>
                    <div class="mega-menu-wrap">
                        <div class="mega-menu-wrap-navigation">
                            <?php wp_nav_menu(array('theme_location' => 'Navigation', 'menu_class' => 'main-nav__navigation-list', 'container' => false)); ?>
                        </div>

                        <div class="menus-container">
                            <div class="desktop-mega-menu eyeglasses">
                                <div class="page-container">
                                    <div class="menus-container__inner">
                                        <?php wp_nav_menu(array('theme_location' => 'Eyeglasses-1', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                        <?php wp_nav_menu(array('theme_location' => 'Eyeglasses-2', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                    </div>
                                    <div class="glasses-product-wrap">

                                        <?php while (have_rows('post_image_1', 'option')): the_row(); ?>
                                            <a class="product-card" href="<?php echo get_sub_field('brand_link_1')?>">
                                                <img class="post-image" src="<?php echo get_sub_field('post_image_image_link_1')['url'] ?>"
                                                     alt="<?php echo get_sub_field('post_image_image_link_1')['alt'] ?>">
                                                <p class="brand-label"><?php echo get_sub_field('post_image_brand_label_1')?></p>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="desktop-mega-menu sunglasses">
                                <div class="page-container">
                                    <div class="menus-container__inner">
                                        <?php wp_nav_menu(array('theme_location' => 'Sunglasses-1', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                        <?php wp_nav_menu(array('theme_location' => 'Sunglasses-2', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                    </div>
                                    <div class="glasses-product-wrap">

                                        <?php while (have_rows('post_image_2', 'option')): the_row(); ?>
                                            <a class="product-card" href="<?php echo get_sub_field('brand_link_2')?>">
                                                <img class="post-image" src="<?php echo get_sub_field('post_image_image_link_2')['url'] ?>"
                                                     alt="<?php echo get_sub_field('post_image_image_link_2')['alt'] ?>">
                                                <p class="brand-label"><?php echo get_sub_field('post_image_brand_label_2')?></p>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="desktop-mega-menu contact-lenses">
                                <div class="page-container">
                                    <div class="menus-container__inner">
                                        <?php wp_nav_menu(array('theme_location' => 'Contact-lenses-1', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                        <?php wp_nav_menu(array('theme_location' => 'Contact-lenses-2', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                    </div>
                                    <div class="glasses-product-wrap">

                                        <?php while (have_rows('post_image_3', 'option')): the_row(); ?>
                                            <a class="product-card" href="<?php echo get_sub_field('brand_link_3')?>">
                                                <img class="post-image" src="<?php echo get_sub_field('post_image_image_link_3')['url'] ?>"
                                                     alt="<?php echo get_sub_field('post_image_image_link_3')['alt'] ?>">
                                                <p class="brand-label"><?php echo get_sub_field('post_image_brand_label_3')?></p>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="desktop-mega-menu special-glasses">
                                <div class="page-container">
                                    <div class="menus-container__inner">
                                        <?php wp_nav_menu(array('theme_location' => 'Special-glasses-1', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                        <?php wp_nav_menu(array('theme_location' => 'Special-glasses-2', 'menu_class' => 'main-nav__glasses-list overall', 'container' => false)); ?>
                                    </div>
                                    <div class="glasses-product-wrap">

                                        <?php while (have_rows('post_image_4', 'option')): the_row(); ?>
                                            <a class="product-card" href="<?php echo get_sub_field('brand_link_4')?>">
                                                <img class="post-image" src="<?php echo get_sub_field('post_image_image_link_4')['url'] ?>"
                                                     alt="<?php echo get_sub_field('post_image_image_link_4')['alt'] ?>">
                                                <p class="brand-label"><?php echo get_sub_field('post_image_brand_label_4')?></p>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="iconic-menu">
                        <?php wp_nav_menu(array('theme_location' => 'Icon-menu', 'menu_class' => 'main-nav__icon-list', 'container' => false)); ?>

                    </div>

                </div>
            </div>
        </header>

        <?php get_template_part('template-parts/aside', 'login') ?>

        <div class="aside-cart-wrapper">
            <div class="aside-cart">
                <div class="page-container aside-cart-container">
                    <div class="aside-header-wrap">
                        <h5 class="aside-headline">My cart </h5>
                        <svg class="aside-cart-close" height="24px" width="24px">
                            <use xlink:href="#close"></use>
                        </svg>
                    </div>
                    <div class="free-delivery">
                        <svg class="shipping-image" height="24px" width="24px">
                            <use xlink:href="#free-shipping-cart"></use>
                        </svg>
                        <p class="free-delivery-text"><?php echo get_field('delivery_text_part1', 'option')?> <span
                                class="free-delivery-text-bold">
                                <?php echo get_field('delivery_text_part2', 'option')?>
                            </span></p>
                    </div>
                    <?php echo do_shortcode('[woocommerce_cart]') ?>
                    <?php if (!is_user_logged_in()) { ?>
                        <div class="control-button">

                            <div class="cart-subtotal">
                                <p><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
                                <p><?php wc_cart_totals_order_total_html(); ?></p>
                            </div>

                            <div class="radio-item">
                                <input type="radio" id="checkoutGuest" name="checkout" value="checked" checked>
                                <label for="checkoutGuest">Checkout as a guest</label>
                            </div>

                            <div class="radio-item">
                                <input type="radio" id="checkoutAccount" name="checkout" value="checked2">
                                <label for="checkoutAccount">Checkout with account</label>
                            </div>
                            <div class="choice">
                                <div class="go-to-login">
                                    <button type="button" class="cart-sign-in">Sign In</button>
                                    <button type="button" class="cart-sign-up">Sign up</button>
                                </div>
                                <div class="go-to-checkout">
                                    <?php do_action('woocommerce_proceed_to_checkout'); ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="control-button2">
                            <div class="cart-subtotal">
                                <p><?php esc_html_e( 'Total', 'woocommerce' ); ?></p>
                                <p><?php wc_cart_totals_order_total_html(); ?></p>
                            </div>
                            <div class="go-to-checkout">
                                <?php do_action('woocommerce_proceed_to_checkout'); ?>
                            </div>
                        </div>
                    <?php  } ?>

                </div>
            </div>
        </div>

    </div>

    <template class="menu-account-header" data-login-template >
        <a href="<?php echo esc_url(home_url('/my-account')); ?>" class="visible">
            <p class="article-title">Hi,&nbsp;</p>
            <p class="article-text"><?php echo get_userdata(get_current_user_id())->display_name; ?>
                <svg viewBox="0 0 24 24" width="24" height="24" class="header-chevron-wrapper">
                    <use xlink:href="#chevron-header"></use>
                </svg>
            </p>
        </a>
    </template>

    <div class="loginCheck" data-login-check <?php
    if ( is_user_logged_in() ) {
        echo 'registered';
    } else {
        echo 'visitor';
    }
    ?>></div>

    <div class="help-popup">

        <a class="faq-link" href="/faq">FAQ</a>

        <div class="help-ifo">
            <div class="working-hours">
                <?php while (have_rows('working_hours_header', 'option')): the_row(); ?>
                    <p class="info-style"><?php echo the_sub_field('info') ?></p>
                <?php endwhile; ?>
            </div>

            <div class="connect-with">
                <?php while (have_rows('connect_item_header', 'option')): the_row(); ?>
                    <div class="connect-with-item">
                        <div class="svg-center">
                            <svg class="connect-icon" width="31" height="31">
                                <use xlink:href="<?php echo get_sub_field('item_icon') ?>"></use>
                            </svg>
                        </div>
                        <a href="mailto:<?php echo get_sub_field('item_text')['url'] ?>" class="connect-text">
                            <?php echo get_sub_field('item_text')['title'] ?>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>

    </div>
    <div class="suggestions-modal-overlay">
        <div class="suggestions-modal">
            <div class="modal-headline">
                <a class="button-close-suggestions-modal">
                    <svg class="right-close" width="24" height="24">
                        <use xlink:href="#cart-close">

                        </use>
                    </svg>
                </a>

            </div>
            <h3 class="suggestions-modal-title"><?php echo get_field('our_suggestions_title', 'option')?></h3>
            <div class="modal-body suggestions-slider">
                <?php get_template_part('template-parts/content', 'suggestions'); ?>
            </div>
        </div>
    </div>

    <div class="header-search-modal-wrap">
        <div class="header-search-modal">

            <div class="head-line">
                <button  type="button" class="button-close-search">
                    <svg width="30" height="30">
                        <use xlink:href="#close"></use>
                    </svg>
                </button>
            </div>
            <div class="header-search">

                <form role="search" method="get" id="searchform" action="/search">
                    <input type="text" name="search_text" class="search-input" id="s" placeholder="Search....." />
                    <svg class="search-icon-active" height="25" width="25">
                        <use xlink:href="#searchForm"></use>
                    </svg>
                </form>

                <div class="result-search">
                    <h5 class="suggestion-item-title"><?php echo get_field('suggested_queries_title', 'option') ?></h5>
                    <div class="recommended-search">

                        <span class="recommended-search-item">

                            <?php if (!$_COOKIE['recent']) { ?>
                                <?php while (have_rows('suggestion_queries', 'option')): the_row(); ?>

                                    <a class="suggestion-queries" href="/search/?search_text=<?php echo get_sub_field('suggested_queries_item') ?>">
                                    <?php echo get_sub_field('suggested_queries_item') ?>
                                </a>

                                <?php endwhile; ?>
                            <?php  } else { ?>
                                <?php
                                $data = $_COOKIE['recent'];
                                $arr = explode(',', $data);

                                $arr = array_filter($arr);

                                foreach ($arr as $v) { ?>
                                   <?php if ($v !== '' || $v !== 'undefined') { ?>
                                    <a class="suggestion-queries recently-searched" href="/search/?search_text=<?php echo "$v";?>">
                                    <?php echo "$v";?>
                                  <?php  } ?>
                                </a>
                                <?php }
                            }
                            ?>

                        </span>

                    </div>
                    <div class="result-search-list"></div>
                </div>

                <div class="result-search-post">
                    <div class="preloader"></div>
                    <div class="result-search-post-list"></div>
                    <div class="founded"></div>
                </div>
            </div>
        </div>
    </div>
