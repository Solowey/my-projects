<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<section class="top-section">

    <div class="size-guid-wrapper">
        <div class="size-guid-popup">
            <div class="size-guide-head">
                <span>Size Guide</span>
                <span class="size-guid-wrapper-close">
                    <svg width="24" height="24">
                        <use xlink:href="#close"></use>
                    </svg>
                </span>
            </div>

            <div class="size-guide-info">
                <?php echo get_field('size_guide', 'option')?>
            </div>
        </div>
    </div>
    <div class="page-container">
<?php
/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action( 'woocommerce_before_main_content' );
?>

<?php while ( have_posts() ) : ?>
    <?php the_post(); ?>

    <?php wc_get_template_part( 'content', 'single-product' ); ?>
    </div>
<?php endwhile; // end of the loop. ?>

        <div class="review reviews-info">

            <div class="page-container">
            <div class="reviews-info-image-container">
                <div class="reviews-info-image">
                    <svg width="81" height="80" viewBox="0 0 81 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#delivery"></use>
                    </svg>
                    <p class="reviews-info-description">Free Shipping Over 50€</p>
                </div>
                <div class="reviews-info-image">
                    <svg width="81" height="80" viewBox="0 0 81 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#secure"></use>
                    </svg>
                    <p class="reviews-info-description">Price Match Guarantee</p>
                </div>
                <div class="reviews-info-image">
                    <svg width="81" height="80" viewBox="0 0 81 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#free-return"></use>
                    </svg>
                    <p class="reviews-info-description">30 days free return</p>
                </div>
            </div>
            </div>
            <div class="similar-slider-container">
                <div class="page-container">
                    <h3 class="slider-header">
                        <?php echo get_field('product_similar_title')?>
                    </h3>
                    <div class="similar-nav-container"></div>
                    <div class="row product-row similar-slider">

                <?php while( have_rows('product_similar_styles') ): the_row(); ?>

                    <?php $post_object = get_sub_field('similar_product'); ?>

                        <?php if( $post_object ): ?>

                            <?php $post = $post_object; setup_postdata( $post ); ?>

                                <?php get_template_part('template-parts/content', 'category') ?>

                            <?php wp_reset_postdata(); ?>

                        <?php endif; ?>

                <?php endwhile; ?>

            </div>
                </div>
            </div>
            <div class="page-container">
                <div class="reviews">

                    <div class="my-comments">
                        <div class="review-headline">
                            <h2 class="review-header">Reviews</h2>
                            <?php wc_get_template_part( 'single-product/rating' ); ?>
                            <?php if ( is_user_logged_in() ) : ?>
                            <div class="review-info">
                                <span class="add-review review-info-desktop" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Write a review</span>

<!--                                <div class="review-sort">Sort By: Date</div>-->

                            </div>

                            <?php endif; ?>

                        </div>
                <div class="product-comments"
                     data-page="<?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>"
                     data-productId="<?php echo get_the_ID()?>"
                     data-all-posts="<?php echo $product->get_review_count() ?>"
                >
                    <?php comments_template( '/woocommerce/single-product-reviews.php' ); ?>
                </div>

                    </div>

            <div class="control-button-container">

                    <div class="load-more">
                        <div class="load-more_text">You have viewed <span id="viewedComments"></span> of
                            <span><?php echo $product->get_review_count() ?></span> posts
                        </div>
                        <div class="load-more_line">
                            <div class="load-more_progress"></div>
                        </div>
                        <button id="load_more_comments" class="load-more_btn">More review</button>
                    </div>

                <?php if ( is_user_logged_in() ) : ?>

                        <span class="add-review-mobile" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Write a review</span>

                <?php endif; ?>
            </div>
                </div>
            </div>

            <div class="recommended-slider-container">
                <div class="page-container">
                    <h3 class="slider-header">
                        <?php echo get_field('product_recomended_title')?>
                    </h3>
                <div class="recommended-nav-container"></div>
                <div class="row product-row recommended-slider">

                    <?php while( have_rows('product_recommended') ): the_row(); ?>

                        <?php $post_object = get_sub_field('similar_product'); ?>

                        <?php if( $post_object ): ?>

                            <?php $post = $post_object; setup_postdata( $post ); ?>

                            <?php get_template_part('template-parts/content', 'category') ?>

                            <?php wp_reset_postdata(); ?>

                        <?php endif; ?>

                    <?php endwhile; ?>

                </div>
                </div>
            </div>
        </div>

</section>
    <section class="recently-product-section">
        <?php get_template_part('template-parts/content', 'recently-product')?>
    </section>
<?php get_template_part('template-parts/subscribe') ?>
<?php
/**
 * woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
?>

<?php
/**
// * woocommerce_sidebar hook.
// *
// * @hooked woocommerce_get_sidebar - 10
// */
//do_action( 'woocommerce_sidebar' );
//?>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
