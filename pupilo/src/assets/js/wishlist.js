/*es-lint-disable */
/* eslint-disable */
(function($) {
    document.addEventListener('DOMContentLoaded', function() {
        let wishItemId;

        $('.three-dots-button').click(function () {
            $('.wishlist-delete-mobile-wrapper').hide(300).show(300);
            wishItemId = $(this).attr('data-id');
            $('.wishlist-overlay').show();
        });

        $('.remove-button-close').click(function () {
            $('.wishlist-delete-mobile-wrapper').hide(300);
            $('.wishlist-overlay').hide();
        });

        $('.remove-from-wishlist').click(function () {
            if (window.innerWidth > 767) {
                wishItemId = $(this).attr('data-prod-id');
            }

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'remove_wishlist',
                    'nonce_code': myajax.nonce,
                    'product_id': wishItemId
                },
                success: function( data ) {
                    $(`.product-item-mobile[data-prod-id=${wishItemId}]`).remove();
                    $(`.product-item[data-prod-id=${wishItemId}]`).remove();
                    $('.wishlist-delete-mobile-wrapper').hide(300);
                    $('.wishlist-overlay').hide();
                    $('.wish-product-count').text($('.wish-product-count').text() - 1);
                    $('.wish-counter').text($('.wish-counter').text() - 1);
                }
            });
        });
    });
})(jQuery);
