import '@scss/pages/_brand';
import 'slick-carousel';

(function($) {
    // document.addEventListener('DOMContentLoaded', function() {
    // });
    $('.slider-brand').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        fade: true,
        arrows: false
    });
    let buttonChangeMobileCol = $('.col_1'),
        productClass = 'col-lg-4';
    buttonChangeMobileCol.on('click', function() {
        $('.product-item').addClass('col-12').removeClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_1').addClass('active-grid-button');
        productClass = 'col-12';
    });

    let buttonChangeMobileCol2 = $('.col_2');
    buttonChangeMobileCol2.on('click', function() {
        $('.product-item').removeClass('col-12').addClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_2').addClass('active-grid-button');
        productClass = 'col-6';
    });

    let buttonChangeMobileCol3 = $('.col_3');
    buttonChangeMobileCol3.on('click', function() {
        $('.product-item').addClass('col-lg-4').removeClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_3').addClass('active-grid-button');
        productClass = 'col-lg-4';
    });

    let buttonChangeMobileCol4 = $('.col_4');
    buttonChangeMobileCol4.on('click', function() {
        $('.product-item').removeClass('col-lg-4').addClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_4').addClass('active-grid-button');
        productClass = 'col-lg-3';
    });
    let currentPage = document.querySelector(".product-row").dataset.page,
        maxPages = document.querySelector(".product-row").dataset.max,
        allPosts = document.querySelector('.product-row').dataset.allPosts,
        loadMoreBtn = document.getElementById('load_more2'),
        viewedPosts = document.getElementById('viewedPosts'),
        headerViewedPosts = document.getElementById('headerViewedPosts'),
        posts = $('.product-row [class*="product-item"]').length,
        percentViewedPosts = posts / allPosts * 100;
    $(".load-more_progress").css("width", `${percentViewedPosts}%`);
    if($(window).width() < 576) {
        $('.featured__grid').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
    }

    viewedPosts.innerText = posts;
    headerViewedPosts.innerText = posts;
    if(Number(currentPage) === Number(maxPages)) {
        loadMoreBtn.remove();
    }
    loadMoreBtn.addEventListener('click', function() {
        // eslint-disable-next-line no-invalid-this
        let button = $(this),
            myajaxurl = `//${window.location.host}/wp-admin/admin-ajax.php`;
        $.ajax({
            url: myajaxurl,
            data: { action: "load_more_products", currentPage, maxPages, productClass },
            type: "POST",
            beforeSend() {
                button.text("Loading");
            },
            success(data) {
                if(data) {
                    let postsList = document.querySelector(".product-row");
                    postsList.innerHTML += data;
                    posts = $('.product-row [class*="product-item"]').length;
                    button.text("Load more");
                    currentPage++;
                    viewedPosts.innerText = posts;
                    headerViewedPosts.innerText = posts;
                    percentViewedPosts = posts / allPosts * 100;
                    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

                    if(Number(currentPage) === Number(maxPages)) {
                        button.remove();
                    }
                } else {
                    button.remove();
                }
            }
        });
    });
    $('#sortBy2').on('click', function() {
        $('#preset_369').addClass('open').show('300');
        $('.page-body').addClass('yith-wcan-preset-modal-open');
    });
})(jQuery);
