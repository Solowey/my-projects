/* eslint-disable */
/*eslint max-lines: 1000*/
(function ($) {
    document.addEventListener('DOMContentLoaded', function () {
        $('.lens-delete').click(function () {
            let token = $(this).attr('data-token');
            let id = $(this).attr('data-id');
            let variation = $(this).attr('data-variation');
            let keyRight = $(this).attr('data-key-right');
            let keyLeft = $(this).attr('data-key-left');

            $.ajax({
                type: "post",
                url: myajax.url,
                data: {
                    action: 'contact_lenses_delete_from_cart',
                    nonce_code: myajax.nonce,
                    token: token,
                    id: id,
                    variation: variation,
                    keyRight: keyRight,
                    keyLeft: keyLeft
                },
                success: function (data) {
                    if (data) {
                        // console.log(data)
                        location.reload();
                    }
                }
            });


        });

        $('.lens-plus, .lens-minus').click(function () {
            let token = $(this).attr('data-token');
            let id = $(this).attr('data-id');
            let variation = $(this).attr('data-variation');
            let keyRight = $(this).attr('data-key-right');
            let keyLeft = $(this).attr('data-key-left');
            let qty = $(this).attr('data-qty');
            let currentQty = $(this).attr('data-current-qty');
            let sign = $(this).attr('data-sign');

            $.ajax({
                type: "post",
                url: myajax.url,
                data: {
                    action: 'contact_lenses_plus_minus',
                    nonce_code: myajax.nonce,
                    token: token,
                    id: id,
                    variation: variation,
                    keyRight: keyRight,
                    keyLeft: keyLeft,
                    qty: qty,
                    currentQty: currentQty,
                    sign: sign
                },
                success: function (data) {
                    location.reload();
                }
            });
        });
    });
})(jQuery);
