import '@scss/pages/_home.scss';
import 'slick-carousel';
/* eslint-disable */
(function ($) {
    $('.intro-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        fade: true,
        arrows: false
    });

    $('.testimonial-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.testimonial-nav-container'),
        responsive: [ {
            breakpoint: 768,
            settings: {
                arrows: false,
            }
        }]
    });

    $('.eye-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
    $('.special-sun-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.special-sun-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
    $('.sun-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.sun-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
    $('.lenses-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.lenses-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
    const init = {
        dots: true,
        autoplay: true,
        infinite: true,
        cssEase: "linear",
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    };

    $(() => {
        const win = $(window);
        let slider = $(".multiple-items");

        win.on("load resize", () => {
            if(win.width() < 769) {
                slider.not(".slick-initialized").slick(init);
            } else if(slider.hasClass("slick-initialized")) {
                slider.slick("unslick");
            }
        });
    });

})(jQuery);

