import '@scss/pages/_single';

jQuery(function($) {
    let coverImg = document.querySelector('.wp-block-cover'),
        container = document.querySelector('.single-main .page-container'),
        shareBtn = document.querySelector('.addtoany_content'),
        like = document.querySelector('.share_buttons .like');
    $(coverImg).insertBefore(container);
    $(shareBtn).insertAfter(like);
});
