import '@scss/_base.scss';
import '@scss/single-product.scss';
import 'slick-carousel';
/* eslint-disable */
/*eslint max-lines: 1000*/
(function($) {
    document.addEventListener('DOMContentLoaded', function() {
        $('.table-name').click(function () {
            $('.total-prescriptions').toggle(300);
            $('.open-saved-pre').toggleClass('rotate-270');
        });

        let breadcrumbsHtml = $('.woocommerce-breadcrumb').html();
        breadcrumbsHtml = breadcrumbsHtml.split('/product-category/eyeglasses/"').join('/eyeglasses/"');
        breadcrumbsHtml = breadcrumbsHtml.split('/product-category/sunglasses/"').join('/sunglasses/"');
        breadcrumbsHtml = breadcrumbsHtml.split('/product-category/contact-lenses/"').join('/contact-lenses/"');
        $('.woocommerce-breadcrumb').html(breadcrumbsHtml);

        $('.single-product-category-show').on('click', function (e) {
            e.preventDefault()
        })
        let prescription = {};
        let lensSelection = [];
        let textConfiguration;
        const sizeSelector = $('#pa_size');
        const colorSelector = $('#pa_color');
        let size = sizeSelector.val() || '';
        let color = colorSelector.val() || '';
        const startWizardButton = $('#start-wizard');

        let basicPriceElement = $('.price.basic-price');
        let basicPriceHtml = $('.price.basic-price').html();

        setTimeout(function () {
            setPrices(sizeSelector, colorSelector, basicPriceElement, basicPriceHtml);
        }, 200);



        // if (size && color) {
        //     startWizardButton.prop('disabled', false);
        // } else {
        //     startWizardButton.prop('disabled', true);
        // }

        $('#pa_size').change(function () {
            size = $(this).val();

            setPrices(sizeSelector, colorSelector, basicPriceElement, basicPriceHtml);

            if (size && color) {
                startWizardButton.prop('disabled', false);
            } else {
                startWizardButton.prop('disabled', true);
            }

            if (size) {
                //     let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                //
                //
                // const params = new URLSearchParams(window.location.search);
                // console.log(window.history)
                // console.log(baseUrl)
                //
                // let name = 'http://localhost:8080';
                // let value = '1';
                // let param = '?pa_atr';
                //
                // params.set(name, value);
                //
                // params.delete(param);
                //
                // params.append(name, value);
                // window.history.replaceState({value}, '', `${baseUrl}?attribute_pa_color=${color}&attribute_pa_size=${size}`);

                if (color) {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_color=' + color + '&attribute_pa_size=' + size;
                    history.replaceState(null, null, newUrl);
                } else {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_size=' + size;
                    history.replaceState(null, null, newUrl);
                }
            } else {
                if (color) {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_color=' + color;
                    history.replaceState(null, null, newUrl);
                } else {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl;
                    history.replaceState(null, null, newUrl);
                }
            }
        });

        $('#pa_color').change(function () {
            color = $(this).val();

            setPrices(sizeSelector, colorSelector, basicPriceElement, basicPriceHtml);

            if (size && color) {
                startWizardButton.prop('disabled', false);
            } else {
                startWizardButton.prop('disabled', true);
            }

            if (color) {
                if (size) {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_color=' + color + '&attribute_pa_size=' + size;
                    history.replaceState(null, null, newUrl);
                } else {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_color=' + color;
                    history.replaceState(null, null, newUrl);
                }
            } else {
                if (size) {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl + '?attribute_pa_size=' + size;
                    history.replaceState(null, null, newUrl);
                } else {
                    let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                    let newUrl = baseUrl;
                    history.replaceState(null, null, newUrl);
                }
            }
        });

        let basicPrice;
        let currentPrice;

        // const productContainer = $('div[class^="single-product"]');
        const productContainer = $('div[id^="product-"]');
        const wizardContainer = $('#wizard');
        // const addToCardButton = $('.single_add_to_cart_button');
        const customAddToCard = $('#custom-add-to-card');
        const wizardBackButton = $('.wizard-back-button');
        const wizardHeader = $('.wizard-header');

        // wizardContainer.show();

        $('.add-to-cart-without-wizard').click(function () {
            $('.single_add_to_cart_button').trigger('click');
        });

        startWizardButton.click(() => {
            if ($('div.product .woocommerce-variation-price ins').length) {
                basicPrice = Number($('div.product .woocommerce-variation-price ins .woocommerce-Price-amount').text().slice(0, -1));
            } else {
                basicPrice = Number($('div.product .woocommerce-variation-price .woocommerce-Price-amount').text().slice(0, -1))
                    || Number($('.basic-price ins').text().slice(0, -1));
            }

            if (basicPrice === 0) {
                basicPrice = Number($('div.product .price-wrapper-with-sale-text ins .woocommerce-Price-amount').text().slice(0, -1))
                    || Number($('div.product .price-wrapper-with-sale-text bdi').text().slice(0, -1));
            }

            currentPrice = $('.current-price');
            $('.current-price').text(basicPrice.toFixed(2) + '€');

            productContainer.hide();
            wizardContainer.removeClass('display-none');
            $('.wizard1').removeClass('display-none');
            $('.woocommerce-breadcrumb').addClass('display-none');
            $('.woocommerce-tabs').addClass('display-none');
            $('.reviews-info').addClass('display-none');
            $('.recently-product-section').addClass('display-none');

            $('.top-section').addClass('background-transparent');

            $('.wizard-image').attr('src', $('.wvg-gallery-image img:first-child').attr('src'));

            if (color && size) {
                $('.total-frame .variations').text(color + ' | ' + size);
            }

            $('.total-frame .price').text(basicPrice.toFixed(2) + '€');
        });

        $('.wizard-header').click(() => {
            // console.log(lensSelection)
        });

        $('input').click(function () {
            setTimeout(() => {
                countAddPrice(lensSelection);
            }, 100);
        });

        wizardBackButton.click(() => {
            tintAp = 0;
            mirrorAp = 0;

            $('.last-step').addClass('display-none');
            $('.step-wrapper').addClass('display-none');
            $('.tint-color-choose').addClass('display-none');
            $('.tint-color-mirrored').addClass('display-none');

            let lastElement = lensSelection.pop();

            if (!!lastElement) {
                // $('.step-wrapper').addClass('display-none');
                $(`.${lastElement.step}`).removeClass('display-none');
            } else {
                productContainer.show();
                $('.top-section').removeClass('background-transparent');
                $('.woocommerce-breadcrumb').removeClass('display-none');
                $('.woocommerce-tabs').removeClass('display-none');
                $('.reviews-info').removeClass('display-none');
                $('.recently-product-section').removeClass('display-none');
            }

            if (!lastElement) {
                $('.wizard').addClass('display-none');
            }

            if (lastElement && lastElement.step === 'step1') {
                if (window.innerWidth <= 767) {
                    $('.wizard2-subtotal').removeClass('display-none');
                    $('.wizard1-subtotal').removeClass('display-none');
                }
            }

            if (lastElement && lastElement.step === 'step2') {
                $('.total-prescriptions').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');

                if (window.innerWidth <= 767) {
                    $('.wizard3-subtotal').addClass('display-none');
                }
            }

            if (lastElement && lastElement.step === 'step31' || lastElement.step === 'step32' || lastElement.step === 'step33' || lastElement.step === 'step34') {
                $('.wizard3 .step-heading-wrapper').removeClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').removeClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').addClass('display-none');

                if (window.innerWidth <= 767) {
                    $('.wizard3-subtotal').removeClass('display-none');
                    $('.wizard4-subtotal').addClass('display-none');
                }
            } else {
                $('.wizard3 .step-heading-wrapper').addClass('display-none');
            }

            if (lastElement && lastElement.step[4] == '4') {
                $('.wizard-total-wrapper').removeClass('display-block');
                $('.wizard4 label').removeClass('checked-label');
                $('.add-to-cart').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
            }

            if (lastElement && lastElement.step === 'step43' || lastElement.step === 'step45' || lastElement.step === 'step410' || lastElement.step === 'step412') {
                $('.wizard4 > .wizard4-subtotal').addClass('display-none');
            }

            setFrameOptions(lensSelection, basicPrice);
            changeHeader(lensSelection, wizardHeader);

            // countAddPrice(lensSelection);
        });

        $('.wizard-header-desktop').click(function () {
            while (!!lensSelection) {
                wizardBackButton.trigger('click');
            }
        });

        $('.wizard2-back-button-desktop').click(function () {
            wizardBackButton.trigger('click');
        });

        $('.add-to-cart').click(function () {
            addToCart(lensSelection, prescription);
        });

///////////////////////////////////////////////////////////// wizard1 start

        $('.wizard1 input[name=usage]').click(function () {
            $('.usage-multifocal').addClass('display-none');
            $('.usage-driving').addClass('display-none');
            $('.wizard2-bottom-buttons .wizard2-continue').removeClass('display-none');
            $('.multifocal-item').removeClass('active-open');
            $('.driving-item').removeClass('active-open');

            let name = $('.label-info p:first-child', $(this).next()).text().split('\n')[0];

            if (this.value === 'distance' || this.value === 'reading') {
                lensSelection.push({
                    'step': 'step1',
                    'fullName': name,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step1').addClass('display-none');
                $('.step2').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            } else if (this.value === 'multifocal') {
                $('.usage-multifocal').removeClass('display-none');
                $('.multifocal-item').addClass('active-open');

                $('input[name=usage-multifocal]').click(function () {
                    let multiAp = Number($(this).attr('data-ad'));

                    if (lensSelection.length === 0) {
                        lensSelection.push({
                            'step': 'step1',
                            'fullName': name + '  ' + $(this).next().text().split(' ')[0],
                            'ap': multiAp
                        });
                    }

                    $('.step1').addClass('display-none');
                    $('.step2').removeClass('display-none');
                    $('.usage-multifocal').addClass('display-none');
                    setFrameOptions(lensSelection, basicPrice);
                });
            } else if (this.value === 'driving') {
                $('.usage-driving').removeClass('display-none');
                $('.driving-item').addClass('active-open');

                $('input[name=usage-driving]').click(function () {

                    if (lensSelection.length === 0) {
                        lensSelection.push({
                            'step': 'step1',
                            'fullName': name + '  ' + $(this).next().text().split(' ')[0],
                            'ap': $(this).attr('data-ad') || 0
                        });
                    }

                    $('.step1').addClass('display-none');
                    $('.step2').removeClass('display-none');
                    $('.usage-driving').addClass('display-none');
                    setFrameOptions(lensSelection, basicPrice);
                });
            } else {
                if (lensSelection.length === 0) {
                    lensSelection.push({
                        'step': 'step1',
                        'fullName': name,
                        'ap': 0
                    });
                }

                $('.step1').addClass('display-none');
                $('.step34').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            }

            changeHeader(lensSelection, wizardHeader);

            if (window.innerWidth <= 767) {
                $('.wizard1-subtotal').addClass('display-none');
            }


            // setCurrentPrice(lensSelection, basicPrice);
            // setFrameOptions(lensSelection);
        });

/////////////////////////// wizard1 finish

//////////////////////////////////////////wizard2 start

        $('.wizard2 .continue-button').click(function () {
            $('.wizard3 .step-heading-wrapper').removeClass('display-none');
            $('.wizard3 .wizard2-bottom-buttons').removeClass('display-none');

            if (window.innerWidth <= 767) {
                // $('.wizard2-subtotal').addClass('display-none');
                $('.wizard3-subtotal').removeClass('display-none');
            }

            if (lensSelection[0].fullName !== 'Non prescription') {
                $('.total-prescriptions').removeClass('display-none');
                $('.table-name').removeClass('display-none');

                $('.sm-distancereading-clear>div').addClass('disabled');
                $('.l-distancereading-clear>div').addClass('disabled');
                $('.sm-distancereading-bluelight>div').addClass('disabled');
                $('.l-distancereading-bluelight>div').addClass('disabled');
                $('.sm-distancereading-sun-thickness>div').addClass('disabled');
                $('.l-distancereading-sun-thickness>div').addClass('disabled');
                $('.sm-distancereading-ajusting-thickness>div').addClass('disabled');
                $('.l-distancereading-ajusting-thickness>div').addClass('disabled');
                $('.sm-distancereading-ajusting-thickness2>div').addClass('disabled');
                $('.l-distancereading-ajusting-thickness2>div').addClass('disabled');
                $('.sm-multifocal-clear>div').addClass('disabled');
                $('.l-multifocal-clear>div').addClass('disabled');
                $('.sm-multifocal-bluelight>div').addClass('disabled');
                $('.l-multifocal-bluelight>div').addClass('disabled');
                $('.sm-multifocal-sun-thickness>div').addClass('disabled');
                $('.l-multifocal-sun-thickness>div').addClass('disabled');
                $('.sm-multifocal-ajusting-thickness>div').addClass('disabled');
                $('.sm-multifocal-ajusting-thickness2>div').addClass('disabled');
                $('.l-multifocal-ajusting-thickness>div').addClass('disabled');
                $('.l-multifocal-ajusting-thickness2>div').addClass('disabled');
                $('.sm-driving-single-clear>div').addClass('disabled');
                $('.l-driving-single-clear>div').addClass('disabled');
                $('.sm-driving-single-sun>div').addClass('disabled');
                $('.l-driving-single-sun>div').addClass('disabled');
                $('.sm-driving-single-ajusting>div').addClass('disabled');
                $('.l-driving-single-ajusting>div').addClass('disabled');
                $('.sm-driving-multifocal-clear>div').addClass('disabled');
                $('.l-driving-multifocal-clear>div').addClass('disabled');
                $('.sm-driving-multifocal-sun>div').addClass('disabled');
                $('.l-driving-multifocal-sun>div').addClass('disabled');
                $('.sm-driving-multifocal-ajusting>div').addClass('disabled');
                $('.l-driving-multifocal-ajusting>div').addClass('disabled');
            }


            if ($('#add-prism').prop('checked') === true) {
                lensSelection.push({
                    'step': 'step2',
                    'ap': 19
                });
            } else {
                lensSelection.push({
                    'step': 'step2',
                    'ap': 0
                });
            }
            setFrameOptions(lensSelection, basicPrice);

            ///save prescription start

            prescription.odSph = $('#od-sph').val();
            prescription.odCyl = $('#od-cyl').val();
            prescription.osSph = $('#os-sph').val();
            prescription.osCyl = $('#os-cyl').val();
            prescription.odAxis = $('#od-axis').val();
            prescription.odAd = $('#od-ad').val();
            prescription.osAxis = $('#os-axis').val();
            prescription.osAd = $('#os-ad').val();
            prescription.pd1 = $('#pd-1').val();
            prescription.comment = $('#comment').val();

            if ($('#two-pd').prop('checked') === true) {
                prescription.pd2 = $('#pd-2').val();
            }

            if ($('#add-prism').prop('checked') === true) {
                prescription.odVertical = $('#od-vertical').val();
                prescription.odVerticalBasedirection = $('#od-v-basedirection').val();
                prescription.osVertical = $('#os-vertical').val();
                prescription.osVerticalBasedirection = $('#os-v-basedirection').val();
                prescription.odHorizontal = $('#od-horizontal').val();
                prescription.odHorizontalBasedirection = $('#od-h-basedirection').val();
                prescription.osHorizontal = $('#os-horizontal').val();
                prescription.osHorizontalBasedirection = $('#os-h-basedirection').val();
            }

            ///save prescription finish

            $('.total-prescriptions tr:nth-child(2) td:nth-child(2)').text(prescription.odSph);
            $('.total-prescriptions tr:nth-child(2) td:nth-child(3)').text(prescription.odCyl);
            $('.total-prescriptions tr:nth-child(2) td:nth-child(4)').text(prescription.odAxis);
            $('.total-prescriptions tr:nth-child(2) td:nth-child(5)').text(prescription.odAd);
            $('.total-prescriptions tr:nth-child(2) td:nth-child(6)').text(prescription.pd1);

            $('.total-prescriptions tr:nth-child(3) td:nth-child(2)').text(prescription.osSph);
            $('.total-prescriptions tr:nth-child(3) td:nth-child(3)').text(prescription.osCyl);
            $('.total-prescriptions tr:nth-child(3) td:nth-child(4)').text(prescription.osAxis);
            $('.total-prescriptions tr:nth-child(3) td:nth-child(5)').text(prescription.osAd);
            if (prescription.pd2) {
                $('.total-prescriptions tr:nth-child(3) td:nth-child(6)').text(prescription.pd2);
            }

            if (lensSelection[0].fullName === 'Reading' || lensSelection[0].fullName === 'Distance') {
                $('.step-wrapper').addClass('display-none');
                $('.step31').removeClass('display-none');
            } else if (lensSelection[0].fullName.indexOf('Multifocal') === 0) {
                $('.step-wrapper').addClass('display-none');
                $('.step32').removeClass('display-none');
            } else {
                $('.step-wrapper').addClass('display-none');
                $('.step33').removeClass('display-none');
            }

            changeHeader(lensSelection, wizardHeader);


            // s-m distance reading clear start

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -5 && +prescription.odSph <= 5)
                    || (+prescription.odCyl >= 0.25 && +prescription.odCyl <= 1 && +prescription.odSph >= -4 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -4.25 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -4.50 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -4.75 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -5 && +prescription.odSph <= 2.25))
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -5 && +prescription.osSph <= 5)
                    || (+prescription.osCyl >= 0.25 && +prescription.osCyl <= 1 && +prescription.osSph >= -4 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -4.25 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -4.50 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -4.75 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -5 && +prescription.osSph <= 2.25))
            ) {
                $('.sm-distancereading-clear>div[data-label=15s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -5 && +prescription.odSph <= 5)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -4 && +prescription.odSph <= 1.75)
                    || (+prescription.odCyl === 0.5 && ((+prescription.odSph >= 0 && +prescription.odSph <= 1.5) || (+prescription.odSph >= -4 && +prescription.odSph <= -0.5)))
                    || (+prescription.odCyl === 0.75 && ((+prescription.odSph >= 0 && +prescription.odSph <= 1.25) || (+prescription.odSph >= -4 && +prescription.odSph <= -0.75)))
                    || (+prescription.odCyl === 1 && ((+prescription.odSph >= 0 && +prescription.odSph <= 1) || (+prescription.odSph >= -4 && +prescription.odSph <= -1)))
                    || (+prescription.odCyl === 1.25 && ((+prescription.odSph >= 0 && +prescription.odSph <= 0.75) || (+prescription.odSph >= -4 && +prescription.odSph <= -1.25)))
                    || (+prescription.odCyl === 1.5 && ((+prescription.odSph >= 0 && +prescription.odSph <= 0.5) || (+prescription.odSph >= -4 && +prescription.odSph <= -1.5)))
                    || (+prescription.odCyl === 1.75 && ((+prescription.odSph >= 0 && +prescription.odSph <= 0.25) || (+prescription.odSph >= -4 && +prescription.odSph <= -1.75)))
                    || (+prescription.odCyl === 2 && +prescription.odSph === 0))
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -5 && +prescription.osSph <= 5)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -4 && +prescription.osSph <= 1.75)
                    || (+prescription.osCyl === 0.5 && ((+prescription.osSph >= 0 && +prescription.osSph <= 1.5) || (+prescription.osSph >= -4 && +prescription.osSph <= -0.5)))
                    || (+prescription.osCyl === 0.75 && ((+prescription.osSph >= 0 && +prescription.osSph <= 1.25) || (+prescription.osSph >= -4 && +prescription.osSph <= -0.75)))
                    || (+prescription.osCyl === 1 && ((+prescription.osSph >= 0 && +prescription.osSph <= 1) || (+prescription.osSph >= -4 && +prescription.osSph <= -1)))
                    || (+prescription.osCyl === 1.25 && ((+prescription.osSph >= 0 && +prescription.osSph <= 0.75) || (+prescription.osSph >= -4 && +prescription.osSph <= -1.25)))
                    || (+prescription.osCyl === 1.5 && ((+prescription.osSph >= 0 && +prescription.osSph <= 0.5) || (+prescription.osSph >= -4 && +prescription.osSph <= -1.5)))
                    || (+prescription.osCyl === 1.75 && ((+prescription.osSph >= 0 && +prescription.osSph <= 0.25) || (+prescription.osSph >= -4 && +prescription.osSph <= -1.75)))
                    || (+prescription.osCyl === 2 && +prescription.osSph === 0))
            ) {
                $('.sm-distancereading-clear>div[data-label=16s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl === 0
                    &&
                    (
                        (+prescription.odSph >= -6 && +prescription.odSph <= -3)
                        || +prescription.odSph === -8
                        || +prescription.odSph === -7.5
                        || +prescription.odSph === -7
                        || +prescription.odSph === -6.5
                    )
                )
                &&
                (
                    +prescription.osCyl === 0
                    &&
                    (
                        (+prescription.osSph >= -6 && +prescription.osSph <= -3)
                        || +prescription.osSph === -8
                        || +prescription.osSph === -7.5
                        || +prescription.osSph === -7
                        || +prescription.osSph === -6.5
                    )
                )
            ) {
                $('.sm-distancereading-clear>div[data-label=167s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5 && +prescription.odCyl <= 0
                    &&
                    +prescription.odSph >= -8 && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5 && +prescription.osCyl <= 0
                    &&
                    +prescription.osSph >= -8 && +prescription.osSph <= 12
                )
            ) {
                $('.sm-distancereading-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5 && +prescription.odCyl <= 0
                    &&
                    +prescription.odSph >= -12 && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5 && +prescription.osCyl <= 0
                    &&
                    +prescription.osSph >= -12 && +prescription.osSph <= 8.5
                )
            ) {
                $('.sm-distancereading-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5 && +prescription.odCyl <= 0
                    &&
                    +prescription.odSph >= -12 && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5 && +prescription.osCyl <= 0
                    &&
                    +prescription.osSph >= -12 && +prescription.osSph <= 10
                )
            ) {
                $('.sm-distancereading-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5 && +prescription.odCyl <= 0
                    &&
                    +prescription.odSph >= -16 && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4.5 && +prescription.osCyl <= 0
                    &&
                    +prescription.osSph >= -16 && +prescription.osSph <= 17
                )
            ) {
                $('.sm-distancereading-clear>div[data-label=174l]').removeClass('disabled');
            }

            // s-m distance reading clear finish

            // s-m distance reading bluelight start

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -4 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -4.25 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -4.5 && +prescription.odSph <= 2)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -4.75 && +prescription.odSph <= 1.75)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -5 && +prescription.odSph <= 1.5)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -5.25 && +prescription.odSph <= 1.25)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -5.5 && +prescription.odSph <= 1)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -5.75 && +prescription.odSph <= 0.75)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -6 && +prescription.odSph <= 0.5)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -4 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -4.25 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -4.5 && +prescription.osSph <= 2)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -4.75 && +prescription.osSph <= 1.75)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -5 && +prescription.osSph <= 1.5)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -5.25 && +prescription.osSph <= 1.25)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -5.5 && +prescription.osSph <= 1)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -5.75 && +prescription.osSph <= 0.75)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -6 && +prescription.osSph <= 0.5)
                )
            ) {
                $('.sm-distancereading-bluelight>div[data-label=15s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl === 0
                    && +prescription.odSph >= -4
                    && +prescription.odSph <= 3.5
                )
                &&
                (
                    +prescription.osCyl === 0
                    && +prescription.osSph >= -4
                    && +prescription.osSph <= 3.5
                )
            ) {
                $('.sm-distancereading-bluelight>div[data-label=16s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-distancereading-bluelight>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.sm-distancereading-bluelight>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-distancereading-bluelight>div[data-label=167l]').removeClass('disabled');
            }

            // s-m distance reading bluelight finish

            // s-m distance reading sun start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -7
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -7
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=15lp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 7
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=16lp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= 0
                    && +prescription.odCyl <= 2
                    && +prescription.odSph >= -6
                    && +prescription.odSph <= 4
                )
                &&
                (
                    +prescription.osCyl >= 0
                    && +prescription.osCyl <= 2
                    && +prescription.osSph >= -6
                    && +prescription.osSph <= 4
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=15s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -8 && +prescription.odSph <= 6)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -8.25 && +prescription.odSph <= 5.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -8.5 && +prescription.odSph <= 5.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -8.75 && +prescription.odSph <= 5.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -9 && +prescription.odSph <= 5)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -9.25 && +prescription.odSph <= 4.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -9.5 && +prescription.odSph <= 4.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -9.75 && +prescription.odSph <= 4.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -10 && +prescription.odSph <= 4))
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -8 && +prescription.osSph <= 6)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -8.25 && +prescription.osSph <= 5.75)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -8.5 && +prescription.osSph <= 5.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -8.75 && +prescription.osSph <= 5.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -9 && +prescription.osSph <= 5)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -9.25 && +prescription.osSph <= 4.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -9.5 && +prescription.osSph <= 4.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -9.75 && +prescription.osSph <= 4.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -10 && +prescription.osSph <= 4))
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=16s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-distancereading-sun-thickness>div[data-label=167l]').removeClass('disabled');
            }

            // s-m distance reading sun finish

            // s-m distance reading ajusting gen 8 thickness start

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -4 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -4 && +prescription.odSph <= 3.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -4 && +prescription.odSph <= 3.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -4 && +prescription.odSph <= 3.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -4 && +prescription.odSph <= 3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -4 && +prescription.odSph <= 2.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -4 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -4 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -4 && +prescription.odSph <= 2)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -4 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -4 && +prescription.osSph <= 3.75)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -4 && +prescription.osSph <= 3.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -4 && +prescription.osSph <= 3.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -4 && +prescription.osSph <= 3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -4 && +prescription.osSph <= 2.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -4 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -4 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -4 && +prescription.osSph <= 2)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=15s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -6 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -6.25 && +prescription.odSph <= 3.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -6.5 && +prescription.odSph <= 3.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -6.75 && +prescription.odSph <= 3.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -7 && +prescription.odSph <= 3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -7.25 && +prescription.odSph <= 2.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -7.5 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -7.75 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -8 && +prescription.odSph <= 2)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -6 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -6.25 && +prescription.osSph <= 3.75)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -6.5 && +prescription.osSph <= 3.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -6.75 && +prescription.osSph <= 3.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -7 && +prescription.osSph <= 3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -7.25 && +prescription.osSph <= 2.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -7.5 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -7.75 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -8 && +prescription.osSph <= 2)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=16s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -9.75 && +prescription.odSph <= -2)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -10 && +prescription.odSph <= -2.25)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -10.25 && +prescription.odSph <= -2.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -10.5 && +prescription.odSph <= -2.75)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -10.75 && +prescription.odSph <= -3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -11 && +prescription.odSph <= -3.25)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -11.25 && +prescription.odSph <= -3.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -11.5 && +prescription.odSph <= -3.75)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -11.75 && +prescription.odSph <= -4)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -9.75 && +prescription.osSph <= -2)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -10 && +prescription.osSph <= -2.25)
                    || (+prescription.osCyl === 0.5 && +prescription.odSph >= -10.25 && +prescription.odSph <= -2.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -10.5 && +prescription.osSph <= -2.75)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -10.75 && +prescription.osSph <= -3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -11 && +prescription.osSph <= -3.25)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -11.25 && +prescription.osSph <= -3.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -11.5 && +prescription.osSph <= -3.75)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -11.75 && +prescription.osSph <= -4)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=167s]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -16
                    && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -16
                    && +prescription.osSph <= 17
                )
            ) {
                $('.sm-distancereading-ajusting-thickness>div[data-label=174l]').removeClass('disabled');
            }

            // s-m distance reading ajusting gen 8 thickness finish

            // s-m distance reading ajusting xtractive thickness 2 start

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -4 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -4 && +prescription.odSph <= 3.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -4 && +prescription.odSph <= 3.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -4 && +prescription.odSph <= 3.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -4 && +prescription.odSph <= 3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -4 && +prescription.odSph <= 2.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -4 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -4 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -4 && +prescription.odSph <= 2)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -4 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -4 && +prescription.osSph <= 3.75)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -4 && +prescription.osSph <= 3.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -4 && +prescription.osSph <= 3.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -4 && +prescription.osSph <= 3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -4 && +prescription.osSph <= 2.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -4 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -4 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -4 && +prescription.osSph <= 2)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=15sx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -6 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -6.25 && +prescription.odSph <= 3.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -6.5 && +prescription.odSph <= 3.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -6.75 && +prescription.odSph <= 3.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -7 && +prescription.odSph <= 3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -7.25 && +prescription.odSph <= 2.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -7.5 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -7.75 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -8 && +prescription.odSph <= 2)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -6 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -6.25 && +prescription.osSph <= 3.75)
                    || (+prescription.osCyl === 0.5 && +prescription.osSph >= -6.5 && +prescription.osSph <= 3.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -6.75 && +prescription.osSph <= 3.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -7 && +prescription.osSph <= 3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -7.25 && +prescription.osSph <= 2.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -7.5 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -7.75 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= -8 && +prescription.osSph <= 2)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=16sx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                ((+prescription.odCyl === 0 && +prescription.odSph >= -6 && +prescription.odSph <= 4)
                    || (+prescription.odCyl === 0.25 && +prescription.odSph >= -6.25 && +prescription.odSph <= 3.75)
                    || (+prescription.odCyl === 0.5 && +prescription.odSph >= -6.5 && +prescription.odSph <= 3.5)
                    || (+prescription.odCyl === 0.75 && +prescription.odSph >= -6.75 && +prescription.odSph <= 3.25)
                    || (+prescription.odCyl === 1 && +prescription.odSph >= -7 && +prescription.odSph <= 3)
                    || (+prescription.odCyl === 1.25 && +prescription.odSph >= -7.25 && +prescription.odSph <= 2.75)
                    || (+prescription.odCyl === 1.5 && +prescription.odSph >= -7.5 && +prescription.odSph <= 2.5)
                    || (+prescription.odCyl === 1.75 && +prescription.odSph >= -7.75 && +prescription.odSph <= 2.25)
                    || (+prescription.odCyl === 2 && +prescription.odSph >= -8 && +prescription.odSph <= 2)
                )
                &&
                ((+prescription.osCyl === 0 && +prescription.osSph >= -6 && +prescription.osSph <= 4)
                    || (+prescription.osCyl === 0.25 && +prescription.osSph >= -6.25 && +prescription.osSph <= 3.75)
                    || (+prescription.osCyl === 0.5 && +prescription.odSph >= -6.5 && +prescription.odSph <= 3.5)
                    || (+prescription.osCyl === 0.75 && +prescription.osSph >= -6.75 && +prescription.osSph <= 3.25)
                    || (+prescription.osCyl === 1 && +prescription.osSph >= -7 && +prescription.osSph <= 3)
                    || (+prescription.osCyl === 1.25 && +prescription.osSph >= -7.25 && +prescription.osSph <= 2.75)
                    || (+prescription.osCyl === 1.5 && +prescription.osSph >= -7.5 && +prescription.osSph <= 2.5)
                    || (+prescription.osCyl === 1.75 && +prescription.osSph >= -7.75 && +prescription.osSph <= 2.25)
                    || (+prescription.osCyl === 2 && +prescription.osSph >= 8 && +prescription.osSph <= 2)
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=16asx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=15lx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=16lx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= 10
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= 10
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-distancereading-ajusting-thickness2>div[data-label=167lx]').removeClass('disabled');
            }

            // s-m distance reading ajusting xtractive thickness 2 finish

            // s-m multifocal clear start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-multifocal-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-multifocal-clear>div[data-label=174l]').removeClass('disabled');
            }

            // s-m multifocal clear finish

            // s-m multifocal blue light start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-bluelight>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-bluelight>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-multifocal-bluelight>div[data-label=167l]').removeClass('disabled');
            }

            // s-m multifocal blue light finish

            // s-m multifocal sun start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-sun-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-sun-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-multifocal-sun-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -6
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -6
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-multifocal-sun-thickness>div[data-label=15lp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-multifocal-sun-thickness>div[data-label=16lp]').removeClass('disabled');
            }

            // s-m multifocal sun finish

            // s-m multifocal ajusting gen8 start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-ajusting-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-multifocal-ajusting-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-multifocal-ajusting-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-multifocal-ajusting-thickness>div[data-label=174l]').removeClass('disabled');
            }

            // s-m multifocal ajusting gen8 finish

            // s-m multifocal ajusting xtctive start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-multifocal-ajusting-thickness2>div[data-label=15lx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-multifocal-ajusting-thickness2>div[data-label=16lx]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                console.log(+prescription.osSph)
                $('.sm-multifocal-ajusting-thickness2>div[data-label=167lx]').removeClass('disabled');
            }

            // s-m multifocal ajusting xtctive finish

            // s-m driving single clear start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-driving-single-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.sm-driving-single-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-driving-single-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -16
                    && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -16
                    && +prescription.osSph <= 17
                )
            ) {
                $('.sm-driving-single-clear>div[data-label=174l]').removeClass('disabled');
            }

            // s-m driving single clear finish

            // s-m driving single sun start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -7
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -7
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=15ln]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 7
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=16ln]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.sm-driving-single-sun>div[data-label=15ld]').removeClass('disabled');
            }

            // s-m driving single sun finish

            // s-m driving single light ajusting start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= 10
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= 10
                    && +prescription.osSph <= 12
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=15lxp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -15
                    && +prescription.odSph <= 8.75
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -15
                    && +prescription.osSph <= 8.75
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=16lxp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 7
                )
            ) {
                $('.sm-driving-single-ajusting>div[data-label=167lxp]').removeClass('disabled');
            }

            // s-m driving single light ajusting finish

            // s-m driving multi clear start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-multifocal-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-multifocal-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-driving-multifocal-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.sm-driving-multifocal-clear>div[data-label=174l]').removeClass('disabled');
            }

            // s-m driving multi clear finish

            // s-m driving multi sun start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -6
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -6
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=15ln]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=16ln]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.sm-driving-multifocal-sun>div[data-label=15ld]').removeClass('disabled');
            }

            // s-m driving multi sun finish

            // s-m driving multi light ajusting start

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=15lxp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -15
                    && +prescription.odSph <= 8.75
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -15
                    && +prescription.osSph <= 8.75
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=16lxp]').removeClass('disabled');
            }

            if (
                (size === 's' || size === 'm')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 7
                )
            ) {
                $('.sm-driving-multifocal-ajusting>div[data-label=167lxp]').removeClass('disabled');
            }

            // s-m driving multi light ajusting finish




           /////////////////////////////////////////// large


            // l distance reading clear start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-distancereading-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.l-distancereading-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-distancereading-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -16
                    && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -16
                    && +prescription.osSph <= 17
                )
            ) {
                $('.l-distancereading-clear>div[data-label=174l]').removeClass('disabled');
            }

            // l distance reading clear finish


            // l distance reading bluelight start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-distancereading-bluelight>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.l-distancereading-bluelight>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-distancereading-bluelight>div[data-label=167l]').removeClass('disabled');
            }

            // l distance reading bluelight finish


            // l distance reading sun start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-distancereading-sun-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.l-distancereading-sun-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-distancereading-sun-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -7
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -7
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-distancereading-sun-thickness>div[data-label=15lp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 7
                )
            ) {
                $('.l-distancereading-sun-thickness>div[data-label=16lp]').removeClass('disabled');
            }

            // l distance reading sun finish


            // l distance reading ajusting gen8 start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-distancereading-ajusting-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-distancereading-ajusting-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-distancereading-ajusting-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -16
                    && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -16
                    && +prescription.osSph <= 17
                )
            ) {
                $('.l-distancereading-ajusting-thickness>div[data-label=174l]').removeClass('disabled');
            }

            // l distance reading ajusting gen8 finish

            // l distance reading ajusting xtractive start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-distancereading-ajusting-thickness2>div[data-label=15lx]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-distancereading-ajusting-thickness2>div[data-label=16lx]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= 10
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= 10
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-distancereading-ajusting-thickness2>div[data-label=167lx]').removeClass('disabled');
            }

            // l distance reading ajusting xtractive finish

            // l multifocal clear start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-multifocal-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-multifocal-clear>div[data-label=174l]').removeClass('disabled');
            }

            // l multifocal clear finish

            // l multifocal bluelight start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-bluelight>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-bluelight>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-multifocal-bluelight>div[data-label=167l]').removeClass('disabled');
            }

            // l multifocal bluelight finish

            // l multifocal sun start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-sun-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-sun-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-multifocal-sun-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -6
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -6
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-multifocal-sun-thickness>div[data-label=15lp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-multifocal-sun-thickness>div[data-label=16lp]').removeClass('disabled');
            }

            // l multifocal sun finish

            // l multifocal ajusting gen8 start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-ajusting-thickness>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-multifocal-ajusting-thickness>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-multifocal-ajusting-thickness>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-multifocal-ajusting-thickness>div[data-label=174l]').removeClass('disabled');
            }

            // l multifocal ajusting gen8 finish

            // l multifocal ajusting xtractive start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-multifocal-ajusting-thickness2>div[data-label=15lx]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-multifocal-ajusting-thickness2>div[data-label=16lx]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-multifocal-ajusting-thickness2>div[data-label=167lx]').removeClass('disabled');
            }

            // l multifocal ajusting xtractive finish

            // l driving single clear start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-driving-single-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.l-driving-single-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-driving-single-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -16
                    && +prescription.odSph <= 17
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -16
                    && +prescription.osSph <= 17
                )
            ) {
                $('.l-driving-single-clear>div[data-label=174l]').removeClass('disabled');
            }

            // l driving single clear finish

            // l driving single sun start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-driving-single-sun>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8.5
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8.5
                )
            ) {
                $('.l-driving-single-sun>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-driving-single-sun>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -7
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -7
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-single-sun>div[data-label=15ln]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 7
                )
            ) {
                $('.l-driving-single-sun>div[data-label=16ln]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.l-driving-single-sun>div[data-label=15ld]').removeClass('disabled');
            }

            // l driving single sun finish

            // l driving single ajusting start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -10
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -10
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4.5
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= 10
                    && +prescription.odSph <= 12
                )
                &&
                (
                    +prescription.osCyl >= -4.5
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= 10
                    && +prescription.osSph <= 12
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=15lxp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -15
                    && +prescription.odSph <= 8.75
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -15
                    && +prescription.osSph <= 8.75
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=16lxp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 7
                )
            ) {
                $('.l-driving-single-ajusting>div[data-label=167lxp]').removeClass('disabled');
            }

            // l driving single ajusting finish

            // l driving multifocal clear start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-multifocal-clear>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-multifocal-clear>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-driving-multifocal-clear>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 10
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 10
                )
            ) {
                $('.l-driving-multifocal-clear>div[data-label=174l]').removeClass('disabled');
            }

            // l driving multifocal clear finish

            // l driving multifocal sun start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -6
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -6
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=15ln]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=16ln]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 6
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 6
                )
            ) {
                $('.l-driving-multifocal-sun>div[data-label=15ld]').removeClass('disabled');
            }

            // l driving multifocal sun finish

            // l driving multifocal ajusting start

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -8
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -8
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=15l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 5
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=16l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11.5
                    && +prescription.odSph <= 8
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11.5
                    && +prescription.osSph <= 8
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=167l]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -11
                    && +prescription.odSph <= 6.5
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -11
                    && +prescription.osSph <= 6.5
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=15lxp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -15
                    && +prescription.odSph <= 8.75
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -15
                    && +prescription.osSph <= 8.75
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=16lxp]').removeClass('disabled');
            }

            if (
                (size === 'l')
                &&
                (
                    +prescription.odCyl >= -4
                    && +prescription.odCyl <= 0
                    && +prescription.odSph >= -12
                    && +prescription.odSph <= 7
                )
                &&
                (
                    +prescription.osCyl >= -4
                    && +prescription.osCyl <= 0
                    && +prescription.osSph >= -12
                    && +prescription.osSph <= 7
                )
            ) {
                $('.l-driving-multifocal-ajusting>div[data-label=167lxp]').removeClass('disabled');
            }
        });

        $('.saved-select').change(function () {
            let currentOption = $(`option[value="${$(this).val()}"]`, $(this));

            $('#od-sph').val(currentOption.attr('data-od-sph')).trigger('change');
            $('#od-cyl').val(currentOption.attr('data-od-cyl')).trigger('change');
            $('#od-axis').val(currentOption.attr('data-od-axis')).trigger('change');
            $('#od-ad').val(currentOption.attr('data-od-ad')).trigger('change');
            $('#os-sph').val(currentOption.attr('data-os-sph')).trigger('change');
            $('#os-cyl').val(currentOption.attr('data-os-cyl')).trigger('change');
            $('#os-axis').val(currentOption.attr('data-os-axis')).trigger('change');
            $('#os-ad').val(currentOption.attr('data-os-ad')).trigger('change');
            $('#pd-1').val(currentOption.attr('data-pd-1')).trigger('change');
            $('#pd-2').val(currentOption.attr('data-pd-2')).trigger('change');
            $('#comment').val(currentOption.attr('data-comment')).trigger('change');
            $('#od-vertical').val(currentOption.attr('data-od-vertical')).trigger('change');
            $('#od-v-basedirection').val(currentOption.attr('data-od-v-basedirection')).trigger('change');
            $('#os-vertical').val(currentOption.attr('data-os-vertical')).trigger('change');
            $('#os-v-basedirection').val(currentOption.attr('data-os-v-basedirection')).trigger('change');
            $('#od-horizontal').val(currentOption.attr('data-od-horizontal')).trigger('change');
            $('#od-h-basedirection').val(currentOption.attr('data-od-h-basedirection')).trigger('change');
            $('#os-horizontal').val(currentOption.attr('data-os-horizontal')).trigger('change');
            $('#os-h-basedirection').val(currentOption.attr('data-os-h-basedirection')).trigger('change');
        });

        $('.add-new-button').click(function () {
            $('.save-new-pre-label').removeClass('display-none');
            $('.wizard2-continue').text('Save and continue');
            $('.wizard2-continue').addClass('save-new-pre-button');
        });

        $(document).on('click', '.save-new-pre-button', function () {
            $(this).removeClass('save-new-pre-button');
            $('.wizard2-continue').text('Select and continue');
            $('.save-new-pre-label').addClass('display-none');

            let name = $('#new-name').val();

            $('#new-name').val();

            if (name) {
                $.ajax({
                    type: 'POST',
                    url: myajax.url,
                    data: {
                        'action': 'save_prescriptions',
                        'nonce_code': myajax.nonce,
                        'prescription_name': name,
                        'od-sph': $('#od-sph').val(),
                        'od-cyl': $('#od-cyl').val(),
                        'os-sph': $('#os-sph').val(),
                        'os-cyl': $('#os-cyl').val(),
                        'od-axis': $('#od-axis').val(),
                        'od-ad': $('#od-ad').val(),
                        'os-axis': $('#os-axis').val(),
                        'os-ad': $('#os-ad').val(),
                        'pd-1': $('#pd-1').val(),
                        'pd-2': $('#pd-2').val(),
                        'comment': $('#comment').val(),
                        'od-vertical': $('#od-vertical').val(),
                        'od-v-basedirection': $('#od-v-basedirection').val(),
                        'os-vertical': $('#os-vertical').val(),
                        'os-v-basedirection': $('#os-v-basedirection').val(),
                        'od-horizontal': $('#od-horizontal').val(),
                        'od-h-basedirection': $('#od-h-basedirection').val(),
                        'os-horizontal': $('#os-horizontal').val(),
                        'os-h-basedirection': $('#os-h-basedirection').val(),
                    },
                    success: function (data) {
                        // console.log(data);
                    }
                });
            }
        });

//////////////////////////////////////wizard2 finish


//////////////////////////////////////////wizard3 start

        $('.step31 input').click(function () {
            let current = $('.label-info p:first-child', $(this).next()).text().split('\n')[0];

            if (current === 'Clear') {
                lensSelection.push({
                    'step': 'step31',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step31').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (size === 'l') {
                    $('.step423').removeClass('display-none');
                } else {
                    $('.step41').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);


            } else if (current === 'Blue light blocking') {
                lensSelection.push({
                    'step': 'step31',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step31').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (size === 'l') {
                    $('.step424').removeClass('display-none');
                } else {
                    $('.step42').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);
            } else if (current === 'Sun') {
                if (lensSelection[lensSelection.length - 1].step !== 'step31') {
                    lensSelection.push({
                        'step': 'step31',
                        'fullName': current,
                        'ap': $(this).attr('data-ad') || 0
                    });
                }

                $('.step31').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step43').removeClass('display-none');

                setFrameOptions(lensSelection, basicPrice);
            } else {
                lensSelection.push({
                    'step': 'step31',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step31').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step45').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            }

            $('.wizard3 .step-heading-wrapper').addClass('display-none');

            changeHeader(lensSelection, wizardHeader);

            if (window.innerWidth <= 767) {
                $('.wizard3-subtotal').addClass('display-none');

                if (current === 'Clear' || current === 'Blue light blocking') {
                    $('.wizard4-subtotal').removeClass('display-none');
                } else {
                    $('.wizard4-subtotal', '.step43').removeClass('display-none');
                    $('.wizard4-subtotal', '.step45').removeClass('display-none');
                }
            }
        });

        $('.step32 input').click(function () {
            let current = $('.label-info p:first-child', $(this).next()).text().split('\n')[0];

            if (current === 'Clear') {
                lensSelection.push({
                    'step': 'step32',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step32').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (size === 'l') {
                    $('.step428').removeClass('display-none');
                } else {
                    $('.step48').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);
            } else if (current === 'Blue light blocking') {
                lensSelection.push({
                    'step': 'step32',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step32').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (size === 'l') {
                    $('.step429').removeClass('display-none');
                } else {
                    $('.step49').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);
            } else if (current === 'Sun') {
                lensSelection.push({
                    'step': 'step32',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step32').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step410').removeClass('display-none');

                setFrameOptions(lensSelection, basicPrice);
            } else {
                lensSelection.push({
                    'step': 'step32',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step32').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step412').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            }

            $('.wizard3 .step-heading-wrapper').addClass('display-none');

            changeHeader(lensSelection, wizardHeader);

            if (window.innerWidth <= 767) {
                $('.wizard3-subtotal').addClass('display-none');
                // $('.wizard4-subtotal').removeClass('display-none');

                if (current === 'Clear' || current === 'Blue light blocking') {
                    $('.wizard4-subtotal').removeClass('display-none');
                } else {
                    $('.wizard4-subtotal', '.step410').removeClass('display-none');
                    $('.wizard4-subtotal', '.step412').removeClass('display-none');
                }
            }
        });

        $('.step33 input').click(function () {
            let current = $('.label-info p:first-child', $(this).next()).text().split('\n')[0];

            if (current === 'Clear') {
                lensSelection.push({
                    'step': 'step33',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step33').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (lensSelection[0].fullName.indexOf('Single') > 0) {
                    if (size === 'l') {
                        $('.step433').removeClass('display-none');
                    } else {
                        $('.step415').removeClass('display-none');
                    }

                } else {
                    if (size === 'l') {
                        $('.step436').removeClass('display-none');
                    } else {
                        $('.step420').removeClass('display-none');
                    }

                }

                setFrameOptions(lensSelection, basicPrice);
            } else if (current === 'Driving + Sun') {
                lensSelection.push({
                    'step': 'step33',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step33').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (lensSelection[0].fullName.indexOf('Single') > 0) {
                    if (size === 'l') {
                        $('.step434').removeClass('display-none');
                    } else {
                        $('.step416').removeClass('display-none');
                    }

                } else {
                    if (size === 'l') {
                        $('.step437').removeClass('display-none');
                    } else {
                        $('.step421').removeClass('display-none');
                    }

                }

                setFrameOptions(lensSelection, basicPrice);
            } else {
                lensSelection.push({
                    'step': 'step33',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step33').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');

                if (lensSelection[0].fullName.indexOf('Single') > 0) {
                    if (size === 'l') {
                        $('.step435').removeClass('display-none');
                    } else {
                        $('.step417').removeClass('display-none');
                    }

                } else {
                    if (size === 'l') {
                        $('.step438').removeClass('display-none');
                    } else {
                        $('.step422').removeClass('display-none');
                    }

                }

                setFrameOptions(lensSelection, basicPrice);
            }

            $('.wizard3 .step-heading-wrapper').addClass('display-none');

            changeHeader(lensSelection, wizardHeader);

            if (window.innerWidth <= 767) {
                $('.wizard3-subtotal').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');
            }
        });

        $('.step34 input').click(function () {
            let current = $('.label-info p:first-child', $(this).next()).text().split('\n')[0];

            if (current === 'Clear') {
                lensSelection.push({
                    'step': 'step34',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step34').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step418').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            } else if (current === 'Blue light blocking') {
                lensSelection.push({
                    'step': 'step34',
                    'fullName': current,
                    'ap': $(this).attr('data-ad') || 0
                });

                $('.step34').addClass('display-none');
                $('.wizard3 .wizard2-bottom-buttons').addClass('display-none');
                $('.wizard4 .wizard2-bottom-buttons').removeClass('display-none');
                $('.step419').removeClass('display-none');
                setFrameOptions(lensSelection, basicPrice);
            }

            $('.wizard3 .step-heading-wrapper').addClass('display-none');

            changeHeader(lensSelection, wizardHeader);

            if (window.innerWidth <= 767) {
                $('.wizard3-subtotal').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');
            }
        });

//////////////////////////////////////wizard3 finish


//////////////////////////////////////////wizard4 start

        let mainStep = '';
        let tintStep = '';
        let tintMirrored = '';
        let tintAp = 0;
        let mirrorAp = 0;

        $('.step41 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step41') {
                lensSelection.push({
                    'step': 'step41',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step41').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step42 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step42') {
                lensSelection.push({
                    'step': 'step42',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step42').addClass('display-none');
            // $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step43 input[name=sm-distancereading-sun]').click(function() {
            tintAp = 0;
            mirrorAp = 0;
            let stepContext = $(this).parent();

            if ($('span:first-child', $(this).next()).text() !== mainStep) {
                $('.tint-colors label', $(this)).removeClass('checked-label');
                $('.continue-button', $('.step43')).prop('disabled', true);
                $('.mirrored').removeClass('step-active');
            }

            mainStep = $('span:first-child', $(this).next()).text();

            $('.tint-color-choose').addClass('display-none');
            $('.tint-color-choose', $(this).parent()).removeClass('display-none');
            $('.tint-color-mirrored').addClass('display-none');

            $('input[name=tint-color]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                tintAp = Number($(this).attr('data-ad'));

                tintStep = $(this).parent().text().trim();
                $('.tint-colors label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button',  $('.step43')).prop('disabled', false);

                $('.tint-color-mirrored').addClass('display-none');
                $('.tint-color-mirrored', stepContext).removeClass('display-none');

                if (mainStep === 'Mirrored') {
                    tintAp = 0;
                    mirrorAp = Number($(this).attr('data-ad'));
                    tintMirrored = '';
                    $('.continue-button', $('.step43')).prop('disabled', false);
                }
            });

            $('input[name=tint-mirror]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                mirrorAp = Number($(this).attr('data-ad'));
                tintMirrored = $(this).parent().text().trim();
                $('.tint-mirror label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button', $('.step43')).prop('disabled', false);
            });

            $('.continue-button', $('.step43')).click(function () {
                if (lensSelection[lensSelection.length - 1].step !== 'step43') {
                    if (tintMirrored !== '') {
                        lensSelection.push({
                            'step': 'step43',
                            'fullName': mainStep + ' - tint ' + tintStep + ' - mirrored ' + tintMirrored,
                            'ap': tintAp + mirrorAp || 0
                        });
                    } else {
                        lensSelection.push({
                            'step': 'step43',
                            'fullName': mainStep + ' - tint ' + tintStep,
                            'ap': tintAp + mirrorAp || 0
                        });
                    }
                }

                $('.step43').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');

                if (size === 'l') {
                    $('.step425').removeClass('display-none');
                } else {
                    $('.step44').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);
                changeHeader(lensSelection, wizardHeader);
            });
        });

        $('.step44 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step44') {
                lensSelection.push({
                    'step': 'step44',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step44').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step45 input[name=sm-distancereading-ajusting]').click(function() {
            let stepContext = $(this).parent();
            tintAp = 0;
            mirrorAp = 0;

            if ($('span:first-child', $(this).next()).text() !== mainStep) {
                $('.tint-colors label', $(this)).removeClass('checked-label');
                $('.continue-button', $('.step45')).prop('disabled', true);
                $('.mirrored').removeClass('step-active');
            }

            // mainStep = $(this).next().text();
            mainStep = $('span:first-child', $(this).next()).text();


            $('.tint-color-choose').addClass('display-none');
            $('.tint-color-choose', $(this).parent()).removeClass('display-none');
            $('.tint-color-mirrored').addClass('display-none');

            $('input[name=tint-color]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                tintAp = Number($(this).attr('data-ad'));
                tintStep = $(this).parent().text().trim();
                $('.tint-colors label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.tint-color-mirrored').addClass('display-none');
                $('.tint-color-mirrored', stepContext).removeClass('display-none');
                $('.continue-button', $('.step45')).prop('disabled', false);
            });

            $('input[name=tint-mirror]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                mirrorAp = Number($(this).attr('data-ad'));
                tintMirrored = $(this).parent().text().trim();
                $('.tint-mirror label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button', $('.step45')).prop('disabled', false);
            });

            $('.continue-button', $('.step45')).click(function () {
                if (lensSelection[lensSelection.length - 1].step !== 'step45') {
                    lensSelection.push({
                        'step': 'step45',
                        'fullName': mainStep + ' - tint ' + tintStep + ' - mirrored ' + tintMirrored,
                        // 'ap': $(this).attr('data-ad') || 0
                        'ap': tintAp + mirrorAp || 0
                    });
                }

                $('.step45').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');

                if (mainStep === 'Transitions Gen 8') {
                    if (size === 'l') {
                        $('.step426').removeClass('display-none');
                    } else {
                        $('.step46').removeClass('display-none');
                        $('.wizard4 > .wizard4-subtotal').removeClass('display-none');
                    }
                } else if (mainStep === 'Transition XTRActive') {
                    if (size === 'l') {
                        $('.step427').removeClass('display-none');
                    } else {
                        $('.step47').removeClass('display-none');
                        $('.wizard4 > .wizard4-subtotal').removeClass('display-none');
                    }
                }

                setFrameOptions(lensSelection, basicPrice);
                changeHeader(lensSelection, wizardHeader);
            });
        });

        $('.step46 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step46') {
                lensSelection.push({
                    'step': 'step46',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step46').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step47 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step47') {
                lensSelection.push({
                    'step': 'step47',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step47').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step48 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step48') {
                lensSelection.push({
                    'step': 'step48',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step48').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step49 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step49') {
                lensSelection.push({
                    'step': 'step49',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step49').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step410 input[name=sm-multifocal-sun]').click(function() {
            tintAp = 0;
            mirrorAp = 0;
            tintMirrored = '';
            let stepContext = $(this).parent();

            if ($('span:first-child', $(this).next()).text() !== mainStep) {
                $('.tint-colors label', $(this)).removeClass('checked-label');
                $('.continue-button', $('.step45')).prop('disabled', true);
                $('.mirrored').removeClass('step-active');
            }

            mainStep = $('span:first-child', $(this).next()).text();

            $('.tint-color-choose').addClass('display-none');
            $('.tint-color-choose', $(this).parent()).removeClass('display-none');
            $('.tint-color-mirrored').addClass('display-none');

            $('input[name=tint-color]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                tintAp = Number($(this).attr('data-ad'));
                tintStep = $(this).parent().text().trim();
                $('.tint-colors label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button', $('.step410')).prop('disabled', false);

                $('.tint-color-mirrored').addClass('display-none');
                $('.tint-color-mirrored', stepContext).removeClass('display-none');

                if (mainStep === 'Mirrored') {
                    tintAp = 0;
                    mirrorAp = Number($(this).attr('data-ad'));
                    tintMirrored = '';
                    $('.continue-button', $('.step410')).prop('disabled', false);
                }
            });

            $('input[name=tint-mirror]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                mirrorAp = 0;
                mirrorAp = Number($(this).attr('data-ad'));
                tintMirrored = $(this).parent().text().trim();
                $('.tint-mirror label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button', $('.step410')).prop('disabled', false);
            });

            $('.continue-button', $('.step410')).click(function () {
                if (lensSelection[lensSelection.length - 1].step !== 'step410') {
                    if (tintMirrored !== '') {
                        lensSelection.push({
                            'step': 'step410',
                            'fullName': mainStep + ' - tint ' + tintStep + ' - mirrored ' + tintMirrored,
                            'ap': tintAp + mirrorAp || 0
                        });
                    } else {
                        lensSelection.push({
                            'step': 'step410',
                            'fullName': mainStep + ' - tint ' + tintStep,
                            'ap': tintAp + mirrorAp || 0
                        });
                    }
                }

                $('.step410').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');

                if (size === 'l') {
                    $('.step430').removeClass('display-none');
                } else {
                    $('.step411').removeClass('display-none');
                }

                setFrameOptions(lensSelection, basicPrice);
                changeHeader(lensSelection, wizardHeader);
            });
        });

        $('.step411 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step411') {
                lensSelection.push({
                    'step': 'step411',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step411').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step412 input[name=sm-distancereading-ajusting412]').click(function() {
            tintAp = 0;
            mirrorAp = 0;
            tintMirrored = '';

            let stepContext = $(this).parent();

            if ($('span:first-child', $(this).next()).text() !== mainStep) {
                $('.tint-colors label', $(this)).removeClass('checked-label');
                $('.continue-button', $('.step412')).prop('disabled', true);
                $('.mirrored').removeClass('step-active');
            }

            mainStep = $('span:first-child', $(this).next()).text();
            // mainStep = $(this).next().text();

            $('.tint-color-choose').addClass('display-none');
            $('.tint-color-choose', $(this).parent()).removeClass('display-none');
            $('.tint-color-mirrored').addClass('display-none');

            $('input[name=tint-color]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                tintAp = Number($(this).attr('data-ad'));
                tintStep = $(this).parent().text().trim();
                $('.tint-colors label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.tint-color-mirrored').addClass('display-none');
                $('.tint-color-mirrored', stepContext).removeClass('display-none');
                $('.continue-button', $('.step412')).prop('disabled', false);
            });

            $('input[name=tint-mirror]', $(this).parent()).click(function () {
                $('.current-tint-color', $(this).parent().parent().prev()).text($(this).parent().text().split('<')[0].trim());

                // tintAp = 0;
                mirrorAp = 0;
                mirrorAp = Number($(this).attr('data-ad'));
                tintMirrored = $(this).parent().text().trim();
                $('.tint-mirror label').removeClass('checked-label');
                $(this).parent().addClass('checked-label');
                $('.continue-button', $('.step412')).prop('disabled', false);
            });

            $('.continue-button', $('.step412')).click(function () {
                if (lensSelection[lensSelection.length - 1].step !== 'step412') {
                    if (tintMirrored !== '') {
                        lensSelection.push({
                            'step': 'step412',
                            'fullName': mainStep + ' - tint ' + tintStep + ' - mirrored ' + tintMirrored,
                            'ap': tintAp + mirrorAp || 0
                        });
                    } else {
                        lensSelection.push({
                            'step': 'step412',
                            'fullName': mainStep + ' - tint ' + tintStep,
                            'ap': tintAp + mirrorAp || 0
                        });
                    }
                }

                $('.step412').addClass('display-none');
                $('.wizard4-subtotal').removeClass('display-none');

                if (mainStep === 'Transitions Gen 8') {
                    if (size === 'l') {
                        $('.step431').removeClass('display-none');
                    } else {
                        $('.step413').removeClass('display-none');
                    }

                } else if (mainStep === 'Transition XTRActive') {
                    if (size === 'l') {
                        $('.step432').removeClass('display-none');
                    } else {
                        $('.step414').removeClass('display-none');
                    }

                }

                setFrameOptions(lensSelection, basicPrice);
                changeHeader(lensSelection, wizardHeader);
            });
        });

        $('.step413 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step413') {
                lensSelection.push({
                    'step': 'step413',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step413').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step414 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step414') {
                lensSelection.push({
                    'step': 'step414',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step414').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step415 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step415') {
                lensSelection.push({
                    'step': 'step415',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step415').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step416 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step416') {
                lensSelection.push({
                    'step': 'step416',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step416').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step417 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step417') {
                lensSelection.push({
                    'step': 'step417',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step417').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step418 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step418') {
                lensSelection.push({
                    'step': 'step418',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step418').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step419 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step419') {
                lensSelection.push({
                    'step': 'step419',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step419').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step420 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step420') {
                lensSelection.push({
                    'step': 'step420',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step420').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step421 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step421') {
                lensSelection.push({
                    'step': 'step421',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step421').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step422 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step422') {
                lensSelection.push({
                    'step': 'step422',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step422').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step423 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step423') {
                lensSelection.push({
                    'step': 'step423',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step423').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step424 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step424') {
                lensSelection.push({
                    'step': 'step424',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step424').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step425 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step425') {
                lensSelection.push({
                    'step': 'step425',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step425').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step426 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step426') {
                lensSelection.push({
                    'step': 'step426',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step426').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step427 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step427') {
                lensSelection.push({
                    'step': 'step427',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step427').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step428 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step428') {
                lensSelection.push({
                    'step': 'step428',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step428').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step429 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step429') {
                lensSelection.push({
                    'step': 'step429',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step429').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step430 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step430') {
                lensSelection.push({
                    'step': 'step430',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step430').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step431 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step431') {
                lensSelection.push({
                    'step': 'step431',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step431').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step432 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step432') {
                lensSelection.push({
                    'step': 'step432',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step432').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step433 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step433') {
                lensSelection.push({
                    'step': 'step433',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step433').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step434 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step434') {
                lensSelection.push({
                    'step': 'step434',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step434').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step435 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step435') {
                lensSelection.push({
                    'step': 'step435',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step435').addClass('display-none');
            $('.wizard-review').removeClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step436 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step436') {
                lensSelection.push({
                    'step': 'step436',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step436').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step437 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step437') {
                lensSelection.push({
                    'step': 'step437',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step437').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

        $('.step438 input').click(function() {
            if (lensSelection[lensSelection.length - 1].step !== 'step438') {
                lensSelection.push({
                    'step': 'step438',
                    'fullName': $(this).next().text().split('\n')[0],
                    'ap': $(this).attr('data-ad') || 0
                });
            }

            $('.step438').addClass('display-none');
            setFrameOptions(lensSelection, basicPrice);
            $('.wizard-total-wrapper').addClass('display-block');
            $('.wizard-subtotal-mobile').addClass('display-none');
            $('.add-to-cart').removeClass('display-none');
            changeHeader(lensSelection, wizardHeader);
        });

//////////////////////////////////////wizard4 finish



/////////////////////////////////////prescription start

    $('.open-additional-prescriptions').click(function () {
        $('.prescription-additional-wrapper').toggleClass('display-none');
        $('svg', $(this)).toggleClass('open');
    });

    $('#two-pd').click(function () {
        if ($(this).prop('checked') === true) {
            $('#pd-2').removeClass('display-none');
        } else {
            $('#pd-2').addClass('display-none');
            // $('#pd-2').val([]);
        }

        $('.select-wrapper-left').toggleClass('display-none');
    });

    $('#add-prism').click(function () {
        if ($(this).prop('checked') === true) {
            $('.prism-wrapper').removeClass('display-none');
        } else {
            $('.prism-wrapper').addClass('display-none');
            // $('#od-vertical').val([]);
            // $('#od-v-basedirection').val([]);
            // $('#os-vertical').val([]);
            // $('#os-v-basedirection').val([]);
            // $('#od-horizontal').val([]);
            // $('#od-h-basedirection').val([]);
            // $('#os-horizontal').val([]);
            // $('#os-h-basedirection').val([]);
        }
    });

/////////////////////////////////////prescription finish



        // customAddToCard.click(() => {
        //     addToCardButton.trigger('click');
        // });

        $('.prescription-login-button').click(function () {
            $('.login-menu-open').trigger('click');
        });

    });

    function addToCart(lensSelection, prescription) {
        const addToCardButton = $('.single_add_to_cart_button');
        const lensesConfiguration = $('textarea[name=lenses_configuration]');
        const prescriptionsConfiguration = $('textarea[name=prescriptions]');
        const addPrice = $('input[name=additional_price]');
        let ap = 0;

        lensSelection.forEach((el, i) => {
            // if (el.fullName) {
            //     stringResult += el.fullName + '|';
            // }

            if (el.ap) {
                ap += Number(el.ap);
            }
        });

        prescriptionsConfiguration.val(JSON.stringify(prescription));
        lensesConfiguration.val(JSON.stringify(lensSelection));
        addPrice.val(ap);

        addToCardButton.trigger('click');
    }

    function countAddPrice(lensSelection) {
        const addPrice = $('input[name=additional_price]');
        let ap = 0;

        lensSelection.forEach((el, i) => {
            if (el.ap) {
                ap = Number(ap) + Number(el.ap);
            }
        });

        addPrice.val(ap);
    }

    function setCurrentPrice(lensSelection, basicPrice) {
        let price = basicPrice;

        lensSelection.forEach((el, i) => {
            if (el.ap) {
                price = Number(price) + Number(el.ap);
            }
        });

        $('.current-price').text(price.toFixed(2) + '€');
    }

    function changeHeader(lensSelection, wizardHeader) {
        let currentPage = lensSelection[lensSelection.length - 1];

        if (!!!currentPage) {
            wizardHeader.text('Usage');
            $('.wizard-circles .circle').removeClass('circle-active').removeClass('circle-completed');
            $('.wizard-circles .circle:nth-child(1)').addClass('circle-active');
        } else if (currentPage.step[4] === '1' && currentPage.fullName !== 'Non prescription') {
            wizardHeader.text('Your Prescription');
            $('.wizard-circles .circle').removeClass('circle-active').removeClass('circle-completed');
            $('.wizard-circles .circle:nth-child(1)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(2)').addClass('circle-active');
        } else if (currentPage.step[4] === '2' || currentPage.fullName === 'Non prescription') {
            wizardHeader.text('Lens Type');
            $('.wizard-circles .circle').removeClass('circle-active').removeClass('circle-completed');
            $('.wizard-circles .circle:nth-child(1)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(2)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(3)').addClass('circle-active');
        } else if (currentPage.step[4] === '3') {
            wizardHeader.text('Lens Thikness');
            $('.wizard-circles .circle').removeClass('circle-active').removeClass('circle-completed');
            $('.wizard-circles .circle:nth-child(1)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(2)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(3)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(4)').addClass('circle-active');
        } else if (currentPage.step[4] === '4') {
            wizardHeader.text('Review');
            $('.wizard-circles .circle').removeClass('circle-active').removeClass('circle-completed');
            $('.wizard-circles .circle:nth-child(1)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(2)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(3)').addClass('circle-completed');
            $('.wizard-circles .circle:nth-child(4)').addClass('circle-completed');
        }
    }

    function setFrameOptions(lensSelection, basicPrice) {
        const frameOptions = $('.frame-options');
        frameOptions.html('');

        lensSelection.forEach((item, index) => {


            if (item.step !== 'step2') {
                if (item.ap) {
                    let price = +item.ap

                    frameOptions.append('<div class="option-row with-price"><p class="option-name">' + item.fullName + '</p><p class="option-price">' + price.toFixed(2) + '€</p></div>');
                } else {
                    frameOptions.append('<div class="option-row without-price"><p class="option-name">' + item.fullName + '</p></div>');
                }
            }
        });

        setCurrentPrice(lensSelection, basicPrice);
    }

    function setPrices(sizeSelector, colorSelector, basicPriceElement, basicPriceHtml) {
        setTimeout(function () {
            let priceWithDiscount = $('.woocommerce-variation-price ins bdi').text().slice(0, -1) || $('.price.basic-price ins bdi').text().slice(0, -1);
            let priceFull = $('.woocommerce-variation-price del bdi').text().slice(0, -1) || $('.price.basic-price del bdi').text().slice(0, -1);
            let percents = ((priceFull - priceWithDiscount) / priceFull * 100).toFixed(0);
            let difference = (priceFull - priceWithDiscount).toFixed(2);

            if (priceWithDiscount && sizeSelector.val() && colorSelector.val()) {
                basicPriceElement.html(`<ins>${$('.woocommerce-variation-price ins bdi').html() || $('.price.basic-price ins bdi').html()}</ins> <del>${$('.woocommerce-variation-price del bdi').html() || $('.price.basic-price del bdi').html()}</del>`);
                $('.product-sale-text').removeClass('display-none');
                $('.product-sale-text per').text(percents);
                $('.product-sale-text dif').text(difference);
            } else if (!priceWithDiscount && (sizeSelector.val() && colorSelector.val())) {
                basicPriceElement.html(`<span class="bl">${$('.woocommerce-variation-price bdi').html() || basicPriceHtml}</span>`);
                $('.product-sale-text').addClass('display-none');
            } else {
                basicPriceElement.html(basicPriceHtml);
                $('.product-sale-text').addClass('display-none');
            }
        }, 200);
    }
})(jQuery);
