import '@scss/checkout';

/* eslint-disable */
/*eslint max-lines: 1000*/
(function ($) {
    document.addEventListener('DOMContentLoaded', function () {
        if (window.location.href.split('/')[5]) {
            window.location.replace(`/confirmation/?order=${window.location.href.split('/')[5]}`);
        }

        $('.show-notes').click(function () {
            $(this).toggle();
            $(this).next().toggle();
            $(this).next().next().toggle();
        });

        $('.hide-notes').click(function () {
            $(this).toggle();
            $(this).prev().toggle();
            $(this).prev().prev().toggle();
        });

        $('.order-notes').keyup(function () {
            $('#order_comments').val($(this).val());
        });

        setTimeout(function () {
            $('#billing_country').val($('.choose-country').val()).change();
            $('.woocommerce-billing-fields input').val('');
        }, 1000);

        $('.choose-country').change(function () {
            $('.next-step-button').prop('disabled', true);
            $('.next-step-button-desktop').prop('disabled', true);
            $('.shipping-summary').html('');

            $('#billing_country').val($('.choose-country').val()).change();

            if ($(this).val() === 'EE') {
                $('.omniva-shipping').removeClass('display-none');
                $('.smartpost-shipping').removeClass('display-none');
                $('.dhl-shipping').addClass('display-none');
                $('.local-pickup').removeClass('display-none');
                $('.shipping-tab2').removeClass('pointer-events-no');
            } else {
                $('.omniva-shipping').addClass('display-none');
                $('.smartpost-shipping').addClass('display-none');
                $('.dhl-shipping').removeClass('display-none');
                $('.local-pickup').addClass('display-none');
                $('.shipping-tab2').addClass('pointer-events-no');
            }

            if ($(this).val() !== 'EE' || $(this).val() !== 'LV' || $(this).val() !== 'LT') {
                $('.payment-method[data-payment=montonio]').addClass('display-none');
            }
        });

        $('.shipping-tab').click(function () {
            if ($(this).hasClass('not-active')) {
                $('.shipping-tab').addClass('not-active');
                $(this).removeClass('not-active');

                if (!$('.shipping-tab1').hasClass('not-active')) {
                    $('.tab1').removeClass('display-none');
                    $('.tab2').addClass('display-none');
                }

                if (!$('.shipping-tab2').hasClass('not-active')) {
                    $('.tab2').removeClass('display-none');
                    $('.tab1').addClass('display-none');
                }
            }
        });

        if ($('#shipping_method li:nth-child(1) .woocommerce-Price-amount').text()) {
            $('.omniva-shipping .shipping-cost .text').text($('#shipping_method li:nth-child(1) .woocommerce-Price-amount').text());
            $('.omniva-shipping .shipping-cost').addClass('not-free-shipping');
        }

        if ($('#shipping_method li:nth-child(2) .woocommerce-Price-amount').text()) {
            $('.smartpost-shipping .shipping-cost .text').text($('#shipping_method li:nth-child(2) .woocommerce-Price-amount').text()).addClass('not-free-shipping');
            $('.smartpost-shipping .shipping-cost').addClass('not-free-shipping');
        }

        let currentMethod = '';

        $('.omniva-shipping').click(function () {
            $('#shipping_method li:nth-child(1) input').trigger('click');

            if (currentMethod === 'smartpost') {
                $('#location-omniva').children().remove();
            }

            currentMethod = 'omniva';

            setTimeout(function () {
                $('#mp-wc-pickup-point-shipping-select option').clone().appendTo($('.omniva-shipping select'));
                $('#mp-wc-pickup-point-shipping-select option').clone().appendTo($('.unregistered-address select'));
            }, 1000);

            if (!$('.checkout-step1-wrapper').hasClass('logged')) {
                $('.omniva-shipping .unregistered-address').addClass('checked-method');
            }
        });

        $('.omniva-shipping').on('keyup', 'input', function () {
            $('#billing_email').val($('.omniva-shipping .email').val());
            $('#billing_first_name').val($('.omniva-shipping .first-name').val());
            $('#billing_last_name').val($('.omniva-shipping .last-name').val());
            $('#billing_phone').val($('.omniva-shipping .tel').val());
        });

        $('.omniva-shipping').on('change', 'select', function () {
            $('#mp-wc-pickup-point-shipping-select').val($(this).val()).change();

            if ($('.email', $('.shipping-body:not(.display-none)')).val()
            && $('.first-name', $('.shipping-body:not(.display-none)')).val()
            && $('.last-name', $('.shipping-body:not(.display-none)')).val()
            && $('.tel', $('.shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }

            $('.shipping-address-item').removeClass('chosen-address');
            $(this).closest('.shipping-address-item').addClass('chosen-address');

            $('.shipping-summary').html();
            $('.shipping-summary').append($('.chosen-address').parent().prev());
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address select option:selected').text() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .email').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .first-name').val() || ''} ${$('.chosen-address .last-name').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address input[type=tel]').val() || ''}</p>`);
        });

        $('.smartpost-shipping').click(function () {
            $('#shipping_method li:nth-child(2) input').trigger('click');

            if (currentMethod === 'omniva') {
                $('#location-smartpost').children().remove();
            }

            currentMethod = 'smartpost';

            setTimeout(function () {
                $('#mp-wc-pickup-point-shipping-select option').clone().appendTo($('.smartpost-shipping select'));
                $('#mp-wc-pickup-point-shipping-select option').clone().appendTo($('.unregistered-address select'));
            }, 1000);
        });

        $('.smartpost-shipping').on('keyup', 'input', function () {
            $('#billing_email').val($('.smartpost-shipping .email').val());
            $('#billing_first_name').val($('.smartpost-shipping .first-name').val());
            $('#billing_last_name').val($('.smartpost-shipping .last-name').val());
            $('#billing_phone').val($('.smartpost-shipping .tel').val());
        });

        $('.smartpost-shipping').on('change', 'select', function () {
            $('#mp-wc-pickup-point-shipping-select').val($(this).val()).change();


            if ($('.email', $('.shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.shipping-body:not(.display-none)')).val()
                && $('.tel', $('.shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }

            $('.shipping-address-item').removeClass('chosen-address');
            $(this).closest('.shipping-address-item').addClass('chosen-address');

            $('.shipping-summary').html('');
            $('.shipping-summary').append($('.chosen-address').parent().prev());
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address select option:selected').text() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .email').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .first-name').val() || ''} ${$('.chosen-address .last-name').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address input[type=tel]').val() || ''}</p>`);
        });

        $('.omniva-shipping').on('click', '.shipping-address-item', function () {
            $('.shipping-body', '.omniva-shipping').addClass('display-none');
            $('.shipping-body', $(this)).removeClass('display-none');
            $('.shipping-body-preview', '.omniva-shipping').removeClass('display-none');
            $('.shipping-body-preview', $(this)).addClass('display-none');

            $('.woocommerce-billing-fields input').val('');
            $('#billing_email').val($('.email', $(this)).val());
            $('#billing_first_name').val($('.first-name', $(this)).val());
            $('#billing_last_name').val($('.last-name', $(this)).val());
            $('#billing_phone').val($('.tel', $(this)).val());
        });

        $('.smartpost-shipping').on('click', '.shipping-address-item', function () {
            $('.shipping-body', '.smartpost-shipping').addClass('display-none');
            $('.shipping-body', $(this)).removeClass('display-none');
            $('.shipping-body-preview', '.smartpost-shipping').removeClass('display-none');
            $('.shipping-body-preview', $(this)).addClass('display-none');

            $('.woocommerce-billing-fields input').val('');
            $('#billing_email').val($('.email', $(this)).val());
            $('#billing_first_name').val($('.first-name', $(this)).val());
            $('#billing_last_name').val($('.last-name', $(this)).val());
            $('#billing_phone').val($('.tel', $(this)).val());
        });

        $('.dhl-shipping').on('click', '.shipping-address-item', function () {
            $('.shipping-body', '.dhl-shipping').addClass('display-none');
            $('.shipping-body', $(this)).removeClass('display-none');
            $('.shipping-body-preview', '.dhl-shipping').removeClass('display-none');
            $('.shipping-body-preview', $(this)).addClass('display-none');

            $('.woocommerce-billing-fields input').val('');
            $('#billing_email').val($('.email', $(this)).val());
            $('#billing_first_name').val($('.first-name', $(this)).val());
            $('#billing_last_name').val($('.last-name', $(this)).val());
            $('#billing_phone').val($('.tel', $(this)).val());
            $('#billing_city').val($('.city', $(this)).val());
            $('#billing_address_1').val($('.address', $(this)).val());
            $('#billing_postcode').val($('.postcode', $(this)).val());
            $('#billing_country').change();

            let currentContext = $(this);

            if ($('#billing_city').val() && $('.dhl-rates-select', currentContext).is(':empty')) {
                $('#shipping_method input').trigger('click');
                getDHLRates($(this));
            }

            if (!$('.dhl-rates-select', $(this)).val()) {
                getDHLRates(currentContext);
            }
        });

        $('.dhl-shipping').on('change', 'select', function () {
            $('.shipping-address-item').removeClass('chosen-address');
            $(this).closest('.shipping-address-item').addClass('chosen-address');

            // console.log($('.chosen-address').parent().parent().prev())
            $('.shipping-summary').html('');
            $('.chosen-address').parent().parent().prev().clone().appendTo($('.shipping-summary'));
            // $('.shipping-summary').append($('.chosen-address').parent().parent().prev());
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address select option:selected').text() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .email').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .first-name').val() || ''} ${$('.chosen-address .last-name').val() || ''}</p>`);
            $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address input[type=tel]').val() || ''}</p>`);
        });

        $('.dhl-shipping').on('change', '.city', function () {
            let country = $('#billing_country').val();
            $('#billing_country').val('EE').change();

            setTimeout(function () {
                $('#billing_country').val(country).change();
            }, 50);
        });

        const dhlUnregistered = $('.dhl-shipping .unregistered-address');
        const omnivaUnregistered = $('.omniva-shipping .unregistered-address');
        const smartpostUnregistered = $('.smartpost-shipping .unregistered-address');

        $('.email', omnivaUnregistered).change(function () {
            if ($('.email', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.omniva-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.first-name', omnivaUnregistered).change(function () {
            if ($('.email', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.omniva-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.last-name', omnivaUnregistered).change(function () {
            if ($('.email', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.omniva-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.tel', omnivaUnregistered).change(function () {
            if ($('.email', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.omniva-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.omniva-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.email', smartpostUnregistered).change(function () {
            if ($('.email', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.first-name', smartpostUnregistered).change(function () {
            if ($('.email', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.last-name', smartpostUnregistered).change(function () {
            if ($('.email', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.tel', smartpostUnregistered).change(function () {
            if ($('.email', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.first-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.last-name', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()
                && $('.tel', $('.smartpost-shipping .shipping-body:not(.display-none)')).val()) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
            }
        });

        $('.email', dhlUnregistered).change(function () {
            $('#billing_email').val($(this).val());
        });

        $('.first-name', dhlUnregistered).change(function () {
            $('#billing_first_name').val($(this).val());
        });

        $('.last-name', dhlUnregistered).change(function () {
            $('#billing_last_name').val($(this).val());
        });

        $('.city', dhlUnregistered).change(function () {
            $('#billing_city').val($(this).val());

            $('#shipping_method input').trigger('click');
            getDHLRates(dhlUnregistered);
        });

        $('.address', dhlUnregistered).change(function () {
            $('#billing_address_1').val($(this).val());
        });

        $('.postcode', dhlUnregistered).change(function () {
            $('#billing_postcode').val($(this).val());
        });

        $('.tel', dhlUnregistered).change(function () {
            $('#billing_phone').val($(this).val());
        });

        $('.shipping-method-wrapper').click(function () {
            $('.shipping-addresses').addClass('display-none');
            $('.shipping-addresses', $(this)).removeClass('display-none');

            $('.shipping-method-wrapper').removeClass('checked-method');
            $(this).addClass('checked-method');
        });

        $('.local-pickup').click(function () {
            $('#shipping_method li:nth-child(3) input').trigger('click');

            $('.next-step-button').prop('disabled', false);
            $('.next-step-button-desktop').prop('disabled', false);

            $('.shipping-address-item').removeClass('chosen-address');
            $(this).closest('.shipping-address-item').addClass('chosen-address');

            $('.shipping-summary').html('');
            // $('.shipping-summary').append();
            $('.local-pickup .shipping-main').clone().appendTo($('.shipping-summary'));
            // $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address select option:selected').text() || ''}</p>`);
            // $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .email').val() || ''}</p>`);
            // $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address .first-name').val() || ''} ${$('.chosen-address .last-name').val() || ''}</p>`);
            // $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.chosen-address input[type=tel]').val() || ''}</p>`);
        });

        // $('#stripe-payment-data').appendTo($('.step2-wrapper'));
        // $('#stripe-payment-data').addClass('display-none');

        $('.next-step-button').click(function () {
            if ($(this).attr('data-step') === '1') {
                let currentCountry = $('.choose-country').val();

                if (currentCountry !== 'EE' || currentCountry !== 'LT' || currentCountry !== 'LV') {
                    $('div[data-payment-name=montonio]').addClass('display-none');
                }

                $(this).attr('data-step', '2');
                $(this).prop('disabled', true);
                $('.next-step-button-desktop').attr('data-step', '2');
                $('.next-step-button-desktop').prop('disabled', true);

                $('.step1-wrapper').addClass('display-none');
                $('.step2-wrapper').removeClass('display-none');

                $('.step1-header').addClass('not-active-step step-completed');
                $('.step2-header').removeClass('not-active-step');

                // $('#payment').appendTo('.step2-wrapper');
                $('.step2-wrapper').append($('#payment'));
                $('#payment').css('display', 'block');

                $('.shipping-summary').removeClass('display-none');

                let paymentMethod = $('.step2-header').attr('data-payment');

                if (paymentMethod === 'montonio') {
                    if ($('div[data-payment-name=montonio]')) {
                        $('div[data-payment-name=montonio]').addClass('method-checked');
                        $('#payment_method_montonio_card_payments').trigger('click');

                        // $('#stripe-payment-data').addClass('display-none');
                        $(this).prop('disabled', false);
                    }
                } else if (paymentMethod === 'paypal') {
                    if ($('div[data-payment-name=paypal]')) {
                        $('div[data-payment-name=paypal]').addClass('method-checked');
                        $('#payment_method_ppec_paypal').trigger('click');

                        // $('#stripe-payment-data').addClass('display-none');
                        $(this).prop('disabled', false);
                    }
                } else if (paymentMethod === 'everypay') {
                    if ($('div[data-payment-name=everypay]')) {
                        $('div[data-payment-name=everypay]').addClass('method-checked');
                        $('#payment_method_everypay_card').trigger('click');

                        // $('#stripe-payment-data').removeClass('display-none');
                        $(this).prop('disabled', false);
                    }
                }

                if (!$('.checkout-step1-wrapper').hasClass('logged')) {
                    $('.shipping-summary').html();
                    $('.shipping-summary').append($('.checked-method .shipping-main'));
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method select option:selected').text()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .email').val()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .first-name').val()} ${$('.checked-method .last-name').val()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .tel').val()}</p>`);
                }
            } else if ($(this).attr('data-step') === '2') {

                $('.step2-wrapper').addClass('display-none');
                $('.step3-wrapper').removeClass('display-none');

                $('.step2-header').addClass('not-active-step step-completed');
                $('.step3-header').removeClass('not-active-step');

                // if ($('.method-checked')) {
                //     $(this).prop('disabled', true);
                // }

                if (window.innerWidth < 992) {
                    $('.method-checked').clone().appendTo($('.payment-summary'));
                }


                $(this).attr('data-step', '3');
                $(this).prop('disabled', true);
            } else if ($(this).attr('data-step') === '3') {
                $('#place_order').trigger('click');
            }
        });

        $('.next-step-button-desktop').click(function () {
            if ($(this).attr('data-step') === '1') {
                let currentCountry = $('.choose-country').val();
                console.log(currentCountry)
                if (currentCountry === 'EE' || currentCountry === 'LT' || currentCountry === 'LV') {
                } else {
                    $('div[data-payment-name=montonio]').addClass('display-none');
                }

                $('.two').addClass('pay');
                $('.three').addClass('active');

                $('.desktop-step1-main').addClass('pointer-events-no');

                $('.next-step-button-desktop').attr('data-step', '2');
                $('.next-step-button-desktop').prop('disabled', true);
                $('.next-step-button').attr('data-step', '2');
                $('.next-step-button').prop('disabled', true);

                $('.step1-desktop-continue-wrapper').addClass('display-none');
                $('.desktop-step1-description').addClass('display-none');

                // $('.step1-wrapper').addClass('display-none');
                $('.checkout-select-shipping').addClass('display-none');
                $('.shipping-tabs-buttons').css('pointer-events', 'none');

                $('.step2-wrapper').removeClass('display-none');
                $('.desktop-after-payment-wrapper').removeClass('display-none');

                $('.step1-header').addClass('not-active-step step-completed');
                $('.step2-header').removeClass('not-active-step');

                // $('#payment').appendTo('.step2-wrapper');
                $('.step2-wrapper').append($('#payment'));
                $('#payment').css('display', 'block');

                $('.shipping-summary').removeClass('display-none');

                let paymentMethod = $('.step2-header').attr('data-payment');

                if (paymentMethod === 'montonio') {
                    if ($('div[data-payment-name=montonio]')) {
                        $('div[data-payment-name=montonio]').addClass('method-checked');
                        $('#payment_method_montonio_card_payments').trigger('click');

                        // $('#stripe-payment-data').addClass('display-none');
                        // $('.next-step-button-desktop').prop('disabled', false);
                    }
                } else if (paymentMethod === 'paypal') {
                    if ($('div[data-payment-name=paypal]')) {
                        $('div[data-payment-name=paypal]').addClass('method-checked');
                        $('#payment_method_ppec_paypal').trigger('click');

                        // $('#stripe-payment-data').addClass('display-none');
                        // $('.next-step-button-desktop').prop('disabled', false);
                    }
                } else if (paymentMethod === 'everypay') {
                    if ($('div[data-payment-name=everypay]')) {
                        $('div[data-payment-name=everypay]').addClass('method-checked');
                        $('#payment_method_everypay_card').trigger('click');

                        // $('#stripe-payment-data').removeClass('display-none');
                        // $('.next-step-button-desktop').prop('disabled', false);
                    }
                }

                if (!$('.checkout-step1-wrapper').hasClass('logged')) {
                    $('.shipping-summary').html();
                    $('.shipping-summary').append($('.checked-method .shipping-main'));
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method select option:selected').text()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .email').val()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .first-name').val()} ${$('.checked-method .last-name').val()}</p>`);
                    $('.shipping-summary .shipping-name').append(`<p class="text-small">${$('.checked-method .tel').val()}</p>`);
                }
            } else if ($('.next-step-button-desktop').attr('data-step') === '2') {

                $('.step2-wrapper').addClass('display-none');
                // $('.step3-wrapper').removeClass('display-none');

                $('.step2-header').addClass('not-active-step step-completed');
                // $('.step3-header').removeClass('not-active-step');

                if (window.innerWidth < 992) {
                    $('.method-checked').clone().appendTo($('.payment-summary'));
                }

                $('#place_order').trigger('click');
            }
        });

        $('#check-terms, #check-terms-mob').change(function () {
            if ($(this).prop('checked')) {
                $('.next-step-button').prop('disabled', false);
                $('.next-step-button-desktop').prop('disabled', false);
                $('#terms').prop('checked', true);
            } else {
                $('.next-step-button').prop('disabled', true);
                $('.next-step-button-desktop').prop('disabled', true);
                $('#terms').prop('checked', false);
            }
        });

        $('.save-paypal-address').click(function () {
            let ppCity = $('#pp-city').val();
            let ppAddress = $('#pp-address').val();
            let ppPostcode = $('#pp-postcode').val();

            $('#billing_city').val(ppCity);
            $('#billing_address_1').val(ppAddress);
            $('#billing_postcode').val(ppPostcode);

            $('div[data-payment-name=paypal]').addClass('method-checked');
            $('#payment_method_ppec_paypal').trigger('click');

            // $('#stripe-payment-data').addClass('display-none');
        });

        $('.step2-wrapper').on('click', '.payment-method', function () {
            $('.payment-method').removeClass('method-checked');
            $(this).addClass('method-checked');

            let paymentMethod = $(this).attr('data-payment-name');

            if (window.innerWidth < 992) {
                $('.next-step-button').prop('disabled', false);
            }

            console.log(paymentMethod)
            if (paymentMethod === 'montonio') {
                if ($('div[data-payment-name=montonio]')) {
                    $('div[data-payment-name=montonio]').addClass('method-checked');
                    $('#payment_method_montonio_card_payments').trigger('click');

                    // $('#stripe-payment-data').addClass('display-none');
                }
            } else if (paymentMethod === 'paypal') {
                if ($('#billing_city').val() && $('#billing_address_1').val() && $('#billing_postcode').val()) {
                    if ($('div[data-payment-name=paypal]')) {
                        $('div[data-payment-name=paypal]').addClass('method-checked');
                        $('#payment_method_ppec_paypal').trigger('click');

                        // $('#stripe-payment-data').addClass('display-none');
                    }
                } else {
                    var myModal = new bootstrap.Modal(document.getElementById('paypalAddressModal'), {
                        keyboard: false
                    })

                    myModal.show();
                }
            } else if (paymentMethod === 'everypay') {
                if ($('div[data-payment-name=everypay]')) {
                    $('div[data-payment-name=everypay]').addClass('method-checked');
                    $('#payment_method_everypay_card').trigger('click');

                    // $('#stripe-payment-data').removeClass('display-none');
                }
            }
        });

        $(document).on('click', '.edit-address-button', function () {
            $('.popup-edit-address-name').attr('data-address-id', $(this).attr('data-address-id'));

            if ($(this).attr('data-address-name')) {
                $('.popup-edit-address-name').val($(this).attr('data-address-name'));
            }

            if ($(this).attr('data-email')) {
                $('.popup-edit-email').val($(this).attr('data-email'));
            }

            if ($(this).attr('data-first-name')) {
                $('.popup-edit-first-name').val($(this).attr('data-first-name'));
            }

            if ($(this).attr('data-last-name')) {
                $('.popup-edit-last-name').val($(this).attr('data-last-name'));
            }

            if ($(this).attr('data-phone')) {
                $('.popup-edit-phone').val($(this).attr('data-phone'));
            }

            if ($(this).attr('data-city')) {
                $('.popup-edit-city').val($(this).attr('data-city'));
            }

            if ($(this).attr('data-address')) {
                $('.popup-edit-address').val($(this).attr('data-address'));
            }

            if ($(this).attr('data-postcode')) {
                $('.popup-edit-postcode').val($(this).attr('data-postcode'));
            }
        });

        let newAddress;
        const emptyShippingAddressItem = $('.empty-shipping-address-item');

        $('.edit-estonian-button', '#editEstonianAddress').click(function (e) {
            newAddress = {
                id: $('.popup-edit-address-name').attr('data-address-id'),
                name: $('.popup-edit-address-name').val(),
                email: $('.popup-edit-email').val(),
                firstName: $('.popup-edit-first-name').val(),
                lastName: $('.popup-edit-last-name').val(),
                phone: $('.popup-edit-phone').val()
            }

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'edit_address',
                    'nonce_code': myajax.nonce,
                    'data': newAddress
                },
                success: function (data) {
                    let context = $(`label[data-address=${data}]`);

                    $('.shipping-body-preview p:nth-child(1)').text($('#editEstonianAddress .popup-edit-email').val());
                    $('.shipping-body-preview p:nth-child(2)').text($('#editEstonianAddress .popup-edit-first-name').val() + ' ' + $('#editEstonianAddress .popup-edit-last-name').val());
                    $('.shipping-body-preview p:nth-child(3)').text($('#editEstonianAddress .popup-edit-phone').val());

                    $('.email', context).val($('#editEstonianAddress .popup-edit-email').val())
                    $('.first-name', context).val($('#editEstonianAddress .popup-edit-first-name').val())
                    $('.last-name', context).val($('#editEstonianAddress .popup-edit-last-name').val())
                    $('.tel', context).val($('#editEstonianAddress .popup-edit-phone').val())
                    $('.btn-close').trigger('click');
                }
            });
        });

        $('.edit-european-button', '#editEuropeanAddress').click(function (e) {
            newAddress = {
                id: $('.popup-edit-address-name', '#editEuropeanAddress').attr('data-address-id'),
                name: $('.popup-edit-address-name', '#editEuropeanAddress').val(),
                email: $('.popup-edit-email', '#editEuropeanAddress').val(),
                firstName: $('.popup-edit-first-name', '#editEuropeanAddress').val(),
                lastName: $('.popup-edit-last-name', '#editEuropeanAddress').val(),
                phone: $('.popup-edit-phone', '#editEuropeanAddress').val(),
                city: $('.popup-edit-city', '#editEuropeanAddress').val(),
                address: $('.popup-edit-address', '#editEuropeanAddress').val(),
                postcode: $('.popup-edit-postcode', '#editEuropeanAddress').val()
            }

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'edit_european_address',
                    'nonce_code': myajax.nonce,
                    'data': newAddress
                },
                success: function (data) {
                    let context = $(`label[data-address=${newAddress.id}]`);

                    $('.shipping-body-preview p:nth-child(1)').text($('#editEstonianAddress .popup-edit-email').val());
                    $('.shipping-body-preview p:nth-child(2)').text($('#editEstonianAddress .popup-edit-first-name').val() + ' ' + $('#editEstonianAddress .popup-edit-last-name').val());
                    $('.shipping-body-preview p:nth-child(3)').text($('#editEstonianAddress .popup-edit-phone').val());

                    $('.email', context).val($('#editEuropeanAddress .popup-edit-email').val());
                    $('.first-name', context).val($('#editEuropeanAddress .popup-edit-first-name').val());
                    $('.last-name', context).val($('#editEuropeanAddress .popup-edit-last-name').val());
                    $('.tel', context).val($('#editEuropeanAddress .popup-edit-phone').val());
                    $('.city', context).val($('#editEuropeanAddress .popup-edit-city').val());
                    $('.address', context).val($('#editEuropeanAddress .popup-edit-address').val());
                    $('.postcode', context).val($('#editEuropeanAddress .popup-edit-postcode').val());

                    $('.edit-address-button', context).attr('data-email', newAddress.email);
                    $('.edit-address-button', context).attr('data-first-name', newAddress.firstName);
                    $('.edit-address-button', context).attr('data-last-name', newAddress.lastName);
                    $('.edit-address-button', context).attr('data-phone', newAddress.phone);
                    $('.edit-address-button', context).attr('data-city', newAddress.city);
                    $('.edit-address-button', context).attr('data-address', newAddress.address);
                    $('.edit-address-button', context).attr('data-postcode', newAddress.postcode);

                    $('.btn-close').trigger('click');

                    setTimeout(function () {
                        getDHLRates(context);
                    }, 100);

                    $('#billing_email').val($('.email', context).val());
                    $('#billing_first_name').val($('.first-name', context).val());
                    $('#billing_last_name').val($('.last-name', context).val());
                    $('#billing_phone').val($('.tel', context).val());
                    $('#billing_city').val($('.city', context).val());
                    $('#billing_address_1').val($('.address', context).val());
                    $('#billing_postcode').val($('.postcode', context).val());
                    $('#billing_country').change();
                }
            });
        });

        $('.omniva-add-new-button').click(function () {
            $('.add-estonian-button', '#addEstonianAddress').addClass('omniva');
        });

        $('.smartpost-add-new-button').click(function () {
            $('.add-estonian-button', '#addEstonianAddress').addClass('smartpost');
        });

        $('.add-estonian-button').click(function (e) {
            let context = $(this);
            e.preventDefault();

            newAddress = {
                // id: $('.popup-add-address-name').attr('data-address-id'),
                name: $('.popup-add-address-name').val(),
                email: $('.popup-add-email').val(),
                firstName: $('.popup-add-first-name').val(),
                lastName: $('.popup-add-last-name').val(),
                phone: $('.popup-add-phone').val()
            }

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'add_address',
                    'nonce_code': myajax.nonce,
                    'data': newAddress
                },
                success: function (data) {
                    let carrier;

                    if (context.hasClass('omniva')) {
                        carrier = 'omniva';
                    } else if (context.hasClass('smartpost')) {
                        carrier = 'smartpost';
                    }

                    let clone = emptyShippingAddressItem.clone();

                    $(`.${carrier}-shipping .add-new-wrapper`).before(clone);
                    clone.removeClass('display-none empty-shipping-address-item').addClass('shipping-address-item');

                    let newAdd = $('.add-new-wrapper', $(`.${carrier}-shipping`)).prev();

                    $('.shipping-label', newAdd).attr('for', `${carrier}-${data}`);
                    $('.shipping-label', newAdd).attr('data-address', `${newAddress.name}`);
                    $('.shipping-label>input', newAdd).attr('id', `${carrier}-${data}`);
                    $('.shipping-header p', newAdd).text(newAddress.name);
                    $('.shipping-header button', newAdd).attr('data-address-id', `${data}`);
                    $('.shipping-header button', newAdd).attr('data-address-name', `${newAddress.name}`);
                    $('.shipping-header button', newAdd).attr('data-address-email', `${newAddress.email}`);
                    $('.shipping-header button', newAdd).attr('data-address-first-name', `${newAddress.firstName}`);
                    $('.shipping-header button', newAdd).attr('data-address-last-name', `${newAddress.lastName}`);
                    $('.shipping-header button', newAdd).attr('data-address-phone', `${newAddress.phone}`);
                    $('.shipping-body-preview p:nth-child(1)', newAdd).text(newAddress.email);
                    $('.shipping-body-preview p:nth-child(2)', newAdd).text(newAddress.firstName + ' ' + newAddress.lastName);
                    $('.shipping-body-preview p:nth-child(3)', newAdd).text(newAddress.phone);
                    $('.shipping-body .email', newAdd).val(`${newAddress.email}`);
                    $('.shipping-body .select-wrapper label', newAdd).attr('for', `location-${data}`);
                    $('.shipping-body .select-wrapper select', newAdd).attr('id', `location-${data}`);
                    $('.shipping-body .first-name', newAdd).val(`${newAddress.firstName}`);
                    $('.shipping-body .last-name', newAdd).val(`${newAddress.lastName}`);
                    $('.shipping-body .tel', newAdd).val(`${newAddress.phone}`);
                }
            });
        });

        $('.add-european-button').click(function (e) {
            let context = $(this);
            e.preventDefault();

            newAddress = {
                id: $('.popup-add-address-name', $('#addEuropeanAddress')).val(),
                name: $('.popup-add-address-name', $('#addEuropeanAddress')).val(),
                email: $('.popup-add-email', $('#addEuropeanAddress')).val(),
                firstName: $('.popup-add-first-name', $('#addEuropeanAddress')).val(),
                lastName: $('.popup-add-last-name', $('#addEuropeanAddress')).val(),
                phone: $('.popup-add-phone', $('#addEuropeanAddress')).val(),
                city: $('.popup-add-city', $('#addEuropeanAddress')).val(),
                address: $('.popup-add-address', $('#addEuropeanAddress')).val(),
                postcode: $('.popup-add-postcode', $('#addEuropeanAddress')).val()
            }

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'add_european_address',
                    'nonce_code': myajax.nonce,
                    'data': newAddress
                },
                success: function (data) {
                    let carrier = 'dhl';

                    // if (context.hasClass('omniva')) {
                    //     carrier = 'omniva';
                    // } else if (context.hasClass('smartpost')) {
                    //     carrier = 'smartpost';
                    // }

                    let clone = $('.empty-dhl-shipping-address-item').clone();

                    $(`.${carrier}-shipping .add-new-wrapper`).before(clone);
                    clone.removeClass('display-none empty-dhl-shipping-address-item').addClass('shipping-address-item');

                    let newAdd = $('.add-new-wrapper', $(`.${carrier}-shipping`)).prev();

                    $('.shipping-label', newAdd).attr('for', `${carrier}-${newAddress.name}`);
                    $('.shipping-label', newAdd).attr('data-address', `${newAddress.name}`);
                    $('.shipping-label>input', newAdd).attr('id', `${carrier}-${newAddress.name}`);
                    $('.shipping-header p', newAdd).text(newAddress.name);
                    $('.shipping-header button', newAdd).attr('data-address-id', `${newAddress.name}`);
                    $('.shipping-header button', newAdd).attr('data-address-name', `${newAddress.name}`);
                    // $('.shipping-header button', newAdd).attr('data-address-email', `${newAddress.email}`);
                    // $('.shipping-header button', newAdd).attr('data-address-first-name', `${newAddress.firstName}`);
                    // $('.shipping-header button', newAdd).attr('data-address-last-name', `${newAddress.lastName}`);
                    $('.shipping-header button', newAdd).attr('data-address-phone', `${newAddress.phone}`);
                    $('.shipping-header button', newAdd).attr('data-bs-target', `#editEuropeanAddress`);
                    $('.shipping-header button', newAdd).attr('data-email', `${newAddress.email}`);
                    $('.shipping-header button', newAdd).attr('data-first-name', `${newAddress.firstName}`);
                    $('.shipping-header button', newAdd).attr('data-last-name', `${newAddress.lastName}`);
                    $('.shipping-header button', newAdd).attr('data-phone', `${newAddress.phone}`);
                    $('.shipping-header button', newAdd).attr('data-city', `${newAddress.city}`);
                    $('.shipping-header button', newAdd).attr('data-address', `${newAddress.address}`);
                    $('.shipping-header button', newAdd).attr('data-postcode', `${newAddress.postcode}`);
                    $('.shipping-body-preview p:nth-child(1)', newAdd).text(newAddress.email);
                    $('.shipping-body-preview p:nth-child(2)', newAdd).text(newAddress.firstName + ' ' + newAddress.lastName);
                    $('.shipping-body-preview p:nth-child(3)', newAdd).text(newAddress.phone);
                    $('.shipping-body .email', newAdd).val(`${newAddress.email}`);
                    $('.shipping-body .select-wrapper label', newAdd).attr('for', `location-${newAddress.name}`);
                    $('.shipping-body .select-wrapper select', newAdd).attr('id', `location-${newAddress.name}`);
                    $('.shipping-body .first-name', newAdd).val(`${newAddress.firstName}`);
                    $('.shipping-body .last-name', newAdd).val(`${newAddress.lastName}`);
                    $('.shipping-body .tel', newAdd).val(`${newAddress.phone}`);
                    $('.shipping-body .city', newAdd).val(`${newAddress.city}`);
                    $('.shipping-body .address', newAdd).val(`${newAddress.address}`);
                    $('.shipping-body .postcode', newAdd).val(`${newAddress.postcode}`);

                    $('.btn-close', '#addEuropeanAddress').trigger('click');
                }
            });
        });

        $(document).on('change', $('.order-total .woocommerce-Price-amount'), function () {
            $('.big-text').text($('.order-total .woocommerce-Price-amount').text());
        });

        $(document).on('change', '.dhl-rates-select', function () {
            $(`#shipping_method input[value=dhlexpress_${$(this).val()}]`).trigger('click');
        });
    });

    function getDHLRates(context) {
        // $('#shipping_method').html('<li><input type="radio">1</li><li><input type="radio">2</li>');
        // $(`#shipping_method input`).trigger('click');

        $.ajax({
            type: 'POST',
            url: myajax.url,
            data: {
                'action': 'dhl_rates',
                'nonce_code': myajax.nonce,
                'country': $('#billing_country option:selected').text(),
                'postcode': $('#billing_postcode').val(),
                'city': $('#billing_city').val(),
                'address': $('#billing_address_1').val()
            },
            beforeSend: function () {
                $('.loadingio-spinner-dual-ring-mlu0dcv4kaf', context).removeClass('display-none')
            },
            success: function (data) {
                $('.loadingio-spinner-dual-ring-mlu0dcv4kaf', context).addClass('display-none')

                let ratesObj = JSON.parse(data);

                let shippingMethod = '';
                let DHLrates = '';

                ratesObj.forEach(function (elem) {
                    shippingMethod += '<li><input type="radio" name="shipping_method[0]" data-index="0" id="shipping_method_0_dhlexpress_' + elem['service_code'] + '" value="dhlexpress_' + elem['service_code'] + '" class="shipping_method"><label for="shipping_method_0_dhlexpress_' + elem['service_code'] + '">' + elem['service_name'] + ': <span class="woocommerce-Price-amount amount"><bdi>' + elem['total_price'] + '<span class="woocommerce-Price-currencySymbol">€</span></bdi></span></label></li>';
                    DHLrates += `<option value="${elem['service_code']}">${elem['service_name']} - ${elem['total_price']}€</option>`;
                });

                // $('#shipping_method').html(shippingMethod);
                $('.dhl-rates-select', context).html(DHLrates);
                // $(`#shipping_method input[value=dhlexpress_${$(this).val()}]`).trigger('click');
                // console.log(ratesObj[0]['service_code'])

                setTimeout(function () {
                    $(`#shipping_method input[value=dhlexpress_${ratesObj[0]['service_code']}]`).trigger('click');
                    $('.next-step-button-desktop').prop('disabled', false);
                }, 3000);
                // $(`#shipping_method input[value=dhlexpress_${ratesObj[0]['service_code']}]`).trigger('click');
                // $(`#shipping_method input`).trigger('click');
                $('.dhl-input-spinner').removeClass('no-pointer');
            }
        });
    }
})(jQuery);
