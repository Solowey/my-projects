import '@scss/single-product-lenses';

/* eslint-disable */
/*eslint max-lines: 1000*/
(function($) {

    document.addEventListener('DOMContentLoaded', function() {
        $('.single-product-category-show').on('click', function (e) {
            e.preventDefault()
        })

        $('#lenses-right-sph').change(function () {
            if ($('#lenses-right-sph').val() && $('#lenses-left-sph').val()) {
                $('.wizard-lens-btn-continue').prop('disabled', false);
            } else {
                $('.wizard-lens-btn-continue').prop('disabled', true);
            }
        });

        $('#lenses-left-sph').change(function () {
            if ($('#lenses-right-sph').val() && $('#lenses-left-sph').val()) {
                $('.wizard-lens-btn-continue').prop('disabled', false);
            } else {
                $('.wizard-lens-btn-continue').prop('disabled', true);
            }
        });

        $('#pa_cl-power option').clone().appendTo('#right-lens');
        $('#pa_cl-power option').clone().appendTo('#left-lens');
        $('#pa_cl-power option').clone().appendTo('#lenses-right-sph');
        $('#pa_cl-power option').clone().appendTo('#lenses-left-sph');
        const productContainer = $('div[id^="product-"]');

        $('.add-prescription').click(function () {
            productContainer.hide();
            $('.lenses-wizard').removeClass('display-none');
            $('.woocommerce-breadcrumb').addClass('display-none');
            $('.woocommerce-tabs').addClass('display-none');
            $('.reviews-info').addClass('display-none');
            $('.recently-product-section').addClass('display-none');
        })

        function closeWizardScreen () {
            productContainer.show();
            $('.lenses-wizard').addClass('display-none');
            $('.lenses-wizard2').addClass('display-none');
            $('.woocommerce-breadcrumb').removeClass('display-none');
            $('.woocommerce-tabs').removeClass('display-none');
            $('.reviews-info').removeClass('display-none');
            $('.recently-product-section').removeClass('display-none');
        }
        $('.button-back-to-product').click(function () {
            closeWizardScreen ();
        })

        $('.lenses-wizard .wizard-lens-back-button-desktop').click(function () {
            closeWizardScreen ();
        })

        $('.lenses-wizard2 .wizard-lens-back-button-desktop').click(function () {
            $('.lenses-wizard').removeClass('display-none');
            $('.lenses-wizard2').addClass('display-none');
        })

        $('.button-back-to-product-mobile').click(function () {
            closeWizardScreen ();
        })

        let prescription = {};

        $('.left-eye-wrap').fadeOut();

        $('#both-eye-checkbox').on('change', function () {
            if($("#both-eye-checkbox").prop('checked')) {
                $('#both-eyes').attr('checked', false);
                $('#left-lens').attr('disabled', true)
                $('#left-lens').val([]);
                $('.left-eye-wrap').fadeOut(200);
            } else {
                $('#both-eyes').attr('checked', 'checked')
                $('#left-lens').attr('disabled', false)
                $('.left-eye-wrap').fadeIn(200);
            }
        })
        $('#lenses-right-sph').on('change', function () {
            $('#right-lens').val($('#lenses-right-sph').val()).change();

            if ($('#both-eye-checkbox').is(':checked')) {
                $('#lenses-left-sph').val($('#lenses-right-sph').val()).change();
                $('#left-lens').val($('#lenses-right-sph').val()).change();
            }
        })
        $('#lenses-left-sph').on('change', function () {
            $('#left-lens').val($('#lenses-left-sph').val()).change();
        })

        $('.wizard-lens-btn-continue').click(function () {
            let leftLens;

            if ($("#both-eye-checkbox").prop('checked')) {
                leftLens = $('#right-lens').val();
            } else {
                leftLens = $('#left-lens').val();
            }

            $('.total-prescriptions tr:nth-child(2) td:nth-child(2)').text($('#right-lens').val());
            $('.total-prescriptions tr:nth-child(2) td:nth-child(3)').text($('#pa_cl-bc').val());
            $('.total-prescriptions tr:nth-child(2) td:nth-child(4)').text($('#pa_cl-dia').val());

            $('.total-prescriptions tr:nth-child(3) td:nth-child(2)').text(leftLens);
            $('.total-prescriptions tr:nth-child(3) td:nth-child(3)').text($('#pa_cl-bc').val());
            $('.total-prescriptions tr:nth-child(3) td:nth-child(4)').text($('#pa_cl-dia').val());

            $('.lenses-wizard').addClass('display-none');
            $('.lenses-wizard2').removeClass('display-none');
        })

        $('.back-to-Prescription').click(function () {
            $('.lenses-wizard').removeClass('display-none');
            $('.lenses-wizard2').addClass('display-none');
        })

        $('.wizard-lens-mobile-step').click(function () {
            $('.wizard-total-wrapper--mobile').fadeIn();
            $('.prescription-step').fadeOut();
            $('.add-contact-lenses-to-cart').removeClass('display-none')
        })

        $('.back-to-quantity').click(function () {
            $('.wizard-total-wrapper--mobile').fadeOut();
            $('.prescription-step').fadeIn();
            $('.add-contact-lenses-to-cart').addClass('display-none')
        })

        $('input[name=quantity-step]').click(function () {
            $('.wizard-lens-mobile-step').prop('disabled', false);
            $('.wizard-lens-btn-add-to-cart').prop('disabled', false);
        });

        $('.choice-count').on('change', function () {
            let checkedCal = $('.choice-count:checked').val();

            switch(checkedCal) {
                case '3':
                    $('#val-3').prop('checked', 'checked');
                    $('#lens-count-total').html($(this).attr('data-month'));
                    $('#lens-total').html($(this).attr('data-cost'));
                    $('.current-price').html($(this).attr('data-cost'));
                    break;
                case '6':
                    $('#val-6').prop('checked', 'checked');
                    $('#lens-count-total').html($(this).attr('data-month'));
                    $('#lens-total').html($(this).attr('data-cost'));
                    $('.current-price').html($(this).attr('data-cost'));
                    break;
                case '12':
                    $('#val-12').prop('checked', 'checked');
                    $('#lens-count-total').html($(this).attr('data-month'));
                    $('#lens-total').html($(this).attr('data-cost'));
                    $('.current-price').html($(this).attr('data-cost'));
                    break;
                default:
                    return true;
            }
        })

        $('.add-contact-lenses-to-cart').click(function () {

            let leftLens;

            if ($('#both-eyes').is(':checked')) {
                leftLens = $('#left-lens').val();
            } else {
                leftLens = $('#right-lens').val();
            }
            $('.add-contact-lenses-to-cart').attr('disabled', 'disabled');
            $.ajax( {
                type: "post",
                url: myajax.url,
                data: {
                    action: 'contact-lenses-add-to-cart',
                    nonce_code: myajax.nonce,
                    productId: $('input[name=add-to-cart]').val(),
                    clBc: $('#pa_cl-bc').val(),
                    clDia: $('#pa_cl-dia').val(),
                    rightLens: $('#right-lens').val(),
                    leftLens: leftLens,
                    quantity: $('input[name=quantity-lenses]:checked').val()
                },
                success: function (data) {
                    location.reload();
                }
            });
        });

        $('.saved-select-lenses').change(function () {
            let currentOption = $(`option[value="${$(this).val()}"]`, $(this));

            if (currentOption.attr('data-lens-both') === 'on') {
                $('#both-eye-checkbox').prop('checked', true).change();
            } else {
                $('#both-eye-checkbox').prop('checked', false).change();
            }

            $('#lenses-right-sph').val(currentOption.attr('data-lens-right')).trigger('change');
            $('#lenses-left-sph').val(currentOption.attr('data-lens-left')).trigger('change');

            // $('#comment').val(currentOption.attr('data-comment')).trigger('change');
        });

        $('.add-new-lenses-button').click(functio`n `() {
            $('.save-new-pre-lens-label').removeClass('display-none');
            $('.wizard-lens-btn-continue').text('Save and continue');
            $('.wizard-lens-btn-continue').addClass('save-new-pre-lens-button');
        });

        $(document).on('click', '.save-new-pre-lens-button', function () {
            $(this).removeClass('save-new-pre-lens-button');
            // $('.wizard2-continue').text('Select and continue');
            // $('.save-new-pre-label').addClass('display-none');

            let name = $('#new-name-lens').val();

            $('#new-name-lens').val();

            let lensBoth;

            if ($('#both-eye-checkbox').prop('checked', true)) {
                lensBoth = 'on';
            } else {
                lensBoth = 'off'
            }

            if (name) {
                $.ajax({
                    type: 'POST',
                    url: myajax.url,
                    data: {
                        'action': 'save_lens_prescriptions',
                        'nonce_code': myajax.nonce,
                        'prescription_name': name,
                        'lens-both': lensBoth,
                        'lens-right': $('#lenses-right-sph').val(),
                        'lens-left': $('#lenses-left-sph').val(),
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        });
    });
/////////////////////////////////////////////////////////////////////////////////////////////////

    $("#pa_cl-dia").prop("selectedIndex", 1);

    let clBcVal =  $('#pa_cl-bc').val();
    let clDiaVal = $('#pa_cl-dia').val();

    $('#pa_cl-bc').on('change', function() {
        if ($('#pa_cl-dia').val() !== '' && $('#pa_cl-bc').val() !== '') {
            $('.start-lenses-wizard').prop('disabled', false)
        } else {
            $('.start-lenses-wizard').prop('disabled', true)
        }

    })
    $('#pa_cl-dia').on('change', function() {
        if ($('#pa_cl-dia').val() !== '' && $('#pa_cl-bc').val() !== '') {
            $('.start-lenses-wizard').prop('disabled', false)
        } else {
            $('.start-lenses-wizard').prop('disabled', true)
        }

    })

////////////////////////////////////////////////////////////////////////////////////////////////////
//     if () {
//         $('.start-lenses-wizard').attr('disabled', false)
//     }
})(jQuery);
