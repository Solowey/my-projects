import '@scss/pages/_cart';
import 'slick-carousel';
/* eslint-disable */
(function ($) {

    $('.edit-lenses-btn').click(function () {
        let removeButtonWrapper = $(this).next()
        let link = $(this).attr('data-link');

        $('.remove', removeButtonWrapper).trigger('click');

        setTimeout(function () {
            window.location.href = link;
        }, 20);
    });

    if ($('.recently-slider')) {
        $('.recently-slider').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 2,
            dots: false,
            arrows: true,
            prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                "</svg>",
            nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                "</svg>",
            appendArrows: $('.recently-nav-container'),
            responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                }
            },
                {
                    breakpoint: 601,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                    }
                }
            ]
        });
    }

    $('.clear-cookie').on('click', function (e) {
        e.preventDefault();
        let myajaxurl = `//${window.location.host}/wp-admin/admin-ajax.php`;
        $.ajax({
            type: "POST",
            url: myajaxurl,
            data: {
                action: "clear",
            }, success(data) {
                if (data) {
                    $(".recently-slider").remove();
                    $(".recently-headline").remove();
                }
            }
        })

    })

    $(document).on('click', '.attribute-color-item[data-varid]', function () {
        let parentProduct = $(this).closest('.product-item');
        let imageToActive = $('img[data-varid="' + $(this).attr('data-varid') + '"]', parentProduct);

        $('img', parentProduct).addClass('display-none');
        imageToActive.removeClass('display-none');

        $('.attribute-color-item', parentProduct).removeClass('attribute-color-after');
        $(this).addClass('attribute-color-after');
    });
    const init = {
        autoplay: true,
        infinite: true,
        cssEase: "linear",
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    };
    $(() => {
        const win = $(window);
        let slider = $(".multiple-items");
        win.on("load change", () => {
            if (win.width() < 769) {
                slider.not(".slick-initialized").slick(init);
            } else if (slider.hasClass("slick-initialized")) {
                slider.slick("unslick");
            }
        });
    });

    $('.wish-product-link').click(function () {

        $.ajax({
            type: 'POST',
            url: myajax.url,
            data: {
                'action': 'wishlist',
                'nonce_code': myajax.nonce,
                'product_id': $(this).attr('data-id')
            },
            success: function( data ) {
                // console.log(data);
            }
        });
    });

    $(document).on('click', "#controlOpen", function () {
        $(this).parents('.cart_item').find('.mobile-control_overlay').addClass('mobile-control_overlay-open');
        $('.control-button').fadeOut(100);
    })

    $(document).on('click', ".mobile-control-button-close", function () {
        $('.mobile-control_overlay').removeClass('mobile-control_overlay-open');
        $('.control-button').fadeIn(100);
    })

    $(document).on('click', ".add-accessories-btn", function (){
        let parentEl = $(this).closest($('.cart_item'));
        if ($(window).width() >= 991) {
            setTimeout(function() {
                parentEl.find('.products').slick({
                    infinite: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false,
                    prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>",
                    nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>",
                    // appendArrows: $('.add-accessories-nav-arrow-container')
                })
            }, 400)
        } else {
            $('.product-item').removeClass('col-12 col-sm-6 col-lg-4');
        }

    })

    $(document).on('click', ".add-suggestions-btn", function (){
        if ($(window).width() >= 991) {
            setTimeout(function() {

                $(".suggestions-slider").slick({
                    infinite: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false,
                    prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>",
                    nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>"
                })
            }, 300)
        } else {
            $('.product-item').removeClass('col-12 col-sm-6 col-lg-4');
        }

    })

    $(document).on('click', '.btn-close', function (){
        let parentEl = $(this).closest($('.cart_item'));
        if (parentEl.find('.products').hasClass("slick-initialized")) {
            parentEl.find('.products').slick("unslick");
        }

    })

    $(document).on('click', ".add-to-cart-accessories", function() {
        let myAtr = $('.attribute-color-after');
        let el = $('.product-thumbnail');

        let productVarId = $(this).parent().find(myAtr).attr('data-varid');

           let productId = $(this).parent().find(el).attr('data-product-id')

           $.ajax({
            type: 'POST',
            url: `//${window.location.host}/wp-admin/admin-ajax.php`,
            data: {
                'action': 'add_accessories',
                'product_id': productId,
                'product_var_id': productVarId
            },
            success: function( data ) {
                if ($(".accessoriesModal .products").hasClass("slick-initialized")) {
                    $(".accessoriesModal .products").slick("unslick");
                }

                $('.btn-close').trigger("click");
                $('button[name="update_cart"]').prop('disabled', false).trigger("click");
                changeTotal()
            }
        });
    })

    $(document).on('click', ".add-to-cart-suggestion-product", function () {
        let myAtr = $('.attribute-color-after');
        let el = $('.product-thumbnail');
        let productVarId = $(this).parent().find(myAtr).attr('data-varid');

        let productId = $(this).parent().find(el).attr('data-product-id')

        $.ajax({
            type: 'POST',
            url: `//${window.location.host}/wp-admin/admin-ajax.php`,
            data: {
                'action': 'add_accessories',
                'product_id': productId,
                'product_var_id': productVarId
            },
            success: function( data ) {
                if ($(".our-suggestions").hasClass("slick-initialized")) {
                    $(".our-suggestions").slick("unslick");
                }

                $('.btn-close').trigger("click");
                $('button[name="update_cart"]').prop('disabled', false).trigger("click");
                changeTotal()
            }
        });
    })

    function changeTotal() {
        setTimeout(function () {
            let cost = $('.our-suggestions-info').attr('data-cost');
            $('.cart-subtotal .cost').text(cost)
        },3000)
    }

    $(document).ready(function () {
        changeTotal();
    })

})(jQuery);
