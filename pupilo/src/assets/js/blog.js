import '@scss/pages/_blog';
/* eslint-disable */

jQuery(function($) {
        let current = document.getElementsByClassName("current-menu-item");
        current[0].scrollIntoView({
            block: "nearest",
            inline: "nearest",
            behavior: "smooth"
        });


    let currentPage = document.querySelector(".articles_grid").dataset.page,
        maxPages = document.querySelector(".articles_grid").dataset.max,
        allPosts = document.querySelector('.articles_grid').dataset.allPosts,
        loadMoreBtn = document.getElementById('load_more'),
        viewedPosts = document.getElementById('viewedPosts'),
        posts = $('.articles_grid [class*="articles_item"]').length,
        percentViewedPosts = posts / allPosts * 100;
    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

    viewedPosts.innerText = posts;

    if(Number(currentPage) === Number(maxPages)) {
        loadMoreBtn.remove();
    }

    loadMoreBtn.addEventListener('click', function() {
        // eslint-disable-next-line no-invalid-this
        let button = $(this);
        $.ajax({
            url: "../wp-admin/admin-ajax.php",
            data: { action: "load_more_posts", currentPage, maxPages },
            type: "POST",
            beforeSend() {
                button.text("Loading");
            },
            success(data) {
                if(data) {
                    let postsList = document.querySelector(".articles_grid");
                    postsList.innerHTML += data;
                    posts = $('.articles_grid [class*="articles_item"]').length;
                    button.text("Load more");
                    currentPage++;
                    viewedPosts.innerText = posts;
                    percentViewedPosts = posts / allPosts * 100;
                    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

                    if(Number(currentPage) === Number(maxPages)) {
                        button.remove();
                    }
                } else {
                    button.remove();
                }
            }
        });
    });

    let btnContainer = document.getElementById("menu-blog-category");
    let btns = btnContainer.getElementsByClassName("menu-item-object-category");
    for (let j = 0; j < btns.length; j++) {
        btns[j].addEventListener("click", function() {
            let data = this.dataset.filter
            filterSelection(data)
            let current = document.getElementsByClassName("current_page_item");
            current[0].className = current[0].className.replace(" current_page_item", "");
            this.className += " current_page_item";
            current[0].scrollIntoView({
                block: "nearest",
                inline: "nearest"
            });
        });
    }
});
