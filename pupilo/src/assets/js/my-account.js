import '@scss/my-account.scss';

/* eslint-disable */
/*eslint max-lines: 1000*/
(function($) {
    document.addEventListener('DOMContentLoaded', function () {
        setTimeout(function () {
            if ($('.page-title-desktop').text() === 'Edit prescription') {
                $('.desktop-button-add-prescriptions').addClass('display-none');
            }
        }, 500);

        if (!window.location.href.split('/')[4]) {
            $('.my-account-link').attr('href', '/');
        }

        if (window.location.href.indexOf('?address-book') > 0) {
            $('.page-title').text('Add new address');
        }

        $('#lens_both').change(function () {
            if ($(this).is(':checked')) {
                $('.left_eye_select_wrapper').addClass('left_eye_disabled');
                $('#lens_left').val($('#lens_right').val()).change();
            } else {
                $('.left_eye_select_wrapper').removeClass('left_eye_disabled');
            }
        });

        $('#lens_right').change(function () {
            if ($('#lens_both').is(':checked')) {
                $('#lens_left').val($(this).val()).change();
            }
        });

        if ($('.edit').length && !$('.edit').hasClass('edit-address')) {
            $('.page-title').text('Edit prescription');
        }

        $('#prescription_name', '.add-prescription-form').keyup(function () {
            if($(this).val() !== '') {
                $('.save-prescription-button', '.add-prescription-form').prop('disabled', false);
            } else {
                $('.save-prescription-button', '.add-prescription-form').prop('disabled', true);
            }
        });

        $('#lens_name').keyup(function () {
            if($(this).val() !== '') {
                $('.save-prescription-button', '.add-lens-form').prop('disabled', false);
            } else {
                $('.save-prescription-button', '.add-lens-form').prop('disabled', true);
            }
        });

        $('.lens-add-comment').click(function () {
            $(this).next('.prescription-comment').toggle();
            $(this).next().next('.add-prism-checkbox').toggle();
        });

        $('#add-prescription, .mobile-button-add-prescriptions, .desktop-button-add-prescriptions').click(function () {
            // if (!$('.prescriptions-tab1').hasClass('not-active')) {
            //     $('.add-prescription-form').removeClass('display-none');
            // } else {
            //     $('.add-lens-form').removeClass('display-none');
            // }
            $('.add-prescription-form').removeClass('display-none');
            $('.prescriptions-tabs').removeClass('display-none');
            $('.my-account-prescriptions-wrapper').addClass('display-none');
            $('.save-prescriptions-wrapper').addClass('display-none');

            $('.page-title').text('New Prescription');
            $('.page-title-desktop').text($('.page-title').text());
            $('.desktop-button-add-prescriptions').addClass('display-none');
        });

        $('.mobile-button-add-prescriptions').click(function () {
            $(this).toggle();
        });

        $('.prescriptions-tab').click(function () {
            if ($(this).hasClass('not-active')) {
                $('.prescriptions-tab').addClass('not-active');
                $(this).removeClass('not-active');

                if (!$('.prescriptions-tab1').hasClass('not-active')) {
                    $('.tab1').removeClass('display-none');
                    $('.tab2').addClass('display-none');
                }

                if (!$('.prescriptions-tab2').hasClass('not-active')) {
                    $('.tab2').removeClass('display-none');
                    $('.tab1').addClass('display-none');
                }
            }
        });

        // $('.prescriptions-cancel-button').click(function () {
        //     $('.add-prescription-form').addClass('display-none');
        //     $('.save-prescriptions-wrapper').removeClass('display-none');
        //     $('.my-account-prescriptions-wrapper').removeClass('display-none');
        //     $('.prescriptions-tabs').addClass('display-none');
        // });
        //
        // $('.lens-cancel-button').click(function () {
        //     $('.add-lens-form').addClass('display-none');
        //     $('.save-prescriptions-wrapper').removeClass('display-none');
        //     $('.my-account-prescriptions-wrapper').removeClass('display-none');
        // });

        $('#add-prism').click(function () {
            if ($(this).prop('checked') === true) {
                $('.prism-wrapper').removeClass('display-none');
            } else {
                $('.prism-wrapper').addClass('display-none');
            }
        });

        $('#two-pd').click(function () {
            if ($(this).prop('checked') === true) {
                $('#pd-2').removeClass('display-none');
            } else {
                $('#pd-2').addClass('display-none');
                // $('#pd-2').val([]);
            }
        });

        let url = window.location.href.split('/');

        if (url[url.length - 1] === '') {
            url.pop();
        }

        // $('.order-item-pre-heading').click(function () {
        //     $('.lenses-configuration', $(this).parent()).toggle(300);
        //     $('.order-product-open-lenses', $(this)).toggleClass('rotate-180');
        // });

        // $('.order-lens-item-pre-heading').click(function () {
        //     $('.lenses-configuration', $(this).parent()).toggle(300);
        //     $('.order-product-open-lenses', $(this)).toggleClass('rotate-180');
        // });

        $('.show-pass').click(function () {
            if ($(this).prev().attr('type') === 'password') {
                $(this).prev().attr('type', 'text');
            } else {
                $(this).prev().attr('type', 'password');
            }
        });

        $('#new_password').keyup(function () {
            let pass = $(this).val();
            let checks = 0;

            if (hasUpperCase(pass)) {
                $('.condition1').addClass('condition-checked');
                checks++;
            } else {
                $('.condition1').removeClass('condition-checked');
            }

            if (pass.length >= 8) {
                $('.condition2').addClass('condition-checked');
                checks++;
            } else {
                $('.condition2').removeClass('condition-checked');
            }

            if (hasNumbers(pass)) {
                $('.condition3').addClass('condition-checked');
                checks++;
            } else {
                $('.condition3').removeClass('condition-checked');
            }

            if (checks === 3) {
                $('input[type="submit"]').prop('disabled', false);
            } else {
                $('input[type="submit"]').prop('disabled', true);

            }
        });

        $(window).on("load resize", () => {
            if (url[url.length - 1] !== 'edit-account') {
                $('.close-account-button').addClass('display-none');
            } else {
                $('.close-account-button').removeClass('display-none');
            }

            if (url[url.length - 1] !== 'my-account' && window.innerWidth < 769) {
                $('.my-account-menu').addClass('display-none');
                // $('.chevron-icon').attr('href', '/my-account');
            } else {
                $('.my-account-menu').removeClass('display-none');
            }

            if (parseInt(url[url.length - 1]) > 0) {
                $('.my-account-link').attr('href', '/my-account/orders');
                $('.button-pdf').removeClass('display-none');
            }

            let buttonPdf = $('.button-pdf');

            if (window.innerWidth >= 769) {
                if (window.location.href.indexOf('edit-address') > 0) {
                    $('.page-title-desktop').text('Delivery addresses and methods');
                } else {
                    $('.page-title-desktop').text($('.page-title').text());
                }



                if ($('.button-pdf').length > 0) {
                    $('.page-title-desktop-wrapper').append(buttonPdf);
                }

                $('.page-title-desktop-wrapper').append($('.desktop-button-add-prescriptions'));
                // $('.desktop-button-add-prescriptions').removeClass('display-none');

                // if (!$('.my-account-prescriptions-wrapper').hasClass('display-none')) {
                //     $('.desktop-button-add-prescriptions').addClass('display-none');
                // } else {
                //     $('.desktop-button-add-prescriptions').removeClass('display-none');
                // }

                $('.open-saved-pre').click(function () {
                    $('.saved-second-row', $(this).closest('.saved_prescription')).toggle(300);
                    $('.open-saved-pre', $(this).closest('.saved_prescription')).toggleClass('rotate-270');
                });

                if ($('.add-new-address').length > 0) {
                    $('.desktop-button-add-prescriptions').attr('href', $('.add-new-address a').attr('href'));
                }
            } else {
                if ($('.button-pdf').length > 0) {
                    $('.title-wrapper').append(buttonPdf);
                }

                $('.title-wrapper').append($('.mobile-button-add-prescriptions'));

                if ($('.add-new-address').length > 0) {
                    $('.mobile-button-add-prescriptions').attr('href', $('.add-new-address a').attr('href'));
                }
            }

            if (window.innerWidth >= 1152) {
                $('.order-header').append($('.woocommerce-customer-details'));
                $('.woocommerce-order-details').append($('.order-total-wrapper'));
            } else {
                $('.woocommerce-MyAccount-content').append($('.woocommerce-customer-details'));
                $('.woocommerce-MyAccount-content').append($('.order-total-wrapper'));
            }
        });

        let facebook = $('strong:contains("Facebook")');
        let google = $('strong:contains("Google")');

        let facebookButton = $('input', facebook.parent().next());
        let googleButton = $('input', google.parent().next());

        if (google.length > 0) {
            $('#google-checkbox').prop('checked', true);
            $('.google-text').text('Your Google is now linked with Pupilo account');
        }

        if (facebook.length > 0) {
            $('#facebook-checkbox').prop('checked', true);
            $('.facebook-text').text('Your Facebook is now linked with Pupilo account');
        }

        $('#google-checkbox').click(function() {

            if ($('#google-checkbox').prop('checked')) {
                $('.theChampGoogleLogin').trigger('click');
                $('.google-text').text('Your Google is now linked with Pupilo account');
            } else {
                googleButton.trigger('click');
                $('.google-text').text('Connect your Google account');
            }
        });

        $('#facebook-checkbox').click(function() {

            if ($('#facebook-checkbox').prop('checked')) {
                $('.theChampFacebookLogin').trigger('click');
                $('.facebook-text').text('Your Facebook is now linked with Pupilo account');
            } else {
                facebookButton.trigger('click');
                $('.facebook-text').text('Connect your Facebook account');
            }
        });

        // $('.wc-address-book-edit').click(function (e) {
        //     e.stopPropagation();
        // });
        //
        // $('.wc-address-book-address').click(function () {
        //     $('.wc-address-book-make-primary', $(this)).trigger('click');
        // });

        $('.payment-check').click(function () {

            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'change_payment_method',
                    'nonce_code': myajax.nonce,
                    'data': $(this).attr('data-method')
                },
                success: function (data) {
                    $('.checked').addClass('display-none');
                    $('.unchecked').removeClass('display-none');

                    $(`div[data-method=${data}]>.checked`).removeClass('display-none');
                    $(`div[data-method=${data}]>.unchecked`).addClass('display-none');
                }
            });
        });

        $('label[for=shipping_country]').text($('label[for=shipping_country]').text().split('/')[0]);

        setTimeout(function () {
            $('label[for=shipping_state]').text('State');
        }, 200);

        $('.add-address-button').click(function (e) {
            if (!$('#shipping_address_nickname').val()) {
                e.preventDefault();
                $('#shipping_address_nickname').css('border-color', '#EC423E');
            }

            if (!validateEmail($('#shipping_email').val())) {
                e.preventDefault();
                $('#shipping_email').css('border-color', '#EC423E');
            }

            if (!$('#shipping_first_name').val()) {
                e.preventDefault();
                $('#shipping_first_name').css('border-color', '#EC423E');
            }

            if (!$('#shipping_last_name').val()) {
                e.preventDefault();
                $('#shipping_last_name').css('border-color', '#EC423E');
            }

            if (!validatePhone($('#shipping_phone').val())) {
                e.preventDefault();
                $('#shipping_phone').css('border-color', '#EC423E');
            }

            if (!$('#shipping_country').val()) {
                e.preventDefault();
                $('.select2-selection--single').css('border-color', '#EC423E');
            }

            if (!$('#shipping_city').val()) {
                e.preventDefault();
                $('#shipping_city').css('border-color', '#EC423E');
            }

            if (!$('#shipping_address_1').val()) {
                e.preventDefault();
                $('#shipping_address_1').css('border-color', '#EC423E');
            }

            if ($('#shipping_postcode').val().length < 5) {
                e.preventDefault();
                $('#shipping_postcode').css('border-color', '#EC423E');
            }
        });

        $('.wc-address-book-delete').attr('id', window.location.href.split('=')[1]);
        // $('.wc-address-book-delete').click(function () {
        //     $.ajax({
        //         type: 'POST',
        //         url: myajax.url,
        //         data: {
        //             'action': 'wc_address_book_delete',
        //             'nonce_code': myajax.nonce,
        //             'name': $(this).attr('id')
        //         },
        //         success: function (data) {
        //
        //         }
        //     });
        // });
    });

    function hasUpperCase(str) {
        return (/[A-Z]/.test(str));
    }

    function hasNumbers(str) {
        return (/[0-9]/.test(str));
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function validatePhone(phone) {
        const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        return re.test(String(phone).toLowerCase());
    }
})(jQuery);

