/* eslint-disable */
(function($) {
    let buttonChangeMobileCol = $('.col_1'),
        productClass = 'col-lg-4';
    buttonChangeMobileCol.on('click', function() {
        $('.product-item').addClass('col-12').removeClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_1').addClass('active-grid-button');
        productClass = 'col-12';
    });

    let buttonChangeMobileCol2 = $('.col_2');
    buttonChangeMobileCol2.on('click', function() {
        $('.product-item').removeClass('col-12').addClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_2').addClass('active-grid-button');
        productClass = 'col-6';
    });

    let buttonChangeMobileCol3 = $('.col_3');
    buttonChangeMobileCol3.on('click', function() {
        $('.product-item').addClass('col-lg-4').removeClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_3').addClass('active-grid-button');
        productClass = 'col-lg-4';
    });

    let buttonChangeMobileCol4 = $('.col_4');
    buttonChangeMobileCol4.on('click', function() {
        $('.product-item').removeClass('col-lg-4').addClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_4').addClass('active-grid-button');
        productClass = 'col-lg-3';
    });
    $('#sortBy').on('click', function() {
        $('#preset_369').addClass('open').show('300');
        $('.page-body').addClass('yith-wcan-preset-modal-open');
    });

    let currentPage = document.querySelector(".container-flex-row").dataset.page,
        maxPages = document.querySelector(".container-flex-row").dataset.max,
        allPosts = document.querySelector('.container-flex-row').dataset.allPosts,
        loadMoreBtn = document.getElementById('load_moress'),
        viewedPosts = document.querySelector('.viewedPosts'),
        posts = $('.container-flex-row [class*="product-item"]').length,
        search = document.querySelector(".container-flex-row").dataset.searched,
        percentViewedPosts = posts / allPosts * 100;
    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

    viewedPosts.innerText = posts;

    if(Number(currentPage) === Number(maxPages)) {
        loadMoreBtn.remove();
    }

    loadMoreBtn.addEventListener('click', function() {

        let button = $(this);
        $.ajax({
            url: myajax.url,
            data: { action: "ba_ajax_search_load_more", currentPage, productClass, maxPages, term : search },
            type: "POST",
            beforeSend() {
                button.text("Loading");
            },
            success(data) {
                if(data) {
                    let postsList = document.querySelector(".container-flex-row");
                    postsList.innerHTML += data;
                    posts = $('.container-flex-row [class*="product-item"]').length;
                    button.text(`Load ${(allPosts - posts) > 32 ? 32: (allPosts - posts)} more`);
                    currentPage++;
                    viewedPosts.innerText = posts;
                    percentViewedPosts = posts / allPosts * 100;
                    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

                    if(Number(currentPage) === Number(maxPages)) {
                        button.remove();
                    }
                } else {
                    button.remove();
                }
            }
        });
    });

    $('#sortBy').on('click', function() {
        $('#preset_369_0').addClass('open').show('300');
        $('.page-body').addClass('yith-wcan-preset-modal-open');
    });

    if ($(window).width() >= 1151) {
        $(document).on('mouseenter', '.filter-tax', function () {
            $(this).find('.filter-content').css('display', 'block')
        })
        $(document).on('mouseleave', '.filter-tax', function () {
            $(this).find('.filter-content').css('display', 'none')
        })
    }

})(jQuery);
