/* eslint-disable */
/*eslint max-lines: 1000*/
(function($) {
    document.addEventListener('DOMContentLoaded', function() {
        const lensesConfigurations = $('p', $('th:contains("Lenses configuration:")').next());
        const prescriptionsConfigurations = $('p', $('th:contains("Prescriptions:")').next());

        lensesConfigurations.each(function (index, elem) {
            const json = JSON.parse($(this).text().split('\\').join(''));

            let output = '<table>';

            json.forEach(function (elem, index) {
                if (index !== 1) {
                    output += '<tr><td>' + elem.fullName + '</td><td>' + elem.ap + '€</td></tr>';
                }
            });

            output += '</table>';

            $(this).html(output);
        });

        console.log(prescriptionsConfigurations)

        prescriptionsConfigurations.each(function (index, elem) {
            const json = JSON.parse($(this).text().split('\\').join(''));

            if (Object.keys(json).length !== 0) {
                const pd2 = json.pd2 || '';

                let output = '<table><tr><td style="border: 1px solid #888;"></td><td style="border: 1px solid #888;"><b>SPH</b></td><td style="border: 1px solid #888;"><b>SYL</b></td><td style="border: 1px solid #888;"><b>Axis</b></td><td style="border: 1px solid #888;"><b>ADD</b></td><td style="border: 1px solid #888;"><b>PD</b></td></tr>'
                    + '<tr><td style="border: 1px solid #888;"><b>OD(R)</b></td><td style="border: 1px solid #888;">' + json.odSph + '</td><td style="border: 1px solid #888;">' + json.odCyl + '</td><td style="border: 1px solid #888;">' + json.odAxis + '</td><td style="border: 1px solid #888;">' + json.odAd + '</td><td style="border: 1px solid #888;">' + json.pd1 + '</td>'
                    + '<tr><td style="border: 1px solid #888;"><b>OD(R)</b></td><td style="border: 1px solid #888;">' + json.osSph + '</td><td style="border: 1px solid #888;">' + json.osCyl + '</td><td style="border: 1px solid #888;">' + json.osAxis + '</td><td style="border: 1px solid #888;">' + json.osAd + '</td><td style="border: 1px solid #888;">' + pd2 + '</td>'
                    + '</tr></table>';

                if (json.comment) {
                    output += '<div>' + json.comment + '</div>'
                }

                if (json.odVertical) {
                    output += '<table>'
                    + '<tr><td style="border: 1px solid #888;"></td><td style="border: 1px solid #888;">Vertical</td><td style="border: 1px solid #888;">Base direction</td><td style="border: 1px solid #888;">Horizontal</td><td style="border: 1px solid #888;">Base direction</td></tr>'
                    + '<tr><td style="border: 1px solid #888;">OD</td><td style="border: 1px solid #888;">' + json.odVertical + '</td><td style="border: 1px solid #888;">' + json.odVerticalBasedirection + '</td><td style="border: 1px solid #888;">' + json.odHorizontal + '</td><td style="border: 1px solid #888;">' + json.odHorizontalBasedirection + '</td></tr>'
                    + '<tr><td style="border: 1px solid #888;">OS</td><td style="border: 1px solid #888;">' + json.osVertical + '</td><td style="border: 1px solid #888;">' + json.osVerticalBasedirection + '</td><td style="border: 1px solid #888;">' + json.osHorizontal + '</td><td style="border: 1px solid #888;">' + json.osHorizontalBasedirection + '</td></tr>'
                    + '</table>'

                }


                $(this).html(output);
            }

        });
    });
})(jQuery);
