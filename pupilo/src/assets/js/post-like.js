/* eslint-disable */

jQuery(document).ready(function($) {
    $(".noactive_svg").click(function() {
        like = $(this);
        post_id = like.data("post_id");
        $.ajax({
            type: "post",
            url: ajax_var.url,
            data: {
                action: 'post-like',
                nonce: ajax_var.nonce,
                post_like: '',
                post_id
            },
            success: function(count) {
                if(count !== "already") {
                    like.addClass("is-active");
                    $(".likecount").text(count)
                }
            }
        });
        return false;
    });
});
