/* eslint-disable */
import '@scss/pages/_brands';

const el = document.querySelector(".alphabet")
const observer = new IntersectionObserver(
    ([e]) => e.target.classList.toggle("stuck", e.intersectionRatio < 1),
    { threshold: [1] }
);

observer.observe(el);

$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});

let topMenu = $(".alphabet"),
    topMenuHeight = topMenu.outerHeight(),
    menuItems = topMenu.find("a"),
    scrollItems = menuItems.map(function(){
        let item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

$(window).scroll(function(){
    let fromTop = $(this).scrollTop()+topMenuHeight;

    let cur = scrollItems.map(function(){
        if ($(this).offset().top + 110 < fromTop )
            return this;
    });
    cur = cur[cur.length-1];
    let id = cur && cur.length ? cur[0].id : "";
    menuItems
        .parent().removeClass("active")
        .end().filter("[href='#"+id+"']").parent().addClass("active");
    // let current = document.getElementsByClassName("alphabet__letter active");
    // current[0].scrollIntoView({
    //     block: "nearest",
    //     inline: "nearest",
    //     behavior: "smooth"
    // });
});

const slider = document.querySelector(".alphabet");
let isDown = false;
let startX;
let scrollLeft;

slider.addEventListener("mousedown", e => {
    isDown = true;
    slider.classList.add("active-scroll");
    startX = e.pageX - slider.offsetLeft;
    scrollLeft = slider.scrollLeft;
});
slider.addEventListener("mouseleave", () => {
    isDown = false;
    slider.classList.remove("active-scroll");
});
slider.addEventListener("mouseup", () => {
    isDown = false;
    slider.classList.remove("active-scroll");
});
slider.addEventListener("mousemove", e => {
    if (!isDown) return;
    e.preventDefault();
    const x = e.pageX - slider.offsetLeft;
    const walk = x - startX;
    slider.scrollLeft = scrollLeft - walk;
});
