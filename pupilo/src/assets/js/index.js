/* eslint-disable */
(function ($) {

    if ($('.main-nav').length) {
        let hamburgerButton = document.getElementById("toggle"),
            menuHamburger = document.getElementById("toggle"),
            htmlHide = document.getElementById("html--hidden"),
            mainNav = document.getElementById("main-nav");

        function closeSubMenu() {
            function hasClass(element, className) {
                return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
            }

            for (let i = 0; i < menuItem.length; i++) {
                if (hasClass(menuItem[i], "activeSubMenu")) {
                    menuItem[i].classList.remove("activeSubMenu");
                }
            }
        }

        hamburgerButton.addEventListener("click", function () {
            menuHamburger.classList.toggle("btn-active");
            htmlHide.classList.toggle("html--hidden");
            mainNav.classList.toggle("menu-open");

            closeSubMenu();
            if (!hamburgerButton.classList.contains('btn-active')) {
                $('#html--hidden').removeClass('html--hidden');
            }
        });

    }

    let link = document.querySelector(".menu-item-has-children a");
    if (link) {
        link.addEventListener("click", function (e) {
            e.preventDefault();
        });
    }

    let menuItem = document.getElementsByClassName("menu-item-has-children");

    for (let i = 0; i < menuItem.length; i++) {
        menuItem[i].addEventListener("click", function () {
            this.classList.add("activeSubMenu");
        });
    }

    $('#back')
        .on('click', function (event) {
            event.stopPropagation();
            closeSubMenu();
        });

    $('.eyeglasses, .open-eyeglasses')
        .mouseenter(function (event) {
            event.stopPropagation();
            $('.eyeglasses')
                .addClass('menu-open');
        });
    $('.eyeglasses, .open-eyeglasses')
        .mouseleave(function () {
            $('.eyeglasses')
                .removeClass('menu-open');
        });

    $('.sunglasses, .open-sunglasses')
        .mouseenter(function (event) {
            event.stopPropagation();
            $('.sunglasses')
                .addClass('menu-open');
        });
    $('.sunglasses, .open-sunglasses')
        .mouseleave(function () {
            $('.sunglasses')
                .removeClass('menu-open');
        });
    $('.contact-lenses, .open-contact-lenses')
        .mouseenter(function (event) {
            event.stopPropagation();
            $('.contact-lenses')
                .addClass('menu-open');
        });
    $('.contact-lenses, .open-contact-lenses')
        .mouseleave(function () {
            $('.contact-lenses')
                .removeClass('menu-open');
        });
    $('.special-glasses, .open-special-glasses')
        .mouseenter(function (event) {
            event.stopPropagation();
            $('.special-glasses')
                .addClass('menu-open');
        });
    $('.special-glasses, .open-special-glasses')
        .mouseleave(function () {
            $('.special-glasses')
                .removeClass('menu-open');
        });
    $(window).on("load resize", () => {
        if ($(window).width() <= 1152) {
            $('.page-header-bottom .header-logo-link').show();
        } else {
            $('.page-header-bottom .header-logo-link').hide();
        }
        $(window).scroll(function () {
            if ($(window).width() >= 1152) {
            if ($(window).scrollTop() >= 110) {
                $('.page-header-top').addClass('sticky-header');
                $('.desktop-mega-menu').css('top', 56);
                $('.page-header-bottom .header-logo-link').show();
                $('.page-header-bottom .iconic-menu').show();
                $('.page-header-bottom').css('justify-content', 'space-between');
            } else {
                $('.page-header-top').removeClass('sticky-header');
                $('.desktop-mega-menu').css('top', 139);
                $('.page-header-bottom .header-logo-link').hide();
                $('.page-header-bottom .iconic-menu').hide();
                $('.page-header-bottom').css('justify-content', 'center');
            }
        }
        });
    });

    if ($(window).width() <= 768) {
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    }

    $(document).ready( () => {
        let hideShowButton = $('<svg viewBox="0 0 24 24" width="24" height="24" class="show-pass-popup">\n' +
            '<use xlink:href="#show-password"></use>\n' +
            '</svg>')
        hideShowButton.insertAfter('.som-pass-strength-input')
        hideShowButton.insertAfter('.som-password-input')

    })
    const contentContainerEl = document.querySelector('[data-login-check]');
    if ( contentContainerEl.hasAttribute('visitor')) {
        $(document).on('click', '.login-menu-open', function (e) {
            e.preventDefault()
            e.stopPropagation()
            $('.login-wrap').addClass('login-wrap-open');
            $('.login-info').addClass('login-info-modal-open');
            $('#html--hidden').addClass('html--hidden');
        });
    }
    $('.woocommerce-form-login__submit').click(() => {
        $('.login-wrap').addClass('login-wrap-open');
        $('.login-info').addClass('login-info-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('[data-login-open]').click(() => {
        $('.login-info').removeClass('login-info-modal-open');
        $('.login-modal').addClass('login-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('.lens-sign-in').click(() => {
        $('.login-info').removeClass('login-info-modal-open');
        $('.login-modal').addClass('login-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('[data-return-to-info]').click(() => {
        $('.login-modal').removeClass('login-modal-open');
        $('.sign-up-modal').removeClass('sign-up-modal-open');
        $('.sign-up-with-email-modal').removeClass('sign-up-with-email-modal-open');
        $('.login-wrap').addClass('login-wrap-open');
        $('#html--hidden').addClass('html--hidden');
        $('.login-info').addClass('login-info-modal-open');
        $('.reset-password-modal').removeClass('reset-password-modal-open')
    });

    $('[data-close-confirm]').click(() => {
        $('.reset-pass-confirmation').removeClass('reset-pass-confirmation-open');
        $('.login-modal').addClass('login-modal-open');
        $('.login-wrap').addClass('login-wrap-open');
        $('#html--hidden').addClass('html--hidden');
    })

    $('[data-return-to-login]').click(() => {
        window.location.href = window.location.protocol + '//' + window.location.hostname
        $('.reset-password-modal').removeClass('reset-password-modal-open');
        $('.login-modal').addClass('login-modal-open');
        $('.login-wrap').addClass('login-wrap-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('[data-sign-up-modal-open]').click(() => {
        $('.sign-up-with-email-modal').addClass('sign-up-with-email-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('[data-sign-up-open]').click(() => {
        $('.sign-up-modal').addClass('sign-up-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('[data-forgot-open]').click(() => {
        $('.recover-password-modal').addClass('recover-password-modal-open');
        $('#html--hidden').addClass('html--hidden');
    });

    $('.login-info-popup-close').click(() => {
        $('.login-wrap').removeClass('login-wrap-open');
        $('.login-info').removeClass('login-info-modal-open');
        $('#html--hidden').removeClass('html--hidden');
    })

    if (window.location.href === window.location.protocol + '//' + window.location.hostname + window.location.pathname + '/?password-reset=true') {
        $('.login-modal').addClass('login-modal-open');
        $('.login-wrap').addClass('login-wrap-open');
        $('#html--hidden').addClass('html--hidden');
    }
    $(window).on('load', () => {
        if (window.location.href === window.location.protocol + '//' + window.location.hostname + window.location.pathname + '?show-reset-form=true&action') {
            window.location.href === window.location.protocol + '//' + window.location.hostname
            $('.reset-password-modal').addClass('reset-password-modal-open');
            $('.login-wrap').addClass('login-wrap-open');
            $('#html--hidden').addClass('html--hidden');
        }
    })

    $(window).on('load', () => {
        if (window.location.href === window.location.protocol + '//' + window.location.hostname + '/?loggined') {
            setTimeout(function(){
                $('[data-registered-user]').addClass('log-in-user-modal-open')
                $('.login-wrap').addClass('login-wrap-open');
            }, 1000);
        }
    })
    $('[data-close-popup]').click(() => {
        $('[data-registered-user]').removeClass('log-in-user-modal-open')
        $('.login-wrap').removeClass('login-wrap-open');
        $('#html--hidden').removeClass('html--hidden');
        window.location.href = window.location.protocol + '//' + window.location.hostname
    })

    $('#reset-pass-submit').click(() => {
        setTimeout(function(){
            window.location.href = window.location.protocol + '//' + window.location.hostname + '/?link-send'
        }, 500)
    })

    $(window).on('load', () => {
        if (window.location.href === window.location.protocol + '//' + window.location.hostname + '/?link-send') {
            setTimeout(function(){
                $('.reset-password-modal').addClass('reset-password-modal-open');
                if ($('.som-password-sent-message').length === 0) {
                    $( ".sent-status-message" ).text('An email has been sent. Please check your inbox.');
                }
            }, 200);
        }

        if (window.location.href.indexOf("/?somresetpass") > -1) {
            $('.reset-password-modal').addClass('reset-password-modal-open');
            $('.login-wrap').addClass('login-wrap-open');
        }
    })

    $(window).on('load', () => {
        if (window.location.href === window.location.protocol + '//' + window.location.hostname + '/?som_password_reset=true') {
            setTimeout(function () {
                $('.reset-pass-confirmation').addClass('reset-pass-confirmation-open');
                $('.login-wrap').addClass('login-wrap-open');
                $('.login-modal').addClass('login-modal-open');
            }, 300);
        }
    });

    $(document).ready( () => {
        $('.show-pass-popup').click(function () {
            if ($(this).prev().attr('type') === 'password') {
                $(this).prev().attr('type', 'text');
            } else {
                $(this).prev().attr('type', 'password');
            }
        });
    })
        //wishlist start

    // $('.wish-product-link, .wish-single-product-link').click(function () {
    $(document).on('click', '.wish-product-link, .wish-single-product-link', function () {
        console.log($(this))
        $.ajax({
            type: 'POST',
            url: myajax.url,
            data: {
                'action': 'wishlist',
                'nonce_code': myajax.nonce,
                'product_id': $(this).attr('data-id')
            },
            success: function( data ) {
                // console.log(data);
            }
        });
    });
    //wishlist finish
    function changeTotal() {
        setTimeout(function () {
            let cost = $('.our-suggestions-info').attr('data-cost');
            $('.cart-subtotal .cost').text(cost)
        },3000)
    }
    $(document).on('click', ".quantity-down", function () {

        if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1).trigger("change");
            $('button[name="update_cart"]').trigger("click");
            changeTotal()
        }
    })

    $(document).on('click', ".quantity-up", function () {
        $(this).prev().val(+$(this).prev().val() + 1).trigger("change");
        $('button[name="update_cart"]').trigger("click");
        changeTotal()
    })
    $('.go-to-checkout').fadeOut()
    $(window).on('load', () => {
        let countCartItem = $('.cart_item').length;
        if (countCartItem > 0 ) {
            $('.control-button').addClass('show-control-button')
            $('.go-to-checkout').fadeIn();
        }
    });
    $(window).on('load', () => {
        let countCartItem = $('.cart_item').length;
        if (countCartItem > 0 ) {
            $('.control-button2').addClass('show-control-button')
            $('.go-to-checkout').fadeIn();
        }
    });

    $(document).on('click', "input[type='radio']", function(){
        if($(this).attr('value') === 'checked') {
            $('.go-to-login').fadeOut(100);
            $('.go-to-checkout').fadeIn(100);
        } else {
            $('.go-to-login').fadeIn(100);
            $('.go-to-checkout').fadeOut(100);
        }
    });

    $('.bag-menu-item').on("click", function(){
        $('.aside-cart-wrapper').addClass('aside-cart-wrapper-open')
        $('#html--hidden').addClass('html--hidden');
    })

    $('.aside-cart-close').on("click", function(){
        $('.aside-cart-wrapper').removeClass('aside-cart-wrapper-open');
        $('#html--hidden').removeClass('html--hidden');
    })

    $(window).on('load', () => {
        let countCartItems = $('.cart_item').length,
            productCount;
        if (countCartItems > 0) {
            productCount = countCartItems + ' item';
            $( '<span class="product-count">' + productCount + '</span>' ).insertAfter( ".bag-menu-item" );
            $( '<span class="cart-header-product-count">' + '(' + productCount + ')' + '</span>' ).insertAfter( ".cart-header-count" );
        }
        else {
            productCount = '';
        }
    });

    $(window).on('load', () => {
        let countCartItems = $('.cart_item').length,
            productCount;
        if (countCartItems > 0) {
            productCount = countCartItems;
            $( '<span class="decor"></span>' ).insertAfter( ".menu-icon--bag" );
            $('.free-delivery').css('display', 'none')
        }
        else {
            productCount = '';
        }

        $( '<span class="mobile-product-count">' + productCount + '</span>' ).insertAfter( ".menu-icon--bag" );

    });

    $(window).on('load', () => {
        let countCartItems = $('.cart_item').length,
            productCount;
        if (countCartItems > 0) {
            productCount = '(' + countCartItems + ')';
        }
        else {
            productCount = '';
        }

        $( '<span class="product-count">' + productCount + '</span>' ).insertAfter( ".aside-headline" );

    });

    $('.cart-sign-in').on("click", function(){
        $('.login-wrap').addClass('login-wrap-open');
        $('.login-modal').addClass('login-modal-open');
        $('#html--hidden').addClass('html--hidden');
        $('.aside-cart-wrapper').removeClass('aside-cart-wrapper-open');
    })

    $('.cart-sign-up').on("click", function(){
        $('.login-wrap').addClass('login-wrap-open');
        $('.sign-up-modal').addClass('sign-up-modal-open');
        $('#html--hidden').addClass('html--hidden');
        $('.aside-cart-wrapper').removeClass('aside-cart-wrapper-open');
    })

    $( document ).on("click", '.order-item-pre-heading', function() {
        $('.lenses-configuration', $(this).parent()).toggle(300);
        $('.order-product-open-lenses', $(this)).toggleClass('rotate-180');
        let parentEl = $(this).closest($('.cart_item'));
        parentEl.find('.base-price').toggleClass('showHidePrice')
    });
    $( document ).on("click", '.order-lens-item-pre-heading', function() {
        $('.lenses-configuration', $(this).parent()).toggle(300);
        $('.order-product-open-lenses', $(this)).toggleClass('rotate-180');
        let parentEl = $(this).closest($('.cart_item'));
        parentEl.find('.base-price').toggleClass('showHidePrice')
    });
    if ($('[data-login-template]').length) {
    const loginTemplateEl = document.querySelector('[data-login-template]').content;

    const loginMenuEl = document.querySelector('.mobile-login-menu-item');
    const loginDesktopMenuEl = document.querySelector('.desktop-login-menu-item');
    const node = document.querySelector(".mobile-login-menu-item a");
    const nodeDesktop = document.querySelector(".desktop-login-menu-item a");

    function renderPosts() {
        if (node) {
            if ( contentContainerEl.hasAttribute('registered')) {
                node.parentNode.removeChild(node);
                nodeDesktop.parentNode.removeChild(nodeDesktop);

                const copyTemplate = loginTemplateEl.cloneNode(true);
                const copyTemplate2 = loginTemplateEl.cloneNode(true);
                loginMenuEl.appendChild(copyTemplate);
                loginDesktopMenuEl.appendChild(copyTemplate2);
                loginMenuEl.classList.add('mobile-login-menu-item-decor')
                loginDesktopMenuEl.classList.add('mobile-login-menu-item-decor')
            } else {
                loginMenuEl.classList.remove('mobile-login-menu-item-decor');
                $('.mobile-login-menu-item').addClass('login-menu-open')
            }
        }
    }

    renderPosts();
    }
    if ($( ".login-menu-item" ).length) {

    $(window).on('load resize scroll', function () {
        let menuPosLeft = $( ".login-menu-item" ).offset().left;
        let helpMenuPosLeft = $('.help-menu').offset().left;
        $('.account-menu').css('left', menuPosLeft);
        $('.help-popup').css('left', helpMenuPosLeft);
    });

    $('.account-menu-list .menu-item').removeClass('mobile-login-menu-item-decor')

    if ( contentContainerEl.hasAttribute('registered')) {
        $('.account-menu, .login-menu-item').click(function () {
            $('.account-menu').toggleClass('account-menu-open');
        });
    }

    $('.help-menu').on("click", function(e) {
       e.preventDefault()
        $('.help-popup').toggleClass('help-menu-open');
    })
    }
    var params = {
        // apiKey: 'CfM4xcxR5jcyUeyWn5734wcrfgQ5tEXqyYx1lLaf',
        apiKey: 'DDrOzO9fZmfDdGJX3pyob0ZSKu33Jwn30gMNuqiW',
        config: {
            preset: 'v8'
        },
    };

    $(document).on("click", '.btn--try-on', function () {
       $('.product-try-on-popup').addClass('product-try-on-popup-open')
        sku = $(this).attr('[data-show]');

        fitmixInstance = FitMix.createWidget('my-fitmix-container1', params, function () {
            fitmixInstance.setFrame(String(sku));

            // setPupillaryDistance can be called in the createWidget callback, or later
            // 40 mm is a test value that will show enormous glasses
            fitmixInstance.setPupillaryDistance(50); // in millimeters
        });
    });

    $(document).on("click", '.product-try-on-popup-close', function () {
        $('.product-try-on-popup').removeClass('product-try-on-popup-open')
    })

    $(document).on('click', ".btn-open-suggestions-modal", function () {

            $('.suggestions-modal-overlay').addClass('suggestions-modal-overlay__open');
        if ($(window).width() >= 991) {
            setTimeout(function() {

                $(".suggestions-slider").slick({
                    infinite: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false,
                    prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>",
                    nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                        "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#59af9e\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
                        "</svg>"
                })
            }, 300)
        }
        $('.aside-cart-close').trigger('click')
        // $('.filter-wrap .close-button').trigger('click')
    })
    $('.button-close-suggestions-modal').on('click', function () {
        $('.suggestions-modal-overlay').removeClass('suggestions-modal-overlay__open')
    })

    $(document).on('click', ".add-to-cart-suggestion-product", function () {
        let myAtr = $('.attribute-color-after');
        let el = $('.product-thumbnail');
        let productVarId = $(this).parent().find(myAtr).attr('data-varid');

        let productId = $(this).parent().find(el).attr('data-product-id')

        $.ajax({
            type: 'POST',
            url: `//${window.location.host}/wp-admin/admin-ajax.php`,
            data: {
                'action': 'add_accessories',
                'product_id': productId,
                'product_var_id': productVarId
            },
            success: function( data ) {
                if ($(".our-suggestions").hasClass("slick-initialized")) {
                    $(".our-suggestions").slick("unslick");
                }

                $('.btn-close').trigger("click");
                $('button[name="update_cart"]').prop('disabled', false).trigger("click");
                changeTotal()
            }
        });
    })

})(jQuery);

jQuery(function($){
    var searchTerm = '';
    $('.search-input').attr('autocomplete','off');
    $('.search-input').keydown(function(){
        searchTerm = $.trim($(this).val());
    });

    $('.search-input').keyup(function(){
        if ($.trim($(this).val()) != searchTerm) {
            searchTerm = $.trim($(this).val());
            if(searchTerm.length > 2){
                $.ajax({
                    url : myajax.url,
                    type: 'POST',
                    data: {
                        'action' : 'ba_ajax_search',
                        'term' : searchTerm
                    },
                    beforeSend: function() {
                        // $('.result-search .result-search-list').fadeOut();
                        $('.result-search .result-search-list').empty();
                        // $('.result-search .preloader').show();
                    },
                    success: function(result) {
                        // $('.result-search .preloader').hide();
                        $('.result-search .result-search-list').fadeIn().html(result);
                        $('.result-search').css('height', '85vh');

                        let titleHeight = $('.product-title').height();
                        let titles = $('.product-title t');

                        titles.each(function () {
                            while ($(this).outerHeight() > titleHeight) {
                                $(this).text(function (index, text) {
                                    return text.replace(/\W*\s(\S)*$/, '...');
                                });
                            }
                        });
                    }
                });
            }
        }
    });

    $('.header-search').mouseleave(function () {
        $('.header-search').removeClass('header-search-open');
        $('#html--hidden').removeClass('html--hidden');
    })
    $('.search-input').focusin(function() {
        $('.result-search').fadeIn();
    })

    $(document).on("submit", "#searchform", function () {
        let recentSearch = $('.search-input').val();
        let linkRecent = Cookies.get('recent');
        if (recentSearch !== '') {

            if (typeof linkRecent === 'undefined') {
                Cookies.set('recent', recentSearch);
            } else if (linkRecent !== 'undefined') {
                let resCookie = linkRecent.split(',');
                if (resCookie.length <= 4){
                    let searchTerms = linkRecent + ',' + recentSearch;
                    Cookies.set('recent', searchTerms);
                } else {
                    let updatedCookie = resCookie.shift();
                    let updateCookieString = resCookie.join()
                    let searchTerms = updateCookieString + ',' + recentSearch;
                    Cookies.set('recent', searchTerms);
                }
            }
        }
    })

    $(window).on('load', function () {
        let linkRecent = Cookies.get('recent');
        let recentTitle = $('.suggestion-item-title');
        if (typeof linkRecent === 'undefined') {
            console.log('empty');

        } else {
            recentTitle.html('Recent searches')
        }
    });

    $(window).on('load resize scroll', function () {
        if ($(window).width() >= 1152) {
        let menuSearchLeft = $( ".search-menu-item" ).offset().left;
        let searsWidthEl = $(".header-search").width();

        $('.header-search').css('left', menuSearchLeft - searsWidthEl + 56);
        }
    });
    $(document).on('click', ".search-menu-item", function () {
        $('.header-search ').addClass('header-search-open');
        let menuSearchLeft = $( ".search-menu-item" ).offset().left;
        let searsWidthEl = $(".header-search").width();
        $('#searchform').removeClass('display-none');
        $('.header-search').css('left', menuSearchLeft - searsWidthEl + 52);
        $('#html--hidden').addClass('html--hidden');
    })

    $(document).on('click', ".menu-icon--search", function () {
        $('.header-search-modal-wrap').addClass('header-search-modal-wrap-open');
        $('.header-search-modal').addClass('header-search-modal-open');
        $('#html--hidden').addClass('html--hidden');
    })
    $(document).on('click', ".button-close-search", function () {
        $('.header-search-modal-wrap').removeClass('header-search-modal-wrap-open');
        $('.header-search-modal').removeClass('header-search-modal-open');
        $('#html--hidden').removeClass('html--hidden');
    })

});
