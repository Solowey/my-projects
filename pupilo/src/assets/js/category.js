import '@scss/pages/_category.scss';
import 'slick-carousel';
import './wishlist';
/*es-lint-disable */
/* eslint-disable */
(function($) {

    // $(window).on('load', () => {
    //     let countWishItems = $('.product-row .product-item').length,
    //         productCount;
    //     if (countWishItems > 0) {
    //         productCount = countWishItems;
    //         $( '<span class="wish-product-count">' + '(' + productCount + ')' + '</span>' ).insertAfter( ".wish-product-count" );
    //         $( '<span class="wish-counter">' + productCount + '</span>' ).insertAfter( ".wish-counter" );
    //     }
    //     else {
    //         productCount = '';
    //     }
    // });

    document.addEventListener('DOMContentLoaded', function() {
        let breadcrumbsHtml = $('.breadcrumbs').html() || $('.woocommerce-breadcrumb').html();

        if (breadcrumbsHtml.indexOf('/product-category/eyeglasses/"') >= 0) {
            breadcrumbsHtml = breadcrumbsHtml.split('/product-category/eyeglasses/"').join('/eyeglasses/"');
        }

        if (breadcrumbsHtml.indexOf('/product-category/sunglasses/"') >= 0) {
            breadcrumbsHtml = breadcrumbsHtml.split('/product-category/sunglasses/"').join('/sunglasses/"');
        }

        if (breadcrumbsHtml.indexOf('/product-category/contact-lenses/"') >= 0) {
            breadcrumbsHtml = breadcrumbsHtml.split('/product-category/contact-lenses/"').join('/contact-lenses/"');
        }

        $('.woocommerce-breadcrumb').html(breadcrumbsHtml);
    });

    let buttonChangeMobileCol = $('.col_1'),
        productClass = 'col-lg-4';
    buttonChangeMobileCol.on('click', function() {
        $('.product-item').addClass('col-12').removeClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_1').addClass('active-grid-button');
        productClass = 'col-12';
    });

    let buttonChangeMobileCol2 = $('.col_2');
    buttonChangeMobileCol2.on('click', function() {
        $('.product-item').removeClass('col-12').addClass('col-6');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_2').addClass('active-grid-button');
        productClass = 'col-6';
    });

    let buttonChangeMobileCol3 = $('.col_3');
    buttonChangeMobileCol3.on('click', function() {
        $('.product-item').addClass('col-lg-4').removeClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_3').addClass('active-grid-button');
        productClass = 'col-lg-4';
    });

    let buttonChangeMobileCol4 = $('.col_4');
    buttonChangeMobileCol4.on('click', function() {
        $('.product-item').removeClass('col-lg-4').addClass('col-lg-3');
        $('.toggle-grid').removeClass('active-grid-button');
        $('.col_4').addClass('active-grid-button');
        productClass = 'col-lg-3';
    });
    $('#sortBy').on('click', function() {
        $('#preset_369').addClass('open').show('300');
        $('.page-body').addClass('yith-wcan-preset-modal-open');
    });

    let currentPage = document.querySelector(".product-row").dataset.page,
        maxPages = document.querySelector(".product-row").dataset.max,
        allPosts = document.querySelector('.product-row').dataset.allPosts,
        loadMoreBtn = document.getElementById('load_more'),
        viewedPosts = document.getElementById('viewedPosts'),
        posts = $('.product-row [class*="product-item"]').length,
        percentViewedPosts = posts / allPosts * 100;
    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

    viewedPosts.innerText = posts;

    if(Number(currentPage) === Number(maxPages)) {
        loadMoreBtn.remove();
    }
    let productCategory = $('[data-category]').attr('data-category');

    loadMoreBtn.addEventListener('click', function() {
        // eslint-disable-next-line no-invalid-this
        let button = $(this);
        $.ajax({
            // url: "../wp-admin/admin-ajax.php",
            url: myajax.url,
            data: { action: "load_more_products", currentPage, maxPages, productClass, productCategory },
            type: "POST",
            beforeSend() {
                button.text("Loading");
            },
            success(data) {
                if(data) {
                    let postsList = document.querySelector(".product-row");
                    postsList.innerHTML += data;
                    posts = $('.product-row [class*="product-item"]').length;
                    button.text("Load more");
                    currentPage++;
                    viewedPosts.innerText = posts;
                    percentViewedPosts = posts / allPosts * 100;
                    $(".load-more_progress").css("width", `${percentViewedPosts}%`);

                    if(Number(currentPage) === Number(maxPages)) {
                        button.remove();
                    }
                } else {
                    button.remove();
                }
            }
        });
    });

    // $(document).ready(function () {
    //     let filterItems = $('.filter-tax');
    //
    //     filterItems.each(function(index, value) {
    //         $(this).on('click', function () {
    //             console.log($(this));
    //         })
    //
    //     });
    // })

    if ($(window).width() >= 1151) {
        $(document).on('mouseenter', '.filter-tax', function () {
            $(this).find('.filter-content').css('display', 'block')
        })
        $(document).on('mouseleave', '.filter-tax', function () {
            $(this).find('.filter-content').css('display', 'none')
        })
    }

    if ($('.log-in-user-modal')[0].hasAttribute('data-registered-user')) {
        $(window).on('load', function () {
            if ($(window).width() >= 1152) {
                let elemEl = $('[data-taxonomy="filter_feather"] .filter-content');
                elemEl.append("<div class='add-prescription-wrap'><p class='menu-add-prescription'> Added your prscription yet?  </p><button class='btn-open-prescription-modal'>Enter Your Precription </button></div>");
            }
            if ($(window).width() <= 1151) {
                let elemEl = $('.filters-container form');
                let contentEl = $("<div class='add-prescription-wrap'><p class='menu-add-prescription'> Added your prscription yet?  </p><button class='btn-open-prescription-modal'>Enter Your Precription </button></div>");
                contentEl.insertAfter(elemEl)
                $('.filter-content').css('z-index', '2 !important');
            }
        });
    }

    $(document).on('click', ".btn-open-prescription-modal", function () {
        $('.enter-you-prescription-modal-overlay').addClass('enter-you-prescription-modal-overlay__open');

        $('.filter-wrap .close-button').trigger('click')
    })
    $('.button-close-prescription-modal').on('click', function () {
        $('.enter-you-prescription-modal-overlay').removeClass('enter-you-prescription-modal-overlay__open')
    })

    if ($(window).width() <= 992) {
        $('.featured-articles').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            fade: true,
            arrows: false
        });
    }

    $('.lens-add-comment').click(function () {
        $(this).next('.prescription-comment').toggle();
        $('.show-more-chevron').toggleClass('show-more-chevron-open');
        $(this).next().next('.add-prism-checkbox').toggle();
    });

    $('#add-prism').click(function () {
        if ($(this).prop('checked') === true) {
            $('.prism-wrapper').removeClass('display-none');
        } else {
            $('.prism-wrapper').addClass('display-none');
        }
    });

    $('#two-pd').click(function () {
        if ($(this).prop('checked') === true) {
            $('#pd-2').removeClass('display-none');
            $('.select-wrapper-left').removeClass('display-none');
        } else {
            $('#pd-2').addClass('display-none');
            $('.select-wrapper-left').addClass('display-none');
            // $('#pd-2').val([]);
        }
    });

    $('#prescription_name-filter', '.add-prescription-form').keyup(function () {
        if($(this).val() !== '') {
            $('.save-prescription-button', '.add-prescription-form').prop('disabled', false);
        } else {
            $('.save-prescription-button', '.add-prescription-form').prop('disabled', true);
        }
    });

    console.log($('select[name=od-sph]', '.wizard2'))
    $('select[name=od-sph]', '.wizard2').change(function () {
        console.log($(this).val())
    });

    $(document).on('click', '.save-filter-prescription', function (e) {
        e.preventDefault();
        e.stopPropagation();

        // $(this).removeClass('save-new-pre-button');
        // $('.wizard2-continue').text('Select and continue');
        // $('.save-new-pre-label').addClass('display-none');

        let name = $('#prescription_name-filter', '.add-prescription-form').val();

        $('#prescription_name-filter', '.add-prescription-form').val();

        if (name) {
            $.ajax({
                type: 'POST',
                url: myajax.url,
                data: {
                    'action': 'save_prescriptions',
                    'nonce_code': myajax.nonce,
                    'prescription_name': name,
                    'od-sph': $('#od-sph', '.add-prescription-form').val(),
                    'od-cyl': $('#od-cyl', '.add-prescription-form').val(),
                    'os-sph': $('#os-sph').val(),
                    'os-cyl': $('#os-cyl').val(),
                    'od-axis': $('#od-axis').val(),
                    'od-ad': $('#od-ad').val(),
                    'os-axis': $('#os-axis').val(),
                    'os-ad': $('#os-ad').val(),
                    'pd-1': $('#pd-1').val(),
                    'pd-2': $('#pd-2').val(),
                    'comment': $('#comment').val(),
                    'od-vertical': $('#od-vertical').val(),
                    'od-v-basedirection': $('#od-v-basedirection').val(),
                    'os-vertical': $('#os-vertical').val(),
                    'os-v-basedirection': $('#os-v-basedirection').val(),
                    'od-horizontal': $('#od-horizontal').val(),
                    'od-h-basedirection': $('#od-h-basedirection').val(),
                    'os-horizontal': $('#os-horizontal').val(),
                    'os-h-basedirection': $('#os-h-basedirection').val(),
                },
                success: function (data) {
                    console.log(data);
                    console.log($('select[name=od-sph]', '.wizard2').val());
                    $('.button-close-prescription-modal').trigger('click');
                }
            });
        }
    });


})(jQuery);
