import '@scss/pages/_faq';
/* eslint-disable */
(function ($) {
    document.addEventListener('DOMContentLoaded', function () {
        let acc = document.getElementsByClassName("faq_accordion");
        let i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                let panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

        filterSelection("all")

        function filterSelection(c) {
            let x, i;
            x = document.getElementsByClassName("acc-wrap");
            if (c == "all") c = "";
            // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
            }
        }

        function w3AddClass(element, name) {
            let i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        function w3RemoveClass(element, name) {
            let i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        let btnContainer = document.getElementById("filter_buttons");
        let btns = btnContainer.getElementsByClassName("filter_button");
        for (let j = 0; j < btns.length; j++) {
            btns[j].addEventListener("click", function () {
                let data = this.dataset.filter
                filterSelection(data)
                let current = document.getElementsByClassName("filter_button-active");
                current[0].className = current[0].className.replace(" filter_button-active", "");
                this.className += " filter_button-active";
            });
        }
    });

})(jQuery);



