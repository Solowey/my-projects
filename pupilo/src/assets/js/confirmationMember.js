import '@scss/pages/_confirmation-member';
import 'slick-carousel';
/* eslint-disable */
(function ($) {
    $('.similar-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.similar-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });

    $('.recommended-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.recommended-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });

    $('.product-like-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.product-like-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });

    $('.btn--confirmation_button--fb').click(function () {
        $('.theChampFacebookLogin').trigger('click');
    });

    $('.btn--confirmation_button-google').click(function () {
        $('.theChampGoogleLogin').trigger('click');
    });

    $('.btn--confirmation_button-email').click(function () {
        $('.log-button').trigger('click');
    });
})(jQuery);
