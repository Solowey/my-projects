/* eslint-disable */
(function($) {
    document.addEventListener('DOMContentLoaded', function() {
        let sku = $('.sku').text();

        setInterval(() => {
            if (sku === 'N/A') {
                $('.try-on').prop('disabled', true);
            } else {
                $('.try-on').prop('disabled', false);
            }
        }, 400);

        $('.variations').click(function () {
            sku = $('.sku').text();

            if (sku === 'N/A') {
                $('.try-on').prop('disabled', true);
            } else {
                $('.try-on').prop('disabled', false);
            }
        });

        var params = {
            // apiKey: 'CfM4xcxR5jcyUeyWn5734wcrfgQ5tEXqyYx1lLaf',
            apiKey: 'DDrOzO9fZmfDdGJX3pyob0ZSKu33Jwn30gMNuqiW',
            config: {
                preset: 'v8'
            },
        };

        $('.try-on').click(function () {
            sku = $('.sku').text();

            fitmixInstance = FitMix.createWidget('my-fitmix-container', params, function () {
                fitmixInstance.setFrame(String(sku));

                // setPupillaryDistance can be called in the createWidget callback, or later
                // 40 mm is a test value that will show enormous glasses
                fitmixInstance.setPupillaryDistance(50); // in millimeters
            });
        });
    });
})(jQuery);
