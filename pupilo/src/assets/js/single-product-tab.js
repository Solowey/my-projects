/* eslint-disable */
(function ($) {

    document.addEventListener('DOMContentLoaded', function() {


    $(window).on("load resize", () => {
        if ($(window).width() <= 768) {
            var acc = document.getElementsByClassName("tab-accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight) {
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }
        }

        if ($(window).width() > 768) {
            const tabs = document.querySelectorAll('[data-tab-target]')
            const tabContents = document.querySelectorAll('[data-tab-content]')
            const tabWrapper = document.getElementById('tabWrapper')

            tabs.forEach((tab, i) => {
                if (!i) {
                    tab.classList.add('active');
                    tabContents[i].classList.add('active');
                    tabWrapper.style.marginBottom = tabContents[i].clientHeight + "px";
                };

                tab.addEventListener('click', () => {
                    const target = document.querySelector(tab.dataset.tabTarget)

                    tabContents.forEach(tabContent => tabContent.classList.remove('active'));

                    tabs.forEach(tab => tab.classList.remove('active'));

                    tab.classList.add('active')
                    target.classList.add('active')
                    tabWrapper.style.marginBottom = target.clientHeight + "px"
                })
            })
        }
        // var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        // var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        //     return new bootstrap.Tooltip(tooltipTriggerEl);
        // });
    });

    let spanElement = '<span class="size-guide">Size Guide</span>';

    $(spanElement).insertAfter($('#data-pa_size'))
    $('.size-guide').click(() => {
        $('.size-guid-wrapper').addClass('size-guid-wrapper-open');
        $('#html--hidden').addClass('html--hidden');
    });
    $('.size-guid-wrapper-close').click(() => {
        $('.size-guid-wrapper').removeClass('size-guid-wrapper-open');
        $('#html--hidden').removeClass('html--hidden');
    });


    $('.similar-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.similar-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });

    $('.recommended-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        prevArrow: "<svg class=\"prev\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        nextArrow: "<svg class=\"next\" width=\"17\" height=\"11\" viewBox=\"0 0 17 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M1.08496 5.60742L16.085 5.60742\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "<path d=\"M11.085 0.857422L16.085 5.60742L11.085 10.3574\" stroke=\"#3F3F48\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
            "</svg>",
        appendArrows: $('.recommended-nav-container'),
        responsive: [ {
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });

    if ($(window).width() <= 768) {

        const stickyForm = $('#sticker-form');
        const buttonWrap = $('.button-wrap');
        const productSectionHeight = $('.type-product').height();
        const formToggle = $('.form-toggle');

        formToggle.on('click', function () {
            stickyForm.toggleClass('sticky-form-open')
        })

        $(window).scroll(function () {

            if ($(window).scrollTop() >= productSectionHeight) {
                stickyForm.addClass('sticky-form')
                buttonWrap.addClass('sticky-wrap')
            } else {
                stickyForm.removeClass('sticky-form')
                buttonWrap.removeClass('sticky-wrap')
            }
        })
    }

/////////////////////////////////////////////////like dislike

        $(document).on ('click', '.like-button', function () {
        let context = $(this);
        let commentID = $(this).attr('data-id');
        let userID = $(this).attr('data-user');

        $.ajax({
            url: `//${window.location.host}/wp-admin/admin-ajax.php`,
            data : {
                'action': 'add_like',
                'comment_id': commentID,
                'user_id': userID
            },
            type : 'POST',

            success : function( data ){
                console.log(data)

                if (data >= 0) {
                    context.next().text(data);
                    $('#comment-like', context).addClass('liked');
                }

            }
        });
    });

    $(document).on('click', '.dislike-button', function () {
        let context = $(this);
        let commentID = $(this).attr('data-id');
        let userID = $(this).attr('data-user');

        $.ajax({

            url: `//${window.location.host}/wp-admin/admin-ajax.php`,
            data : {
                'action': 'add_dislike',
                'comment_id': commentID,
                'user_id': userID
            },
            type : 'POST',

            success : function( data ){
                console.log(data)

                if (data >= 0) {
                    context.next().text(data);
                    $('#comment-dislike', context).addClass('disliked');
                }

            }
        });
    });

    $(document).on('change', '#pa_color', function() {

        let insertItem = '<span class="attribute-name">Color &nbsp;</span><span class="attribute-value">'+$('#pa_color').val()+'</span>'

        $('#data-pa_color').html(insertItem);
    })

    $(document).on('change', '#pa_size', function() {
        let insertItem2 = '<span class="attribute-name">Size &nbsp;</span><span class="attribute-value"> ' + $('#pa_size option:selected').text() + '</span>'
        if($('#pa_size option:selected').text() === 'Choose an option') {
            insertItem2 = 'Size';
        }

        $('#data-pa_size').html(insertItem2);
    })


    // $( '<div class="preview-block"></div>' ).insertAfter( "#attachment" );
    //
    // $(function() {
    //     // Multiple images preview in browser
    //     var imagesPreview = function(input, placeToInsertImagePreview) {
    //         if (input.files) {
    //             var filesAmount = input.files.length;
    //             for (i = 0; i < filesAmount; i++) {
    //                 var reader = new FileReader();
    //                 reader.onload = function(event) {
    //                     $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
    //                 }
    //                 reader.readAsDataURL(input.files[i]);
    //             }
    //         }
    //     };
    //
    //     $('#attachment').on('change', function() {
    //         imagesPreview(this, 'div.preview-block');
    //     });
    // });

        let parent_post_id = document.querySelector(".product-comments").dataset.productId,
            currentPage = document.querySelector(".product-comments").dataset.page,
            allPosts = document.querySelector('.product-comments').dataset.allPosts,
            loadMoreBtn = document.getElementById('load_more_comments'),
            // maxPages = document.querySelector(".commentlist").dataset.max || 0,
            viewedComments = document.getElementById('viewedComments'),
            posts = $('.commentlist li').length,
            percentViewedPosts = posts / allPosts * 100;

        let maxPages;
        if (!!document.querySelector(".commentlist")) {
            maxPages = document.querySelector(".commentlist").dataset.max;
        } else {
            maxPages = 1;
        }

            $(".load-more_progress").css("width", `${percentViewedPosts}%`);

        viewedComments.innerText = posts;

        if(Number(currentPage) === Number(maxPages)) {
            loadMoreBtn.remove();
        }
        $('#load_more_comments').click( function(){
            var button = $(this);



            $.ajax({
                url: `//${window.location.host}/wp-admin/admin-ajax.php`,
                data : {
                    'action': 'cloadmore',
                    'post_id': parent_post_id,
                    'page' : currentPage,
                    'max_pages' : maxPages
                },
                type : 'POST',
                beforeSend : function ( xhr ) {
                    button.text('Loading...'); // preloader here
                },
                success : function( data ){
                    if( data ) {
                        $('ol.commentlist').append( data );
                        posts = $('.commentlist li').length;
                        button.text("Load more");
                        currentPage++;
                        viewedComments.innerText = posts;
                        percentViewedPosts = posts / allPosts * 100;
                        $(".load-more_progress").css("width", `${percentViewedPosts}%`);

                        if(Number(currentPage) === Number(maxPages)) {
                            button.remove();
                        }
                    } else {
                        button.remove();
                    }
                    console.log(currentPage);
                    console.log(maxPages);
                }


            });

        });
});
})(jQuery);
