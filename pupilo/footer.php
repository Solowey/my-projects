</div>
<!--close content class tag-->

<?php
//    if (!is_page('checkout')) {
//?>
</div>
<footer class="page-footer" role="contentinfo">

    <!--Popup for fitting box-->

    <div class="product-try-on-popup" aria-labelledby="tryOnModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 >Virtual try on</h5>
                    <button type="button" class="product-try-on-popup-close">
                        <svg height="24px" width="24px">
                            <use xlink:href="#close"></use>
                        </svg>
                    </button>
                </div>
                <div class="modal-body" id="my-fitmix-container1"></div>
            </div>
        </div>
    </div>

    <!---->

    <div class="footer-info-wrap page-container">
    <div class="footer-accordion-menu">
        <?php while (have_rows('accordion', 'option')): the_row(); ?>
            <div class="accordion-items">
                <button class="accordion"><?php echo the_sub_field('accordion_item_title') ?></button>

                <div class="panel">
                    <?php while (have_rows('accordion_items', 'option')): the_row(); ?>
                    <a class="panel-link" href="<?php echo the_sub_field('item_link') ?>"><?php echo the_sub_field('item_text') ?></a>
                    <?php endwhile; ?>
                </div>

            </div>
        <?php endwhile; ?>
    </div>

    <div class="footer-info">

        <div class="footer-logo">
            <a class="footer-logo-link" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                <svg viewBox="0 0 138 30" class="footer-logo">
                    <use xlink:href="<?php echo get_field('site_logo', 'option') ?>"></use>
                </svg>
            </a>
            <p class="tag-line">Eyewear for life</p>
        </div>

        <div class="working-hours">
            <?php while (have_rows('working_hours', 'option')): the_row(); ?>
                <p class="info-style"><?php echo the_sub_field('info') ?></p>
            <?php endwhile; ?>
        </div>

        <div class="connect-with">
            <?php while (have_rows('connect_item', 'option')): the_row(); ?>
            <a class="item-link" href="<?php echo the_sub_field('item_link') ?>">
                <div class="connect-with-item">
                  <div class="svg-center">
                    <svg class="connect-icon" width="31" height="31">
                        <use xlink:href="<?php echo the_sub_field('item_icon') ?>"></use>
                    </svg>
                 </div>
                    <p class="connect-text"><?php echo the_sub_field('item_text') ?></p>
                </div>
            </a>

            <?php endwhile; ?>
        </div>

    </div>
    </div>

    <div class="page-footer__inner page-container">
        <div class="footer-bottom-info">
        <div class="footer-social">
            <?php while (have_rows('footer_social_group', 'option')): the_row(); ?>
                <a class="footer-social__icon" href="<?php echo the_sub_field('social_link') ?>">

                    <svg viewBox="0 0 40 41" class="social-icon">
                        <use xlink:href="<?php echo the_sub_field('social_icon') ?>"></use>
                    </svg>

                </a>
            <?php endwhile; ?>
        </div>
        <div class="terms-condition">
            <a href="<?php echo get_field('page_link', 'option') ?>">Terms & Conditions</a>
            <a href="<?php echo get_field('sitemap_link', 'option') ?>">Sitemap</a>
        </div>
        <div class="copyright">&copy;Pupilo<?php echo date("Y"); ?> All rights reserved.</div>
        </div>
    </div>
    <!-- Start of  Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=769c53be-5e31-4437-8af7-add71ae30a41"> </script>
    <!-- End of  Zendesk Widget script -->
</footer>

<?php
//    }
//?>


<!--close wrapper class tag-->
<?php wp_footer(); ?>

</body>
</html>
