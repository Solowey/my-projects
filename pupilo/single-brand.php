<?php get_header(); ?>

<div class="main-single-brand">

    <section class="slider-brand">
        <?php while (have_rows('brand-slider')): the_row(); ?>
        <div class="slider-brand-item">
            <div class="slider__bg">

                <img class="single-brand-banner" src="<?php echo get_sub_field('img')['url'] ?>"
                     alt="<?php echo get_sub_field('img')['alt'] ?>">
                <div class="single-brand-banner-mobile-wrap">

                    <img class="single-brand-banner-mobile" src="<?php echo get_sub_field('mobile_img')['url'] ?>"
                         alt="<?php echo get_sub_field('mobile_img')['alt'] ?>">

                    <h2 class="slider__title--mobile"><?php echo get_sub_field('title') ?></h2>

                </div>

                <div class="page-container">
                    <h2 class="slider__title"><?php echo get_sub_field('title') ?></h2>
                    <p class="slider__subtitle"><?php echo get_sub_field('subtitle') ?></p>
                </div>

            </div>

        </div>
        <?php endwhile; ?>
    </section>

    <section class="brand-products">
        <?php
        global $post;
        global $product;
        global $wpdb;
        $popularPost = new WP_Query(array(
            'posts_per_page' => 3,
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'ignore_sticky_posts' => true
        ));?>
        <div class="filter">
            <div class="filter-container">
                <div class="filter-wrap">
                    <?php echo do_shortcode('[yith_wcan_filters slug="default-preset"]') ?>
                </div>
                <div class="reset">
                    <?php echo do_shortcode('[yith_wcan_reset_button]') ?>
                </div>
            </div>
            <div class="filter-button-wrap">
                <button id="sortBy2" class="sort-by main-modal-button">Sort by</button>
                <?php echo do_shortcode('[yith_wcan_mobile_modal_opener]') ?>
            </div>
        </div>
        <div class="page-container">
            <div class="orderby">
                <?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?>
            </div>
            <div class="info-panel">

                <div class="headline breadcrumbs">
                    <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
                    <h2 class="page-title">Brand items (<span class="counter" id="headerViewedPosts"></span>)</h2>
                </div>

                <div class="mobile-grid-button">
                    <a class="toggle-grid col_2">
                        <span class="vertical-line"></span>
                        <span class="vertical-line"></span>
                    </a>
                    <a class="toggle-grid col_1 active-grid-button">
                        <span class="vertical-line"></span>
                    </a>
                </div>
                <div class="product-sort">
                    <div class="sort"><?php echo do_shortcode('[yith_wcan_filters slug="draft-preset"]') ?></div>
                    <div class="desktop grid-button">
                        <a class="toggle-grid col_4">
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                        </a>
                        <a class="toggle-grid col_3 active-grid-button">
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                            <span class="vertical-line"></span>
                        </a>
                    </div>
                </div>
            </div>

            <?php
            $loop = new WP_Query(array(
                'post_type' => 'product',
                'posts_per_page' => 5,
                'orderby' => 'date',

            ));
            $allProducts = $loop->found_posts; ?>
            <div class="row product-row" data-all-posts="<?php echo $allProducts ?>"

                 data-page="<?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>"
                 data-max="<?php echo $loop->max_num_pages; ?>">
                <?php $counter = 0 ?>
                <?php while ($loop->have_posts()): $loop->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'category'); ?>

                <?php endwhile ?>

            </div>

            <div class="load-more">
                <div class="load-more_text">You have viewed <span id="viewedPosts"></span> of
                    <span><?php echo $allProducts ?></span> products
                </div>
                <div class="load-more_line">
                    <div class="load-more_progress"></div>
                </div>
                <button id="load_more2" class="load-more_btn">Load 32 more</button>
            </div>
        </div>
    </section>
    <div class="page-container">
        <div class="featured">
            <h3 class="featured__title">Featured articles</h3>
            <a href="<?php echo get_site_url() ?>/blog" class="featured__title-mob">Good to know</a>
            <div class="featured__grid">
                <?php while ($popularPost->have_posts()) : $popularPost->the_post(); ?>
                    <div class="grid__item">
                        <div class="featured__img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="featured__text">
                            <p class="category_title"><a
                                    href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></p>
                            <p class="category_subtitle"><?php echo mb_strimwidth(get_the_excerpt(), 0, 125, '...') ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="flex justify-content-center">
                <a class="blog-link" href="<?php echo get_site_url() ?>/blog">View more articles</a>
            </div>
        </div>
    </div>

</div>

<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>

<?php get_template_part('template-parts/subscribe') ?>

<?php get_footer(); ?>



