<?php
/*
 * Template Name: Confirmation Template
 */
?>

<?php get_header(); ?>
<?php
    $order_id = $_GET['order'];

    $order = wc_get_order( $order_id );

    $user_id = $order->get_user();

//    $user = get_user_meta();


    WC()->cart->empty_cart();
?>


<main class="confirmation-member-main">

    <?php
        $email = $order->get_billing_email();
        $order_number = $order_id;
    ?>

    <div class="page-container">
        <div class="confirmation">
            <div class="icon">
                <svg width="180" height="172">
                    <use xlink:href="#congratulation"></use>
                </svg>
            </div>
            <h2 class="confirm_heading">Your order is complete!</h2>
            <p class="confirm_text">
                We received your order and payment and started to prepare your package. Your order ID is
                <span><?php echo $order_number; ?></span> and we
                just sent a order confirmation to your email <span><?php echo $email; ?></span>.
            </p>

            <?php
            if ($user_id && get_current_user_id() == $user_id) {
            ?>
            <div class="button-wrap confirm-button-view-order-wrapper">
                <a class="btn btn--confirmation_member_button-outlined confirm-button-view-order" href="/my-account/view-order/<?php echo $order_number; ?>/">View my order</a>
            </div>
                <?php
            } else {
            ?>

                <div class="confirm_buttons">
                    <div class="social_sign-up">
                        <button class="btn--confirmation_button btn--confirmation_button--fb">
                            Facebook
                        </button>
                        <button class="btn--confirmation_button btn--confirmation_button-google">
                            Google
                        </button>
                    </div>
                    <div class="email_sign-up">
                        <button class="btn--confirmation_button btn--confirmation_button-email">
                            Signup with email
                        </button>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
    </div>


    <?php
    if ($user_id && get_current_user_id() == $user_id) {
    ?>
    <div class="page-container without-padding">
        <div class="banner">
            <div class="banner_glasses">
                <img class="glasses_img"
                     src="<?php echo get_field('glasses_img')['url'] ?>"
                     alt="">
            </div>
            <div class="banner_text">
                <h2 class="heading">We have something interesting for you!</h2>
                <div class="buttons">
                    <a class="btn btn--confirmation_member_button" href="<?php echo get_field('view_super_deals'); ?>">View Super Deals</a>
                    <a class="btn btn--confirmation_member_button" href="<?php echo get_field('browse_new_arrivals'); ?>">Browse New Arrivals</a>
                </div>
            </div>
        </div>
    </div>
        <?php
    }
    ?>

    <div class="page-container">
        <div class="social">
            <p class="social_text">Folow us</p>
            <div class="social_icons">
                <div class="social_icon">
                    <?php
                    $social_links = get_field('footer_social_group', 'options');
                    ?>
                    <a href="<?php echo $social_links[0]['social_link']; ?>" target="_blank">
                        <svg width="49" height="48">
                            <use xlink:href="#fb"></use>
                        </svg>
                    </a>
                </div>
                <div class="social_icon">
                    <a href="<?php echo $social_links[1]['social_link']; ?>" target="_blank">
                        <svg enable-background="new 0 0 24 24" height="48" viewBox="0 0 24 24" width="49"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <linearGradient id="SVGID_1_" gradientTransform="matrix(0 -1.982 -1.844 0 -132.522 -51.077)"
                                            gradientUnits="userSpaceOnUse" x1="-37.106" x2="-26.555" y1="-72.705"
                                            y2="-84.047">
                                <stop offset="0" stop-color="#fd5"/>
                                <stop offset=".5" stop-color="#ff543e"/>
                                <stop offset="1" stop-color="#c837ab"/>
                            </linearGradient>
                            <path
                                d="m1.5 1.633c-1.886 1.959-1.5 4.04-1.5 10.362 0 5.25-.916 10.513 3.878 11.752 1.497.385 14.761.385 16.256-.002 1.996-.515 3.62-2.134 3.842-4.957.031-.394.031-13.185-.001-13.587-.236-3.007-2.087-4.74-4.526-5.091-.559-.081-.671-.105-3.539-.11-10.173.005-12.403-.448-14.41 1.633z"
                                fill="url(#SVGID_1_)"/>
                            <path
                                d="m11.998 3.139c-3.631 0-7.079-.323-8.396 3.057-.544 1.396-.465 3.209-.465 5.805 0 2.278-.073 4.419.465 5.804 1.314 3.382 4.79 3.058 8.394 3.058 3.477 0 7.062.362 8.395-3.058.545-1.41.465-3.196.465-5.804 0-3.462.191-5.697-1.488-7.375-1.7-1.7-3.999-1.487-7.374-1.487zm-.794 1.597c7.574-.012 8.538-.854 8.006 10.843-.189 4.137-3.339 3.683-7.211 3.683-7.06 0-7.263-.202-7.263-7.265 0-7.145.56-7.257 6.468-7.263zm5.524 1.471c-.587 0-1.063.476-1.063 1.063s.476 1.063 1.063 1.063 1.063-.476 1.063-1.063-.476-1.063-1.063-1.063zm-4.73 1.243c-2.513 0-4.55 2.038-4.55 4.551s2.037 4.55 4.55 4.55 4.549-2.037 4.549-4.55-2.036-4.551-4.549-4.551zm0 1.597c3.905 0 3.91 5.908 0 5.908-3.904 0-3.91-5.908 0-5.908z"
                                fill="#fff"/>
                        </svg>
                    </a>
                </div>
                <div class="social_icon">
                    <a href="<?php echo $social_links[2]['social_link']; ?>" target="_blank">
                        <svg width="49" height="48">
                            <use xlink:href="#yt"></use>
                        </svg>
                    </a>
                </div>
                <div class="social_icon">
                    <a href="<?php echo $social_links[3]['social_link']; ?>" target="_blank">
                        <svg width="49" height="48">
                            <use xlink:href="#tw"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
<!--    <div class="similar-slider-container">-->
<!--        <div class="page-container">-->
<!--            <h3 class="slider-header">-->
<!--                --><?php //echo get_field('product_similar_title')?>
<!--            </h3>-->
<!--            <div class="similar-nav-container"></div>-->
<!--            <div class="row product-row similar-slider">-->
<!---->
<!--                --><?php //while( have_rows('product_similar_styles') ): the_row(); ?>
<!---->
<!--                    --><?php //$post_object = get_sub_field('similar_product'); ?>
<!---->
<!--                    --><?php //if( $post_object ): ?>
<!---->
<!--                        --><?php //$post = $post_object; setup_postdata( $post ); ?>
<!---->
<!--                        --><?php //get_template_part('template-parts/content', 'category') ?>
<!---->
<!--                        --><?php //wp_reset_postdata(); ?>
<!---->
<!--                    --><?php //endif; ?>
<!---->
<!--                --><?php //endwhile; ?>
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->


    <?php
    if (get_current_user_id() == $user_id) {
    ?>
    <div class="recommended-slider-container">
        <div class="page-container">
            <h3 class="slider-header">
                <?php echo get_field('product_recomended_title')?>
                <span class="arrow-right">
                        <svg width="21" height="17" viewBox="0 0 21 17" class="categories_link--arrow">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </span>
            </h3>

            <div class="recommended-nav-container"></div>
            <div class="row product-row recommended-slider">

                <?php while( have_rows('product_recommended') ): the_row(); ?>

                    <?php $post_object = get_sub_field('similar_product'); ?>

                    <?php if( $post_object ): ?>

                        <?php $post = $post_object; setup_postdata( $post ); ?>

                        <?php get_template_part('template-parts/content', 'category') ?>

                        <?php wp_reset_postdata(); ?>

                    <?php endif; ?>

                <?php endwhile; ?>

            </div>
        </div>
    </div>
        <?php
    }
    ?>

<!--    <div class="product-like-slider-container">-->
<!--        <div class="page-container">-->
<!--            <h3 class="slider-header">-->
<!--                --><?php //echo get_field('product_you_make_like_title')?>
<!--            </h3>-->
<!--            <div class="product-like-nav-container"></div>-->
<!--            <div class="row product-row product-like-slider">-->
<!---->
<!--                --><?php //while( have_rows('product_like') ): the_row(); ?>
<!---->
<!--                    --><?php //$post_object = get_sub_field('similar_product'); ?>
<!---->
<!--                    --><?php //if( $post_object ): ?>
<!---->
<!--                        --><?php //$post = $post_object; setup_postdata( $post ); ?>
<!---->
<!--                        --><?php //get_template_part('template-parts/content', 'category') ?>
<!---->
<!--                        --><?php //wp_reset_postdata(); ?>
<!---->
<!--                    --><?php //endif; ?>
<!---->
<!--                --><?php //endwhile; ?>
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</main>


<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>
<?php get_template_part('template-parts/subscribe') ?>
<?php get_footer(); ?>
