<?php
/*
 * Template Name: Terms Template
 */
?>

<?php get_header(); ?>
<main class="terms-main">
<div class="page-container">
<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>

<div class="terms-container">
<?php echo get_the_content()?> 
</div>
</main>
<?php get_template_part('template-parts/subscribe') ?>
<?php get_footer(); ?>
