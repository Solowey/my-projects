<?php
/*
 * Template Name: Brands Template
 */

$query = new WP_Query([
    'posts_per_page' => -1,
    'ignore_sticky_posts' => true,
    'post_type' => 'brand',
    'sort' => 'ASC'
]);
$alphabet = range('A', 'Z');
?>

<?php get_header(); ?>
<main class="brands-main page-container">
        <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
        <h2 class="brands__title">Brands</h2>
        <p class="brands__subtitle">Epic brands you know and love.</p>

        <div>
                <div class="alphabet ">
                    <?php foreach ($alphabet as $letter) { ?>
                        <div class="alphabet__letter">
                            <a href="#<?php echo $letter; ?>">
                                <?php echo $letter; ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="brands_items">
                    <?php
                    foreach ($alphabet as $letter) {
                        $flag = true;

                        if ($query->have_posts()) :
                            while ($query->have_posts()) :
                                $query->the_post();

                                if (strtolower(substr(get_the_title(), 0, 1)) === strtolower($letter)) {
                                    if ($flag) { ?>
                                        <div class="item__wrap">
                                            <p class="item__letter">
                                                <span id="<?php echo $letter ?>"></span>
                                                <a ><?php echo $letter ?></a>
                                            </p>
                                        </div>
                                        <?php $flag = false;
                                    } ?>
                                    <div class="item__brand">
                                        <a href="<?php echo get_the_permalink(); ?>" ">
                                        <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                        </a>
                                    </div>
                                <?php }
                            endwhile;
                        endif;

                        $flag = true;
                    } ?>
</main>
<section class="recently-product-section">
    <?php get_template_part('template-parts/content', 'recently-product')?>
</section>
<?php get_template_part('template-parts/subscribe') ?>
<?php get_footer(); ?>
