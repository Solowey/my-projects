<?php

if (is_page('checkout')) {
    get_header('cart');
} else {
    get_header();
}


?>

<!--	<main id="main" class="page-main test" role="main">-->
<!--    <div class="page-container row">-->
<!--        --><?php
//        $args = array(
//            'post_type' => 'product',
//            'posts_per_page' => 20,
//            'product_cat' => 'eyeglasses'
//        );
//
//        $eye_query = new WP_Query($args);
//
//        if ($eye_query->have_posts()) : while ($eye_query->have_posts()) : $eye_query->the_post(); ?>
<!---->
<!--            --><?php //get_template_part('template-parts/content', 'category'); ?>
<!---->
<!--        --><?php //endwhile;
//            wp_reset_postdata(); ?>
<!---->
<!--        --><?php //else : ?>
<!---->
<!--            --><?php //get_template_part('template-parts/content', 'none'); ?>
<!---->
<!--        --><?php //endif; ?>
<!--    </div>-->
<!---->
<!--	</main>-->

<main id="primary" class="site-main">

    <?php
    while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content', 'page' );

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
            comments_template();
        endif;

    endwhile; // End of the loop.
    ?>

</main>

<?php
if (is_account_page()) {
    get_template_part('template-parts/subscribe');
}

get_footer(); ?>
