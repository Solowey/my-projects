<?php
/*
 * Template Name: Contacts Template
 */
?>

<?php get_header(); ?>
<main class="contacts-main">
    <div class="page-container">
        <?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
        <div class="contacts_info">
            <div class="contacts-left">
                <div>
                    <?php if (get_field('img')): ?>
                        <img class="contact_img" alt="Contact-img" src="<?php the_field('img'); ?>"/>
                    <?php endif; ?>
                </div>
                <div>
                    <h2 class="contacts_heading">Contact Us</h2>
                    <p class="contacts_subheading">If you have any questions, don’t hesitate to reach us. We are here to
                        help you!</p>
                    <div class="contacts_buttons">
                        <button class="btn--contacts_button btn--contacts_button-contained">Call Us</button>
                        <button class="btn--contacts_button btn--contacts_button-outlined">Email us</button>
                    </div>
                </div>
            </div>

            <div class="contacts-right">
                <h3 class="right-heading">Pupilo LLC</h3>
                <div>
                    <div class="right_contact_item">
                        <div>
                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none">
                                <use xlink:href="#contacts-mail"></use>
                            </svg>
<!--                            <img-->
<!--                                src="--><?php //echo esc_url(home_url('/')); ?><!--wp-content/themes/pupilo/src/assets/images/mail.svg"-->
<!--                                alt="">-->
                        </div>
                        <div>
                            <?php if (get_field('email')): ?>
                                <p> <?php the_field('email'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="right_contact_item">
                        <div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <use xlink:href="#contacts-phone"></use>
                            </svg>
<!--                            <img-->
<!--                                src="--><?php //echo esc_url(home_url('/')); ?><!--wp-content/themes/pupilo/src/assets/images/phone.svg"-->
<!--                                alt="">-->
                        </div>
                        <div>
                            <?php if (get_field('phone')): ?>
                                <p> <?php the_field('phone'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="right_contact_item">
                        <div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <use xlink:href="#contacts-map-pin"></use>
                            </svg>
<!--                            <img-->
<!--                                src="--><?php //echo esc_url(home_url('/')); ?><!--wp-content/themes/pupilo/src/assets/images/map-pin.svg"-->
<!--                                alt="">-->
                        </div>
                        <div>
                            <?php if (get_field('address')): ?>
                                <p> <?php the_field('address'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="social d-flex align-items-center">
                    <p class="social_title">Follow us</p>
                    <div class="d-flex align-items-center ">
                        <?php while (have_rows('contacts_social')): the_row(); ?>
                            <div>
                                <a href="<?php echo the_sub_field('contacts_link') ?>">
                                    <svg class="social-icon">
                                        <use xlink:href="
                                                <?php echo the_sub_field('contacts_icon') ?>"></use>
                                    </svg>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <div class="doubts">
                        <p>Some doubts? </p>
                    <a class="doubts_link" href="">Frequently asked questions</a>
                </div>
            </div>
        </div>


        <div class="team">
            <h2 class="team_heading">Meet our team </h2>
            <div class="filter_buttons" id="filter_buttons">
                <div class="filter_button filter_button-active" data-filter='all'>All</div>
                <?php while (have_rows('team_member_filter')): the_row(); ?>
                    <div class="filter_button" data-filter="<?php echo the_sub_field("team_member_department_name") ?>">
                        <?php echo the_sub_field('team_member_department_name') ?>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="team_members">
                <?php while (have_rows('team_member')): the_row(); ?>
                    <div class="team_member <?php echo get_sub_field('team_member_department') ?>"  style="background-image: url(<?php echo get_sub_field('team_member_img')['url'] ?>)">
                        <div class="team_member_info">
                            <div class="team_member_discription">
                                <p><?php echo the_sub_field('team_member_name') ?></p>
                                <p><?php echo the_sub_field('team_member_position') ?></p>
                            </div>
                            <div class="team_member_icon">
                                <a href="mailto:<?php echo the_sub_field('team_member_email') ?>">
                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none">
                                        <use xlink:href="#contacts-white-mail"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>

</main>
<?php get_template_part('template-parts/subscribe') ?>
<?php get_footer(); ?>
